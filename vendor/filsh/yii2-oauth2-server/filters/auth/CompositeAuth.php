<?php

namespace filsh\yii2\oauth2server\filters\auth;

use filsh\yii2\oauth2server\Request;
use \Yii;

class CompositeAuth extends \yii\filters\auth\CompositeAuth
{
    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        $server = Yii::$app->getModule('oauth2')->getServer();
        $request = new Request();
        $server->verifyResourceRequest($request);
        
        return parent::beforeAction($action);
    }
}