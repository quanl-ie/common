<?php
/**
 * Created by PhpStorm.
 * User: quanl
 * Date: 2018/6/20
 * Time: 13:48
 */
namespace common\lib;

class AlipayConfig
{
      //=======【基本信息设置】=====================================
      //签名方式,默认为RSA2(RSA2048)
      const SIGN_TYPE = 'RSA2';
      //支付宝公钥
      const PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAqKlzBxGI0LKhCuTPcaOrXRh1ZvUCLQW8G6nMdtl3jTDJXCrglBR6PFG4EinsvV+PA12U582GKRN12KvRcHDKAxL7CgmIgDn3zAwjyxNCuBGQPNi3nfGcCAJoaY0o7NAiEjl12AA2RbKni7cArDVzruqS57eMCpD9POce5hLWqTyHwWFQcDkWqAtA638DXizpstBgkAM+bZT6w/CC+DL2H8i5MhzSHkz57w10DjrOkhdtFF9a4o7hAhxd10y6ywct28o+wUBKQG7ZAA3O/ipvaspuhCKf6g4ZUmwwqtF/PNi7nwK27p54iApQHCebZ8NVH3FbY1A8eDDzDrX+cbY6uQIDAQAB";
      //商户私钥
      const PRIVATE_KEY = "MIIEpAIBAAKCAQEAnTZHHIz+KA8tCY2xJuLYaQEh8rIkU4eQ6o7JFF827D07U0q0wMtFfjxYn7S9jbOeL91RD3whCfCvYnP1VccBRcIhf4skYwE0c6OsxOJnb/WwtX1pNqoFCPGnJsfeV5OWisr1Rpd+EEWLv4Ak6ko011WM9CxIzzyVs8XcoUpVwYqkdoP/drZZn2dOcDEvx0n2BddGrfnpqVAfNJlH0o+JlMdDQSsOjCsza6oLUdK9ggwkB/dwT4GAtXmHSlwR38+Fz258f/FL8gREjqERfwwlSn5CwW8Y6piuMeliLcKpe+ZxbwIujF3SQ/nIfiD1rdjmZXrO1YXnywzQD8GuLEs85wIDAQABAoIBABX+EdScpLFV7ib3+2BFO6D015rr8Mj+nfcyIk8HQtepeTMdsEuIJCCUsJEm5517ExX/D+fqpXSOhvpu3lMxk6v+jFEbVevDr2K98t1YDsBKzn2VHbLX9fxaUdfK/f+iVdz0p3tIlgoQsIwJkSB1YQ7+mMlPqJ/VCdwsQcEOdRCt7E/9laH5WpW8kFvGJuYKv+BhsUD7mV8ApGbkwnwLJ/2eFOyt4uSl8U2+eQXv6vDmB2l+qpRxzMQbzb4+CtnNym12AZQuKcH4lXbbppOLiG4LO93JdkZVzCDi34PMAZu3nMp+qNwCS7wDzTMeeP7q58N4BkOglNQX/YeKa40xkAECgYEAzKzEVuoXU+9kGUc4bJK1/Fixunl2yQSUnxCHjmj4LrdTLNUqrEkOpt5pYKqh6gJ8xaM4z2KnVNbaSkLpTr5KJyvRlayIF8K5RhWrO1gHB0mkVIEc45coaQIxuO8k97Fvad5bQpGK2FRyUxM8WLkToxx4MQ6nqtjnX+sveB9fF4ECgYEAxKKaa1qccAtmqt/3WPrmBBaKeOtUX8n7SzYhi9HBPSIWMdI70P2df/BxLPy4nsGntYb573hYkuiokaKYe+ANEe1RbJKi5Ro6s+CUgxNNm0dWv/temr+quoAqBfyeb+ZapTdsv3OWcioRs1NVwTetZbtjvZ53sPnUhvD4b3isyGcCgYEAgjS2gwRQ6ti0OVCRmKCMnYhfKF31L1R2p+qAEhy9JhsCEtdZWljvtUBwkUp7zr7eWWzIy0XbZaF5JR0EB1APJHQ/1JP50APUkb1fPVV2wzAaFpg8pJyY3JTExP196wU594O5UtP8HDs8r5ThIyA6Wd3WqH7GM23hY6fefvCWMgECgYASHDfEs2+9Eg++pSoO0+zja4ANav82Y0/1uWKVkhgOHVmV9Rr1QKaE5gxGbHBEjul3WpsiZx8hSLRd1W/d2ODbSdQ5/7dpCmVls8V53QlEx+lBfVFVR1neG3fDXWTqQXkPxm8Jowe0NBO1RHOdfBdIS14AVNwJPLlKRF9dROwE+QKBgQCyDWpKB1KxJYkWTbpyKmtDOnVhjhVf+88JId30L5DCz48p5TgEwuMe0eW9p7QNSq4l6dlK7VL4FXf1eKiPV9xBjQJFBhr1RwIwL+QWxeJaO3DbGOGTrepRiwYfnDsZwtEomQp8buEVsLI0jIt2CCKo2F/OCyYQbx9OD9SD+6sFFA==";
      //编码格式
      const CHARSET = 'UTF-8';
      //支付宝网关
      const GATEWAY_URL = 'https://openapi.alipay.com/gateway.do';
      //应用ID
      const APPID = '2018061960363658';
      //同步通知地址
      const RETURN_URL = 'http://pay.uservices.cn/pay/ali-return';
      //异步通知地址,只有扫码支付预下单可用
      const NOTIFY_URL = "http://www.bfznkj.com:10025/v5/order/ali-pay-notify-url";//'http://pay.uservices.cn/pay/ali-notify';
      //最大查询重试次数
      const MAX_QUERY_RETRY = '10';
      //查询间隔
      const QUERY_DURATION = '3';
}