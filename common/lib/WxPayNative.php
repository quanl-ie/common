<?php

/**
 * Created by PhpStorm.
 * 微信扫码支付接口
 * User: quanl
 * Date: 2018/6/15
 * Time: 9:02
 */
namespace common\lib;
require_once "wxPayApi/lib/WxPay.Exception.php";
require_once "wxPayApi/lib/WxPay.Config.php";
require_once "wxPayApi/lib/WxPay.Data.php";
use Yii;

class WxPayNative
{

      public static $mch_id = WxPayConfig::MCHID ;
      public static $app_id = WxPayConfig::APPID ;
      public static $api_key = WxPayConfig::KEY ;
      public static $notify_url = WxPayConfig::NOTIFY_URL ;
      
      /**
       * 发起订单
       *  float $payAmount 收款总费用 单位元
       *  string $orderNo 唯一的订单号
       *  string $orderName 订单名称
       *  string $payTime 订单发起时间
       *  string $attach 附加数据 用于标识来源
       * @param array $order
       * @return array
       */
      public static function createOrder($order)
      {
            $url = "https://api.mch.weixin.qq.com/pay/unifiedorder";
            $unified = array(
                  'appid' => self::$app_id,
                  'attach' => 'pay',
                  'body' => $order['orderName'],
                  'mch_id' => self::$mch_id,
                  'nonce_str' => self::createNonceStr(),
                  'notify_url' => self::$notify_url,
                  'out_trade_no' => $order['orderNo'],
                  'spbill_create_ip' => '127.0.0.1',
                  'total_fee' => intval($order['payAmount'] * 100),       //单位 转为分
                  'trade_type' => 'NATIVE',
            );
            $unified['sign'] = self::getSign($unified, self::$api_key);
            $responseXml = self::curlPost($url,self::arrayToXml($unified));
            $unifiedOrder = simplexml_load_string($responseXml, 'SimpleXMLElement', LIBXML_NOCDATA);
            if ($unifiedOrder === false) {
                  die('parse xml error');
            }
            if ($unifiedOrder->return_code != 'SUCCESS') {
                  die($unifiedOrder->return_msg);
            }
            if ($unifiedOrder->result_code != 'SUCCESS') {
                  die($unifiedOrder->err_code);
            }
            $codeUrl = (array)($unifiedOrder->code_url);
            if(!$codeUrl[0]) exit('get code_url error');
            $arr = array(
                  "appId" => self::$app_id,
                  "timeStamp" => $order['payTime'],
                  "nonceStr" => self::createNonceStr(),
                  "package" => "prepay_id=" . $unifiedOrder->prepay_id,
                  "signType" => 'MD5',
                  "code_url" => $codeUrl[0],
            );
            $arr['paySign'] = self::getSign($arr, self::$api_key);
            return $arr;

      }

      /**
       * 关闭订单
       */
      public static function closeOrder($order) {
            $url = 'https://api.mch.weixin.qq.com/pay/closeorder';

            $unified = array(
                'appid' => self::$app_id,
                'mch_id' => self::$mch_id,
                'nonce_str' => self::createNonceStr(),
                'out_trade_no' => $order['orderNo'],
                'sign_type' => 'MD5',
            );
            $unified['sign'] = self::getSign($unified, self::$api_key);
            $responseXml = self::curlPost($url,self::arrayToXml($unified));
            return $responseXml;
      }

      /**
       * 生成二维码
       */
      public static function createQr($code_url){
            require_once Yii::getAlias('@common').'/lib/phpqrcode/phpqrcode.php';
            $value = $code_url;                  //二维码内容
            $errorCorrectionLevel = 'L';    //容错级别
            $matrixPointSize = 5;           //生成图片大小
            //生成二维码图片
            ob_start();
            \QRcode::png($value,false, $errorCorrectionLevel, $matrixPointSize, 2);
            $qrcode  = "data:image/png;;base64,". base64_encode(ob_get_contents());
            ob_end_clean();
            return $qrcode;
      }
      /**
       * 支付结果通用通知
       * 通过 notify_url 调用
       * @return return_code return_msg
       */
      public static function notify()
      {
            $postStr = file_get_contents("php://input");
            $postObj = simplexml_load_string($postStr, 'SimpleXMLElement', LIBXML_NOCDATA);
            if ($postObj === false) {
                  die('parse xml error');
            }
            if ($postObj->return_code != 'SUCCESS') {
                  die($postObj->return_msg);
            }
            if ($postObj->result_code != 'SUCCESS') {
                  die($postObj->err_code);
            }
            $arr = (array)$postObj;
            unset($arr['sign']);
            if (self::getSign($arr, self::$api_key) == $postObj->sign) {
                return $arr;
            }
          return [];
      }
      /**
       *
       * 查询订单，WxPayOrderQuery中out_trade_no、transaction_id至少填一个
       * appid、mchid、spbill_create_ip、nonce_str不需要填入
       * @param WxPayOrderQuery $input
       * @return 成功时返回，其他抛异常
       */
      public static function orderQuery($trade_id)
      {
            $url = "https://api.mch.weixin.qq.com/pay/orderquery";
            //检测必填参数
            if(!$trade_id){
                  throwException("参数错误！请提供流水号！");
            }
            $input['transaction_id'] =$trade_id;
            $input['appid']  = self::$app_id;
            $input['mch_id'] = self::$mch_id;
            $input['nonce_str'] = self::createNonceStr();//随机字符串
            $input['sign'] = self::getSign($input, self::$api_key);
            $responseXml = self::curlPost($url,self::arrayToXml($input));
            $result = WxPayResults::Init($responseXml);
            return $result;
      }
      /**
       * curl get
       *
       * @param string $url
       * @param array $options
       * @return mixed
       */
      public static function curlGet($url = '', $options = array())
      {
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 30);
            if (!empty($options)) {
                  curl_setopt_array($ch, $options);
            }
            //https请求 不验证证书和host
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            $data = curl_exec($ch);
            curl_close($ch);
            return $data;
      }

      public static function curlPost($url = '', $postData = '', $useCert = false, $options = array())
      {
            if (is_array($postData)) {
                  $postData = http_build_query($postData);
            }
            $ch = curl_init();
            //如果有配置代理这里就设置代理
            if(WxPayConfig::CURL_PROXY_HOST != "0.0.0.0" && WxPayConfig::CURL_PROXY_PORT != 0){
                  curl_setopt($ch,CURLOPT_PROXY, WxPayConfig::CURL_PROXY_HOST);
                  curl_setopt($ch,CURLOPT_PROXYPORT, WxPayConfig::CURL_PROXY_PORT);
            }
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
            curl_setopt($ch, CURLOPT_TIMEOUT, 30); //设置cURL允许执行的最长秒数
            if (!empty($options)) {
                  curl_setopt_array($ch, $options);
            }
            if($useCert == true){
                  //设置证书
                  //使用证书：cert 与 key 分别属于两个.pem文件
                  curl_setopt($ch,CURLOPT_SSLCERTTYPE,'PEM');
                  curl_setopt($ch,CURLOPT_SSLCERT, WxPayConfig::SSLCERT_PATH);
                  curl_setopt($ch,CURLOPT_SSLKEYTYPE,'PEM');
                  curl_setopt($ch,CURLOPT_SSLKEY, WxPayConfig::SSLKEY_PATH);
                  //验证host
                  curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,TRUE);
                  curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,2);//严格校验
            }else{
                  //https请求 不验证证书和host
                  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            }
            $data = curl_exec($ch);
            curl_close($ch);
            return $data;
      }
      /**
       *
       * 产生随机字符串，不长于16位
       * @param int $length
       * @return 产生的随机字符串
       */
      public static function createNonceStr($length = 32)
      {
            $chars = "abcdefghijklmnopqrstuvwxyz0123456789";
            $str = '';
            for ($i = 0; $i < $length; $i++) {
                  $str .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);
            }
            return $str;
      }

      public static function arrayToXml($arr)
      {
            $xml = "<xml>";
            foreach ($arr as $key => $val) {
                  if (is_numeric($val)) {
                        $xml .= "<" . $key . ">" . $val . "</" . $key . ">";
                  } else
                        $xml .= "<" . $key . "><![CDATA[" . $val . "]]></" . $key . ">";
            }
            $xml .= "</xml>";
            return $xml;
      }
      /**
       * 获取签名
       */
      public static function getSign($params, $key)
      {
            ksort($params, SORT_STRING);
            $unSignParaString = self::formatQueryParaMap($params, false);
            $signStr = strtoupper(md5($unSignParaString . "&key=" . $key));
            return $signStr;
      }

      protected static function formatQueryParaMap($paraMap, $urlEncode = false)
      {
            $buff = "";
            ksort($paraMap);
            foreach ($paraMap as $k => $v) {
                  if (null != $v && "null" != $v) {
                        if ($urlEncode) {
                              $v = urlencode($v);
                        }
                        $buff .= $k . "=" . $v . "&";
                  }
            }
            $reqPar = '';
            if (strlen($buff) > 0) {
                  $reqPar = substr($buff, 0, strlen($buff) - 1);
            }
            return $reqPar;
      }
      /**
       * 获取毫秒级别的时间戳
       */
      private static function getMillisecond()
      {
            //获取毫秒的时间戳
            $time = explode ( " ", microtime () );
            $time = $time[1] . ($time[0] * 1000);
            $time2 = explode( ".", $time );
            $time = $time2[0];
            return $time;
      }
}