<?php
/**
 * Created by PhpStorm.
 * User: quanl
 * Date: 2018/6/20
 * Time: 13:08
 */
namespace common\lib;
use webapp\models\Department;
use webapp\models\WorkOrderTrade;
use Yii;

require_once 'alipay/aop/AlipayConfig.php';
class AliPayNative
{
      public static $gatewayUrl  = AlipayConfig::GATEWAY_URL;
      public static $appId = AlipayConfig::APPID ;
      public static $returnUrl  = AlipayConfig::RETURN_URL;
      public static $notifyUrl = AlipayConfig::NOTIFY_URL ;
      public static $rsaPrivateKeyFilePath ='';
      public static $alipayPublicKey = AlipayConfig::PUBLIC_KEY;
      public static $rsaPrivateKey  = AlipayConfig::PRIVATE_KEY;
      public static $charset  = AlipayConfig::CHARSET;
      public static $signType  = AlipayConfig::SIGN_TYPE;
      /**
       * 发起订单
       * @param array $requestConfigs
       * @return array
       */
      public static function createOrder($requestConfigs)
      {
            //请求参数
            if(!isset($requestConfigs['out_trade_no']) || $requestConfigs['out_trade_no'] == ''){
                  $return['msg'] = "订单号不能为空";
                  $return['result'] = false;
                  return $return;
            }
            if(!isset($requestConfigs['total_amount']) || $requestConfigs['total_amount'] == ''){
                  $return['msg'] = "订单金额不能为空";
                  $return['result'] = false;
                  return $return;
            }
            if(!isset($requestConfigs['subject']) || $requestConfigs['subject'] == ''){
                  $return['msg'] = "订单标题不能为空";
                  $return['result'] = false;
                  return $return;
            }
            $commonConfigs = array(
                  //公共参数
                  'app_id' => self::$appId,
                  'method' => 'alipay.trade.precreate',             //接口名称
                  'format' => 'JSON',
                  'charset'=>self::$charset,
                  'sign_type'=>self::$signType,
                  'timestamp'=>date('Y-m-d H:i:s'),
                  'version'=>'1.0',
                  'notify_url' => self::$notifyUrl,
                  'biz_content'=>json_encode($requestConfigs),
            );
            $commonConfigs["sign"] = self::generateSign($commonConfigs, $commonConfigs['sign_type']);
            $result = self::curlPost(self::$gatewayUrl,$commonConfigs);
            return json_decode($result,true);
      }
      /**
       * 回调
       */
      public function notifyUrl()
      {
            $data = file_get_contents("php://input");
            $notifyData = explode("&",urldecode($data));
            parse_str(urldecode($data),$notifyData);

            //判断是否支付成功
            if ($notifyData && $notifyData["trade_status"] == "TRADE_SUCCESS")
            {
                  //$order = WorkOrderTrade::getPageList(["out_trade_no"=>$notify_data["out_trade_no"]]);
                  //echo "success";
                  return $notifyData;
            }
            return [];
      }
      /**
       * 订单查询
       * @param $trade_no 支付宝交易号（流水）
       * @return array
       */
      public static function query($trade_no)
      {
            //请求参数
            $requestConfigs = array(
                  'trade_no'=>$trade_no
            );
            $commonConfigs = array(
                  //公共参数
                  'app_id' => self::$appId,
                  'method' => 'alipay.trade.query',             //接口名称
                  'format' => 'JSON',
                  'charset'=>self::$charset,
                  'sign_type'=>self::$signType,
                  'timestamp'=>date('Y-m-d H:i:s'),
                  'version'=>'1.0',
                  'biz_content'=>json_encode($requestConfigs),
            );
            $commonConfigs["sign"] = self::generateSign($commonConfigs, $commonConfigs['sign_type']);
            $result = self::curlPost(self::$gatewayUrl,$commonConfigs);
            return json_decode($result,true);
      }

      /**
       * 生成二维码
       * @param $qr_code
       * @return string
       */
      public static function createQr($qr_code){
            require_once Yii::getAlias('@common').'/lib/phpqrcode/phpqrcode.php';
            $value = $qr_code;                  //二维码内容
            $errorCorrectionLevel = 'L';    //容错级别
            $matrixPointSize = 5;           //生成图片大小
            //生成二维码图片
            ob_start();
            \QRcode::png($value,false, $errorCorrectionLevel, $matrixPointSize, 2);
            $qrcode  = "data:image/png;;base64,". base64_encode(ob_get_contents());
            ob_end_clean();
            return $qrcode;
      }
      public static function generateSign($params, $signType = "RSA") {
            return self::sign(self::getSignContent($params), $signType);
      }
      public static function sign($data, $signType = "RSA") {
            $priKey=self::$rsaPrivateKey;
            $res = "-----BEGIN RSA PRIVATE KEY-----\n" .
                  wordwrap($priKey, 64, "\n", true) .
                  "\n-----END RSA PRIVATE KEY-----";
            ($res) or die('您使用的私钥格式错误，请检查RSA私钥配置');
            if ("RSA2" == $signType) {
                  openssl_sign($data, $sign, $res, version_compare(PHP_VERSION,'5.4.0', '<') ? SHA256 : OPENSSL_ALGO_SHA256);                       //OPENSSL_ALGO_SHA256是php5.4.8以上版本才支持
            } else {
                  openssl_sign($data, $sign, $res);
            }
            $sign = base64_encode($sign);
            return $sign;
      }
      /**
       * 校验$value是否非空
       *  if not set ,return true;
       *    if is null , return true;
       **/
      public static function checkEmpty($value) {
            if (!isset($value))
                  return true;
            if ($value === null)
                  return true;
            if (trim($value) === "")
                  return true;
            return false;
      }
      public static function getSignContent($params) {
            ksort($params);
            $stringToBeSigned = "";
            $i = 0;
            foreach ($params as $k => $v) {
                  if (false === self::checkEmpty($v) && "@" != substr($v, 0, 1)) {
                        // 转换成目标字符集
                        $v = self::characet($v, self::$charset);
                        if ($i == 0) {
                              $stringToBeSigned .= "$k" . "=" . "$v";
                        } else {
                              $stringToBeSigned .= "&" . "$k" . "=" . "$v";
                        }
                        $i++;
                  }
            }
            unset ($k, $v);
            return $stringToBeSigned;
      }
      /**
       * 转换字符集编码
       * @param $data
       * @param $targetCharset
       * @return string
       */
      static function characet($data, $targetCharset) {
            if (!empty($data)) {
                  $fileType = self::$charset;
                  if (strcasecmp($fileType, $targetCharset) != 0) {
                        $data = mb_convert_encoding($data, $targetCharset, $fileType);
                        //$data = iconv($fileType, $targetCharset.'//IGNORE', $data);
                  }
            }
            return $data;
      }
      public static function curlPost($url = '', $postData = '', $options = array())
      {
            if (is_array($postData)) {
                  $postData = http_build_query($postData);
            }
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
            curl_setopt($ch, CURLOPT_TIMEOUT, 30); //设置cURL允许执行的最长秒数
            if (!empty($options)) {
                  curl_setopt_array($ch, $options);
            }
            //https请求 不验证证书和host
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            $data = curl_exec($ch);
            curl_close($ch);
            return $data;
      }
}

