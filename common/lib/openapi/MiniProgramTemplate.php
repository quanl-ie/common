<?php
namespace common\lib\openapi;


use common\helpers\Helper;

class MiniProgramTemplate
{

    /**
     * @see https://open.weixin.qq.com/cgi-bin/showdocument?action=dir_list&t=resource/res_list&verify=1&id=open1500465446_j4CgR&token=&lang=zh_CN
     * @param $offset  offset和count用于分页，表示从offset开始，拉取count条记录，offset从0开始，count最大为20。
     * @param $count offset和count用于分页，表示从offset开始，拉取count条记录，offset从0开始，count最大为20。
     * @return mixed
     * @author xi
     */
    public static function getList($accessToken,$offset = 0,$count = 20)
    {
        $postData = [
            "offset" => $offset,
            "count"  => $count
        ];
        $url = "https://api.weixin.qq.com/cgi-bin/wxopen/template/library/list?access_token=" . $accessToken;

        $jsonStr = Helper::curlPost($url, json_encode($postData));
        $arr = json_decode($jsonStr,true);
        return $arr;
    }

    /**
     * 获取模板库某个模板标题下关键词库
     * @param $id 模板标题id，可通过接口获取，也可登录小程序后台查看获取
     * @return mixed
     * @author xi
     */
    public static function getDetail($accessToken,$id)
    {
        $postData = [
            "id" => $id
        ];
        $url = "https://api.weixin.qq.com/cgi-bin/wxopen/template/library/get?access_token=" . $accessToken;

        $jsonStr = Helper::curlPost($url, json_encode($postData));
        $arr = json_decode($jsonStr,true);
        return $arr;
    }

    /**
     * 组合模板并添加至帐号下的个人模板库
     *
     * @param $id 模板标题id
     * @param array $keywordIdList 开发者自行组合好的模板关键词列表，关键词顺序可以自由搭配（例如[3,5,4]或[4,5,3]），最多支持10个关键词组合
     * @return mixed
     * @author xi
     */
    public static function add($accessToken,$id,array $keywordIdList)
    {
        $postData = [
            "id" => $id,
            "keyword_id_list" => $keywordIdList
        ];
        $url = "https://api.weixin.qq.com/cgi-bin/wxopen/template/add?access_token=". $accessToken;

        $jsonStr = Helper::curlPost($url, json_encode($postData));
        $arr = json_decode($jsonStr,true);
        return $arr;
    }

    /**
     * 获取帐号下已存在的模板列表
     * @param int $offset offset和count用于分页，表示从offset开始，拉取count条记录，offset从0开始，count最大为20。最后一页的list长度可能小于请求的count
     * @param int $count offset和count用于分页，表示从offset开始，拉取count条记录，offset从0开始，count最大为20。最后一页的list长度可能小于请求的count
     * @return mixed
     * @author xi
     */
    public static function getTemplateList($accessToken,$offset = 0, $count = 20)
    {
        $postData = [
            'offset' => $offset,
            'count'  => $count
        ];
        $url = "https://api.weixin.qq.com/cgi-bin/wxopen/template/list?access_token=" . $accessToken;

        $jsonStr = Helper::curlPost($url, json_encode($postData));
        $arr = json_decode($jsonStr,true);
        return $arr;
    }

    /**
     * 删除帐号下的某个模板
     * @param $templateId 要删除的模板id
     * @return mixed
     * @author xi
     */
    public static function delTemplate($accessToken,$templateId)
    {
        $postData = [
            "template_id" => $templateId
        ];
        $url = "https://api.weixin.qq.com/cgi-bin/wxopen/template/del?access_token=" . $accessToken;

        $jsonStr = Helper::curlPost($url, json_encode($postData));
        $arr = json_decode($jsonStr,true);
        return $arr;
    }
}