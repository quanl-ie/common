<?php
namespace common\lib\openapi;

use common\helpers\Helper;
use Yii;

class MiniProgramLogin
{

    /**
     * 登录凭证校验 临时登录凭证code 获取 session_key 和 openid 等
     * @param $jsCode
     * @return mixed
     * @author xi
     */
    public static function jscode2Session($appid,$jsCode)
    {
        $componentAppid = WeChatAuthorization::getAppId();
        $componentAccessToken = WeChatAuthorization::apiComponentToken();

        $url = "https://api.weixin.qq.com/sns/component/jscode2session?appid=$appid&js_code=$jsCode&grant_type=authorization_code&component_appid=$componentAppid&component_access_token=$componentAccessToken";

        $jsonStr = Helper::curlGet($url);
        $result = json_decode($jsonStr,true);
        return $result;
    }

    /**
     * 解密获取用户信息
     * @param $sessionKey
     * @param $encryptedData
     * @param $iv
     * @return int
     * @author xi
     */
    public static function getUserInfo($sessionKey,$encryptedData,$iv)
    {
        $appid  = Yii::$app->params['miniProgram']['appid'];
        $obj = new WXBizDataCrypt($appid, $sessionKey);
        $result = $obj->decryptData($encryptedData, $iv);

        return $result;
    }
}