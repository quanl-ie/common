<?php
namespace common\lib\openapi;


use common\helpers\Helper;

class MiniProgramCode
{


    /**
     * 为授权的小程序帐号上传小程序代码
     * @see https://open.weixin.qq.com/cgi-bin/showdocument?action=dir_list&t=resource/res_list&verify=1&id=open1489140610_Uavc4&token=&lang=zh_CN
     * @param $templateId 代码库中的代码模版ID
     * @param $extJson 第三方自定义的配置
     * @param $userVersion 代码版本号，开发者可自定义
     * @param $userDesc 代码描述，开发者可自定义
     * @return mixed
     * @author xi
     */
    public static function commit($accessToken,$templateId, $extJson, $userVersion, $userDesc)
    {
        $postData = [
            "template_id" => $templateId,
            "ext_json" => $extJson, //ext_json需为string类型，参数详见https://mp.weixin.qq.com/debug/wxadoc/dev/framework/config.html
            "user_version" => $userVersion,
            "user_desc" => $userDesc,
        ];
        $url = "https://api.weixin.qq.com/wxa/commit?access_token=" . $accessToken;

        $jsonStr = Helper::curlPost($url, json_encode($postData));
        $arr = json_decode($jsonStr, true);
        return $arr;
    }

    /**
     * 获取体验小程序的体验二维码
     * @param string $path 指定体验版二维码跳转到某个具体页面（如果不需要的话，则不需要填path参数，可在路径后以“?参数”方式传入参数）具体的路径加参数需要urlencode，比如page/index?action=1编码后得到page%2Findex%3Faction%3D1
     * @return mixed
     * @author xi
     */
    public static function getQrcode($accessToken,$path = '')
    {
        if ($path != '') {
            $path = "&path=" . urlencode($path);
        }
        $url = "https://api.weixin.qq.com/wxa/get_qrcode?access_token=" . $accessToken . $path;
        return $url;
    }

    /**
     * 获取授权小程序帐号的可选类目
     * @return mixed
     * @author xi
     */
    public static function getCategory($accessToken)
    {
        $url = "https://api.weixin.qq.com/wxa/get_category?access_token=" . $accessToken;

        $jsonStr = Helper::curlGet($url);
        $arr = json_decode($jsonStr, true);
        return $arr;
    }

    /**
     * 获取小程序的第三方提交代码的页面配置（仅供第三方开发者代小程序调用）
     * @return mixed
     * @author xi
     */
    public static function getPage($accessToken)
    {
        $url = "https://api.weixin.qq.com/wxa/get_page?access_token=" . $accessToken;

        $jsonStr = Helper::curlGet($url);
        $arr = json_decode($jsonStr, true);
        return $arr;
    }

    /**
     * 将第三方提交的代码包提交审核（仅供第三方开发者代小程序调用）
     * @param $itemList 提交审核项的一个列表（至少填写1项，至多填写5项）
     * itemList = >  {
            "address":"page/logs/logs",
            "tag":"学习 工作",
            "first_class": "教育",
            "second_class": "学历教育",
            "third_class": "高等",
            "first_id":3,
            "second_id":4,
            "third_id":5,
            "title": "日志"
            }
     * @return mixed
     * @author xi
     */
    public static function submitAudit($accessToken,array $itemList)
    {
        $postData = [
            'item_list' => $itemList
        ];

        $url = "https://api.weixin.qq.com/wxa/submit_audit?access_token=" . $accessToken;

        $jsonStr = Helper::curlPost($url,json_encode($postData,JSON_UNESCAPED_UNICODE));
        $arr = json_decode($jsonStr, true);
        return $arr;
    }

    /**
     * 查询某个指定版本的审核状态（仅供第三方代小程序调用）
     * @param $auditId 提交审核时获得的审核id
     * @return mixed
     * @author xi
     */
    public static function getAuditStatus($accessToken,$auditId)
    {
        $postData = [
            'auditid' => $auditId
        ];
        $url = "https://api.weixin.qq.com/wxa/get_auditstatus?access_token=" . $accessToken;

        $jsonStr = Helper::curlPost($url,json_encode($postData));
        $arr = json_decode($jsonStr, true);
        return $arr;
    }

    /**
     * 查询最新一次提交的审核状态（仅供第三方代小程序调用）
     * @return mixed
     * @author xi
     */
    public static function getLatestAuditStatus($accessToken)
    {
        $url = "https://api.weixin.qq.com/wxa/get_latest_auditstatus?access_token=". $accessToken;

        $jsonStr = Helper::curlGet($url);
        $arr = json_decode($jsonStr, true);
        return $arr;
    }

    /**
     * 发布已通过审核的小程序（仅供第三方代小程序调用）
     * @return mixed
     * @author xi
     */
    public static function release($accessToken)
    {
        $postData = [];
        $url = "https://api.weixin.qq.com/wxa/release?access_token=" . $accessToken;

        $jsonStr = Helper::curlPost($url,"{}");
        $arr = json_decode($jsonStr, true);
        return $arr;
    }

    /**
     * 修改小程序线上代码的可见状态（仅供第三方代小程序调用）
     * @param string $action
     * @return mixed
     * @throws \Exception
     * @author xi
     */
    public static function changeVisitStatus($accessToken,$action = 'open')
    {
        if(!in_array($action,['close','open'])){
            throw new \Exception("传入参数 $action 取消范围不正确");
        }

        $postData = [
            'action' => $action
        ];
        $url = "https://api.weixin.qq.com/wxa/change_visitstatus?access_token=" . $accessToken;

        $jsonStr = Helper::curlPost($url,json_encode($postData));
        $arr = json_decode($jsonStr, true);
        return $arr;
    }

    /**
     * 小程序版本回退（仅供第三方代小程序调用）
     * @return mixed
     * @author xi
     */
    public static function revertCodeRelease($accessToken)
    {
        $url = "https://api.weixin.qq.com/wxa/revertcoderelease?access_token=" . $accessToken;

        $jsonStr = Helper::curlGet($url);
        $arr = json_decode($jsonStr, true);
        return $arr;
    }

    /**
     * 查询当前设置的最低基础库版本及各版本用户占比 （仅供第三方代小程序调用）
     * @return mixed
     * @author xi
     */
    public static function getWeAppSupportVersion($accessToken)
    {
        $postData = [];
        $url = "https://api.weixin.qq.com/cgi-bin/wxopen/getweappsupportversion?access_token=" . $accessToken;

        $jsonStr = Helper::curlPost($url,json_encode($postData));
        $arr = json_decode($jsonStr, true);
        return $arr;
    }

    /**
     * 设置最低基础库版本（仅供第三方代小程序调用）
     * @param $version
     * @return mixed
     * @author xi
     */
    public static function setWeAppSupportVersion($accessToken,$version)
    {
        $postData = [
            'version' => $version
        ];
        $url = "https://api.weixin.qq.com/cgi-bin/wxopen/setweappsupportversion?access_token=" . $accessToken;

        $jsonStr = Helper::curlPost($url,json_encode($postData));
        $arr = json_decode($jsonStr, true);
        return $arr;
    }

    /**
     * 小程序审核撤回
     * @return mixed
     * @author xi
     */
    public static function unDoCodeAudit($accessToken)
    {
        $url = "https://api.weixin.qq.com/wxa/undocodeaudit?access_token=" . $accessToken;

        $jsonStr = Helper::curlGet($url);
        $arr = json_decode($jsonStr, true);
        return $arr;
    }

    /**
     * 分阶段发布接口 (灰度发布)
     * @param $grayPercentage
     * @return mixed
     * @throws \Exception
     */
    public static function grayRelease($accessToken,$grayPercentage)
    {
        if(!is_int($grayPercentage) || $grayPercentage<0 || $grayPercentage>100){
            throw new \Exception("参数 $grayPercentage 取值范围 1到100的整数");
        }

        $postData = [
            'gray_percentage' => $grayPercentage
        ];
        $url = "https://api.weixin.qq.com/wxa/grayrelease?access_token=" . $accessToken;

        $jsonStr = Helper::curlPost($url,json_encode($postData));
        $arr = json_decode($jsonStr, true);
        return $arr;
    }

    /**
     * 取消分阶段发布 (灰度发布)
     * @return mixed
     * @author xi
     */
    public static function revertGrayRelease($accessToken)
    {
        $url = "https://api.weixin.qq.com/wxa/revertgrayrelease?access_token=" . $accessToken;

        $jsonStr = Helper::curlGet($url);
        $arr = json_decode($jsonStr, true);
        return $arr;
    }

    /**
     * 查询当前分阶段发布详情
     * @return mixed
     * @author xi
     */
    public static function getGrayReleasePlan($accessToken)
    {
        $url = "https://api.weixin.qq.com/wxa/getgrayreleaseplan?access_token=" . $accessToken;

        $jsonStr = Helper::curlGet($url);
        $arr = json_decode($jsonStr, true);
        return $arr;
    }


    /**
     * 增加或修改二维码规则
     * @see 具体参数查看 https://open.weixin.qq.com/cgi-bin/showdocument?action=dir_list&t=resource/res_list&verify=1&id=open1489140610_Uavc4&token=&lang=zh_CN
     * @param $prefix
     * @param $permitSubRule
     * @param $path
     * @param $openVersion
     * @param array $debugUrl
     * @param $isEdit
     * @return mixed
     * @author xi
     */
    public static function qrcodeJumpAdd($accessToken,$prefix,$permitSubRule,$path,$openVersion,array $debugUrl,$isEdit)
    {
        $postData = [
            "prefix"          => $prefix,
            "permit_sub_rule" => $permitSubRule,
            "path"            => $path,
            "open_version"    => $openVersion,
            "debug_url"       => $debugUrl,
            "is_edit"         => $isEdit,
        ];
        $url = "https://api.weixin.qq.com/cgi-bin/wxopen/qrcodejumpadd?access_token=" . $accessToken;

        $jsonStr = Helper::curlPost($url,json_encode($postData));
        $arr = json_decode($jsonStr, true);
        return $arr;
    }

    /**
     * 获取已设置的二维码规则
     * @return mixed
     * @author xi
     */
    public static function getQrcodeJump($accessToken)
    {
        $postData = [];
        $url = "https://api.weixin.qq.com/cgi-bin/wxopen/qrcodejumpget?access_token=" . $accessToken;

        $jsonStr = Helper::curlPost($url,json_encode($postData));
        $arr = json_decode($jsonStr, true);
        return $arr;
    }

    /**
     * 获取校验文件名称及内容
     * @return mixed
     * @author xi
     */
    public static function downloadQrcodejump($accessToken)
    {
        $postData = [];
        $url = "https://api.weixin.qq.com/cgi-bin/wxopen/qrcodejumpdownload?access_token=". $accessToken;

        $jsonStr = Helper::curlPost($url,json_encode($postData));
        $arr = json_decode($jsonStr, true);
        return $arr;
    }

    /**
     * 删除已设置的二维码规则
     * @param $prefix 二维码规则
     * @return mixed
     * @author xi
     */
    public static function deleteQrcodejump($accessToken,$prefix)
    {
        $postData = [
            'prefix' => $prefix
        ];
        $url = "https://api.weixin.qq.com/cgi-bin/wxopen/qrcodejumpdelete?access_token=" . $accessToken;

        $jsonStr = Helper::curlPost($url,json_encode($postData));
        $arr = json_decode($jsonStr, true);
        return $arr;
    }

    /**
     * 发布已设置的二维码规则
     * @param $prefix
     * @return mixed
     * @author xi
     */
    public static function publishQrcodejump($accessToken,$prefix)
    {
        $postData = [
            'prefix' => $prefix
        ];
        $url = "https://api.weixin.qq.com/cgi-bin/wxopen/qrcodejumppublish?access_token=" . $accessToken;

        $jsonStr = Helper::curlPost($url,json_encode($postData));
        $arr = json_decode($jsonStr, true);
        return $arr;
    }
}