<?php
namespace common\lib\openapi;


use common\helpers\Helper;

class MiniProgramAccount
{

    /**
     * 创建 开放平台帐号并绑定公众号/小程序
     * @see https://open.weixin.qq.com/cgi-bin/showdocument?action=dir_list&t=resource/res_list&verify=1&id=open1498704199_1bcax&token=&lang=zh_CN
     * @param $appid
     * @return mixed
     */
    public static function create($appid)
    {
        $postData = [
            'appid' => $appid
        ];
        $url = "https://api.weixin.qq.com/cgi-bin/open/create?access_token=" . WeChatAuthorization::apiComponentToken();

        $jsonStr = Helper::curlPost($url,json_encode($postData));
        $arr = json_decode($jsonStr,true);
        return $arr;
    }

    /**
     * 将 公众号/小程序绑定到开放平台帐号下
     * @param $appid  授权公众号或小程序的appid
     * @param $openAppid 开放平台帐号appid
     * @return mixed
     * @author xi
     */
    public static function bind($appid,$openAppid)
    {
        $postData = [
            "appid"      => $appid,
            "open_appid" => $openAppid
        ];
        $url = "https://api.weixin.qq.com/cgi-bin/open/bind?access_token=" . WeChatAuthorization::apiComponentToken();

        $jsonStr = Helper::curlPost($url,json_encode($postData));
        $arr = json_decode($jsonStr,true);
        return $arr;
    }

    /**
     * 将公众号/小程序从开放平台帐号下解绑
     * @param $appid
     * @param $openAppid
     * @return mixed
     * @author xi
     */
    public static function unBind($appid,$openAppid)
    {
        $postData = [
            "appid"      => $appid,
            "open_appid" => $openAppid
        ];
        $url = "https://api.weixin.qq.com/cgi-bin/open/unbind?access_token=" . WeChatAuthorization::apiComponentToken();

        $jsonStr = Helper::curlPost($url,json_encode($postData));
        $arr = json_decode($jsonStr,true);
        return $arr;
    }

    /**
     * 获取公众号/小程序所绑定的开放平台帐号
     * @param $appid
     * @return mixed
     * @author xi
     */
    public static function getOpenAppid($appid)
    {
        $postData = [
            "appid" => $appid
        ];
        $url = "https://api.weixin.qq.com/cgi-bin/open/get?access_token=" . WeChatAuthorization::apiComponentToken();

        $jsonStr = Helper::curlPost($url,json_encode($postData));
        $arr = json_decode($jsonStr,true);
        return $arr;
    }

}