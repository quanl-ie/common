<?php
namespace common\lib\openapi;


use common\helpers\Helper;

class MiniProgramBasic
{


    /**
     * 设置小程序服务器域名 ( 添加，修改，删除) 注：没有获取，获取请用下面一个方法 getDomain
     * @param string $action
     * @return mixed
     * @throws \Exception
     * @author xi
     * @date 2018-3-15
     */
    public static function modifyDomain($accessToken,$action = 'add',array $requestdomain, array $wsrequestdomain,array $uploaddomain,array $downloaddomain)
    {
        if(!in_array($action, ['add','set','delete'])){
            throw new \Exception("传入参数 $action 取消范围不正确");
        }

        $postData = [
            "action"          => $action,
            "requestdomain"   => $requestdomain,
            "wsrequestdomain" => $wsrequestdomain,
            "uploaddomain"    => $uploaddomain,
            "downloaddomain"  => $downloaddomain,
        ];
        $url = "https://api.weixin.qq.com/wxa/modify_domain?access_token=" . $accessToken;
        $jsonStr = Helper::curlPost($url, json_encode($postData));
        $arr = json_decode($jsonStr,true);
        return $arr;
    }

    /**
     * 获取已设置小程序服务器域名
     * @return mixed
     * @author xi
     */
    public static function getDomain($accessToken)
    {
        $postData = [
            "action" => 'get',
        ];
        $url = "https://api.weixin.qq.com/wxa/modify_domain?access_token=" . $accessToken;

        $jsonStr = Helper::curlPost($url, json_encode($postData));
        $arr = json_decode($jsonStr,true);
        return $arr;
    }

    /**
     * 设置小程序业务域名（仅供第三方代小程序调用）
     * @param string $action
     * @param array $webviewdomain
     * @return mixed
     * @throws \Exception
     * @author xi
     */
    public static function setWebViewDomain($accessToken,$action='add', array $webviewdomain)
    {
        if(!in_array($action, ['add','set','delete'])){
            throw new \Exception("传入参数 $action 取消范围不正确");
        }

        $postData = [
            "action"        => $action,
            "webviewdomain" => $webviewdomain
        ];
        $url = "https://api.weixin.qq.com/wxa/setwebviewdomain?access_token=" . $accessToken;

        $jsonStr = Helper::curlPost($url, json_encode($postData));
        $arr = json_decode($jsonStr,true);
        return $arr;
    }

    /**
     * 获取已设置小程序服务器域名
     * @return mixed
     * @author xi
     */
    public static function getwebviewdomain($accessToken)
    {
        $postData = [
            "action" => 'get',
        ];
        $url = "https://api.weixin.qq.com/wxa/setwebviewdomain?access_token=" . $accessToken;

        $jsonStr = Helper::curlPost($url, json_encode($postData));
        $arr = json_decode($jsonStr,true);
        return $arr;
    }

    /**
     * 绑定微信用户为小程序体验者
     * @see https://open.weixin.qq.com/cgi-bin/showdocument?action=dir_list&t=resource/res_list&verify=1&id=open1489140588_nVUgx&token=&lang=zh_CN
     * @param $wechatid
     * @return mixed
     * @author xi
     */
    public static function bindTester($wechatid)
    {
        $postData = [
            "wechatid" => $wechatid
        ];
        $url = "https://api.weixin.qq.com/wxa/bind_tester?access_token=" . WeChatAuthorization::apiComponentToken();

        $jsonStr = Helper::curlPost($url, json_encode($postData));
        $arr = json_decode($jsonStr,true);
        return $arr;
    }

    /**
     * 解除绑定小程序的体验者
     * @param $wechatid
     * @return mixed
     * @author xi
     */
    public static function unBindTester($wechatid)
    {
        $postData = [
            "wechatid" => $wechatid
        ];
        $url = "https://api.weixin.qq.com/wxa/unbind_tester?access_token=". WeChatAuthorization::apiComponentToken();

        $jsonStr = Helper::curlPost($url, json_encode($postData));
        $arr = json_decode($jsonStr,true);
        return $arr;
    }

    /**
     *  设置小程序隐私设置（是否可被搜索）
     * @param $status
     * @return mixed
     * @throws \Exception
     * @author xi
     */
    public static function changeWxaSearchStatus($status)
    {
        if(!in_array($status ,[1,0])){
            throw new \Exception("传入参数 status 取值范围不正确");
        }

        $postData = [
            "status" => $status
        ];
        $url = "https://api.weixin.qq.com/wxa/changewxasearchstatus?access_token=" . WeChatAuthorization::apiComponentToken();

        $jsonStr = Helper::curlPost($url, json_encode($postData));
        $arr = json_decode($jsonStr,true);
        return $arr;
    }

    /**
     * 查询小程序当前隐私设置（是否可被搜索）
     * @return mixed
     * @author xi
     */
    public static function getWxaSearchStatus()
    {
        $url = "https://api.weixin.qq.com/wxa/getwxasearchstatus?access_token=" . WeChatAuthorization::apiComponentToken();

        $jsonStr = Helper::curlGet($url);
        $arr = json_decode($jsonStr,true);
        return $arr;
    }

    /**
     * code 换取 session_key
     * @see https://open.weixin.qq.com/cgi-bin/showdocument?action=dir_list&t=resource/res_list&verify=1&id=open1492585163_FtTNA&token=&lang=zh_CN
     * @param $appid
     * @param $jsCode
     * @return string
     */
    public static function jscode2session($appid,$jsCode)
    {
        $componentAppid = WeChatAuthorization::getAppId();
        $url = "https://api.weixin.qq.com/sns/component/jscode2session?appid=$appid&js_code=$jsCode&grant_type=authorization_code&component_appid=$componentAppid&component_access_token=".WeChatAuthorization::apiComponentToken();
        return $url;
    }
}