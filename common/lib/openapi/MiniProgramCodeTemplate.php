<?php
namespace common\lib\openapi;


use common\helpers\Helper;

class MiniProgramCodeTemplate
{

    /**
     * 获取草稿箱内的所有临时代码草稿
     * @see https://open.weixin.qq.com/cgi-bin/showdocument?action=dir_list&t=resource/res_list&verify=1&id=open1506504150_nMMh6&token=&lang=zh_CN
     * @return mixed
     * @author xi
     */
    public static function getTemplateDraftList()
    {
        $url = "https://api.weixin.qq.com/wxa/gettemplatedraftlist?access_token=" . WeChatAuthorization::apiComponentToken();

        $jsonStr = Helper::curlGet($url);
        $arr = json_decode($jsonStr,true);
        return $arr;
    }

    /**
     * 获取代码模版库中的所有小程序代码模版
     * @return mixed
     * @author xi
     */
    public static function getTemplateList()
    {
        $url = "https://api.weixin.qq.com/wxa/gettemplatelist?access_token=".WeChatAuthorization::apiComponentToken();

        $jsonStr = Helper::curlGet($url);
        $arr = json_decode($jsonStr,true);
        return $arr;
    }

    /**
     * 删除指定小程序代码模版
     * @param $templateId 要删除的模版ID
     * @return mixed
     * @author xi
     */
    public static function deleteTemplate($templateId)
    {
        $postData = [
            "template_id" => $templateId
        ];
        $url = "https://api.weixin.qq.com/wxa/deletetemplate?access_token=".WeChatAuthorization::apiComponentToken();

        $jsonStr = Helper::curlPost($url,$postData);
        $arr = json_decode($jsonStr,true);
        return $arr;
    }
}