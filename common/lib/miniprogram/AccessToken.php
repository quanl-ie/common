<?php
namespace common\lib\miniprogram;

use Yii;
use common\helpers\Helper;

class AccessToken
{

    const ACCESS_TOKEN_KEY = 'xcxaccesstoken';


    /**
     * 获取 accesstoken
     * @return mixed
     */
    public static function getAccessToken()
    {
        if(Yii::$app->cache->exists(self::ACCESS_TOKEN_KEY))
        {
            return Yii::$app->cache->get(self::ACCESS_TOKEN_KEY);
        }
        else
        {
            $accessToken = self::_getAccessToken();
            Yii::$app->cache->set(self::ACCESS_TOKEN_KEY, $accessToken,7000 );
            return $accessToken;
        }
    }

    public static function _getAccessToken()
    {
        $appid  = Yii::$app->params['miniProgram']['appid'];
        $secret = Yii::$app->params['miniProgram']['secret'];

        $url = 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid='.$appid.'&secret='.$secret;
        $access_token = Helper::curlGet($url);
        $access_token = json_decode($access_token, true);
        $token = $access_token['access_token'];
        return $token;
    }





}