<?php
namespace common\lib\miniprogram;

use common\helpers\Helper;
use Yii;

class Login
{

    /**
     * 登录凭证校验 临时登录凭证code 获取 session_key 和 openid 等
     * @param $jsCode
     * @return mixed
     * @author xi
     */
    public static function jscode2Session($jsCode)
    {
        $appid  = Yii::$app->params['miniProgram']['appid'];
        $secret = Yii::$app->params['miniProgram']['secret'];

        $url = "https://api.weixin.qq.com/sns/jscode2session?appid=$appid&secret=$secret&js_code=$jsCode&grant_type=authorization_code";

        $jsonStr = Helper::curlGet($url);
        $result = json_decode($jsonStr,true);
        return $result;
    }

    /**
     * 解密获取用户信息
     * @param $sessionKey
     * @param $encryptedData
     * @param $iv
     * @return int
     * @author xi
     */
    public static function getUserInfo($sessionKey,$encryptedData,$iv)
    {
        $appid  = Yii::$app->params['miniProgram']['appid'];
        $obj = new WXBizDataCrypt($appid, $sessionKey);
        $result = $obj->decryptData($encryptedData, $iv);

        return $result;
    }
}