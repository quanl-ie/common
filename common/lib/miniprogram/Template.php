<?php
namespace common\lib\miniprogram;

use common\helpers\Helper;
use Yii;

class Template
{

    /**
     * 获取小程序模板库标题列表
     */
    public static  function getList($page,$pageSize)
    {
        $token = AccessToken::getAccessToken();
        $url = 'https://api.weixin.qq.com/cgi-bin/wxopen/template/library/list?access_token='.$token;
        $post_data = ['offset' => $page, 'count' => $pageSize];

        $res = Helper::curlPostJson($url, $post_data);
        return $res;
    }

    /**
     * 获取模板库某个模板标题下关键词库
     */
    public static  function getLibrary($id)
    {
        $token = AccessToken::getAccessToken();
        $url = 'https://api.weixin.qq.com/cgi-bin/wxopen/template/library/get?access_token='.$token;
        $post_data = ['id' => $id];

        $res = Helper::curlPostJson($url, $post_data);
        return $res;
    }

    /**
     * 发送模板消息
     */
    public static function send($token,$openId,$templateId,$form_id,$dataArr,$page)
    {
//        $token = AccessToken::getAccessToken();
        $url = 'https://api.weixin.qq.com/cgi-bin/message/wxopen/template/send?access_token='.$token;
        $post_data = [
            'touser'      => $openId,
            'template_id' => $templateId,
            'form_id'     => $form_id,
            'data'        => $dataArr,
            'page'        => $page
        ];

        $res = Helper::curlPostJson($url, $post_data);
        return $res;
    }
}