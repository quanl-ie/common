<?php
namespace common\lib;

use JPush\Model as M;
use JPush\JPushClient;
use JPush\Exception\APIConnectionException;
use JPush\Exception\APIRequestException;
use common\models\Technician;

class Push
{
    //最大用几个进程发送
    const MAX_PAGES   = 2;
    const IOS         = 1;
	const IOS_APP_KEY = 'f2fbde99a34bc23ebabf2312';
	const IOS_SECRET  = 'e336300890ae9afc5c86466a';
	const ANDROID_APP_KEY = 'f2fbde99a34bc23ebabf2312';
	const ANDROID_SECRET  = 'e336300890ae9afc5c86466a';
	const LOCK_KEY = 'redis_push_key';

	const TITLE = '智能云集';
	const XM_MAX_PAGES = 20;

	//线上 true 测试 false
	const ONLINE = false;

    private static $_ios_instance = null;
    private static $_android_instance = null;
    
    public static function getIosInstance()
	{
        if (is_null ( self::$_ios_instance ))
		{
			self::$_ios_instance = new JPushClient(self::IOS_APP_KEY, self::IOS_SECRET);
        }
        return self::$_ios_instance;
    }

    public static function getAndroidInstance()
    {
        if (is_null ( self::$_android_instance ))
        {
            self::$_android_instance = new JPushClient(self::ANDROID_APP_KEY, self::ANDROID_SECRET);
        }
        return self::$_android_instance;
    }
    
    
    /**
     * 单条消息
     * @param string $title          发送标题
     * @param string $content        发送内容
     * @param int    $technician_id  用户ID
     * @param int    $type           消息类型  1、订单消息  2、技师审核消息  3、用户账号消息  4、活动消息  5、位置上传消息
     * @param string $type_content   消息跳转条件  type为1该字段为订单号   type为2该字段为技师ID
     * @param int    $order_status   订单状态
     * @param string $jpush_id
     * @return array
     */
    public static function pushMsg($title, $content, $technician_id, $type, $type_content, $order_status = 0, $jpush_id = '', $platform = '') {
        //查询用户的极光唯一ID和设备
        $tecArr = Technician::find()->where(['id' => $technician_id])->asArray()->one();
        if(!$title) {
            return json_encode(['success' => false, 'message' => "title为空"]);
        }
        if(!$content) {
            return json_encode(['success' => false, 'message' => "content为空"]);
        }
        if(!$tecArr) {
            return json_encode(['success' => false, 'message' => "用户信息错误"]);
        }
        if(!in_array($type,[1,2,3,5])) {
            return json_encode(['success' => false, 'message' => "type类型错误"]);
        }
        if(!$type_content) {
            return json_encode(['success' => false, 'message' => "type_content为空"]);
        }
        if(!intval($order_status) && $type == 1) {
            return json_encode(['success' => false, 'message' => "订单状态为空"]);
        }
        //生成要发送的内容
        $userId          = $tecArr['jpush_id'];
        $objectType      = $type;
        $objectContent   = $type_content;
        $objectStatus    = $order_status;
        if($platform == '') {
            $platform        = strtolower($tecArr['mobile_system']);
        }
        $alert  = [
            "alert" => $title,
        ];
        if($platform == 'android') {
            $alert = [
                "title" => $title,
                "alert" => $content
            ];
        }
        if($type == 3) {
            $userId = $jpush_id;
        }
        //调用发送接口
        $res = self::sendSingle($userId, json_encode($alert), $objectType, $objectContent, $objectStatus, strtolower($platform));
        if($res['status'] != 1) {
            return json_encode(['success' => false, 'message' => $res['message']]);
        }
        return json_encode(['success' => true, 'message' => '']);
    }


    /**
     * 自定义消息发送
     */
    public static function sendCustom($technician_id)
    {
        try
        {
            //查询用户的极光唯一ID和设备
            $tecArr = Technician::find()->where(['id' => $technician_id])->asArray()->all();
            $client = new JPushClient(self::IOS_APP_KEY, self::IOS_SECRET);
            foreach ($tecArr as $val)
            {
				if($val['jpush_id'])
				{
					$platform = strtolower($val['mobile_system']);
					if(!$platform) {
						return json_encode(['status' => 0, 'message' => '用户未登陆', 'data' => []]);
					}
					if($platform == 'android') {
						$client = new JPushClient(self::ANDROID_APP_KEY, self::ANDROID_SECRET);
					}

					$result = $client->push()
						->setPlatform(M\platform($platform))
						->setAudience(M\Audience(M\registration_id([$val['jpush_id']])))
						->setMessage(M\message('upload position', null, null, array('pushType'=>'10000')));
					$result->setOptions(M\options(null, 10000, null, self::ONLINE));
					$result->send();
				}
            }
        }
        catch (\Exception $e)
        {
        }

    }


    /**
	 * 发送单个
	 * @param array $userIds 用户ids
	 * @param string $notification 发送内容
	 * @param string $objectType 发送类型
	 * @param string/int $objectId  到达的id
	 * @return array
	 * @author xi
	 * @date 2017-07-15
	 */
	private static function sendSingle($userId,$notification,$objectType,$objectContent, $objectStatus,$platform)
    {
        $data['userid']        = $userId;
        $data['notification']  = $notification;
        $data['objectType']    = $objectType;
        $data['objectContent'] = $objectContent;
        $data['objectStatus']  = $objectStatus;
        $data['platform']      = $platform;
		$status  = 0;
		$data    = [];
		$message = '';
		$paramsJson = '';

		if(!$userId){
			$message = 'userIds 不能为空';
		}
		else if(trim($notification) == ''){
			$message = 'notification 不能为空';
		}
		else if(!in_array($objectType,[1,2,3,4,5,6,7])){
			$message = "objectType 取值范围为：[1,2,3,4,5,6,7]";
		}
		else if(!$objectContent){
			$message = "objectContent 不能为空";
		}
		else if(!in_array($platform, ['ios','android'])){
		    $message = "platform 取值范围为：['ios','android']";
		}
		else
		{
		    $alias = [$userId];

			$extend = array(
				'object_type'    => $objectType,
				'object_content' => $objectContent,
                'object_status'  => $objectStatus,
			);

			$data = [
			    'msg_id' => ''
			];

			try
			{
			    $notificationParams = '';
			    if($platform == 'ios'){
				    $client = self::getIosInstance();
				    $notificationParams = M\ios($notification, NULL, null, null, $extend, null);
			    }
			    else if($platform == 'android'){
			        $client = self::getAndroidInstance();
			        $notificationParams = M\android($notification, NULL, null, $extend);
			    }

				$result = $client->push();
				$result->setPlatform(M\Platform($platform));
				$result->setAudience(M\Audience(M\registration_id($alias)));
				$result->setNotification(M\notification($notification, $notificationParams));

                if($platform == 'android') {
                    $notification = [
                        "android" => array_merge( [
                            "builder_id" =>  1,
                            "extras" =>$extend
                        ],json_decode($notification,true))
                    ];

                    $result->setNotification($notification);
                }
                else
                {
                    $notification = [
                        "ios" => array_merge( [
                            "sound" => "default",
                            "badge" => "+1",
                            "extras" =>$extend
                        ],json_decode($notification,true))
                    ];

                    $result->setNotification($notification);
                }

				$result->setOptions(M\options(null, 10000, null, self::ONLINE));
				$paramsJson = $result->getJSON();
				$response = $result->send();
                //判断接口频率是否用完，如果快要没了，就等一下
                if($response->response->headers['x-rate-limit-remaining'] <= 5)
                {
                    //还有多少秒归零 
                    $limtReset = $response->response->headers['x-rate-limit-reset'];
                    sleep($limtReset);
                }

				$status = 1;
				$data = [
					'msg_id' => (string)$response->msg_id,
				];
				$message = $response->json;
			}
			catch (APIRequestException $e)
			{
				$message = json_encode([
					'httpCode' => $e->httpCode,
					'code'     => $e->code,
					'message'  => $e->message,
					'json'     => $e->json,
					'rateLimitLimit'     => $e->rateLimitLimit,
					'rateLimitRemaining' => $e->rateLimitRemaining,
					'rateLimitReset'     => $e->rateLimitReset
				]);

				$jsonArr = json_decode($e->json,true);
				if(isset($jsonArr['msg_id'])){
    				$data = [
    				    'msg_id' => number_format($jsonArr['msg_id'],0,'','')
    				];
				}

				//判断接口频率是否用完，如果快要没了，就等一下
				if($e->rateLimitRemaining <= 5)
				{
				    //还有多少秒归零
				    sleep($e->rateLimitReset);
				}
			}
			catch (APIConnectionException $e)
			{
				$message = json_encode([
					'message'           => $e->getMessage(),
					'isResponseTimeout' => $e->isResponseTimeout
				]);
			}
		}

       return [
			'status'     => $status,
			'data'       => $data,
			'message'    => $message,
			'paramsJson' => $paramsJson
	   ];
    }

	/**
	 * 发送全部
	 * @param string $notification 发送内容
	 * @param string $objectType 发送类型
	 * @param string/int $objectId  到达的id
	 * @param string $platform
	 * @return array
	 * @author xi
	 * @date 2017-07-15
	 */
	private static function sendAll($notification,$objectType,$objectId,$platform)
    {
		$status  = 0;
		$data    = [];
		$message = '';
		$paramsJson = '';

		if(trim($notification) == ''){
			$message = 'notification 不能为空';
		}
		else if(!in_array($objectType,['goods','assignment','subject','webview',''])){
			$message = "objectType 取值范围为：['goods','assignment','subject','webview']";
		}
		else if(!$objectId){
			$message = "objectId 不能为空";
		}
		else if(!in_array($platform, ['ios','android'])){
		    $message = "platform 取值范围为：['ios','android']";
		}
		else
		{
			$extend = array(
				'object_type' => $objectType,
				'object_id'   => $objectId,
			);

			$data = [
			    'msg_id' => ''
			];

			try
			{
			    $notificationParams = '';
			    if($platform == 'ios'){
				    $client = self::getIosInstance();
				    $notificationParams = M\ios($notification, NULL, null, null, $extend, null);
			    }
			    else if($platform == 'android'){
			        $client = self::getAndroidInstance();
			        //$notificationParams = M\android($notification, NULL, null, $extend);
			    }
				$result = $client->push();
				$result->setPlatform(M\Platform($platform));
				$result->setAudience('all');

                if($platform == 'android') {
                    $result->setNotification(json_decode($notification,true));
                }
                else{
                    $result->setNotification(M\notification($notification, $notificationParams));
                }

				$result->setOptions(M\options(null, 10000, null, null));

				$paramsJson = $result->getJSON();
				$response = $result->send();

				$status = 1;
				$data = [
					'msg_id' => $response->msg_id,
				];
				$message = $response->json;
			}
			catch (APIRequestException $e)
			{
				$message = json_encode([
					'httpCode' => $e->httpCode,
					'code'     => $e->code,
					'message'  => $e->message,
					'json'     => $e->json,
					'rateLimitLimit'     => $e->rateLimitLimit,
					'rateLimitRemaining' => $e->rateLimitRemaining,
					'rateLimitReset'     => $e->rateLimitReset
				]);

				print_r($message);die;

				$jsonArr = json_decode($e->json);
				$data = [
				    'msg_id' => $jsonArr->msg_id
				];
			}
			catch (APIConnectionException $e)
			{
				$message = json_encode([
					'message'           => $e->getMessage(),
					'isResponseTimeout' => $e->isResponseTimeout
				]);
			}
		}


       $result =  [
			'status'     => $status,
			'data'       => $data,
			'message'    => $message,
			'paramsJson' => $paramsJson
	   ];
       //$this->logs(" platform:" . $platform. " " . json_encode($result) );
       return $result;
    }

    /**
     * 获取发送结果
     * @param string $msgIds msg_ids
     * @return array
     * @author xi
     * @date 2017-07-15
     */
    private static function getReport($msgIds)
    {
        $status  = 0;
        $message = '';
        $data    = [];

        try
        {
            $client  = self::getInstance();
            $result = $client->report($msgIds);

            foreach($result->received_list as  $item)
            {
                $data [$item->msg_id] = [
                    'android_received' => $item->android_received ,
                    'ios_apns_sent'    => $item->ios_apns_sent
                ];
            }
            $status = 1;

            //判断接口频率是否用完，如果快要没了，就等一下
            if($result->response->headers['x-rate-limit-remaining'] == 1)
            {
                //还有多少秒归零 
                $limtReset = $result->response->headers['x-rate-limit-reset'];
                sleep($limtReset);
            }
        }
        catch (APIRequestException $e) {
            $message = json_encode([
                    'httpCode' => $e->httpCode,
                    'code'     => $e->code,
                    'message'  => $e->message,
                    'json'     => $e->json,
                    'rateLimitLimit'     => $e->rateLimitLimit,
                    'rateLimitRemaining' => $e->rateLimitRemaining,
                    'rateLimitReset'     => $e->rateLimitReset
                ]);
        }
        catch (APIConnectionException $e) {
            $message = json_encode([
                'message'           => $e->getMessage(),
                'isResponseTimeout' => $e->isResponseTimeout
            ]);
        }

        return [
            'status'  => $status,
            'data'    => $data,
            'message' => $message
        ];
    }

    /**
     * 检查发送条数
     * @param int $pushId
     * @param int $userId
     * @return array
     * @author xi
     */
    private static function checkSendCount($pushId,$userId,$from)
    {
        $cacheOneCount   = 'pushcacheonecount'.$from.$pushId.$userId;
        $cacheThreeCount = 'pushcachethreecount'.$from.$userId;

        $cacheTime = strtotime('tomorrow') - time();

        if(Yii::$app->cache->exists($cacheThreeCount))
        {
            $count = Yii::$app->cache->get($cacheThreeCount);
            if( $count >= 3 ){
                return [
                    'status'  => 0,
                    'message' => '一天只能发送3条'
                ];
            }
            Yii::$app->cache->set($cacheThreeCount, $count+1,$cacheTime);
        }
        else{
            Yii::$app->cache->set($cacheThreeCount, 1,$cacheTime);
        }

        if(!Yii::$app->cache->exists($cacheOneCount)){
            Yii::$app->cache->set($cacheOneCount, 1,$cacheTime);
        }
        else {
            return [
                'status'  => 0,
                'message' => 'push id = '.$pushId . ', 已发送过了'
            ];
        }

        return [
            'status'  => 1,
            'message' => 'success'
        ];
    }

}

