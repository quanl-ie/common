<?php
namespace common\lib\wechat;

use common\helpers\Helper;

class WeChatThirdCardAccount
{
    //微信接口基本地址
    const WECHAT_BASE_URL = 'https://api.weixin.qq.com';
    //母商户资质申请地址
    const WHCHAT_UPLOAD_CARD_AGENT_QUALIFICATION_URL = '/cgi-bin/component/upload_card_agent_qualification?';
    //母商户资质审核查询地址
    const WHCHAT_CHECK_CARD_AGENT_QUALIFICATION_URL = '/cgi-bin/component/check_card_agent_qualification?';
    //子商户资质申请地址
    const WHCHAT_UPLOAD_CARD_MERCHANT_QUALIFICATION_URL = '/cgi-bin/component/upload_card_merchant_qualification?';
    //子商户资质申请地址
    const WHCHAT_CHECK_CARD_MERCHANT_QUALIFICATION_URL = '/cgi-bin/component/check_card_merchant_qualification?';
    //上传media
    const WHCHAT_UPLOAD_MEDIA_URL = '/cgi-bin/media/upload?';
    //卡券开放类目查询地址
    const WHCHAT_CARD_GETAPPLYPROTOCOL_URL = '/card/getapplyprotocol?';
    //拉取单个子商户信息
    const WHCHAT_GET_CARD_MERCHAN_URL = '/cgi-bin/component/get_card_merchan?';
    //拉取子商户列表
    const WHCHAT_BATCHGET_CARD_MERCHANT_URL = '/cgi-bin/component/batchget_card_merchant?';



    /**
     * 母商户资质申请
     * @param array $requestParams
     * $requestParams = [
     *      'register_capital' => '100000',注册资本，数字，单位：分
     *      'business_license_media_id' => 'Y0vGhBIycyhEFTyq6km0pGpCPzdqoxMn',营业执照扫描件的media_id
     *      'tax_registration_certificate_media_id' => 'Y0vGhBIycyhEFTyq6km0pGp',税务登记证扫描件的media_id
     *      'last_quarter_tax_listing_media_id' => 'Y0vGhBIycyhEFTyq6km0',上个季度纳税清单扫描件media_id
     * ]
     * @return array
     */
    public static function uploadAgentQualification($requestParams = [])
    {
        if (empty($requestParams)) {
            return false;
        }
        $url = static::WECHAT_BASE_URL . static::WHCHAT_UPLOAD_CARD_AGENT_QUALIFICATION_URL . 'access_token=' . WeChatAccessToken::getAccessToken();
        $requestParams = json_encode($requestParams,JSON_UNESCAPED_UNICODE);
        return json_decode(Helper::curlPost($url,$requestParams),true);
    }


    /**
     * 母商户资质审核查询接口
     *
     * @return void
     * @author 
     **/
    public static function checkAgentQualification($requestParams = [])
    {
        if (empty($requestParams)) {
            return false;
        }
        $url = static::WECHAT_BASE_URL . static::WHCHAT_CHECK_CARD_AGENT_QUALIFICATION_URL . 'access_token=' . WeChatAccessToken::getAccessToken();
        $requestParams = json_encode($requestParams,JSON_UNESCAPED_UNICODE);
        return json_decode(Helper::curlGet($url,$requestParams),true);
    }


    /**
     * 子商户资质申请
     * @param array $requestParams
     * $requestParams = [
     *      'appid' => '100000',子商户公众号的appid
     *      'name' => 'Y0vGhBIycyhEFTyq6km0pGpCPzdqoxMn',子商户商户名，用于显示在卡券券面
     *      'logo_meida_id' => 'Y0vGhBIycyhEFTyq6km0pGp',子商户logo，用于显示在子商户卡券的券面
     *      'business_license_media_id' => 'Y0vGhBIycyhEFTyq6km0',  营业执照或个体工商户执照扫描件的media_id
     *      'agreement_file_media_id' => 'Y0vGhBIycyhEFTyq6km0',子商户与第三方签署的代理授权函的media_id
     *      'primary_category_id' => 'Y0vGhBIycyhEFTyq6km0',一级类目id
     *      'secondary_category_id' => 'Y0vGhBIycyhEFTyq6km0',二级类目id
     *      'operator_id_card_media_id' => 'xxxxx',当子商户为个体工商户且无公章时，授权函须签名，并额外提交该个体工商户经营者身份证扫描件的media_id(非必传)
     * ]
     * @return array
     */
    public static function uploadMerchantQualification($requestParams = [])
    {
        if (empty($requestParams)) {
            return false;
        }
        $url = static::WECHAT_BASE_URL . static::WHCHAT_UPLOAD_CARD_MERCHANT_QUALIFICATION_URL . 'access_token=' . WeChatAccessToken::getAccessToken();
        $requestParams = json_encode($requestParams,JSON_UNESCAPED_UNICODE);
        return json_decode(Helper::curlPost($url,$requestParams),true);
    }
    

    /**
     * 子商户资质审核查询接口
     *
     * @return void
     * @author 
     **/

    public static function checkMerchantQualification($requestParams = [])
    {
        if (!isset($requestParams['appid'])) {
            return false;
        }
        $url = static::WECHAT_BASE_URL . static::WHCHAT_CHECK_CARD_MERCHANT_QUALIFICATION_URL . 'access_token=' . WeChatAccessToken::getAccessToken();
        $requestParams = json_encode($requestParams,JSON_UNESCAPED_UNICODE);
        return json_decode(Helper::curlPost($url,$requestParams),true);
    }

    //图片上传media接口
    public static function uploadMedia($filename,$type='image')
    {
        $url = static::WECHAT_BASE_URL . static::WHCHAT_UPLOAD_MEDIA_URL .  'access_token=' . WeChatAccessToken::getAccessToken() .'&type='.$type;
        $result = exec('curl -F media=@'.$filename.$url);
        
        return json_decode($result,true);
       
    }

    /**
     * 卡券开放类目查询接口
     *
     * @return void
     * @author 
     **/
    public static function getapplyprotocol($requestParams = [])
    {
        $url = static::WECHAT_BASE_URL . static::WHCHAT_CARD_GETAPPLYPROTOCOL_URL . 'access_token=' . WeChatAccessToken::getAccessToken();
        $requestParams = json_encode($requestParams,JSON_UNESCAPED_UNICODE);
        return json_decode(Helper::curlGet($url,$requestParams),true);
    }


    /**
     * 拉取单个子商户信息接口
     *
     * @return void
     * @author 
     **/
    public static function getCardMerchant($requestParams=[])
    {
        if (!isset($requestParams['appid'])) {
            return false;
        }

        $url = static::WECHAT_BASE_URL . static::WHCHAT_GET_CARD_MERCHAN_URL . 'access_token=' . WeChatAccessToken::getAccessToken();
        $requestParams = json_encode($requestParams,JSON_UNESCAPED_UNICODE);
        return json_decode(Helper::curlPost($url,$requestParams),true);
    }


    /**
     * 拉取子商户列表接口
     *
     * @return void
     * @author 
     **/
    public static function batchgetCardMerchant($requestParams=[])
    {

        if (!isset($requestParams['next_get'])) {
            return false;
        }
        $url = static::WECHAT_BASE_URL . static::WHCHAT_BATCHGET_CARD_MERCHANT_URL . 'access_token=' . WeChatAccessToken::getAccessToken();
        $requestParams = json_encode($requestParams,JSON_UNESCAPED_UNICODE);
        return json_decode(Helper::curlPost($url,$requestParams),true);
    }

}