<?php
namespace common\lib\wechat;

use common\helpers\Helper;

class WeChatMenu
{
    
    /**
     * 创建菜单
     * @param array $arr 菜单数组
     * @return boolean|array 如查成功返回真，否则返回数组信息
     * @author xi
     * @since 2015-1-6
     */
    public static function add($arr,$accessToken='')
    {
        foreach ($arr as $key=>$val){
            $arr[$key]['name'] = urlencode($arr[$key]['name']);
            if(isset($val['sub_button']) && !empty($val['sub_button'])){
                foreach ($val['sub_button'] as $k=>$v){
                    $arr[$key]['sub_button'][$k]['name'] = urlencode($arr[$key]['sub_button'][$k]['name']);
                }
            }
        }
        
        $menu['button'] = $arr;
        $json = json_encode($menu);
        $json = urldecode($json);

         //获取ACCESS_TOKEN
        if($accessToken==''){
            $accessToken = WeChatAccessToken::getAccessToken();
        }
        $url = 'https://api.weixin.qq.com/cgi-bin/menu/create?access_token='.$accessToken;
        $result = Helper::curlPost($url, $json);
        $result = json_decode($result,true);
        if(isset($result['errcode']) && $result['errcode'] == 0){
            return true;
        }
        return $result;
    }
    
}