<?php
namespace common\lib\wechat;

use common\helpers\Helper;


class WeChatCardNoThird
{
    //微信api卡卷url
    const WX_API_CARD_URL = 'https://api.weixin.qq.com/card';
    //创建子商户接口
    const SUBMERCHANT_SUBMIT = '/submerchant/submit?access_token=%s';
    const GET_APPLY_PROTOCOL = '/getapplyprotocol?access_token=%s';
    const SUBMERCHANT_UPDATE = '/submerchant/update?access_token=%s';
    const SUBMERCHANT_GET = '/submerchant/get?access_token=%s';
    const SUBMERCHANT_BATCHGET = '/submerchant/batchget?access_token=%s';
    const CREATE = '/create?access_token=%s';

    /**
     * 创建子商户接口
     * @see http://mp.weixin.qq.com/wiki/13/a1b2bedbca3990108195f13bd7e27401.html
     * @param $brand_name	是	String(36)	兰州拉面	子商户名称（12个汉字内），该名称将在制券时填入并显示在卡券页面上
     * @param $logo_url	是	string(128)	http://mmbiz.xxxx	子商户logo，可通过上传logo接口获取。该logo将在制券时填入并显示在卡券页面上
     * @param $protocol	是	String(36)	mdasdfkl ：	授权函ID，即通过上传临时素材接口上传授权函后获得的meida_id
     * @param $end_time	是	unsigned int	15300000	授权函有效期截止时间（东八区时间，单位为秒），需要与提交的扫描件一致
     * @param $primary_category_id	是	int	2	一级类目id,可以通过本文档中接口查询
     * @param $secondary_category_id	是	int	2	二级类目id，可以通过本文档中接口查询
     * @param $agreement_media_id	否	string(36)	2343343424	营业执照或个体工商户营业执照彩照或扫描件
     * @param $operator_media_id	否	string(36)	2343343424	营业执照内登记的经营者身份证彩照或扫描件
     * @return string|array 成功返回string|失败返回array
     * @author junliang.zhang
     * @since  2016-02-17
     * */
    public static function submerChantSubmit($brand_name,$logo_url,$protocol,$agreement_media_id,$operator_media_id,$end_time,$primary_category_id = '',$secondary_category_id = '')
    {
        $data = ['info' => [
            'brand_name' => $brand_name,
            'logo_url' => $logo_url,
            'protocol' => $protocol,
            'agreement_media_id' => $agreement_media_id,
            'operator_media_id' => $operator_media_id,
            'end_time' => $end_time,
            'primary_category_id' => $primary_category_id,
            'secondary_category_id' => $secondary_category_id
        ]];
        return static::getData($data,static::SUBMERCHANT_SUBMIT);
    }
    /**
     * 卡券开放类目查询接口
     * @see http://mp.weixin.qq.com/wiki/13/a1b2bedbca3990108195f13bd7e27401.html
     * @param 用接口去查询获取实时卡券类目
     * @return string|array 成功返回array|失败返回string
     * @author junliang.zhang
     * @since  2016-02-17
     * */
    public static function getApplyProtocol($data)
    {
        return static::getData($data,static::GET_APPLY_PROTOCOL,'get');
    }
    /**
     * 更新子商户接口
     * @see http://mp.weixin.qq.com/wiki/13/a1b2bedbca3990108195f13bd7e27401.html
     * @param $merchant_id	是	int	12	子商户id，一个母商户公众号下唯一。
     * @param $brand_name	是	String(36)	兰州拉面	子商户名称（12个汉字内），该名称将在制券时填入并显示在卡券页面上
     * @param $logo_url	是	string(128)	http://mmbiz.xxxx	子商户logo，可通过上传logo接口获取。该logo将在制券时填入并显示在卡券页面上
     * @param $protocol	是	String(36)	mdasdfkl ：	授权函ID	，即通过上传临时素材接口上传授权函后获得的meida_id
     * @param $end_time	是	unsigned int	15300000	授权函有效期截止时间（东八区时间，单位为秒），需要与提交的扫描件一致
     * @param $agreement_media_id	否	string(36)	dhskdjklfjk	营业执照或个体工商户营业执照彩照或扫描件
     * @param $operator_media_id	否	string(36)	dhskdjklfjk	营业执照内登记的经营者身份证彩照或扫描件
     * @param $primary_category_id	是	int	2	一级类目id,可以通过本文档中接口查询
     * @param $secondary_category_id	是	int	2	二级类目id，可以通过本文档中接口查询
     * @return string|array 成功返回string|失败返回array
     * @author junliang.zhang
     * @since  2016-02-17
     * */
    public static function submerChantUpdate($merchant_id,$brand_name,$logo_url,$protocol,$agreement_media_id,$operator_media_id,$end_time,$primary_category_id = '',$secondary_category_id = '')
    {
        $data = ['info' => [
            'merchant_id' => $merchant_id,
            'brand_name' => $brand_name,
            'logo_url' => $logo_url,
            'protocol' => $protocol,
            'agreement_media_id' => $agreement_media_id,
            'operator_media_id' => $operator_media_id,
            'end_time' => $end_time,
            'primary_category_id' => $primary_category_id,
            'secondary_category_id' => $secondary_category_id
        ]];
        return static::getData($data, static::SUBMERCHANT_UPDATE);
    }
    /**
     * 拉取单个子商户信息接口
     * @see http://mp.weixin.qq.com/wiki/13/a1b2bedbca3990108195f13bd7e27401.html
     * @param $merchant_id	是	int	12	子商户id，一个母商户公众号下唯一。
     * @return string|array 成功返回string|失败返回array
     * @author junliang.zhang
     * @since  2016-02-17
     * */
    public static function submerChantGet($merchant_id)
    {
        $data = [
            'merchant_id' => $merchant_id,
        ];
        return static::getData($data, static::SUBMERCHANT_GET);
    }
    /**
     * 批量拉取子商户信息接口
     * @see http://mp.weixin.qq.com/wiki/13/a1b2bedbca3990108195f13bd7e27401.html
     * @param $begin_id	起始的子商户id，一个母商户公众号下唯一
     * @param $limit	拉取的子商户的个数，最大值为100
     * @param $status	子商户审核状态，填入后，只会拉出当前状态的子商户
     * @return string|array 成功返回string|失败返回array
     * @author junliang.zhang
     * @since  2016-02-17
     * */
    public static function submerChantBatchget($begin_id,$limit,$status)
    {
        $data = [
            'begin_id' => $begin_id,
            'limit' => $limit,
            'status' => $status,
        ];
        return static::getData($data, static::SUBMERCHANT_BATCHGET);
    }
    /**
     * 创建子商户卡券接口
     * @see http://mp.weixin.qq.com/wiki/13/a1b2bedbca3990108195f13bd7e27401.html
     * @param see http://mp.weixin.qq.com/wiki/13/0bdfcbc15653762842de7e51aac317e9.html
     * @return string|array 成功返回string|失败返回array
     * @author junliang.zhang
     * @since  2016-02-17
     * */
    public static function create($data)
    {
        $data = [
            'card' => $data
        ];
        return static::getData($data, static::CREATE);
    }
    /**
     * 获取api结果
     * @param $data 参数
     * @param $url 地址
     */
    private static function getData($data,$url,$way = 'post')
    {
        $access_token = WeChatAccessToken::getAccessToken();
        $url = sprintf(static::WX_API_CARD_URL.$url,$access_token);
        switch($way)
        {
            case 'get':
                $result_json = Helper::curlGet($url);
            break;
            case 'post':
                $data = json_encode($data);
                $data = urldecode($data);
                $result_json = Helper::curlPost($url, $data);
            break;
            default:
                return false;
            break;
        }
        $result = json_decode($result_json,true);
        if(isset($result['errcode']) && $result['errcode'] != 0)
        {
            return $result['errmsg'];
        }
        else
        {
            return $result;
        }
    }
}