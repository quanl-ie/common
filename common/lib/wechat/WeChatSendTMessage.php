<?php
namespace common\lib\wechat;

use Yii;
use common\helpers\Helper;

class WeChatSendTMessage
{

    /**
     * 发送模板消息
     * @param $messageModel
     * @return json
     * @author niu
     */
    public static function sendTMessage($nMessageModel)
    {
        $result = "";

        do{
            if(!$nMessageModel)
                break;

            //拼合数据
            $json = [
                "touser"  => $nMessageModel['touser'],
                "template_id"  => $nMessageModel['template_id'],
                "url"  => $nMessageModel['url'],
                "topcolor"  => $nMessageModel['topcolor'],
                "data" => $nMessageModel['data']
            ];

            //调用发送接口
            $appId = Yii::$app->params['wechat_jnxh']['appid'];
            $appsecret = Yii::$app->params['wechat_jnxh']['appsecret'];
            $accessToken = WeChatAccessTokenTemplete::getAccessToken($appId,$appsecret,'ynxh_weixin_access_token');
            if(!$accessToken)
                break;

            $url = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=$accessToken";

            $result = Helper::curlPost($url, json_encode($json));
            $result = json_decode($result,true);

        }while(false);

        return $result;
    }
}