<?php
namespace common\lib\wechat;

use common\helpers\Helper;
use Yii;

class WeChatJsSDK
{
    /**
     * 获取js 配置文件
     * @return array
     * @author xi
     * @since 2015-1-12
     */
    public static function getSignPackage($url='') 
    {
        $appid = \Yii::$app->params['wechat']['appid'];
        
        $jsapiTicket = self::getJsApiTicket('jsapi');
        if($url==''){
            $url = Yii::$app->request->absoluteUrl;
        }
        $timestamp = time();
        $nonceStr = WeChatRand::get(16);
    
        // 这里参数的顺序要按照 key 值 ASCII 码升序排序
        $string = "jsapi_ticket=$jsapiTicket&noncestr=$nonceStr&timestamp=$timestamp&url=$url";
    
        $signature = sha1($string);
    
        $signPackage = array(
            "appId"     => $appid,
            "nonceStr"  => $nonceStr,
            "timestamp" => $timestamp,
            "url"       => $url,
            "signature" => $signature,
            "rawString" => $string
        );
        return $signPackage;
    }
    
    
    /**
     * 获取 ticket ，如果没有就缓存起来
     * @return string
     * @author xi
     * @since 2015-1-12
     */
    public static function getJsApiTicket($ticketType = 'jsapi') 
    {
        $cache_key = 'jsapi_ticket_json'.$ticketType;
        $data = json_decode(Yii::$app->cache->get($cache_key),true);
        
        $accessToken = WeChatAccessToken::getAccessToken();
        if( isset($data['accessToken']) && $data['accessToken']!=$accessToken){
            Yii::$app->cache->delete($cache_key);
        }
        
        if(!isset($data['expire_time']) || $data['expire_time'] < time())
        {
            
            $url = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?type=$ticketType&access_token=$accessToken";
            $res = json_decode(Helper::curlGet($url),true);
            
            $ticket = '';
            if (isset($res['ticket']) && $res['ticket'])
             {
                 $jsonArr = array(
                     'expire_time'  => time()+7000,
                     'jsapi_ticket' => $res['ticket'],
                     'accessToken'  => $accessToken
                 );
                
                Yii::$app->cache->set($cache_key,json_encode($jsonArr),7200);
                
                $ticket = $res['ticket'];
            }
            return $ticket;
        }
        else {
            return $data['jsapi_ticket'];
        }
    
    }
    
    /**
     * 生成卡券签名
     * @param string $openid
     * @param string $card_id
     * @return string
     */
    public static function getCardSignature($cardid)
    {
        $ticket = self::getJsApiTicket('wx_card');
        $timestamp = time();
        
        $arr = [
            $timestamp,
            $cardid,
            $ticket,
        ];
        
        
        natsort($arr);
        $string = implode('', $arr);
        
        $signature = sha1($string);
        return [
            'signature' => $signature,
            'timestamp' => $timestamp,
            'cardid'    => $cardid,
			'ticket'    => $ticket,
        ];
    }
}