<?php
namespace common\lib\wechat;

use common\helpers\Helper;
use Yii;
use common\models\Log;

class WeChatPay
{
    
    /**
     * 获取jsapi 参数
     * @param unknown $prepay_id
     * @return string json
     */
	public static function getParameters($prepay_id)
	{
		$jsApiObj["appId"]     = \Yii::$app->params['wechat']['appid'];
	    $jsApiObj["timeStamp"] = "".time();
	    $jsApiObj["nonceStr"]  = WeChatRand::get(32);
		$jsApiObj["package"]   = "prepay_id=$prepay_id";
	    $jsApiObj["signType"]  = "MD5";
	    $jsApiObj["paySign"]   = self::getSign($jsApiObj);
		
		return json_encode($jsApiObj);
	}
	
	/**
	 * 对参数排序
	 * @param array $paraMap
	 * @param string $urlencode
	 * @return string
	 */
	public static function formatBizQueryParaMap($paraMap, $urlencode=false)
	{
	    $buff = "";
	    ksort($paraMap);
	    foreach ($paraMap as $k => $v){
	        if($urlencode){
	            $v = urlencode($v);
	        }
	        $buff .= $k . "=" . $v . "&";
	    }
	    $reqPar = '';
	    if (strlen($buff) > 0){
	        $reqPar = substr($buff, 0, strlen($buff)-1);
	    }
	    return $reqPar;
	}
	
	/**
	 * 生成签名 
	 * @param array $Obj
	 * @return string
	 */
	public static function getSign($Obj)
	{
	    $Parameters = array();
	    foreach ($Obj as $k => $v){
	        $Parameters[$k] = $v;
	    }
	    //签名步骤一：按字典序排序参数
	    ksort($Parameters);
	    $String = self::formatBizQueryParaMap($Parameters, false);
	    //签名步骤二：在string后加入KEY
	    $String = $String."&key=".\Yii::$app->params['wechat']['key'];
	    //签名步骤三：MD5加密
	    $String = md5($String);
	    //签名步骤四：所有字符转为大写
	    $result = strtoupper($String);
	    return $result;
	}
	
	/**
	 * 获取 prepayid
	 * @param array $params
	 * @return array
	 */
	public static function getPrepayId($params)
	{
	    if(!isset($params['out_trade_no']) || !$params['out_trade_no']){
	        return array(
	            'status' => 0,
	            'message'=>'参数out_trade_no没有填写'
	        );
	    }
	    if(!isset($params['body']) || !$params['body']){
	        return array(
	            'status' => 0,
	            'message'=>'参数body没有填写'
	        );
	    }
	    if(!isset($params['total_fee']) || !$params['total_fee']){
	        return array(
	            'status' => 0,
	            'message'=>'参数total_fee没有填写'
	        );
	    }
	    if(!isset($params['openid']) || !$params['openid']){
	        return array(
	            'status' => 0,
	            'message'=>'参数openid没有填写'
	        );
	    }
        if(!isset($params['notify_url']) || !$params['notify_url']) {
            $params['notify_url'] = \Yii::$app->params['wechat']['notify_url'];
        }
	    $params['trade_type'] = 'JSAPI';
	    $params['appid']      = \Yii::$app->params['wechat']['appid'];
	    $params['mch_id']     = \Yii::$app->params['wechat']['mch_id'];
	    $params['nonce_str']  = WeChatRand::get(32);
	    $params['spbill_create_ip'] = $_SERVER['REMOTE_ADDR'];
	    $params['sign']       = self::getSign($params);
	    
	    
	    $xml = self::arrayToXml($params);
	    
	    $url = "https://api.mch.weixin.qq.com/pay/unifiedorder";
	    $response = Helper::curlPost($url, $xml);
	    $result = self::xmlToArray($response);
        
	    if(isset($result['prepay_id'])){
	        return array(
	            'status' => 1,
	            'message' => '成功',
	            'prepayId'=> $result['prepay_id']
	        );
	    }
	    return array(
            'status' => 0,
            'message'=> $response
        );
	    
	}
	
	/**
	 * 数组转 xml
	 * @param array $arr
	 * @return string
	 */
	public static function arrayToXml($arr)
	{
	    $xml = "<xml>";
	    foreach ($arr as $key=>$val)
	    {
	        if (is_numeric($val))
	        {
	            $xml.="<".$key.">".$val."</".$key.">";
	
	        }
	        else
	            $xml.="<".$key."><![CDATA[".$val."]]></".$key.">";
	    }
	    $xml.="</xml>";
	    return $xml;
	}
	
	/**
	 * xml 转数组 
	 * @param xml $xml
	 * @return array
	 */
	public static function xmlToArray($xml)
	{
	    $array_data = json_decode(json_encode(simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA)), true);
	    return $array_data;
	}
	
	/**
	 * 发红包接口
	 * 
	 * @see http://pay.weixin.qq.com/wiki/doc/api/cash_coupon.php?chapter=13_5
	 * 
	 * @param string $openid 微信 openid
	 * @param string $money 红包的钱
	 * @param string $nick_name  提供方名称
	 * @param string $send_name  商户名称
	 * @param string $wishing  红包祝福语
	 * @param string $act_name 活动名称
	 * @param string $remark 备注
	 * @return array
	 */
	public static function sendRed($openid,$money,$nick_name,$send_name,$wishing,$act_name,$remark)
	{
	    $url = "https://api.mch.weixin.qq.com/mmpaymkttransfers/sendredpack";
	    
	    //自已的订单号
	    $mch_billno = self::microtime().rand(10, 99).rand(10, 99).rand(10, 99).rand(100, 999);
	    $nonce_str  = WeChatRand::get(32);
	    $appid      = \Yii::$app->params['wechat']['appid'];
	    $mch_id     = \Yii::$app->params['wechat']['mch_id'];
	    
	    $params = [
	        'nonce_str' => $nonce_str,
	        'mch_billno'=> $mch_billno,
	        'mch_id'    => $mch_id,
	        'wxappid'   => $appid,
	        'nick_name' => $nick_name,
	        'send_name' => $send_name,
	        're_openid' => $openid,
	        'total_amount' => $money,
	        'min_value'    => $money,
	        'max_value'    => $money,
	        'total_num'    => 1,
	        'wishing'      => $wishing,
	        'client_ip'    => '223.223.180.89',
	        'act_name'     => $act_name,
	        'remark'       => $remark,
// 	        'logo_imgurl'  => '',
// 	        'share_content'=> '',
// 	        'share_url'    => '',
// 	        'share_imgurl' => '',
	    ];
	    
	    $sign = self::getSign($params);
	    $params['sign'] = $sign;
	    
	    $xml    = self::arrayToXml($params);
	    $result = Helper::curlPostSsl($url, $xml);
	    
	    Log::save($params,'wechatpay_before.log');
	    Log::save($result,'wechatpay_after.log');

	    $result = (array)simplexml_load_string($result, 'SimpleXMLElement', LIBXML_NOCDATA);
	    
	    return $result;
	}
	
	/**
	 * 产生毫秒
	 * @return string
	 */
	public static function microtime()
	{
        list($usec, $sec) = explode(" ", microtime());
        return $sec . str_replace('0.','',$usec);
	}
	
	/**
	 * 检验签名
	 * @param array $data
	 * @return boolean
	 */
	public static function checkSign($data)
	{
	    $tmpData = $data;
	    unset($tmpData['sign']);
	    $sign = self::getSign($tmpData);//本地签名
	    if (isset($data['sign']) && $data['sign'] == $sign) {
	        return true;
	    }
	    return true;
	}
	
	/**
	 * 扫码支付
	 * @param array $params
	 * @return array
	 * @author xi
	 */
	public static function scanCode($params)
	{
	    if(!isset($params['out_trade_no']) || !$params['out_trade_no']){
	        return array(
	            'status' => 0,
	            'message'=>'参数out_trade_no没有填写'
	        );
	    }
	    if(!isset($params['body']) || !$params['body']){
	        return array(
	            'status' => 0,
	            'message'=>'参数body没有填写'
	        );
	    }
	    if(!isset($params['total_fee']) || !$params['total_fee']){
	        return array(
	            'status' => 0,
	            'message'=>'参数total_fee没有填写'
	        );
	    }
	    if(!isset($params['notify_url']) || !$params['notify_url']){
	        return array(
	            'status' => 0,
	            'message'=>'参数notify_url没有填写'
	        );
	    }
	    
	    $params['body'] = Helper::subString($params['body'], 30);
	    
	    $params['trade_type'] = 'NATIVE';
	    $params['appid']      = \Yii::$app->params['wechat']['appid'];
	    $params['mch_id']     = \Yii::$app->params['wechat']['mch_id'];
	    $params['nonce_str']  = WeChatRand::get(32);
	    $params['spbill_create_ip'] = $_SERVER['REMOTE_ADDR'];
	    $params['sign']       = self::getSign($params);
	     
	     
	    $xml = self::arrayToXml($params);
	     
	    $url = "https://api.mch.weixin.qq.com/pay/unifiedorder";
	    $response = Helper::curlPost($url, $xml);
	    $result = self::xmlToArray($response);

	    if(isset($result['return_code']) && isset($result['result_code']) && strtoupper($result['result_code']) == 'SUCCESS' && strtoupper($result['return_code']) == 'SUCCESS'){
	        return array(
	            'status'  => 1,
	            'message' => '成功',
	            'result'  => $result
	        );
	    }
	    return array(
	        'status' => 0,
	        'message'=> $response
	    );
	     
	}
	
	/**
	 * 查询订单支付状态
	 * @see https://pay.weixin.qq.com/wiki/doc/api/native.php?chapter=9_2
	 * @param string $order_num
	 * @return array
	 */
	public static function orderQuery($order_num)
	{
	    if($order_num)
	    {
	        $params['appid']      = \Yii::$app->params['wechat']['appid'];
	        $params['mch_id']     = \Yii::$app->params['wechat']['mch_id'];
	        $params['out_trade_no'] = $order_num;
	        $params['nonce_str']  = WeChatRand::get(32);
	        $params['sign']       = self::getSign($params);
	        
	        
	        $xml = self::arrayToXml($params);
	        
	        $url = "https://api.mch.weixin.qq.com/pay/orderquery";
	        $response = Helper::curlPost($url, $xml);
	        $result = self::xmlToArray($response);
	        
	        return $result;
	    }
	    return [];
	}
	
	/**
	 * 下载对帐单
	 * @param string $bill_date
	 * @return Ambigous <multitype:, mixed>|multitype:
	 */
	public static function downloadBill($bill_date)
	{
		if($bill_date )
		{
			$params['appid']      = \Yii::$app->params['wechat']['appid'];
			$params['mch_id']     = \Yii::$app->params['wechat']['mch_id'];
			
			$params['bill_date']  = $bill_date;
			$params['nonce_str']  = WeChatRand::get(32);
			
			$params['bill_type']  = 'SUCCESS';
			$params['sign']       = self::getSign($params);
	 		 
			 
			$xml = self::arrayToXml($params);
			 
			$url = "https://api.mch.weixin.qq.com/pay/downloadbill";
			$response = Helper::curlPost($url, $xml);
			
			if(strpos($response,'<xml>')===false)
			{
				$result = [];
				$arr = explode("\n",$response);
				foreach($arr as $val)
				{
					if(trim($val)!='')
					{
						$tmp = explode(',',$val);
						if(is_array($tmp)){
							foreach($tmp as $k=>$v){
								$tmp[$k] = str_replace('`','',$v);
							}
						}
						$result[] = $tmp;
					}
				}
				
				return [
					'arr' => $result,
					'str' => $response
				];
			}
			else {
				$result = self::xmlToArray($response);
				return $result;
			}
			
		}
		return [
			'return_code' => 'FAIL',
			'return_msg'  => 'No bill_date Exist',
			'error_code'  => '0'
		];
	}
	
	
}