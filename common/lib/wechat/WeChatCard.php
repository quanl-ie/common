<?php
namespace common\lib\wechat;

use common\helpers\Helper;
use common\models\wechat\WeChatJsSDK;


class WeChatCard
{
    
    /**
     * 上传卡券LOGO
     * @param array $files logo文件数据�?
     * @return string|array 添加成功返回logo链接，否则返回数组信�?
     * @author li
     * @since 2015-06-01
     */
    public static function addLogo($files)
    {   
        $json = exec('curl -F "buffer=@'.$files.'" "https://api.weixin.qq.com/cgi-bin/media/uploadimg?access_token='.WeChatAccessToken::getAccessToken().'"');
        $arr = json_decode($json,true);
        
        return $arr;
    }
    
    /**
     * 创建卡券
     * 
     * @see http://mp.weixin.qq.com/wiki/8/b7e310e7943f7763450eced91fa793b0.html
     * 
     * @param string $card_type 卡券类型
     * @param array  $base_info 卡券基本信息
     * @param string $deal_detail $gift $default_detail 卡券描述（团购|礼品|优惠�?
     * @param int    $least_cost $reduce_cost $discount 卡券描述（代金|折扣�?
     * @return array; 返回数组
     * @author li
     * @since  2015-06-01
     * */
    public static function createCard($card_type,$base_info,$deal_detail='',$least_cost=0,$reduce_cost=0,$discount=0,$gift='',$default_detail='') {

        $url = "https://api.weixin.qq.com/card/create?access_token=".WeChatAccessToken::getAccessToken();
        
        
        foreach ($base_info as $key=>$val){
            if(!is_array($val)){
                $base_info[$key] = urlencode($val);
            }
        }
        
        //团购�?
        if($card_type == 'GROUPON') {
            $data['card'] = array(
                "card_type" => "GROUPON",
                'groupon' => array(
                        'base_info' => $base_info,
                        'deal_detail' => $deal_detail
                    )
            );
            
        }
        //代金�?
        else if($card_type == 'CASH') {
            $data['card'] = array(
                'card_type' => 'CASH',
                'cash' => array(
                        'base_info' => $base_info,
                		'least_cost' => $least_cost,
                		'reduce_cost' => $reduce_cost
                		//'deal_detail' => $deal_detail
                    )
            		
            );
        }
        //折扣�?
        elseif ($card_type=='DISCOUNT') {
            $data['card'] = array(
                'card_type' => 'DISCOUNT',
                'discount'  => array(
                    'base_info' => $base_info,
                    
                ),
                'discount' => $discount
            );
        }
        //礼品�?
        elseif ($card_type == 'GIFT') {
            $data['card'] = array(
                'card_type' => 'GIFT',
                'gift'  => array(
                    'base_info' => $base_info,
                    'gift' => $gift
                )
            );
        }
        //优惠�?
        elseif ($card_type == 'GENERAL_COUPON') {
            $data['card'] = array(
                'card_type' => 'GENERAL_COUPON',
                'gift'  => array(
                    'base_info' => $base_info,
                    'default_detail' => $default_detail
                )
            );
        }
        
        $data = json_encode($data);
        $data = urldecode($data);
        
        $data = '{
    "card": {
        "card_type": "CASH", 
        "cash": {
            "base_info": {
                "logo_url": "http://mmbiz.qpic.cn/mmbiz/iaL1LJM1mF9aRKPZJkmG8xXhiaHqkKSVMMWeN3hLut7X7hicFNjakmxibMLGWpXrEXB33367o7zHN0CwngnQY7zb7g/0", 
                "brand_name": "微信餐厅", 
                "code_type": "CODE_TYPE_TEXT", 
                "title": "微信接口测试", 
                "sub_title": "周末狂欢必备", 
                "color": "Color010", 
                "notice": "使用时向服务员出示此券", 
                "service_phone": "020-88888888", 
                "description": "不可与其他优惠同享
如需团购券发票，请在消费时向商户提出
店内均可使用，仅限堂食", 
                "date_info": {
                    "type": "DATE_TYPE_FIX_TIME_RANGE", 
                    "begin_timestamp": 1397577600, 
                    "end_timestamp": 1472724261
                }, 
                "sku": {
                    "quantity": 10
                }, 
                "get_limit": 3, 
                "use_custom_code": false, 
                "bind_openid": false, 
                "can_share": true, 
                "can_give_friend": true, 
                "location_id_list": [
                    123, 
                    12321, 
                    345345
                ], 
                "center_title": "顶部居中按钮", 
                "center_sub_title": "按钮下方的wording", 
                "center_url": "http://weixin.10020.net/test/printcode/", 
                "custom_url_name": "立即使用", 
                "custom_url": "http://weixin.10020.net/test/printcode/", 
                "custom_url_sub_title": "6个汉字tips", 
                "promotion_url_name": "更多优惠", 
                "promotion_url": "http://weixin.10020.net", 
                "source": "大众点评"
            }, 
            "advanced_info": {
                "use_condition": {
                    "accept_category": "鞋类", 
                    "reject_category": "阿迪达斯", 
                    "can_use_with_other_discount": true
                }, 
                "abstract": {
                    "abstract": "微信餐厅推出多种新季菜品，期待您的光临", 
                    "icon_url_list": [
                        "http://mmbiz.qpic.cn/mmbiz/p98FjXy8LacgHxp3sJ3vn97bGLz0ib0Sfz1bjiaoOYA027iasqSG0sj  piby4vce3AtaPu6cIhBHkt6IjlkY9YnDsfw/0"
                    ]
                }, 
                "text_image_list": [
                    {
                        "image_url": "http://mmbiz.qpic.cn/mmbiz/p98FjXy8LacgHxp3sJ3vn97bGLz0ib0Sfz1bjiaoOYA027iasqSG0sjpiby4vce3AtaPu6cIhBHkt6IjlkY9YnDsfw/0", 
                        "text": "此菜品精选食材，以独特的烹饪方法，最大程度地刺激食 客的味蕾"
                    }, 
                    {
                        "image_url": "http://mmbiz.qpic.cn/mmbiz/p98FjXy8LacgHxp3sJ3vn97bGLz0ib0Sfz1bjiaoOYA027iasqSG0sj piby4vce3AtaPu6cIhBHkt6IjlkY9YnDsfw/0", 
                        "text": "此菜品迎合大众口味，老少皆宜，营养均衡"
                    }
                ], 
                "time_limit": [
                    {
                        "type": "MONDAY", 
                        "begin_hour": 0, 
                        "end_hour": 10, 
                        "begin_minute": 10, 
                        "end_minute": 59
                    }, 
                    {
                        "type": "HOLIDAY"
                    }
                ], 
                "business_service": [
                    "BIZ_SERVICE_FREE_WIFI", 
                    "BIZ_SERVICE_WITH_PET", 
                    "BIZ_SERVICE_FREE_PARK", 
                    "BIZ_SERVICE_DELIVER"
                ]
            }, 
        	"least_cost": 10000,
            "reduce_cost": 10
        }
    }
}';
        
//         print_r(WeChatAccessToken::getAccessToken());
//         echo '<br/>';
//         print_r($data);
//         die;
        
        $arr = json_decode(Helper::curlPost($url, $data),true);
        
        return $arr;
    }
    
    /**
     * 创建二维�?
     * @return array
     * @author li
     * @since  2015-06-01
     * */
    public static function qrcodeCreate($card_id) {
        $url = "https://api.weixin.qq.com/card/qrcode/create?access_token=".WeChatAccessToken::getAccessToken();
        $data["action_name"] = "QR_CARD";
        $data["action_info"] = array(
            'card' => array(
                'card_id' => $card_id,
                'is_unique_code' => false,
                'outer_id' => 1
            ),
        );
        
        $data = json_encode($data);
        
        return json_decode(Helper::curlPost($url, $data),true);
    }
    
    /**
     * 获取api_ticket
     * @return array
     * @author li
     * @since  2015-06-01
     * */
    public static function getTicket() {
        $type = "wx_card";
        
        $ticket = WeChatJsSDK::getJsApiTicket($type);
        
        return

        $ticket;
    }
  
    
    
    /**
     * 查看卡券详情
     * @author li
     * @since  2015-06-01
     * */
    public static function inquireCard($card_id) {
        $url = "https://api.weixin.qq.com/card/get?access_token=".WeChatAccessToken::getAccessToken();
        
        $data['card_id'] = $card_id;
        $data = json_encode($data);
        
        //return $data;
        
        $jsonStr = Helper::curlPost($url, $data);
        return json_decode($jsonStr,true);
    }
    
    
    /*卡券核销部分*/

    /**
     * 消耗code
     * @param  $code string
     * @param  array
     * @author li
     * @since  2015-06-01
     * */
    public static function consumeCard($code,$card_id) {
        $url = "https://api.weixin.qq.com/card/code/consume?access_token=".WeChatAccessToken::getAccessToken();
        $data['code'] = $code;
        $data['card_id'] = $card_id;
        
        $data = json_encode($data,true);
        
        return Helper::curlPost($url, $data);
    }
    
    
    /**
     * Code解码接口
     * @param  string $encrypt_code
     * @return array 返回数组
     * @author li
     * @since  2015-06-01
     * */
    public static function decryptCode($encrypt_code) {
        $url = "https://api.weixin.qq.com/card/code/decrypt?access_token=".WeChatAccessToken::getAccessToken();
        $data = json_encode(array('encrypt_code' => $encrypt_code));
        
        return json_decode(Helper::curlPost($url, $data),true);
    }
    
    
    /*卡券管理*/
    
    /**
     * 删除卡券(删除一类卡�?
     * @author li
     * @since  2015-06-01
     * */
    public static function deleteCard($card_id) {
        $url = "https://api.weixin.qq.com/card/delete?access_token=".WeChatAccessToken::getAccessToken();
        
        $data['card_id'] = $card_id;
        
        return json_decode(Helper::curlPost($url, $data),true);
    }
    
    /**
     * 查询code(�?code 对应的用户openid、卡券有效期等信息�?
     * @return array
     * @author li
     * @since  2015-06-01
     * https://api.weixin.qq.com/card/code/get?access_token=TOKEN
     * */
    public static function getCode($code) {
        $url = "https://api.weixin.qq.com/card/code/get?access_token=".WeChatAccessToken::getAccessToken();
        $data['code'] = $code;
	$data = json_encode($data);
        return json_decode(Helper::curlPost($url, $data),true);
    } 
    
    /**
     * 批量查询卡列�?
     * @param $offset int 查询卡列表的起始偏移�?
     * @param $count  int 查询卡片数量
     * @author li
     * @since  2015-06-01
     * */
    public static function batchget($offset,$count) {
        $url ="https://api.weixin.qq.com/card/batchget?access_token=".WeChatAccessToken::getAccessToken();
        
        $data['offset'] = $offset;
        $data['count'] = $count;
        $data = json_encode($data);
        
        return json_decode(Helper::curlPost($url, $data),true);
    }
    
    
    /**
     * 更改code
     * @param  $code    int   code�?
     * @param  $card_id string   cardID
     * @param  new_card 新code�?
     * @return array
     * @author li
     * @since  2015-06-02
     * */
    public static function updateCode($code,$card_id,$new_code) {
        $url = "https://api.weixin.qq.com/card/code/update?access_token=".WeChatAccessToken::getAccessToken();
        
        $data['code'] = $code;
        $data['card_id'] = $card_id;
        $data['new_code'] = $new_code;
        
        $data = json_encode($data);
        
        return json_decode(Helper::curlPost($url, $data),true);
    }
    
    /**
     * 设置卡券失效接口
     * @param  $code int code�?
     * @param  $card_id string cardID(自定义code必填)
     * @author li
     * @since  2015-06-02
     * */
    public static function unavailable($code,$card_id=0) {
        $url = "https://api.weixin.qq.com/card/code/unavailable?access_token".WeChatAccessToken::getAccessToken();
        $data['code'] = $code;
        if($card_id!=0) {
            $data['card_id'] = $card_id;
        }
        
        $data = json_encode($data);
        
        $jsonStr = Helper::curlPost($url, $data);
        
        return json_decode($jsonStr,true);
    }
    
    /**
     * 更改卡券信息接口
     * @param $card_id  string 卡券ID
     * @param $base_info  array  要修改的信息
     * @param $bonus_cleared  string 积分清零规则
     * @param $bonus_rules  string 积分规则
     * @return string|array 成功返回string|失败返回array
     * @author li
     * @since  2015-06-02
     * */
    public static  function updateCard($card_id,$base_info,$bonus_cleared,$bonus_rules,$prerogative='') {
        $url ="https://api.weixin.qq.com/card/update?access_token".WeChatAccessToken::getAccessToken();
        
        $data['card_id'] = $card_id;
        $data['member_card'] = array(
            'base_info' => $base_info,
            'bonus_cleared' => $bonus_cleared,
            'bonus_rules'   => $bonus_rules
        );
        if(!empty($prerogative)) {
            $data['member_card']['prerogative'] = $prerogative;
        }
        
        $data = json_encode($data);
        $data = urldecode($data);
        
        $jsonStr = Helper::curlPost($url, $data);
        
        $arr = json_decode($jsonStr,true);
        if($arr['errcode']==0) {
            return $arr['errmsg'];
        }else{
            return $arr;
        }
    }
    
    /**
     * 库存修改接口
     * @param $card_id  卡券ID
     * @param $increase_stock_value  int  增加数量
     * @param $reduce_stock_value  int 减少数量
     * @return string|array 成功返回string | 失败返回array
     * @author li
     * @since  2015-06-02
     * */
    public static function modifyStock($card_id,$increase_stock_value,$reduce_stock_value) {
        $url ="https://api.weixin.qq.com/card/modifystock?access_token=".WeChatAccessToken::getAccessToken();
        
        $data['card_id'] = $card_id;
        $data['increase_stock_value'] = $increase_stock_value;
        $data['reduce_stock_value'] = $reduce_stock_value;
        
        $data = json_encode($data);
        
        $jsonStr = Helper::curlPost($url, $data);
        
        $arr = json_decode($jsonStr,true);
        if($arr['errcode'] == 0) {
            return $arr['errmsg'];
        }else{
            return $arr;
        }
    }
    
    /**
     * 设置卡券测试白名�?
     * @param array $openids
     * @param array $usernames
     * @author xi
     * @return array|string
     * @date 2015-6-3
     */
    public static function testWhiteList($openids,$usernames = array())
    {
        $url = "https://api.weixin.qq.com/card/testwhitelist/set?access_token=".WeChatAccessToken::getAccessToken();
        
        $data = [
            'openid' => $openids,
            'username' => $usernames
        ];
        
        $jsonStr = Helper::curlPost($url, json_encode($data));
        $arr = json_decode($jsonStr,true);
        if(isset($arr['errcode']) && $arr['errcode']==0){
            return $arr;
        }
        return $jsonStr;
    }
    
    /**
     * 
     * @param array $open_ids  填写图文消息的接收者，一串OpenID列表，OpenID最�?个，最�?0000�?
     * @param string $card_id 卡券id
     * @return string|array
     * @author xi
     * @date 2015-6-3
     */
    public static function sendByOpenIds(array $open_ids=array(),$card_id)
    {
        $url = "https://api.weixin.qq.com/cgi-bin/message/mass/send?access_token=".WeChatAccessToken::getAccessToken();
        
        $data = [
            "touser" => $open_ids,
            "wxcard" => [
                'card_id' => $card_id
            ],
            'msgtype' => 'wxcard'
        ];
        
        $jsonStr = Helper::curlPost($url, json_encode($data));
        $arr = json_decode($jsonStr,true);
        if(isset($arr['errcode']) && $arr['errcode']==0){
            return $arr;
        }
        return $jsonStr;
    }
}