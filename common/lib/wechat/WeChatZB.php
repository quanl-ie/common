<?php
namespace common\lib\wechat;

use common\models\Log;
use common\helpers\Helper;

class WeChatZB
{
    /**
     * 上传素材
     * @param string $filename
     * @return mixed
     * @author xi
     * @date 2015-4-20
     */
    public static function uploadMaterial($filename)
    {
        $jsonStr = exec('curl -F "media=@'.$filename.'" "https://api.weixin.qq.com/shakearound/material/add?access_token='.WeChatAccessToken::getAccessToken().'"');
        
        $arr = json_decode($jsonStr,true);
        if($arr['errcode']==0){
            return $arr['data']['pic_url'];
        }
        else{
            Log::mail('微信周边 上传素材报错',$jsonStr);
            return $jsonStr;
        }
    }
    
    /**
     * 周边 新增页面
     * @param string $title 在摇一摇页面展示的主标题，不超过6个字
     * @param string $description 在摇一摇页面展示的副标题，不超过7个字
     * @param string $page_url 点击摇到打开哪个页面
     * @param string $comment 页面的备注信息，不超过15个字
     * @param string $icon_url 在摇一摇页面展示的图片。图片需用 uploadMaterial 方法来处理
     * @return string|array
     * @author xi
     * @date 2015-4-20
     */
    public static function addPage($title,$description,$page_url,$comment,$icon_url)
    {
        $url = "https://api.weixin.qq.com/shakearound/page/add?access_token=".WeChatAccessToken::getAccessToken();
        
        $data = [
            'title'       => $title,
            'description' => $description,
            'page_url'    => $page_url,
            'comment'     => $comment,
            'icon_url'    => $icon_url
        ];
        
        $jsonStr = Helper::curlPost($url, json_encode($data));
        $arr = json_decode($jsonStr,true);
        if($arr['errcode']==0){
            return $arr;
        }
        else {
            Log::mail('微信摇周边接口 增加页面报错', $jsonStr);
            return $jsonStr;
        }
    }
    
    /**
     * 修改页面信息
     * @param int $page_id 摇周边页面唯一ID
     * @param string $title 在摇一摇页面展示的主标题，不超过6个字
     * @param string $description 在摇一摇页面展示的副标题，不超过7个字
     * @param string $page_url 跳转链接
     * @param string $comment 页面的备注信息，不超过15个字
     * @param string $icon_url 在摇一摇页面展示的图片。图片需先上传至微信侧服务器，用“素材管理-上传图片素材”接口上传图片，返回的图片URL再配置在此处
     * @return array|string
     * @author xi
     * @date 2015-4-21
     */
    public static function modifyPage($page_id,$title,$description,$page_url,$comment,$icon_url)
    {
        $url = "https://api.weixin.qq.com/shakearound/page/update?access_token=".WeChatAccessToken::getAccessToken();
        
        $data = [
            "page_id"     => $page_id,
            "title"       => $title,
            "description" => $description,
            "page_url"    => $page_url,
            "comment"     => $comment,
            "icon_url"    => $icon_url
        ];
        
        $jsonStr = Helper::curlPost($url, json_encode($data));
        $arr = json_decode($jsonStr,true);
        if($arr['errcode']==0){
            return $arr;
        }
        else {
            Log::mail('微信周边接口 修改页面报错', $jsonStr);
            return $jsonStr;
        }
    }
    
    /**
     * 删除已有的页面，包括在摇一摇页面出现的主标题、副标题、图片和点击进去的超链接。只有页面与设备没有关联关系时，才可被删除
     * @param array $page_ids
     * @return string|array
     * @author xi
     * @date 2015-4-21
     */
    public static function deletePage(array $page_ids)
    {
        $url = "https://api.weixin.qq.com/shakearound/page/delete?access_token=".WeChatAccessToken::getAccessToken();
        
        $data = [
            'page_ids' => $page_ids
        ];
        
        $jsonStr = Helper::curlPost($url, json_encode($data));
        $arr = json_decode($jsonStr,true);
        if($arr['errcode']==0){
            return $arr;
        }
        else {
            Log::mail('微信周边接口 页面删除报错', $jsonStr);
            return $jsonStr;
        }
    }
    
    /**
     * 页面列表
     * @param number $page
     * @param number $pageSize
     * @return mixed
     * @author xi
     * @date 2015-4-21
     */
    public static function listPages($page=1,$pageSize = 10)
    {
        $url = "https://api.weixin.qq.com/shakearound/page/search?access_token=".WeChatAccessToken::getAccessToken();
        
        $data = [
            'page_ids' => [],
            'begin' => ($page-1)*$pageSize,
            'count' => $pageSize
        ];
        
        $jsonStr = Helper::curlPost($url, json_encode($data));
        $arr = json_decode($jsonStr,true);
        if($arr['errcode']==0){
            return $arr;
        }
        else {
            Log::mail('微信周边接口 页面管理列表报错', $jsonStr);
            return $jsonStr;
        }
    }
    
    /**
     * 页面与设备建 立关系
     * @param int $device_id
     * @param string $uuid
     * @param int $major
     * @param int $minor
     * @param array $page_ids
     * @param number $bind
     * @param number $append
     * @return string|array
     * @author xi
     * @date 2015-4-21
     */
    public static function bindPage($device_id, $uuid, $major, $minor, array $page_ids, $bind=1,$append=1)
    {
        $url = "https://api.weixin.qq.com/shakearound/device/bindpage?access_token=".WeChatAccessToken::getAccessToken();
        
        $data = [
            "device_identifier" => [
                "device_id" => $device_id,
                "uuid"      => $uuid,
                "major"     => $major,
                "minor"     => $minor
            ],
            "page_ids" => $page_ids,
            "bind"     => $bind,
            "append"   => $append,
        ];
        
        $jsonStr = Helper::curlPost($url, json_encode($data));
        $arr = json_decode($jsonStr,true);
        
        if($arr['errcode'] == 40001)
        {
            WeChatAccessToken::autoUpdateAccessToken();
            $url = "https://api.weixin.qq.com/shakearound/device/bindpage?access_token=".WeChatAccessToken::getAccessToken();
            $jsonStr = Helper::curlPost($url, json_encode($data));
            $arr = json_decode($jsonStr,true);
        }
        
        
        if($arr['errcode']==0){
            return $arr;
        }
        else {
            Log::mail('微信周边接口 绑定页面报错', $jsonStr);
            return $jsonStr;
        }
    }
    
    /**
     * 申请设备
     * @param int $quantity 申请的设备ID的数量，单次新增设备超过500个，需走人工审核流程
     * @param string $apply_reason 申请理由，不超过100个字
     * @param string $comment 备注，不超过15个汉字或30个英文字母
     * @param int $poi_id  门店id
     * @return string|array
     * @author xi
     * @date 2015-4-21
     */
    public static function applyDevice($quantity,$apply_reason,$comment = false,$poi_id = false)
    {
        $url = "https://api.weixin.qq.com/shakearound/device/applyid?access_token=".WeChatAccessToken::getAccessToken();
        
        $data = [
            "quantity"     => $quantity,
            "apply_reason" => $apply_reason,
            "comment"      => $comment,
            "poi_id"       => $poi_id
        ];
        if($comment==false){
            unset($data['comment']);
        }
        if($poi_id == false){
            unset($data['poi_id']);
        }
        
        $jsonStr = Helper::curlPost($url, json_encode($data));
        $arr = json_decode($jsonStr,true);
        if($arr['errcode']==0){
            return $arr;
        }
        else {
            Log::mail('微信周边接口 申请设备报错', $jsonStr);
            return $jsonStr;
        }
    }
    
    /**
     * 修改设备信息
     * @param int $device_id 设备编号，若填了UUID、major、minor，则可不填设备编号，若二者都填，则以设备编号为优先
     * @param string $uuid  UUID、major、minor，三个信息需填写完整，若填了设备编号，则可不填此信息。
     * @param int $major
     * @param int $minor
     * @param string $comment 设备的备注信息，不超过15个汉字或30个英文字母。
     * @return boolean|mixed
     * @author xi
     * @date 2015-4-27
     */
    public static function modifyDevice($device_id,$uuid,$major,$minor,$comment)
    {
        $url = "https://api.weixin.qq.com/shakearound/device/update?access_token=".WeChatAccessToken::getAccessToken();
        $data = [
            "device_identifier" => [
                "device_id" => $device_id,
                "uuid"      => $uuid,
                "major"     => $major,
                "minor"     => $minor
            ],
            "comment" => $comment
        ];
        
        $jsonStr = Helper::curlPost($url, json_encode($data));
        $arr = json_decode($jsonStr,true);
        if($arr['errcode']==0){
            return true;
        }
        else {
            Log::mail('微信周边接口 删除页面报错', $jsonStr);
            return $jsonStr;
        }
    }
    
    /**
     * 设备分页
     * @param number $page
     * @param number $pageSize
     * @return mixed
     * @author xi
     * @date 2015-4-27
     */
    public static function devicePages($page = 1,$pageSize=10)
    {
        $url = "https://api.weixin.qq.com/shakearound/device/search?access_token=".WeChatAccessToken::getAccessToken();
        $data = [
            "begin" => ($page-1)*$pageSize,
            "count" => $pageSize
        ];
        
        $jsonStr = Helper::curlPost($url, json_encode($data));
        $arr = json_decode($jsonStr,true);
        if($arr['errcode']==0){
            return $arr;
        }
        else {
            Log::mail('微信周边接口 页 设备列表报错', $jsonStr);
            return $jsonStr;
        }
    }
    
    /**
     * 查询单个设备信息
     * @param unknown $device_id 设备id
     * @param string $uuid 
     * @param number $major
     * @param number $minor
     * @return array|string
     * @author xi
     * @date 2015-5-5
     */
    public static function findOneDevice($device_id,$uuid='',$major=0,$minor=0)
    {
        $url = "https://api.weixin.qq.com/shakearound/device/search?access_token=".WeChatAccessToken::getAccessToken();
        
        if($device_id){
            $device_identifiers['device_id'] = $device_id;
        }
        else if($uuid!='' && $major>0 && $minor>0){
            $device_identifiers['uuid']  = $uuid;
            $device_identifiers['major'] = $major;
            $device_identifiers['minor'] = $minor;
        }
        
        
        $data = [
            "device_identifiers" =>[
                $device_identifiers
            ]
        ];
        
        $jsonStr = Helper::curlPost($url, json_encode($data));
        $arr = json_decode($jsonStr,true);
        if($arr['errcode']==0){
            return $arr;
        }
        else {
            Log::mail('微信周边接口 查询单个设备报错', $jsonStr);
            return $jsonStr;
        }
    }
    
    /**
     * 周边取用户信息
     * @param string $ticket
     * @return array
     * @author xi
     * @date 2015-5-21
     */
    public static function getShakeInfo($ticket)
    {
        $url = "https://api.weixin.qq.com/shakearound/user/getshakeinfo?access_token=".WeChatAccessToken::getAccessToken();
        
        $data = [
            "ticket" => $ticket,
            "need_poi" => 1
        ];
        
        $jsonStr = Helper::curlPost($url, json_encode($data));
        $arr = json_decode($jsonStr,true);
        if($arr['errcode']!=0){
//             Log::mail('微信周边接口 页 取用户信息报错', $jsonStr);
            Log::save("微信周边接口 页 取用户信息报错".$jsonStr);
        }
        
        return $arr;
        
    }
    
    /**
     * 以设备为维度的数据统计接口
     * 
     * @see http://mp.weixin.qq.com/wiki/0/8a24bcacad40fe7ee98d1573cb8a6764.html
     * 
     * @param int $device_id
     * @param int $begin_date
     * @param int $end_date
     * @param string $uuid
     * @param int $major
     * @param int $minor
     * @return mixed|string
     * @author xi
     * @date 2015-6-1
     */
    public static function statisticsDevice($device_id,$begin_date,$end_date,$uuid,$major,$minor) 
    {
        $url = "https://api.weixin.qq.com/shakearound/statistics/device?access_token=".WeChatAccessToken::getAccessToken();
        if($device_id) {
            $device_identifier['device_id'] = $device_id;
            $device_identifier['uuid']  = $uuid;
            $device_identifier['major'] = $major;
            $device_identifier['minor'] = $minor;
        }
        
        $data = [
             "device_identifier"=>$device_identifier,
             "begin_date"=> $begin_date,		
             "end_date"  => $end_date
        ];

        
        $jsonStr = Helper::curlPost($url, json_encode($data));
        $jsonArr = json_decode($jsonStr,true);
        if($jsonArr['errcode']==0) {
            return $jsonArr;
        }
        return $jsonStr;
    }
    
    /**
     *  以页面为维度统计
     * @see http://mp.weixin.qq.com/wiki/0/8a24bcacad40fe7ee98d1573cb8a6764.html
     * @param int $page_id 指定页面的设备ID
     * @param int $begin_date 起始日期时间戳，最长时间跨度为30天
     * @param int $end_date 结束日期时间戳，最长时间跨度为30天
     * @return array
     * @author xi
     * @date 2015-6-1
     */
    public static function statisticsPage($page_id,$begin_date,$end_date) 
    {
        
        if(intval($page_id)<=0){
            return [
                'errcode'=> 99999,
                'errmsg' => '请输入正确的页面id'
            ]; 
        }
        
        if($begin_date>$end_date || $end_date>time()){
            return [
                'errcode'=> 99999,
                'errmsg' => '查询时间不正确'
            ];
        }
        
        $days = ($end_date - $begin_date)/84600;
        if($days>30){
            return [
                'errcode'=> 99999,
                'errmsg' => '时间跨度最大为 30 天'
            ];
        }
        
        $url = "https://api.weixin.qq.com/shakearound/statistics/page?access_token=".WeChatAccessToken::getAccessToken();
        
        $data = [
            "page_id"   => $page_id,
            "begin_date"=> $begin_date,
            "end_date"  => $end_date
        ];
        
        $jsonStr = Helper::curlPost($url, json_encode($data));
        $jsonArr = json_decode($jsonStr,true);
        if($jsonArr['errcode']==0) {
            return $jsonArr;
        }
        return $jsonStr;
    }
    
    /**
     * 批量查询设备统计数据接口
     * @param int $date
     * @param int $page
     * @return array/string
     * @author xi
     * @date 2015-8-11
     */
    public static function statisticsDevicelist($date,$page=1)
    {
        if(!is_int($date)){
            return [
                'errcode'=> 99999,
                'errmsg' => '日期必须是时间戳'
            ];
        }
        
        $url = "https://api.weixin.qq.com/shakearound/statistics/devicelist?access_token=".WeChatAccessToken::getAccessToken();
        $data = [
            "date"       => $date,
            "page_index" => intval($page)
        ];
        $jsonStr = Helper::curlPost($url, json_encode($data));
        $jsonArr = json_decode($jsonStr,true);
        if($jsonArr['errcode']==0) {
            return $jsonArr;
        }
        return $jsonStr;
        
    }
    
    /**
     * 根据页面id 查询关联关系
     * @param unknown $page_id
     * @param unknown $begin
     * @param number $count
     */
    public static function relationPage($page_id,$begin,$count=50)
    {
        if($count>50){
            return [
                'errcode'=> 99999,
                'errmsg' => '查询数量最大为50个'
            ];
        }
        
        $url = "https://api.weixin.qq.com/shakearound/relation/search?access_token=".WeChatAccessToken::getAccessToken();
        $data = [
            "type"    => 2,
            "page_id" => intval($page_id),
            "begin"   => intval($begin),
            "count"   => intval($count)
        ];
        
        $jsonStr = Helper::curlPost($url, json_encode($data));
        $jsonArr = json_decode($jsonStr,true);
        if($jsonArr['errcode']==0) {
            return $jsonArr;
        }
        return $jsonStr;
    }
}