<?php
/**
 * Created by PhpStorm.
 * User: Jaryee
 * Date: 2016/9/13
 * Time: 13:23
 */

namespace common\lib\wechat;

use Yii;
use common\models\AdminFinancialWxUser;
use common\models\AdminWxWithdrawalsRule;
use common\models\wechat\WeChatPay;
use common\models\ClubOrder;
use common\models\Precedent;
use common\models\AdminUser;
/**
 * This is the model class for table "admin_financial_wx_in".
 * 微信总帐户到个人账户打款记录
 * @property integer $id
 * @param int $account_id
 * @param int $type
 * @param int $admin_uid
 * @param double $pay_sum
 * @param double $cost
 * @param string $order_id
 * @param int $status
 */
class WeChatCompanyPay extends Precedent
{
    /**
     * API 参数
     * @var array
     * 'mch_appid'         # 公众号APPID
     * 'mchid'             # 商户号
     * 'device_info'       # 设备号
     * 'nonce_str'         # 随机字符串
     * 'partner_trade_no'  # 商户订单号
     * 'openid'            # 收款用户openid
     * 'check_name'        # 校验用户姓名选项 针对实名认证的用户
     * 're_user_name'      # 收款用户姓名
     * 'amount'            # 付款金额
     * 'desc'              # 企业付款描述信息
     * 'spbill_create_ip'  # Ip地址
     * 'sign'              # 签名
     */
    public $parameters = [];

    public static function tableName()
    {
        return 'admin_financial_wx_in';
    }


    /**
     * 定时任务，根据设置给帐户自动提款
     */
    public function payByAuto()
    {
        $lbRet = false;

        do{
            //获取设置信息
            $set_sum = 20000; //一次打款金额
            $set_retained = 5000; //保留金额
            $setModel = AdminWxWithdrawalsRule::getNowRuleData();
            if($setModel){
                if(isset($setModel['max_limit']))
                    $set_sum = $setModel['max_limit'];
                if(isset($setModel['retained_amount']))
                    $set_retained = $setModel['retained_amount'];
            }
            if($set_sum <= 0)
                break; //转出金额为0，就不做任何操作了

            if($set_retained > 0){
                //查询帐户剩余是否够转的

            }


        }while(false);

        return $lbRet;
    }

    /**
     * 给指定帐户打入指定的钱数
     */
    public function payByID($id,$pay_sum)
    {
        $ReturnMsg = "";

        do{
            //找到指定帐户
            $userModel = AdminFinancialWxUser::find()
                ->where(['id' => $id])
                ->one();
            if(!$userModel)
                break;

           //拼接转账数据
            $this->parameters['partner_trade_no'] = ClubOrder::generateOrderNumber();//商户订单号
            $this->parameters['openid'] = $userModel->openid;//收款用户OPENID
            $this->parameters['check_name'] = 'FORCE_CHECK';//强制校验真实姓名和身份证号
            $this->parameters['re_user_name'] = $userModel->name;//收款人真实姓名
            $this->parameters['amount'] = (int)($pay_sum * 100);//金额
            $this->parameters['desc'] = $userModel->name . '提现';//俱乐部提现
            $this->parameters['spbill_create_ip'] = $_SERVER['SERVER_ADDR'];//调用者IP地址

            $dataModel = $this->createXml();
            if(!$dataModel)
                break;

            //调用打款接口
            $dataResult = $this->curl_post_ssl($dataModel,"https://api.mch.weixin.qq.com/mmpaymkttransfers/promotion/transfers");
            if(!$dataResult)
                break;
            //把XML转换成数组对像
            $dataArray = $this->simplest_xml_to_array($dataResult);
            if(!$dataArray)
                break;

            //成功返回数据
            if(isset($dataArray['return_code'])){
                if($dataArray['return_code'] == "SUCCESS" && $dataArray['result_code'] == "SUCCESS"){
                    //成功
                    $this->order_id = $dataArray['partner_trade_no'];
                    $this->status = 1;

                    $ReturnMsg = "200";
                }else{
                    //失败
                    $ReturnMsg = $dataArray['return_msg'];
                    break;
                }
            }else{
                if(isset($dataArray['return_msg']))
                    $ReturnMsg = $dataArray['return_msg'];
                else
                    $ReturnMsg = "打款失败,原因不明,请找管理员!";
            }

        }while(false);

        return $ReturnMsg;
    }

    /**
     * 支付 接口暂时跳过
     * @param $id
     * @param $pay_sum
     * @return int
     */
    public function payByIDZfb($id,$pay_sum)
    {
        $ReturnMsg = 200;
        return $ReturnMsg;
    }

    /**
     * 生成请求xml数据
     * @return string
     */
    private function createXml()
    {
        $this->parameters['mch_appid'] = \Yii::$app->params['wechat']['appid'];
        $this->parameters['mchid']     = \Yii::$app->params['wechat']['mch_id'];
        $this->parameters['nonce_str'] = WeChatRand::get(32);
        $this->parameters['sign']      = WeChatPay::getSign($this->parameters);
        return $this->arrayToXml($this->parameters);
    }

    /**
     * 最简单的XML转数组
     * @param string $xmlstring XML字符串
     * @return array XML数组
     */
    private function simplest_xml_to_array($xmlstring) {
        //禁止引用外部xml实体
        libxml_disable_entity_loader(true);
        $values = json_decode(json_encode(simplexml_load_string($xmlstring, 'SimpleXMLElement', LIBXML_NOCDATA)), true);
        return $values;
    }

    /**
     * 数组转换XML
     * @return string
     */
    private function arrayToXml($arr,$dom=0,$item=0){
        if (!$dom){
            $dom = new \DOMDocument("1.0");
        }
        if(!$item){
            $item = $dom->createElement("root");
            $dom->appendChild($item);
        }
        foreach ($arr as $key=>$val){
            $itemx = $dom->createElement(is_string($key)?$key:"item");
            $item->appendChild($itemx);
            if (!is_array($val)){
                $text = $dom->createTextNode($val);
                $itemx->appendChild($text);

            }else {
                arrtoxml($val,$dom,$itemx);
            }
        }
        return $dom->saveXML();
    }

    function curl_post_ssl($vars,$url, $second=30,$aHeader=array())
    {
        $ch = curl_init();
        //超时时间
        curl_setopt($ch,CURLOPT_TIMEOUT,$second);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER, 1);
        //这里设置代理，如果有的话
        //curl_setopt($ch,CURLOPT_PROXY, '10.206.30.98');
        //curl_setopt($ch,CURLOPT_PROXYPORT, 8080);
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
        curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,false);

        //以下两种方式需选择一种

        //第一种方法，cert 与 key 分别属于两个.pem文件
        $dirname = dirname(__FILE__);
        //默认格式为PEM，可以注释
        curl_setopt($ch,CURLOPT_SSLCERTTYPE,'PEM');
        curl_setopt($ch,CURLOPT_SSLCERT,$dirname.'/cert/apiclient_cert.pem');
        //默认格式为PEM，可以注释
        curl_setopt($ch,CURLOPT_SSLKEYTYPE,'PEM');
        curl_setopt($ch,CURLOPT_SSLKEY,$dirname.'/cert/apiclient_key.pem');
        curl_setopt($ch,CURLOPT_CAINFO,$dirname.'/cert/rootca.pem');
        //print_r($dirname.'/cert/apiclient_key.pem');

        //第二种方式，两个文件合成一个.pem文件
       // curl_setopt($ch,CURLOPT_SSLCERT,getcwd().'/all.pem');

        if( count($aHeader) >= 1 ){
            curl_setopt($ch, CURLOPT_HTTPHEADER, $aHeader);
        }

        curl_setopt($ch,CURLOPT_POST, 1);
        curl_setopt($ch,CURLOPT_POSTFIELDS,$vars);
        $data = curl_exec($ch);
        if($data){
            curl_close($ch);
            return $data;
        }
        else {
            $error = curl_errno($ch);
            echo "call faild, errorCode:$error\n";
            curl_close($ch);
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['create_time'], 'safe'],
            [['order_id'], 'string', 'max' => 255],
            [['pay_sum','cost'], 'double'],
            [['account_id','status','admin_uid','type'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => '订单号',
            'pay_sum' => '打款数',
            'cost' => '手续费',
            'account_id' => '收款人ID',
        ];
    }

    /**
     * BeforeSave
     * @param bool $insert
     * @return bool
     * @author niu
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if($insert) {
                $this->create_time = date('Y-m-d H:i:s', time());
            } else {
                $this->create_time =  date('Y-m-d H:i:s', time());
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     * Get one by ID
     *
     * @param $id
     * @return one
     * @author niu
     */
    public static function getByID($id)
    {
        $query = self::find()->where(['id' => $id,'status' => 1])->asArray()->all();
        return $query;
    }


    /**
     * getManageList
     *
     * @return array
     * @author niu
     */
    public static function getManageList($where =array() ,$order='',$page=1,$pageSize=10)
    {
        $db = self::find();
        if($where){
            foreach ($where as $key=>$val)
            {
                if(is_array($val)){
                    $db->andWhere([$val[0],$key,$val[1]]);
                }
                else {
                    $db->andWhere([$key=>$val]);
                }
            }
        }
        //总数
        $totalNum = $db->count();

        //当有结果时进行组合数据
        if($totalNum>0)
        {
            //总页数
            $totalPage = ceil($totalNum/$pageSize);

            if($page<1)
            {
                $page = 1;
            }
            else if($page>$totalPage)
            {
                $page = $totalPage;
            }

            $db->offset(($page-1)*$pageSize);
            $db->limit($pageSize);
            if($order!=''){
                $db->orderBy($order);
            }
//            $query = $db->asArray()->all();
//            $list = [];
//            //$status = self::getStatus();
//            foreach ($query as $key=>$val)
//            {
//                //$val['statusDes'] = isset($status[$val['status']]) ? $status[$val['status']] : '';
//                $list[] = $val;
//            }
            $list = [];
            $query  = $db->asArray()->all();
            $nameList = [];
            foreach ($query as $key => $value) {
                $nameList[] = $value['admin_uid'];
            }

            $nameArr = self::getAdminName($nameList);
            //整合数据
            foreach ($query as $k => $v) {
                $v['admin_uid'] = $nameArr[$v['admin_uid']];
                $list[] = $v;
            }
            return array(
                'totalNum'  => $totalNum,
                'totalPage' => $totalPage,
                'page'      => $page,
                'list'      => $list
            );
        }
        else
        {
            return array(
                'totalNum'  => 0,
                'totalPage' => 0,
                'page'      => $page,
                'list'      => array()
            );
        }
    }

    /**
     * 获取操作员名称
     * @author lzg
     * @param array
     * @return array
     * @data 2016-09-18
     */
    public static function getAdminName($data){
        $newdata = array_unique($data);
        $list = AdminUser::find()->select('id,user_name,nick_name,mobile')->where(['id'=>$newdata])->asArray()->all();
        $nameArr = [];
        foreach ($list as $key => $val) {
            if($val['user_name'] != '' ){
                $nameArr[$val['id']] = $val['user_name'];
            }elseif($val['nick_name'] != '' ){
                $nameArr[$val['id']] = $val['nick_name'];
            }else{
                $nameArr[$val['id']] = $val['mobile'];
            }
        }

        return $nameArr ;

    }


}
