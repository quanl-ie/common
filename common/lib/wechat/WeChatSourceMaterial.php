<?php
namespace common\lib\wechat;

use Yii;
use common\helpers\Helper;

class WeChatSourceMaterial
{
	/*
	* 永久添加图片素材
	* @param string $filename
	* @return mixed
	* @author lzg
	* @date 2016-8-02
	*/
	public static function addMaterial($filename,$accessToken='')
	{	
		if(!$accessToken)
			return false;
		
		$jsonStr = exec('curl -F "media=@'.$filename.'" "https://api.weixin.qq.com/cgi-bin/material/add_material?access_token='.$accessToken.'"');
		$arr = json_decode($jsonStr,true);
		if(isset($arr['url'])){
			return $arr;
		}
		else{
			return $arr;
		}
	}

	/**
	 * 上传图文消息内的图片:(图片仅支持jpg/png格式，大小必须在1MB以下)
	 * @param $filename
	 * @param string $accessToken
	 * @author Jianlong.li
	 *
	 * @return bool|mixed
	 */
	public static function uploadImg($filename,$accessToken=''){
		if(!$accessToken)
			return false;

		$jsonStr = exec('curl -F "media=@'.$filename.'" "https://api.weixin.qq.com/cgi-bin/media/uploadimg?access_token='.$accessToken.'"');
		$arr = json_decode($jsonStr,true);
		if(isset($arr['url'])){
			return $arr;
		}
		else{
			return $arr;
		}
	}

	/**
	 * 修改永久图文素材
	 * @param $articles
	 * @param string $accessToken
	 */
	public static function updateNews($articles , $index ,$media_id, $accessToken = ''){
		if(!$accessToken)
			return false;

		$url= "https://api.weixin.qq.com/cgi-bin/material/update_news?access_token=".$accessToken;
		$data = [
			'articles' => $articles
		];
		$data['articles'] = $articles;
		$data['media_id'] = $media_id;
		$data['index'] = $index;
		//var_dump($data);exit;

		$template = htmlspecialchars_decode(urldecode(json_encode($data)));
		$result = Helper::curlPost($url, $template);
		$result = json_decode($result,true);
		return $result;
	}
	/*
	* 永久添加图文素材
	* @param string $filename
	* @return mixed
	* @author Jianlong.li 
	* @date 2016-8-02
	*/
	public static function addNews($articles,$accessToken='')
	{	
		if(!$accessToken)
			return false;
		
		$url= "https://api.weixin.qq.com/cgi-bin/material/add_news?access_token=".$accessToken;
		$data = [
			'articles' => $articles 
		];
		$template = htmlspecialchars_decode(urldecode(json_encode($data)));
		$result = Helper::curlPost($url, $template);
		$result = json_decode($result,true);
		return $result;
	}
	


	/**
	* 获取素材详情 -- 则响应的直接为素材的内容，开发者可以自行保存为文件
	* @see  http://mp.weixin.qq.com/wiki/4/b3546879f07623cb30df9ca0e420a5d0.html
	* @param string $media_id
	* @author lzg
	*/
	public static function getMaterial($media_id,$accessToken = ''){ 
		
		if(!$accessToken)
			return false;

		$url = "https://api.weixin.qq.com/cgi-bin/material/get_material?access_token=".$accessToken;
		$data = [
		'media_id' => $media_id
		];
		$template = json_encode($data);
		$template = urldecode($template);
		$result = Helper::curlPost($url, $template);
		$result = json_decode($result,true);
		return $result;
	}


	/**
	* 获取素材总数
	* @author lzg
	* @return 
	* 		 voice_count 语音总数量
	*        video_count 视频总数量
	*        image_count 图片总数量
	*        news_count  图文总数量
	*/
	public static function getMaterialcount($accessToken = ''){ 
		if(!$accessToken)
			return false;
		$url = "https://api.weixin.qq.com/cgi-bin/material/get_materialcount?access_token=".$accessToken;
		$result = Helper::curlGet($url);
		$result = json_decode($result,true);
		return $result;
	}

	/**
	* 获取素材列表
	* @param array @data 
	*      type  素材的类型，图片（image）、视频（video）、语音 （voice）、图文（news）
	*      offset 从全部素材的该偏移位置开始返回，0表示从第一个素材 返回
	*      count(返回素材的数量，取值在1到20之间)
	* @author lzg
	*/
	public static function  batchgetMaterial($data,$accessToken = ''){ 
		if(!$accessToken)
			return false;

		$url = "https://api.weixin.qq.com/cgi-bin/material/batchget_material?access_token=".$accessToken;

		$template = json_encode($data);
		$template = urldecode($template);
		$result   = Helper::curlPost($url, $template);
		$result   = json_decode($result,true);
		return $result;
	}


	/**
	* 删除永久素材
	* @param string $media_id
	* @author lzg
	* @return string
	*/
	public static function delMaterial($media_id,$accessToken = ''){ 
		if(!$accessToken)
			return false;
		$url="https://api.weixin.qq.com/cgi-bin/material/del_material?access_token=".$accessToken;
		$data['media_id'] = $media_id ;
		$template = json_encode($data);
		$template = urldecode($template);
		$result = Helper::curlPost($url, $template);
		$result = json_decode($result,true);
		return $result;
	}

}
