<?php
namespace common\lib\wechat;

use common\helpers\Helper;
class WeChatStatistics
{

    /**
     * 获取用户增减数据 时间跨度 7天
     * @param string $begin_date
     * @param string $end_date
     * @return array
     * @author xi
     * @date 2015-5-20
     */
    public static function getUserSummary($begin_date,$end_date)
    {
        if(!preg_match('/^\d{4}-\d{1,2}-\d{1,2}$/', $begin_date)){
            return [
                'errcode'=> 99999,
                'errmsg' => '开始日期格式不正确'
            ];
        }
        if(!preg_match('/^\d{4}-\d{1,2}-\d{1,2}$/', $end_date)){
            return [
                'errcode'=> 99999,
                'errmsg' => '结束日期格式不正确'
            ];
        }
        if($end_date== date('Y-m-d')){
            return [
                'errcode'=> 99999,
                'errmsg' => '结束日期不可以是今天时间'
            ];
        }
        $d = (strtotime($end_date) - strtotime($begin_date))/(3600*24) ;
        if( $d>7 || $d<0 ){
            return [
                'errcode'=> 99999,
                'errmsg' => '开始时间与结束时间最大不超过7天'
            ];
        }
        
        $url = 'https://api.weixin.qq.com/datacube/getusersummary?access_token='.WeChatAccessToken::getAccessToken();
        
        $params = [
            "begin_date" => $begin_date,
            "end_date"   => $end_date
        ];
        $arr = Helper::curlPost($url,json_encode($params));
        $arr = json_decode($arr,true);
        
        return $arr;
    }
    
    /**
     * 统计用户数
     * @param string $begin_date 开始时间
     * @param string $end_date 结束时间   不能大于当天， 开始与结束间隔最大 7天
     * @return array
     * @author xi
     * @date 2015-5-19
     */
    public static function getUserCumulate($begin_date,$end_date)
    {
        if(!preg_match('/^\d{4}-\d{1,2}-\d{1,2}$/', $begin_date)){
            return [
                'errcode'=> 99999,
                'errmsg' => '开始日期格式不正确'
            ];
        }
        if(!preg_match('/^\d{4}-\d{1,2}-\d{1,2}$/', $end_date)){
            return [
                'errcode'=> 99999,
                'errmsg' => '结束日期格式不正确'
            ];
        }
        if($end_date== date('Y-m-d')){
            return [
                'errcode'=> 99999,
                'errmsg' => '结束日期不可以是今天时间'
            ];
        }
        $d = (strtotime($end_date) - strtotime($begin_date))/(3600*24) ;
        if( $d>7 || $d<0 ){
            return [
                'errcode'=> 99999,
                'errmsg' => '开始时间与结束时间最大不超过7天'
            ];
        }
        
        $url = 'https://api.weixin.qq.com/datacube/getusercumulate?access_token='.WeChatAccessToken::getAccessToken();
        
        $params = [
            "begin_date" => $begin_date, 
            "end_date"   => $end_date
        ];
        $arr = Helper::curlPost($url,json_encode($params));
        $arr = json_decode($arr,true);
        
        return $arr;
    }
    

    
    /**
     * 获取图文群发每日数据
     * 
     * @see http://mp.weixin.qq.com/wiki/8/c0453610fb5131d1fcb17b4e87c82050.html
     * 
     * @param string $begin_date 
     * @return array
     * @author xi
     */
    public static function getArticleSummary($begin_date)
    {
        if(strtotime($begin_date)>= strtotime(date('Y-m-d'))){
            return [
                'errcode'=> 99999,
                'errmsg' => '只能查询昨天以前的数据'
            ];
        }
        $url = "https://api.weixin.qq.com/datacube/getarticlesummary?access_token=".WeChatAccessToken::getAccessToken();
        $params = [
            "begin_date" => $begin_date,
            "end_date"   => $begin_date
        ];
        
        $arr = Helper::curlPost($url,json_encode($params));
        $arr = json_decode($arr,true);
        
        return $arr;
    }
    
    /**
     * 获取图文群发总数据
     *   获取的是，某天群发的文章，从群发日起到接口调用日（但最多统计发表日后7天数据），每天的到当天的总等数据。
     *   例如某篇文章是12月1日发出的，发出后在1日、2日、3日的阅读次数分别为1万，则getarticletotal获取到的数据为，
     *   距发出到12月1日24时的总阅读量为1万，距发出到12月2日24时的总阅读量为2万，距发出到12月1日24时的总阅读量为3万。
     *
     * @see http://mp.weixin.qq.com/wiki/8/c0453610fb5131d1fcb17b4e87c82050.html
     * 
     * @param string $begin_date
     * @return array
     * @author xi
     */
    public static function getArticleTotal($begin_date)
    {
        if(strtotime($begin_date)>= strtotime(date('Y-m-d'))){
            return [
                'errcode'=> 99999,
                'errmsg' => '只能查询昨天以前的数据'
            ];
        }
        $url = "https://api.weixin.qq.com/datacube/getarticletotal?access_token=".WeChatAccessToken::getAccessToken();
        $params = [
            "begin_date" => $begin_date,
            "end_date"   => $begin_date
        ];
        
        $arr = Helper::curlPost($url,json_encode($params));
        $arr = json_decode($arr,true);
        
        return $arr;
    }
    
    /**
     * 获取图文统计数据 开始 ->结束 最大跨度 3 天
     * @param string $begin_date
     * @param string $end_date
     * @return array
     * @author xi
     * @date 2015-5-20
     */
    public static function getUserRead($begin_date,$end_date)
    {
        if(strtotime($begin_date)>= strtotime(date('Y-m-d'))){
            return [
                'errcode'=> 99999,
                'errmsg' => '只能查询昨天以前的数据'
            ];
        }
        
        $d = (strtotime($end_date)-strtotime($begin_date))/84600;
        if($d>3 || $d<0){
            return [
                'errcode'=> 99999,
                'errmsg' => '开始日期与结束日期跨度最大3天'
            ];
        }
            
            
        $url = "https://api.weixin.qq.com/datacube/getuserread?access_token=".WeChatAccessToken::getAccessToken();
        $params = [
            "begin_date" => $begin_date,
            "end_date"   => $end_date
        ];
        
        $arr = Helper::curlPost($url,json_encode($params));
        $arr = json_decode($arr,true);
        
        return $arr;
    }
    
    /**
     * 获取图文统计分时数据 
     * @param string $begin_date
     * @return array
     * @author xi
     * @date 2015-5-20
     */
    public static function getUserReadHour($begin_date)
    {
        if(strtotime($begin_date)>= strtotime(date('Y-m-d'))){
            return [
                'errcode'=> 99999,
                'errmsg' => '只能查询昨天以前的数据'
            ];
        }
        
        $url = "https://api.weixin.qq.com/datacube/getuserreadhour?access_token=".WeChatAccessToken::getAccessToken();
        $params = [
            "begin_date" => $begin_date,
            "end_date"   => $begin_date
        ];
        
        $arr = Helper::curlPost($url,json_encode($params));
        $arr = json_decode($arr,true);
        
        return $arr;
    }
    
    
    /**
     * 获取图文分享转发数据  最大跨度7天
     * @param string $begin_date
     * @param string $end_date
     * @return array
     * @author xi
     */
    public static function getUserShare($begin_date,$end_date)
    {
        $url = "https://api.weixin.qq.com/datacube/getusershare?access_token=".WeChatAccessToken::getAccessToken();
    
        $params = [
            "begin_date" => $begin_date,
            "end_date"   => $end_date
        ];
    
        $arr = Helper::curlPost($url,json_encode($params));
        $arr = json_decode($arr,true);
    
        return $arr;
    }
    
    /**
     * 获取图文分享转发分时数据 
     * @param string $begin_date
     * @return array
     * @author xi
     * @date 2015-5-20
     */
    public static function getUserShareHour($begin_date)
    {
        if(strtotime($begin_date)>= strtotime(date('Y-m-d'))){
            return [
                'errcode'=> 99999,
                'errmsg' => '只能查询昨天以前的数据'
            ];
        }
        
        $url = "https://api.weixin.qq.com/datacube/getusersharehour?access_token=".WeChatAccessToken::getAccessToken();
        $params = [
            "begin_date" => $begin_date,
            "end_date"   => $begin_date
        ];
        
        $arr = Helper::curlPost($url,json_encode($params));
        $arr = json_decode($arr,true);
        
        return $arr;
    }
}