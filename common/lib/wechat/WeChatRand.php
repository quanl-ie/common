<?php
namespace common\lib\wechat;

class WeChatRand
{
    /**
     * 生成随机串
     * @param int $length
     * @return string
     * @author xi
     * @since 2015-1-12
     */
    public static function get($length = 16) {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $str = "";
        for ($i = 0; $i < $length; $i++) {
            $str .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);
        }
        return $str;
    }
}