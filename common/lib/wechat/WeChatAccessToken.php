<?php
namespace common\lib\wechat;

use common\helpers\Helper;
use Yii;

class WeChatAccessToken
{
    
    /**
     * 获取微信Access_Token
     * @author xi
     * @since 2015-1-5 
     */
    public static function getAccessToken()
    {
        //检测本地是否已经拥有access_token，并且检测access_token是否过期
        $accessToken = self::_checkAccessToken();

        if($accessToken === false){
            $accessToken = self::_getAccessToken();
        }
        
        return isset($accessToken['access_token'])?$accessToken['access_token']:'';
    }
    
    /**
     * 主动更新 token 
     * @return string
     */
    public static function autoUpdateAccessToken()
    {
        return self::_getAccessToken();
    }

    /**
     * 从微信服务器获取微信ACCESS_TOKEN
     * @return string
     * @author xi
     * @since 2015-1-5
     */
    private static function _getAccessToken()
    {
        $appid     = \Yii::$app->params['wechat']['appid'];
        $appsecret = \Yii::$app->params['wechat']['appsecret'];
        
        $url = 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid='.$appid.'&secret='.$appsecret;
        $accessToken = Helper::curlGet($url);

        $accessTokenArr = json_decode($accessToken,true);
        if(!isset($accessTokenArr['access_token'])){
            return '';
        }
        $accessTokenArr['time'] = time();
        
        //缓冲 token
        Yii::$app->cache->set('weixin_access_token',json_encode($accessTokenArr),7260);
        return $accessTokenArr;
    }

    /**
     *  检测微信ACCESS_TOKEN是否过期
     *              -10是预留的网络延迟时间
     * @return bool
     * @author xi
     * @since 2015-1-5
     */
    private static function _checkAccessToken()
    {
        $data = Yii::$app->cache->get('weixin_access_token');
        $accessToken['value'] = $data;
        if(!empty($accessToken['value'])){
            $accessToken = json_decode($accessToken['value'], true);
            
            if(time() - $accessToken['time'] < $accessToken['expires_in']-10){
                return $accessToken;
            }
        }
        return false;
    }
}
?>
