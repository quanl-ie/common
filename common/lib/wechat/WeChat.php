<?php
namespace common\lib\wechat;

class WeChat
{
    
    /**
     * 获取(关注，消息)类型
     * @return array
     * @author xi
     * @since 2015-1-7
     */
    private static function getData()
    {
        $postStr = isset($GLOBALS["HTTP_RAW_POST_DATA"])?$GLOBALS["HTTP_RAW_POST_DATA"]:'';
        if($postStr!='')
        {
            libxml_disable_entity_loader(true);
            $arr = (array)simplexml_load_string($postStr, 'SimpleXMLElement', LIBXML_NOCDATA);
            if($arr){
                return $arr;
            }
        }
        return '';
    }
    
    /**
     * 运行
     * @author xi
     * @since 2015-1-7
     */
    public static function run()
    {
        $data = self::getData();
        
        $tousername = $data['FromUserName'];
        
        //事件(关注、取消)
        if(strtolower($data['MsgType'])=='event')
        {
            //新关注用户发消息
            if($data['Event']=='subscribe')
            {
                
            }
            else if($data['Event'] == 'unsubscribe')
            {
            }
            //地理位置
            else if(strtolower($data['Event']) == 'location')
            {
                
            }
            //审核成功推送事件
            elseif(strtolower($data['Event']) == 'card_pass_check' || strtolower($data['Event']) == 'card_not_pass_check') 
            {
            }
            //领取卡券推送事件
            elseif(strtolower($data['Event']) == 'user_get_card')
            {
            }
            //删除卡券推送事件
            elseif(strtolower($data['Event']) == 'user_del_card')
            {
            }
            //卡券核销事件推送
            elseif(strtolower($data['Event']) == 'user_consume_card') 
            {
            }
            
            
        }
        else if(strtolower($data['MsgType'])=='text')
        {
            $content = "test";
            return WeChatSendMessage::text($tousername, $content);
        }
    }
}