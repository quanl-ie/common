<?php
namespace common\lib\wechat;

use common\helpers\Helper;
use Yii;

class WeChatOAuthTemplete{
    

    /**
     * 获取 code 
     * @param string $redirect_uri 微信跳回页面地址
     * @param int $state
     * @param string $scope
     * @return void
     * @author xi
     * @since 2015-1-5
     */
    public static function getCode($redirect_uri, $state=1, $scope='snsapi_base',$appid='')
    {
        //公众号的唯一标识
        if($appid == '')
            $appid = \Yii::$app->params['wechat']['appid'];

        //授权后重定向的回调链接地址，请使用urlencode对链接进行处理
        $redirect_uri = urlencode($redirect_uri);
        //返回类型，请填写code
        $response_type = 'code';
        //构造请求微信接口的URL
        $url = 'https://open.weixin.qq.com/connect/oauth2/authorize?appid='.$appid.'&redirect_uri='.$redirect_uri.'&response_type='.$response_type.'&scope='.$scope.'&state='.$state.'#wechat_redirect';
        //header('Location: '.$url, true, 301);
        //die;
        return $url;
    }

    /**
     * 通过code换取网页授权access_token
     * @param string $code 上一个方法返回的 code
     * @return json
     * @author xi
     * @since 2015-1-5
     */
    public static function getAccessTokenAndOpenId($code,$appid='',$appsecret='')
    {
        if($appid == '')
            $appid     = \Yii::$app->params['wechat']['appid'];
        if($appsecret == '')
            $appsecret = \Yii::$app->params['wechat']['appsecret'];
        
        //填写为authorization_code
        $grant_type = 'authorization_code';
        //构造请求微信接口的URL
        $url = 'https://api.weixin.qq.com/sns/oauth2/access_token?appid='.$appid.'&secret='.$appsecret.'&code='.$code.'&grant_type='.$grant_type.'';
        //请求微信接口, Array(access_token, expires_in, refresh_token, openid, scope)
        return Helper::curlGet($url);
    }

    /**
     * 刷新access_token（如果需要）
     * @param string $refreshToken
     * @return mixed
     * @author xi
     * @since 2015-1-5
     */
    public static function refreshToken($refreshToken,$appid = '')
    {
        if($appid == '')
            $appid     = \Yii::$app->params['wechat']['appid'];
        
        $queryUrl = 'https://api.weixin.qq.com/sns/oauth2/refresh_token?appid='.$appid.'&grant_type=refresh_token&refresh_token='.$refreshToken;
        return Helper::curlGet($queryUrl);
    }

    /**
     * 拉取用户信息(需scope为 snsapi_userinfo)
     * 如果网页授权作用域为snsapi_userinfo，则此时开发者可以通过access_token和openid拉取用户信息了。
     * @param $accessToken 网页授权接口调用凭证。通过本类的第二个方法getAccessTokenAndOpenId可以获得一个数组，数组中有一个字段是access_token，就是这里的参数。注意：此access_token与基础支持的access_token不同
     * @param $openId 用户的唯一标识
     * @param $lang 返回国家地区语言版本，zh_CN 简体，zh_TW 繁体，en 英语
     *
     * @return array("openid"=>"用户的唯一标识",
                     "nickname"=>'用户昵称',
                     "sex"=>"1是男，2是女，0是未知",
                     "province"=>"用户个人资料填写的省份"
                     "city"=>"普通用户个人资料填写的城市",
                     "country"=>"国家，如中国为CN",
                     //户头像，最后一个数值代表正方形头像大小（有0、46、64、96、132数值可选，0代表640*640正方形头像），用户没有头像时该项为空
                     "headimgurl"=>"http://wx.qlogo.cn/mmopen/g3MonUZtNHkdmzicIlibx6iaFqAc56vxLSUfpb6n5WKSYVY0ChQKkiaJSgQ1dZuTOgvLLrhJbERQQ4eMsv84eavHiaiceqxibJxCfHe/46",
                     //用户特权信息，json 数组，如微信沃卡用户为chinaunicom
                     "privilege"=>array("PRIVILEGE1", "PRIVILEGE2"),
                );
     */
    public static function getUserInfo($accessToken, $openId, $lang='zh_CN')
    {
        $queryUrl = 'https://api.weixin.qq.com/sns/userinfo?access_token='. $accessToken . '&openid='. $openId .'&lang=zh_CN';
        return Helper::curlGet($queryUrl);
    }

    /**
     * 检验授权凭证（access_token）是否有效
     * @param $accessToken 网页授权接口调用凭证。通过本类的第二个方法getAccessTokenAndOpenId可以获得一个数组，数组中有一个字段是access_token，就是这里的参数。注意：此access_token与基础支持的access_token不同
     * @param $openId
     * @return array("errcode"=>0,"errmsg"=>"ok")
     */
    public static function checkAccessToken($accessToken, $openId)
    {
        $queryUrl = 'https://api.weixin.qq.com/sns/auth?access_token='.$accessToken.'&openid='.$openId;
        $queryAction = 'GET';
        return Helper::curlGet($queryUrl);
    }
}