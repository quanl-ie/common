<?php
namespace common\lib\wechat;

use common\helpers\Helper;
use Yii;


class WeChatTicket
{
    
    /**
     * 上传卡券LOGO
     * @param array $files logo文件数据流
     * @return string|array 添加成功返回logo链接，否则返回数组信息
     * @author li
     * @since 2015-06-01
     */
    public static function add($files)
    {   
        //$files = "/data0/wwwx.10020.net/frontend/web/upload/2015/03/19/coupon/239386/20150319083254_854019.jpg"; 
        $json = exec('curl -F "buffer=@'.$files.'" "https://api.weixin.qq.com/cgi-bin/media/uploadimg?access_token='.WeChatAccessToken::getAccessToken().'"');
        $arr = json_decode($json,true);
        if(!isset($arr['errcode'])) {
            Yii::$app->db->createCommand()->insert('card_logo', [
                'card_logo'     => $arr['url']
            ])->execute();
        }
        return $arr;
    }
    
    /**
     * 创建卡券
     * @param string $card_type 卡券类型
     * @param array  $base_info 卡券基本信息
     * @param string $deal_detail $gift $default_detail 卡券描述（团购|礼品|优惠）
     * @param int    $least_cost $reduce_cost $discount 卡券描述（代金|折扣）
     * @return array; 返回数组
     * @author li
     * @since  2015-06-01
     * */
    public static function buffer($card_type,$base_info,$deal_detail='',$least_cost=0,$reduce_cost=0,$discount=0,$gift='',$default_detail='') {
        //http请求方式: POST
        //https://api.weixin.qq.com/card/create?access_token=ACCESS_TOKEN
        $url = "https://api.weixin.qq.com/card/create?access_token=".WeChatAccessToken::getAccessToken();
        
        if($card_type == 'GROUPON') {
            $data['card'] = array(
                "card_type" => "GROUPON",
                'groupon' => array(
                        'base_info' => $base_info,
                        'deal_detail' => $deal_detail
                    ),
                
            );
            
        }else if($card_type == 'CASH') {
            $data['card'] = array(
                'card_type' => 'CASH',
                'cash' => array(
                        'base_info' => $base_info,
                        'least_cost' => $least_cost,
                        'reduce_cost' => $reduce_cost
                    ),
                
            );
        }elseif ($card_type=='DISCOUNT') {
            $data['card'] = array(
                'card_type' => 'DISCOUNT',
                'discount'  => array(
                    'base_info' => $base_info,
                    'discount' => $discount
                ),
            );
        }elseif ($card_type == 'GIFT') {
            $data['card'] = array(
                'card_type' => 'GIFT',
                'gift'  => array(
                    'base_info' => $base_info,
                    'gift' => $gift
                ),
                
            );
        }elseif ($card_type == 'GENERAL_COUPON') {
            $data['card'] = array(
                'card_type' => 'GENERAL_COUPON',
                'gift'  => array(
                    'base_info' => $base_info,
                    'default_detail' => $default_detail
                ),
                
            );
        }
        
        $data = json_encode($data);
        $data = urldecode($data);
        
        $arr = json_decode(Helper::curlPost($url, $data));
        if($arr['errcode'] == 0) {
            Yii::$app->db->createCommand()->insert('card', [
                'cardid'     => $arr['card_id']
            ])->execute();
        }
        return $arr;
    }
    
    /**
     * 创建二维码
     * @return array
     * @author li
     * @since  2015-06-01
     * */
    public static function qrcodeCreate($card_id) {
        $url = "https://api.weixin.qq.com/card/qrcode/create?access_token=".WeChatAccessToken::getAccessToken();
        $data["action_name"] = "QR_CARD";
        $data["action_info"] = array(
            'card' => array(
                'card_id' => $card_id,
                'is_unique_code' => false,
                'outer_id' => 1
            ),
        );
        
        $data = json_encode($data);
        
        return json_decode(Helper::curlPost($url, $data));
    }
    
    /**
     * 获取api_ticket
     * @return array
     * @author li
     * @since  2015-06-01
     * */
    public static function getTicket() {
        $url = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=".WeChatAccessToken::getAccessToken()."&type=wx_card";
        
        $jsonArr = json_decode(Helper::curlGet($url));
        
        if($jsonArr['errcode'] == 0) {
            Yii::$app->cache->set("api_ticket", $jsonArr['ticket'], 7200);
        }
        return $jsonArr;
    }
    
    
    /**
     * 投放卡券
     * @return array 返回数组
     * @author li
     * @since  2015-06-01
     * */
    /*public static function throwin($card_id) {
        $url = "https://api.weixin.qq.com/card/qrcode/create?access_token=".WeChatAccessToken::getAccessToken();
        
        $data = json_encode(array('code_id'=>$card_id));
        
        return json_encode(Helper::curlPost($url, $data));
    }*/
    
    
    /**
     * 查看卡券详情
     * @author li
     * @since  2015-06-01
     * */
    public static function inquireCard($card_id) {
        $url = "https://api.weixin.qq.com/card/get?access_token=".WeChatAccessToken::getAccessToken();
        
        $data['card_id'] = $card_id;
        $data = json_encode($data);
        
        return json_decode(Helper::curlPost($url, $data));
    }
    
    
    /*卡券核销部分*/

    /**
     * 消耗code
     * @param  $code string
     * @param  array
     * @author li
     * @since  2015-06-01
     * */
    public static function consume($code,$card_id) {
        $url = "https://api.weixin.qq.com/card/code/consume?access_token=".WeChatAccessToken::getAccessToken();
        $data['code'] = $code;
        $data['card_id'] = $card_id;
        
        $data = json_encode($data);
        
        return Helper::curlPost($url, $data);
    }
    
    
    /**
     * Code解码接口
     * @param  string $encrypt_code
     * @return array 返回数组
     * @author li
     * @since  2015-06-01
     * */
    public static function dataStatic($encrypt_code) {
        //https://api.weixin.qq.com/card/code/decrypt?access_token=TOKEN
        $url = "https://api.weixin.qq.com/card/code/decrypt?access_token=".WeChatAccessToken::getAccessToken();
        $data = json_encode(array('encrypt_code' => $encrypt_code));
        
        return json_decode(Helper::curlPost($url, $data));
    }
    
    
    /*卡券管理*/
    
    /**
     * 删除卡券(删除一类卡券)
     * @author li
     * @since  2015-06-01
     * */
    public static function deleteCard($card_id) {
        $url = "https://api.weixin.qq.com/card/delete?access_token=".WeChatAccessToken::getAccessToken();
        
        $data['card_id'] = $card_id;
        
        return json_decode(Helper::curlPost($url, $data));
    }
    
    /**
     * 查询code(该 code 对应的用户openid、卡券有效期等信息。)
     * @return array
     * @author li
     * @since  2015-06-01
     * https://api.weixin.qq.com/card/code/get?access_token=TOKEN
     * */
    public static function getCode($code) {
        $url = "https://api.weixin.qq.com/card/code/get?access_token=".WeChatAccessToken::getAccessToken();
        $data['code'] = $code;
        return json_decode(Helper::curlPost($url, $data));
    } 
    
    /**
     * 批量查询卡列表
     * @param $offset int 查询卡列表的起始偏移量
     * @param $count  int 查询卡片数量
     * @author li
     * @since  2015-06-01
     * */
    public static function batchget($offset,$count) {
        $url ="https://api.weixin.qq.com/card/batchget?access_token=".WeChatAccessToken::getAccessToken();
        
        $data['offset'] = $offset;
        $data['count'] = $count;
        $data = json_encode($data);
        
        return json_decode(Helper::curlPost($url, $data));
    }
    
    
    /**
     * 更改code
     * @param  $code    int   code码
     * @param  $card_id string   cardID
     * @param  new_card 新code码
     * @return array
     * @author li
     * @since  2015-06-02
     * */
    public function Update($code,$card_id,$new_code) {
        $url = "https://api.weixin.qq.com/card/code/update?access_token=".WeChatAccessToken::getAccessToken();
        
        $data['code'] = $code;
        $data['card_id'] = $card_id;
        $data['new_code'] = $new_code;
        
        $data = json_encode($data);
        
        return json_decode(Helper::curlPost($url, $data));
    }
    
    /**
     * 设置卡券失效接口
     * @param  $code int code码
     * @param  $card_id string cardID(自定义code必填)
     * @author li
     * @since  2015-06-02
     * */
    public function unavailable($code,$card_id=0) {
        $url = "https://api.weixin.qq.com/card/code/unavailable?access_token".WeChatAccessToken::getAccessToken();
        $data['code'] = $code;
        if($card_id!=0) {
            $data['card_id'] = $card_id;
        }
        
        $data = json_encode($data);
        
        return json_decode(Helper::curlPost($url, $data));
    }
    
    /**
     * 更改卡券信息接口
     * @param $card_id  string 卡券ID
     * @param $base_info  array  要修改的信息
     * @param $bonus_cleared  string 积分清零规则
     * @param $bonus_rules  string 积分规则
     * @return string|array 成功返回string|失败返回array
     * @author li
     * @since  2015-06-02
     * */
    public function updateCard($card_id,$base_info,$bonus_cleared,$bonus_rules,$prerogative='') {
        $url ="https://api.weixin.qq.com/card/update?access_token".WeChatAccessToken::getAccessToken();
        
        $data['card_id'] = $card_id;
        $data['member_card'] = array(
            'base_info' => $base_info,
            'bonus_cleared' => $bonus_cleared,
            'bonus_rules'   => $bonus_rules
        );
        if(!empty($prerogative)) {
            $data['member_card']['prerogative'] = $prerogative;
        }
        
        $data = json_encode($data);
        $data = urldecode($data);
        
        $arr = json_decode(Helper::curlPost($url, $data));
        if($arr['errcode']==0) {
            return $arr['errmsg'];
        }else{
            return $arr;
        }
    }
    
    /**
     * 库存修改接口
     * @param $card_id  卡券ID
     * @param $increase_stock_value  int  增加数量
     * @param $reduce_stock_value  int 减少数量
     * @return string|array 成功返回string | 失败返回array
     * @author li
     * @since  2015-06-02
     * */
    public function modifystock($card_id,$increase_stock_value,$reduce_stock_value) {
        $url ="https://api.weixin.qq.com/card/modifystock?access_token=".WeChatAccessToken::getAccessToken();
        
        $data['card_id'] = $card_id;
        $data['increase_stock_value'] = $increase_stock_value;
        $data['reduce_stock_value'] = $reduce_stock_value;
        
        $data = json_encode($data);
        
        $arr = json_decode(Helper::curlPost($url, $data));
        if($arr['errcode'] == 0) {
            return $arr['errmsg'];
        }else{
            return $arr;
        }
    }
}