<?php
namespace common\lib\wechat;

use common\helpers\Helper;
use Yii;

class WeChatUserManager
{
    /**
     * 获取用户信息
     * @param string $openId
     * @return mixed
     */
    public static function getUserInfo($openId,$accessToken=null)
    {
        //获取ACCESS_TOKEN
        if($accessToken==null){
            $accessToken = WeChatAccessToken::getAccessToken();
        }
        $queryUrl = 'https://api.weixin.qq.com/cgi-bin/user/info?access_token='.$accessToken.'&openid='.$openId;
        return Helper::curlGet($queryUrl);
    }
    
    /**
     * 获取用户列表
     * @author xi
     * @date 2015-4-1
     */
    public static function getUserList($accessToken='',$next_openid='')
    {
        if($accessToken==''){
            $accessToken = WeChatAccessToken::getAccessToken();
        }
        $queryUrl = "https://api.weixin.qq.com/cgi-bin/user/get?access_token=$accessToken&next_openid=$next_openid";
        $data = Helper::curlGet($queryUrl);
        $data = json_decode($data,true);
        return $data;
    }
    
    /**
     * 判断是否关注
     * @param string $openid 用户openid
     * @return boolean
     * @author xi
     * @date 2015-6-17
     */
    public static function hasSubscribe($openid,$accessToken=null)
    {
        //查看用户是否关注
        $userInfo =  WeChatUserManager::getUserInfo($openid,$accessToken);
        $userInfo = json_decode($userInfo,true);
        
        //临时解决 token 问题
        if(!isset($userInfo['openid'])){
            Yii::$app->cache->delete('weixin_access_token');
            $userInfo =  WeChatUserManager::getUserInfo($openid,$accessToken);
            $userInfo = json_decode($userInfo,true);
        }
        
        if(isset($userInfo['subscribe']))
            return $userInfo['subscribe']==1?true:false;
        return false;
    }
}