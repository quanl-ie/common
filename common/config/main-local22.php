<?php
return [
    'components' => [
       /* 'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=mysql.uservices.cn:3307;dbname=dxt_manufactor',
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8mb4',
        ],*/
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=mysql.uservices.cn:3307;dbname=dxt_manufactor',
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8mb4',
        ],
        'o2odb' => [
                'class' => 'yii\db\Connection',
                'dsn' => 'mysql:host=mysql.uservices.cn:3307;dbname=o2o',
                'username' => 'root',
                'password' => '',
                'charset' => 'utf8mb4',
        ],
		'order_db' => [
                'class' => 'yii\db\Connection',
                'dsn' => 'mysql:host=mysql.uservices.cn:3307;dbname=dxt_order',
                'username' => 'root',
                'password' => '',
                'charset' => 'utf8mb4',
        ],
        'service_provider_db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=mysql.uservices.cn:3307;dbname=service_provider',
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8mb4',
        ],
        /*    'mailer' => [
                'class' => 'yii\swiftmailer\Mailer',
                'viewPath' => '@common/mail',

                // send all mails to a file by default. You have to set
                // 'useFileTransport' to false and configure a transport
                // for the mailer to send real emails.
                'useFileTransport' => true,
            ],*/
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
		'redis' => [
            'class' => 'yii\redis\Connection',
            'hostname' => 'redis.uservices.cn',
            'port' => 6379,
            'database' => 0,
			'password' => 'b89e41cb7be6c52s'
        ],
    
        'redisNo' => [
            'class' => 'yii\redis\Connection',
            'hostname' => 'redis.uservices.cn',
            'port' => 6379,
            'database' => 1,
            'password' => 'b89e41cb7be6c52s'
        ],
        
    ],
];
