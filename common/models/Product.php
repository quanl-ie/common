<?php
namespace common\models;

use webapp\models\ProductMaterials;
use yii\db\ActiveRecord;
use Yii;
/**
 * 产品管理
 * @author lxq liuxingqi@services.cn
 * Class Product
 * @package webapp\models
 */
class  Product extends BaseModel
{
    
    public static function tableName()
    {
        return 'product';
    }
    
    public static function getType()
    {
        return ['1'=>'成品','2'=>'配件','3'=>'物料清单'];
    }
    
    public static function getTypeName($id = '')
    {
        return self::getType()[$id];
    }
    
    /**
     * 获取单条数据
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/3/20
     * Time: 13:52
     * @param array $where
     * @return array|null|ActiveRecord
     */
    public static function getOne($where = [],$flag = '')
    {
        if ($flag) {
            return self::find()->where($where)->asArray()->one();
        }else{
            return self::find()->where($where)->one();
        }
        
    }
    
    /**
     * 获取多条数据
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/3/20
     * Time: 13:52
     * @param array $where
     * @return array|ActiveRecord[]
     */
    public static function getList($where = [])
    {
        return self::find()->where($where)->asArray()->all();
    }
    /**
     * 非良品产品列表
     */
    public static function badList($map,$where,$page=1,$pageSize=10,$type = 1)
    {
        $db = self::find();
        $db->from(self::tableName(). ' as a');
        $db->leftJoin('`'.DepotProdStock::tableName().'` as b','a.id = b.prod_id');
        $db->where($map);
        //判断where
        if (!empty($where)) {
            foreach ($where as $val) {
                $db->andFilterWhere($val);
            }
        }

        //总数
        $totalNum = $db->count();

        //当有结果时进行组合数据
        if ($totalNum>0) {
            //总页数
            $totalPage = ceil($totalNum/$pageSize);
            if($page<1)
            {
                $page = 1;
            }
            else if($page>$totalPage)
            {
                $page = $totalPage;
            }
            $db->offset(($page-1)*$pageSize);
            $db->orderBy(['a.id'=> SORT_DESC]);
            $db->limit($pageSize);
            $db->asArray();
            $list = $db->all();
            if ($type == 2) {
                $brandId   = array_filter(array_unique(array_column($list,'brand_id')));
                $brand     = ServiceBrand::find()->where(['id'=>$brandId])->asArray()->all();
                $brandName = array_column($brand,'title','id');
                foreach ($list as $key => $val) {
                    if (isset($brandName[$val['brand_id']])) {
                        $list[$key]['brand_name'] = $brandName[$val['brand_id']];
                    }else{
                        $list[$key]['brand_name'] = '';
                    }
                }
            }
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => $totalPage,
                'list'       => $list
            ];
        }
        else
        {
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => 0,
                'list'       => []
            ];
        }
    }
    /**
     * 产品列表（分页，搜索）
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/3/21
     * Time: 15:22
     * @param $where
     * @param int $page
     * @param int $pageSize
     * @return array
     */
    public static function index($map,$where,$page=1,$pageSize=10,$type = 1)
    {
        $db = self::find();
        $db->where($map);
        //判断where
        if (!empty($where)) {
            foreach ($where as $val) {
                $db->andFilterWhere($val);
            }
        }

        //总数
        $totalNum = $db->count();
        
        //当有结果时进行组合数据
        if ($totalNum>0) {
            //总页数
            $totalPage = ceil($totalNum/$pageSize);
            if($page<1)
            {
                $page = 1;
            }
            else if($page>$totalPage)
            {
                $page = $totalPage;
            }
            $db->offset(($page-1)*$pageSize);
            $db->orderBy(['id'=> SORT_DESC]);
            $db->limit($pageSize);
            $db->asArray();
            $list = $db->all();
            if ($type == 2) {
                $brandId   = array_filter(array_unique(array_column($list,'brand_id')));
                $brand     = ServiceBrand::find()->where(['id'=>$brandId])->asArray()->all();
                $brandName = array_column($brand,'title','id');
                foreach ($list as $key => $val) {
                    if (isset($brandName[$val['brand_id']])) {
                        $list[$key]['brand_name'] = $brandName[$val['brand_id']];
                    }else{
                        $list[$key]['brand_name'] = '';
                    }
                }
            }
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => $totalPage,
                'list'       => $list
            ];
        }
        else
        {
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => 0,
                'list'       => []
            ];
        }
    }
    
    /**
     * 添加
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/3/21
     * Time: 11:01
     * @param $data
     * @return bool
     */
    public static function add($data)
    {
        //开启事务
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();
        try {
            //添加产品、配件
            if (isset($data['product']) && !empty($data['product']) && $data['product']['type_id'] != 3)
            {
                //添加主表信息
                $model = new self();
                foreach ($data['product'] as $key => $val) {
                    $model->$key = $val;
                }
                if (!$model->save(false))
                {
                    $transaction->rollBack();
                    return false;
                }
                
                //关联物料清单数据
                if (isset($data['materials']) && !empty($data['materials']))
                {
                    foreach ($data['materials'] as $key => $val) {
                        $productMaterialsModel = new ProductMaterials();
                        
                        $productMaterialsModel->parent_id   = $val['parent_id'];
                        $productMaterialsModel->prod_id     = $model->id;
                        $productMaterialsModel->num         = $val['num'];
                        $productMaterialsModel->price       = $val['price'];
                        $productMaterialsModel->status      = $val['status'];
                        $productMaterialsModel->is_directly = $val['is_directly'];
                        
                        $productMaterialsModel->save(false);
                    }
                }
                
            }
            
            //添加物料清单
            if (isset($data['product']) && !empty($data['product']) && !empty($data['materials']) && $data['product']['type_id'] == 3)
            {

                //添加主表信息
                $model = new self();
                foreach ($data['product'] as $key => $val) {
                    $model->$key = $val;
                }
                if (!$model->save(false))
                {
                    $transaction->rollBack();
                    return false;
                }
                
                //添加关联数据
                foreach ($data['materials'] as $key => $val) {
                    $productMaterialsModel = new ProductMaterials();
        
                    $productMaterialsModel->parent_id   = $model->id;
                    $productMaterialsModel->prod_id     = $val['prod_id'];
                    $productMaterialsModel->num         = $val['num'];
                    $productMaterialsModel->price       = $val['price'];
                    $productMaterialsModel->status      = $val['status'];
                    $productMaterialsModel->is_directly = $val['is_directly'];
        
                    $productMaterialsModel->save(false);
                }
                
                //修改合同价格
                if (isset($data['addContract']) && !empty($data['addContract'])) {
                    foreach ($data['addContract'] as $key => $val) {
                        $contractModel = Contract::findOne(['id' => $val['id']]);
                        $contractModel->total_amount = $val['total_amount'];
                        $contractModel->save(false);
                    }
    
                }
                
                //添加合同数据
                if (isset($data['addContractDetail']) && !empty($data['addContractDetail']))
                {
                    foreach ($data['addContractDetail'] as $key => $val) {
                        $contractDetail[$key]['prod_id'] = $model->id;
    
                        $contractDetailModel = new ContractDetail();
    
                        $contractDetailModel->department_id     = $val['department_id'];
                        $contractDetailModel->department_top_id = $val['department_top_id'];
                        $contractDetailModel->parent_id         = $val['parent_id'];
                        $contractDetailModel->prod_id           = $model->id;
                        $contractDetailModel->prod_type         = $val['prod_type'];
                        $contractDetailModel->prod_num          = $val['prod_num'];
                        $contractDetailModel->price             = $val['price'];
                        $contractDetailModel->amount            = $val['amount'];
                        $contractDetailModel->rate              = $val['rate'];
                        $contractDetailModel->total_amount      = $val['total_amount'];
                        $contractDetailModel->sale_price        = $val['sale_price'];
                        $contractDetailModel->finish_num        = $val['finish_num'];
                        $contractDetailModel->total_num         = $val['total_num'];
                        $contractDetailModel->status            = $val['status'];
                        
                        $contractDetailModel->save(false);
                    }
                }
            }
            
            
            $transaction->commit();
            return true;
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }
    
    /**
     * 编辑
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/8/6
     * Time: 13:07
     * @param $data
     * @return bool
     * @throws \Exception
     */
    public static function edit($data)
    {
        //开启事务
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();
        try {
            //修改产品、配件
            if (isset($data['product']) && !empty($data['product']) && $data['product']['type_id'] != 3)
            {
                //修改主表信息
                $model = self::findOne(['id'=>$data['product']['id']]);
                foreach ($data['product'] as $key => $val) {
                    $model->$key = $val;
                }
                if (!$model->save(false))
                {
                    $transaction->rollBack();
                    return false;
                }
                //添加关联关系
                if (isset($data['addMaterials']) && !empty($data['addMaterials']))
                {
                    foreach ($data['addMaterials'] as $key => $val) {
                        $productMaterials = new ProductMaterials();
            
                        $productMaterials->parent_id   = $val['parent_id'];
                        $productMaterials->prod_id     = $val['prod_id'];
                        $productMaterials->num         = $val['num'];
                        $productMaterials->price       = $val['price'];
                        $productMaterials->status      = $val['status'];
                        $productMaterials->is_directly = $val['is_directly'];
            
                        $productMaterials->save(false);
                    }
                    
                }
                //删除关联关系
                if (isset($data['delMaterials']) && !empty($data['delMaterials']))
                {
                    ProductMaterials::updateAll(['status' => 2],['id' =>$data['delMaterials'],'status'=>1]);
                }
            }
            
            //修改物料清单
            if (isset($data['product']) && !empty($data['product']) && $data['product']['type_id'] == 3)
            {
                //修改主表信息
                $model = self::findOne(['id'=>$data['product']['id']]);
                foreach ($data['product'] as $key => $val) {
                    $model->$key = $val;
                }
                if (!$model->save(false))
                {
                    $transaction->rollBack();
                    return false;
                }
                
                //修改物料列表
                if (isset($data['editMaterials']) && !empty($data['editMaterials']))
                {
                    foreach ($data['editMaterials'] as $key => $val) {
                        $editMaterials = ProductMaterials::findOne(['parent_id'=>$val['parent_id'],'prod_id'=>$val['prod_id'],'status'=>1,'is_directly'=>1]);
                        $editMaterials->num = $val['num'];
                        $editMaterials->save(false);
                    }
                }
                
                //删除物料列表
                if (isset($data['delMaterials']) && !empty($data['delMaterials']))
                {
                    foreach ($data['delMaterials'] as $key => $val) {
                        $delMaterials = ProductMaterials::findOne(['id'=>$val['id']]);
                        $delMaterials->status = $val['status'];
                        $delMaterials->save(false);
                    }
                }
    
                //添加物料列表
                if (isset($data['addMaterials']) && !empty($data['addMaterials']))
                {
                    foreach ($data['addMaterials'] as $key => $val) {
                        $addMaterials = new ProductMaterials();
                        
                        $addMaterials->parent_id   = $val['parent_id'];
                        $addMaterials->prod_id     = $val['prod_id'];
                        $addMaterials->num         = $val['num'];
                        $addMaterials->price       = $val['price'];
                        $addMaterials->status      = $val['status'];
                        $addMaterials->is_directly = $val['is_directly'];
                        
                        $addMaterials->save(false);
                    }
                }
                
                //修改合同信息
                if (isset($data['editContractDetail']) && !empty($data['editContractDetail']))
                {
                    foreach ($data['editContractDetail'] as $key => $val) {
                        
                        $contractDetailModel = new ContractDetail();
                        
                        $contractDetailModel->department_id     = $val['department_id'];
                        $contractDetailModel->department_top_id = $val['department_top_id'];
                        $contractDetailModel->parent_id         = $val['parent_id'];
                        $contractDetailModel->prod_id           = $val['prod_id'];
                        $contractDetailModel->prod_type         = $val['prod_type'];
                        $contractDetailModel->prod_num          = $val['prod_num'];
                        $contractDetailModel->price             = $val['price'];
                        $contractDetailModel->amount            = $val['amount'];
                        $contractDetailModel->rate              = $val['rate'];
                        $contractDetailModel->total_amount      = $val['total_amount'];
                        $contractDetailModel->sale_price        = $val['sale_price'];
                        $contractDetailModel->finish_num        = $val['finish_num'];
                        $contractDetailModel->total_num         = $val['total_num'];
                        $contractDetailModel->status            = $val['status'];
                        
                        $contractDetailModel->save(false);
                    }
                    
                    if (isset($data['editContract']) && !empty($data['editContract']))
                    {
                        foreach ($data['editContract'] as $key => $val) {
                            $contract = Contract::findOne(['id'=>$val['id']]);
                            $contract->total_amount = $val['total_amount'];
                            $contract->save(false);
                        }
                    }
                }
                //删除合同信息
                if (isset($data['delContractDetail']) && !empty($data['delContractDetail']))
                {
                    foreach ($data['delContractDetail'] as $key => $val) {
                        
                        $delContractDetail = ContractDetail::findOne(['id'=>$val]);
                        $delContractDetail->status = 2;
                        $delContractDetail->save(false);
                    }
                    
                    if (isset($data['delContract']) && !empty($data['delContract']))
                    {
                        foreach ($data['delContract'] as $key => $val) {
                            $delContract = Contract::findOne(['id'=>$val['id']]);
                            $delContract->total_amount = $val['total_amount'];
                            $delContract->save(false);
                        }
                    }
                }
            }
            
            $transaction->commit();
            return true;
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }
    
    public static function editStatus($data)
    {
        $model = self::findOne(['id'=>$data['id']]);
        $model->status = $data['status'];
        if ($model->save(false)) {
            return true;
        }
        return false;
    }
    
    public static function search($where)
    {
        $db = self::find();
        if (!empty($where)) {
            foreach ($where as $val) {
                $db->andFilterWhere($val);
            }
        }
        return $db->asArray()->all();
    }
}