<?php
    /**
     * Created by PhpStorm.
     * User: xingq
     * Date: 2018/5/14
     * Time: 14:47
     */

    namespace common\models;
    use Yii;
    
    class ScheduleTechnician extends BaseModel
    {
    
        public static function tableName()
        {
            return 'schedule_technician';
        }
        
        /**
         * 获取单条数据
         * User: xingq
         * Email: liuxingqi@services.cn
         * Date: 2018/5/14
         * Time: 14:49
         * @param array $where
         * @return array|null|\yii\db\ActiveRecord
         */
        public static function getOne($where = [])
        {
            return self::find()->where($where)->asArray()->one();
            
        }
    
        /**
         * 获取多条数据
         * User: xingq
         * Email: liuxingqi@services.cn
         * Date: 2018/5/14
         * Time: 14:49
         * @param array $where
         * @return array|\yii\db\ActiveRecord[]
         */
        public static function getList($where = [])
        {
            return self::find()->where($where)->asArray()->all();
        }
        //添加数据
        public static function add($data){
            $model = new self();
            //处理数据
            foreach ($data as $key => $val) {
                $model->$key = $val;
            }
            //print_r($model);exit;
            //开启事务
            $db = Yii::$app->db;
            $transaction = $db->beginTransaction();
            try {
                //库房表添加数据
                $model->save(false);
                $transaction->commit();
                return true;
            } catch(\Exception $e) {
                $transaction->rollBack();
                throw $e;
            }

        }
        //编辑修改数据
        public static function edit($data){
            $tecArr = Technician::findOne(['id' => $data['tech_id']]);
            //查询库房数据
            $model = self::findOne(['tech_id'=>$data['tech_id'],'apply_date'=>$data['apply_date'],'department_top_id'=>$data['department_top_id'],'department_id'=>$tecArr->store_id]);
            //处理数据
            if($model){
                foreach ($data as $key => $val) {
                    $model->$key = $val;
                }
                //开启事务
                $db = Yii::$app->db;
                $transaction = $db->beginTransaction();
                try {
                    $model->save(false);
                    $transaction->commit();
                    return true;
                } catch(\Exception $e) {
                    $transaction->rollBack();
                    throw $e;
                }
            }
            return false;
        }
    }