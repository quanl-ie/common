<?php
namespace common\models;

use Yii;

class SaleOrderView extends BaseModel
{
    public static function tableName()
    {
        return 'sale_order_view';
    }

    public static function primaryKey()
    {
        return ['id'];
    }
}