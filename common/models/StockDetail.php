<?php
namespace common\models;

use yii\db\ActiveRecord;
use common\models\Product;
/**
 * 出入库产品明细模型
 * @author lxq liuxingqi@services.cn
 * Class Product
 * @package webapp\models
 */
class  StockDetail extends BaseModel
{
    
    public static function tableName()
    {
        return 'stock_detail';
    }
    
    /**
     * 获取单条数据
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/3/23
     * Time: 13:40
     * @param array $where
     * @param string $flag
     * @return array|null|ActiveRecord
     */
    public static function getOne($where = [],$flag = 2)
    {
        if ($flag ==1) {
            return self::find()->where($where)->asArray()->one();
        }else{
            return self::find()->where($where)->one();
        }
    }
    /**
     * 获取多条数据
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/3/23
     * Time: 13:39
     * @param array $where
     * @return array|ActiveRecord[]
     */
    public static function getList($where = [])
    {
        return self::find()->where($where)->asArray()->all();
    }
    
    /**
     * 获取关联产品详情
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/4/24
     * Time: 18:31
     * @param array $where
     * @return array|ActiveRecord[]
     */
    public static function getProduct($where = [])
    {
        $db = self::find();
        $db->from(self::tableName() . ' as a');
        //判断where
        if (!empty($where)) {
            foreach ($where as $val) {
                $db->andFilterWhere($val);
            }
        }

        $db->leftJoin([Product::tableName() . ' as b'],' a.prod_id = b.id');
        $db->select('a.id,a.prod_id,a.prod_num,a.depot_id,a.prod_batch,a.prod_date,a.serial_number,a.effective_date,b.prod_name,b.prod_no,b.brand_id,b.class_id,b.type_id,b.model,b.unit_id,b.status');
        $db->asArray();
        $list = $db->all();
        //echo $db->createCommand()->getRawSql();exit;
        //print_r($list);exit;
        return $list;
       
    }

}