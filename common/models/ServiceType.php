<?php

namespace common\models;

use webapp\models\ManufactorFlow;
use webapp\models\ManufactorType;
use Yii;

class ServiceType extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'service_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'department_id', 'status', 'del_status', 'create_id', 'create_time', 'update_id', 'update_time'], 'required'],
            [['id', 'department_id', 'status', 'del_status', 'create_id', 'create_time', 'update_id', 'update_time'], 'integer'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'            => '主键',
            'title'         => '服务类型名称',
            'department_id' => '（部门表）department 表id     0为公有',
            'status'        => '状态，1-正常 | 2-禁用',
            'del_status'    => '删除状态，2-删除 | 1-正常',
            'create_id'     => '创建人id',
            'create_time'   => '创建时间',
            'update_id'     => '修改人id',
            'update_time'   => '更新时间',
        ];
    }

    /**
     * 获取列表
     * @author liuxingqi <lxq@c-ntek.com>
     * @param type $where
     * @return type
     */
    public static function getList($where = [])
    {
        return self::find()->where(['del_status'=>1])->andFilterWhere($where)->asArray()->all();
    }
    
    /**
     * 编辑类型
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/5
     * Time: 17:55
     * @param $data
     * @return bool
     * @throws \Exception
     */
    public static function add($data,$typeIds,$departmentId)
    {
        
        if (isset($data) && !empty($data))
        {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                foreach ($data as $key => $val) {
                    //添加类型
                    $model = new self();
                    
                    $model->title         = $val['title'];
                    $model->department_id = $val['department_id'];
                    $model->sort          = $val['sort'];
                    $model->status        = $val['status'];
                    $model->del_status    = $val['del_status'];
                    $model->create_id     = $val['create_id'];
                    $model->create_time   = $val['create_time'];
                    $model->update_id     = $val['update_id'];
                    $model->update_time   = $val['update_time'];
                    
                    if (!$model->save(false)) {
                        $transaction->rollBack();
                        return false;
                    }
                    
                    //添加类型关联关系数据
                    $manType = new ManufactorType();
                    
                    $manType->department_id     = $departmentId;
                    $manType->direct_company_id = $val['department_id'];
                    $manType->type_id           = $model->id;
                    $manType->status            = $val['status'];
                    $manType->del_status        = $val['del_status'];
                    $manType->create_time       = $val['create_time'];
                    $manType->update_time       = $val['update_time'];
                    
                    if (!$manType->save(false)) {
                        $transaction->rollBack();
                        return false;
                    }
                    
                    //添加服务流程
                    $flow = new ServiceFlow();
                    
                    $flow->department_id = $val['department_id'];
                    $flow->type_id       = $model->id;
                    $flow->title         = $val['title'];
                    $flow->is_default    = 1;
                    $flow->status        = $val['status'];
                    $flow->del_status    = $val['del_status'];
                    $flow->oprate_id     = $val['create_id'];
                    $flow->create_time   = $val['create_time'];
                    $flow->update_time   = $val['update_time'];
                    
                    if (!$flow->save(false)) {
                        $transaction->rollBack();
                        return false;
                    }
                    
                }
                $transaction->commit();
                return true;
            } catch(\Exception $e) {
                $transaction->rollBack();
                throw $e;

            }

        }
    }
    
    /**
     * 修改类型状态（禁用、启用）
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/5
     * Time: 13:39
     * @param $id
     * @param $status
     * @return bool
     */
    public static function changeStatus($id,$status)
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            
            $time   = time();
            $userId = Yii::$app->user->id;
            
            //服务类型
            $model = self::findOne(['id'=>$id]);
            $model->status      = $status;
            $model->update_id   = $userId;
            $model->update_time = $time;
    
            if (!$model->save(false)) {
                $transaction->rollBack();
                return false;
            }
            
            //服务流程
            $flow = ServiceFlow::getList(['type_id'=>$id]);
            
            if (!empty($flow))
            {
                $flowId = array_column($flow,'id');
                $attr = [
                    'status'      => $status,
                    'oprate_id'   => $userId,
                    'update_time' => $time,
                ];
    
                ServiceFlow::updateAll($attr,['id' =>$flowId]);

            }
            
            $transaction->commit();
            return true;
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        
        }
        
        
        
        
        
        
    }
    
    
    /**
     * 根据id获取名称数组
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/4/11
     * Time: 9:54
     * @param $id
     * @return array
     */
    public static function getName($id)
    {
        $type = self::find()->where(['id'=>$id])->asArray()->all();
        $data = [];
        if (!empty($type)) {
            $data = array_column($type,'title','id');
        }
        return $data;
    }
    /**
     *  获取品牌
     * @date: 2018-6-7
     * @author: liquan
     * @param: variable
     * @return: array
     */
    public static function getTypeName($id=null)
    {
        $data = [];
        $typeData = self::find()
            ->select('id,title')
            ->andWhere(['status'=>'1','isdelete'=>'0']);
        $typeData = $typeData->asArray()->all();
        if(!empty($typeData)){
            foreach ($typeData as $v){
                $data[$v['id']]=$v['title'];
            }
        }
        return $data;
    }
    public static function getTitle($id=null)
    {

        $brandData = self::find()
            ->select('title')
            ->where(['id'=>$id])->asArray()->one();
        if($brandData){
            return $brandData['title'];
        }
        return '';
    }
    /**
     * 获取服务类型
     * @author liwenyong
     * @date    2017-12-20
     */
    public static function getServiceType() {
        $data = self::find()->where(['status'=>1,'del_status' => 1])->asArray()->all();
        $serviceTypeArr = [];
        if(!$data) {
            return $serviceTypeArr;
        }
        foreach ($data as $v) {
            $serviceTypeArr[$v['id']] = $v['title'];
        }
        return $serviceTypeArr;
    }
}
