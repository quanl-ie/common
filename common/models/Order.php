<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;

class Order extends ActiveRecord
{
    public static function getDb() {
        return Yii::$app->order_db;
    }
    
    public static function tableName()
    {
        return 'work_order';
    }

    /**
     * 查出服务时间
     * @param $index
     * @return mixed|string
     */
    public static function getPlantimeType($plantimetype,$plantime,$format='Y-m-d')
    {
        $data = [
            1 => '全天',
            2 => '上午',
            3 => '下午',
            4 => '晚上',
            5 => '具体时间'
        ];

        if($plantimetype == 5){
            return $plantime;
        }
        else {
            return date($format,strtotime($plantime)) . ' '. (isset($data[$plantimetype])?$data[$plantimetype]:'');
        }
    }

    public static function findOneByOrderNo($orderNo)
    {
        $result = [];
        $where = [
            'a.order_no'   => $orderNo,
        ];

        $query = self::find()
            ->from(self::tableName() . ' as a')
            ->innerJoin([OrderDetail::tableName() . ' as b'],' a.order_no = b.order_no')
            ->where($where)
            ->select('a.*,b.sale_order_id')
            ->asArray()->one();
        if($query)
        {
            return $query;
        }
        return $result;
    }
}