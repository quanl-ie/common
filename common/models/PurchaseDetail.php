<?php

namespace common\models;

use Yii;
class PurchaseDetail extends BaseModel
{
    /**
     * @inheritdoc 采购单详情表
     */
    public static function tableName()
    {
        return 'purchase_detail';
    }
    public static function getOne($where = [])
    {
        return self::find()->where($where)->asArray()->one();
    }
    public static function getList($where = [],$flag=0)
    {
        if($flag){
            return self::find()->where($where)->select("parent_id,sum(`prod_num`) as total_prod_num")->groupBy("parent_id")->asArray()->all();
        }
        return self::find()->where($where)->asArray()->all();
    }
    public static function getIndexList ($maps,$where,$page,$pageSize)
    {
        $db = self::find();
        $db->where($maps);
        //判断where
        if (!empty($where)) {
            foreach ($where as $val) {
                $db->andFilterWhere($val);
            }
        }
        //总数
        $totalNum = $db->count();
        //当有结果时进行组合数据
        if ($totalNum>0) {
            //总页数
            $totalPage = ceil($totalNum/$pageSize);
            if($page<1)
            {
                $page = 1;
            }
            else if($page>$totalPage)
            {
                $page = $totalPage;
            }
            $db->offset(($page-1)*$pageSize);
            $db->orderBy(['id'=> SORT_DESC]);
            $db->limit($pageSize);
            //print_r($db->createCommand()->getRawSql());die;
            $db->asArray();
            $list = $db->all();
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => $totalPage,
                'list'       => $list
            ];
        }
        else
        {
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => 0,
                'list'       => []
            ];
        }
    }
    //添加数据
    public static function add($data){
        $model = new self();
        //处理数据
        foreach ($data as $key => $val) {
            $model->$key = $val;
        }
        //print_r($model);exit;
        //开启事务
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();
        try {
            //库房表添加数据
            $model->save(false);
            $transaction->commit();
            return true;
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }

    }
    //编辑修改数据
    public static function edit($data){
        //查询库房数据
        $model = self::findOne(['id'=>$data['id']]);
        //处理数据
        foreach ($data as $key => $val) {
            $model->$key = $val;
        }
        //开启事务
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();
        try {
            $model->save(false);
            $transaction->commit();
            return true;
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }

    }
    //获取产品明细
    public static function getProduct($where = [])
    {
        $db = self::find();
        $db->from(self::tableName() . ' as a');
        //判断where
        if (!empty($where)) {
            foreach ($where as $val) {
                $db->andFilterWhere($val);
            }
        }
        $db->leftJoin([Product::tableName() . ' as b'],' a.prod_id = b.id');
        $db->select('a.id,a.prod_id,a.prod_num,a.price,a.total_amount,a.finish_num,a.return_num,b.prod_name,b.prod_no,b.brand_id,b.class_id,b.type_id,b.model,b.unit_id,b.purchase_price,b.suggest_sale_price,b.status');
        $db->asArray();
        $list = $db->all();

        return $list;

    }
}
