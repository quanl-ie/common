<?php
namespace common\models;

use webapp\models\ManufactorFlow;
use Yii;
use yii\db\ActiveRecord;

/**
 * 服务区域
 * @author liuxingqi <lxq@c-ntek.com>
 * @date 2018-01-31
 */
class ServiceFlow extends BaseModel
{

    public static function tableName()
    {
        return 'service_flow';
    }
    
    /**
     * 获取单个服务流程
     * @author liuxingqi <lxq@c-ntek.com>
     * @param type $where
     * @return type
     */
    public static function getOne($where = [])
    {
        return self::find()->where(['status'=>1])->andWhere($where)->one();
    }
    
    /**
     * 获取多个服务流程
     * @author liuxingqi <lxq@c-ntek.com>
     * @param type $where
     * @return type
     */
    public static function getList($where = [])
    {
        return self::find()->where(['status'=>[1,2]])->andWhere($where)->asArray()->all();
    }
    
    /**
     * 添加和编辑服务流程
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/15
     * Time: 15:39
     * @param $data
     * @param $id
     * @return bool
     * @throws \Throwable
     */
    public static function add($data,$id)
    {
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();
        try {
        
            //修改服务流程状态
            if ($id != 0)
            {
                $model = self::findOne(['id'=>$id]);
                $model->status            = 3;
                $model->oprate_id         = $data['oprate_id'];
                $model->update_time       = $data['update_time'];
                
                if (!$model->save(false)) {
                    $transaction->rollBack();
                    return false;
                }
            }
            
            //添加新的服务流程
            if (!empty($data))
            {
                $model = new self();
                foreach ($data as $key => $val) {
                    $model->$key = $val;
                }
                if (!$model->save(false)) {
                    $transaction->rollBack();
                    return false;
                }
            }
            
            $transaction->commit();
            return true;
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
    
        }
    }
    
    /**
     * 修改状态
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/15
     * Time: 15:38
     * @param $data
     * @return bool
     */
    public static function changeStatus($data)
    {
    
        if (empty($data)) {
            return false;
        }
    
        $model = self::findOne(['id' => $data['id']]);
        unset($data['id']);
        
        foreach ($data as $key => $val) {
            $model->$key = $val;
        }
        
        if ($model->save(false)) {
            return true;
    
        }
        return false;
    }

    public static function getServiceProcess($id, $flow_id)
    {
        $data = self::find()->where(['type_id' => $id, 'id' => $flow_id, 'status' => 1])->asArray()->all();
        $process = [];
        foreach ($data as $k => $v) {
            $process[$k]['id']                = $v['id'];
            $process[$k]['service_flow_name'] = $v['title'];
        }
        return $process;
    }

    public static function getTitle($id)
    {
        $query = self::find()->where(['id' => $id])->asArray()->one();
        if($query){
            return $query['title'];
        }

        return '';
    }

    /**
     * 根据id 查出客户信息
     * @param array $ids
     * @return array
     */
    public static function getFlowByIds(array $ids)
    {
        $where = [
            'id'     => $ids,
            'status' => 1
        ];
        $result = self::findAllByAttributes($where , 'id,title','id');
        return $result;
    }

    /**
     * 获取默认服务流程
     * @param $topDepartmentId
     * @param $typeId
     * @return int|mixed
     * @author xi
     */
    public static function getDefaultWorkStage($topDepartmentId,$typeId)
    {
        $where = [
            'department_id' => $topDepartmentId,
            'type_id'       => $typeId,
            'status'        => 1,
            'del_status'    => 1,
            'is_default'    => 1
        ];

        $query =  self::findOneByAttributes($where,'id');
        if($query){
            return $query['id'];
        }

        return 0;
    }
}