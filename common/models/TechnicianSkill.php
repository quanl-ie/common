<?php

namespace common\models;
use yii\db\ActiveRecord;

class TechnicianSkill extends ActiveRecord
{
    public static function tableName()
    {
        return 'technician_skill';
    }
}