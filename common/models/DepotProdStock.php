<?php

namespace common\models;

use Yii;

class DepotProdStock extends BaseModel
{
    /**
     * @inheritdoc 库房库存表
     */
    public static function tableName()
    {
        return 'depot_prod_stock';
    }
    
    /**
     * 根据条件获取单条数据
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/3/21
     * Time: 16:49
     * @param array $where
     * @return array|null|\yii\db\ActiveRecord
     */
    public static function getOne($where = [],$falg = '')
    {
        if ($falg) {
            return self::find()->where($where)->one();
        }
        return self::find()->where($where)->asArray()->one();
    }
    
    /**
     * 根据条件获取多条数据
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/3/21
     * Time: 16:49
     * @param array $where
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getList($where = [],$flag = 0)
    {
        if($flag == 0){
            return self::find()->where($where)->asArray()->all();
        }
        $db = self::find();
        //判断where
        if (!empty($where)) {
            foreach ($where as $val) {
                $db->andFilterWhere($val);
            }
        }
        $db->asArray();

        return $db->asArray()->all();
    }
    
    /**
     * 库存管理
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/3/30
     * Time: 15:13
     * @param $map
     * @param $where
     * @param int $page
     * @param int $pageSize
     * @return array
     */
    public static function getIndex($map,$where,$page=1,$pageSize=10)
    {
        $db = self::find();
        $db->from(self::tableName() . ' as a');
        $db->where($map);
        //判断where
        if (!empty($where)) {
            foreach ($where as $val) {
                $db->andFilterWhere($val);
            }
        }

        //总数
        $totalNum = $db->count();
        //当有结果时进行组合数据
        if ($totalNum>0) {
            //总页数
            $totalPage = ceil($totalNum/$pageSize);
            if($page<1)
            {
                $page = 1;
            }
            else if($page>$totalPage)
            {
                $page = $totalPage;
            }
            $db->leftJoin([Product::tableName() . ' as b'],' a.prod_id = b.id');
            $db->select('a.prod_id,a.prod_batch,sum(a.bad_num) as bad_num,sum(a.num) as num,sum(a.freeze_num) as freeze_num,sum(a.total_num) as total_num,sum(a.on_sale_num) as on_sale_num,a.depot_id,b.prod_name,b.brand_id,b.class_id,b.unit_id,b.prod_no');
            $db->offset(($page-1)*$pageSize);
            $db->groupBy('a.prod_id');
            $db->orderBy(['total_num'=> SORT_DESC]);
            $db->limit($pageSize);
            $db->asArray();
            $list = $db->all();
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => $totalPage,
                'list'       => $list
            ];
        }
        else
        {
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => 0,
                'list'       => []
            ];
        }
    }
    
    public static function getPagData($map,$page=1,$pageSize=10,$where=[])
    {
        $db = self::find();
        $db->where($map);
        //判断where
        if (!empty($where)) {
            foreach ($where as $val) {
                $db->andFilterWhere($val);
            }
        }
    
        //总数
        $totalNum = $db->count();
        //当有结果时进行组合数据
        if ($totalNum>0) {
            //总页数
            $totalPage = ceil($totalNum/$pageSize);
            if($page<1)
            {
                $page = 1;
            }
            else if($page>$totalPage)
            {
                $page = $totalPage;
            }
            $db->offset(($page-1)*$pageSize);
            $db->orderBy(['total_num'=> SORT_DESC]);
            $db->limit($pageSize);
            $db->asArray();
            $list = $db->all();
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => $totalPage,
                'list'       => $list
            ];
        }
        else
        {
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => 0,
                'list'       => []
            ];
        }
    }
    public static function updateNum($data,$type=0){

        $model = self::findOne(['direct_company_id'=>$data['direct_company_id'],'prod_id'=>$data['prod_id'],'depot_id'=>$data['depot_id']]);
        //处理数据
        if($type == 0){
            $model->on_sale_num = intval($model->on_sale_num) + intval($data['prod_num']);
        }else{
            $model->on_sale_num = intval($model->on_sale_num) - intval($data['prod_num']);
        }
        //print_r($model);exit;
        //开启事务
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();
        try {
            $model->save(false);
            $transaction->commit();
            return true;
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }
}
