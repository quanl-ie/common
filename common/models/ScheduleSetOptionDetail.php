<?php
    /**
     * Created by PhpStorm.
     * User: xingq
     * Date: 2018/5/14
     * Time: 14:47
     */
    
    namespace common\models;
    
    
    class ScheduleSetOptionDetail extends BaseModel
    {
    
        public static function tableName()
        {
            return 'schedule_set_option_detail';
        }
        
        /**
         * 获取单条数据
         * User: xingq
         * Email: liuxingqi@services.cn
         * Date: 2018/5/14
         * Time: 14:49
         * @param array $where
         * @return array|null|\yii\db\ActiveRecord
         */
        public static function getOne($where = [])
        {
            return self::find()->where($where)->asArray()->one();
            
        }
    
        /**
         * 获取多条数据
         * User: xingq
         * Email: liuxingqi@services.cn
         * Date: 2018/5/14
         * Time: 14:49
         * @param array $where
         * @return array|\yii\db\ActiveRecord[]
         */
        public static function getList($where = [])
        {

            $db = self::find();
            $db->from(self::tableName() . ' as a');
            //判断where
            if (!empty($where)) {
                foreach ($where as $val) {
                    $db->andFilterWhere($val);
                }
            }
            $db->leftJoin([ScheduleSetOption::tableName() . ' as b'],' a.option_id = b.id');
            $db->select('a.tech_id,b.schedule_set_id,b.repeate_type,b.repeate_date,b.end_date,b.description,b.create_time');
            $db->asArray();
            
            $list = $db->all();
            return $list;
        }
    
    
    
        /**
         * 技师排班设置组合
         * User: xingq
         * Email: liuxingqi@services.cn
         * Date: 2018/5/17
         * Time: 13:36
         * @param $techId
         * @param $srcType
         * @param $srcId
         * @param string $startTime
         * @param string $days
         * @return array
         */
        public static function getSetOptionDetail($techId,$departmentId,$startTime = '',$days = '')
        {
            //查询单独设置的班次
            $scheduleTechnician = ScheduleTechnician::getList(['department_id'=>$departmentId,'tech_id'=>$techId]);
            //处理数据
            $scheduleTechnicianData = [];
            $order = [];
            if (!empty($scheduleTechnician)) {
                foreach ($scheduleTechnician as $key => $val) {
                    if (in_array($val['tech_id'],$techId)) {
                        $scheduleTechnicianData[$key]['tech_id'] = $val['tech_id'];
                        $scheduleTechnicianData[$key]['schedule_set_id'] = $val['schedule_set_id'];
                        $scheduleTechnicianData[$key]['repeate_type'] = 0;
                        $scheduleTechnicianData[$key]['repeate_date'] = $val['apply_date'];
                        $scheduleTechnicianData[$key]['end_date'] = $val['apply_date'];
                        $scheduleTechnicianData[$key]['description'] = $val['description'];
                        $scheduleTechnicianData[$key]['create_time'] = $val['create_time'];
                    
                        //取工单数据
                        $order[$key]['tech_id'] = $val['tech_id'];
                        $order[$key]['finish_order'] = $val['finish_order'];
                        $order[$key]['all_order'] = $val['all_order'];
                        $order[$key]['apply_date'] = $val['apply_date'];
                    }
                }
            }
            $detail = ScheduleSetOptionDetail::getList([['in','a.tech_id',$techId]]);
            $mergeArr = array_merge_recursive($scheduleTechnicianData,$detail);

            $data = [];
            //根据查询的数据计算出所有已设置班次的日期
            foreach ($mergeArr as $key => $val) {
            
                for($i=0;$i<$days;$i++){
                    $day = date('Y-m-d',$startTime+$i*86400);
                    $endDay = $val['end_date'] ? $val['end_date'] : $day;

                   switch ($val['repeate_type']) {
                        case 0:
                            
                            if (date('Y-m-d',strtotime($endDay)) == $day) {
                                $data[$val['tech_id']][$day][$val['create_time']]['tech_id'] = $val['tech_id'];
                                $data[$val['tech_id']][$day][$val['create_time']]['description'] = $val['description'];
                                $data[$val['tech_id']][$day][$val['create_time']]['schedule_set_id'] = $val['schedule_set_id'];
                            
                            }
                            break;
                        case 1:
                            if ($day >= date('Y-m-d',strtotime($val['create_time'])) && date('Y-m-d',strtotime($endDay)) >= $day) {
                                $data[$val['tech_id']][$day][$val['create_time']]['tech_id'] = $val['tech_id'];
                                $data[$val['tech_id']][$day][$val['create_time']]['description'] = $val['description'];
                                $data[$val['tech_id']][$day][$val['create_time']]['schedule_set_id'] = $val['schedule_set_id'];
                            
                            }
                            break;
                        case 2:
                            $week = date("N",strtotime(date($day)));
                            $weeks = explode(',',$val['repeate_date']);
                        
                            if ($day >= date('Y-m-d',strtotime($val['create_time'])) && date('Y-m-d',strtotime($endDay)) >= $day && in_array($week,$weeks)) {
                                $data[$val['tech_id']][$day][$val['create_time']]['tech_id'] = $val['tech_id'];
                                $data[$val['tech_id']][$day][$val['create_time']]['description'] = $val['description'];
                                $data[$val['tech_id']][$day][$val['create_time']]['schedule_set_id'] = $val['schedule_set_id'];
                            
                            }
                            break;
                        case 3:
                            $repeateDate = explode(',',$val['repeate_date']);

                            if ($day >= date('Y-m-d',strtotime($val['create_time'])) && date('Y-m-d',strtotime($endDay)) >= $day && in_array(date('d',strtotime($day)),$repeateDate)) {
                                $data[$val['tech_id']][$day][$val['create_time']]['tech_id'] = $val['tech_id'];
                                $data[$val['tech_id']][$day][$val['create_time']]['description'] = $val['description'];
                                $data[$val['tech_id']][$day][$val['create_time']]['schedule_set_id'] = $val['schedule_set_id'];
                            
                            }
                            break;
                        case 4:
                            if ($day >= date('Y-m-d',strtotime($val['create_time'])) && date('Y-m-d',strtotime($val['repeate_date'])) <= $day && date('Y-m-d',strtotime($endDay)) >= $day) {
                                $data[$val['tech_id']][$day][$val['create_time']]['tech_id'] = $val['tech_id'];
                                $data[$val['tech_id']][$day][$val['create_time']]['description'] = $val['description'];
                                $data[$val['tech_id']][$day][$val['create_time']]['schedule_set_id'] = $val['schedule_set_id'];
                            
                            }
                            break;
                    }
                }
            }

            //根据最后排版设置时间选取最近时间设置的标准
            $res = [];
            foreach ($data as $key => $val) {
                foreach ($val as $k => $v) {
                    $arr = max(array_keys($v));
                    $res[$key][$k] = $v[$arr];
                }
            }
            
            //组合技师完成单量数据
            foreach ($order as $key => $val) {
                if (isset($res[$val['tech_id']][date('Y-m-d', strtotime($val['apply_date']))])) {
                    $res[$val['tech_id']][date('Y-m-d', strtotime($val['apply_date']))]['finsh_order'] = $val['finish_order'];
                    $res[$val['tech_id']][date('Y-m-d', strtotime($val['apply_date']))]['all_order'] = $val['all_order'];
                }
            
            }
        
            return $res;
        
        }
        
        
        
        
    }