<?php
namespace common\models;

use Yii;

class Department extends BaseModel
{
    public static function tableName()
    {
        return 'department';
    }

    /**
     * 获取顶级 id
     * @param $departmentId
     * @return mixed
     * @author xi
     * @date 2018-6-28
     */
    public static function getDepartmentTopId($departmentId)
    {
        $departmentTopId = 0;
        $query = self::findOneByAttributes(['id'=>$departmentId],'parent_ids');
        if($query)
        {
            if($query['parent_ids'] == '' || $query['parent_ids'] == 0){
                $departmentTopId = $departmentId;
            }
            else {
                $departmentTopId = explode(',',$query['parent_ids'])[0];
            }
        }

        return $departmentTopId;
    }

    /**
     * 获取直属id
     * @param $departmentId
     * @return int|mixed
     * @author xi
     */
    public static function getDirectCompanyId($departmentId)
    {
        $query = self::findOneByAttributes(['id'=>$departmentId],'direct_company_id');
        if($query)
        {
            return $query['direct_company_id'];
        }
        return 0;
    }

}