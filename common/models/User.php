<?php
namespace common\models;

use common\models\oauth\OauthAccessTokens;
use OAuth2\Storage\UserCredentialsInterface;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */
class User extends ActiveRecord implements IdentityInterface,UserCredentialsInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 1;
    const verified_code = '';


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null )
    {
        $user_token = OauthAccessTokens::findOne(['access_token' => $token,'client_id' => $type]);
        if($user_token)
            return self::findOne(['id' => $user_token->user_id]);
        return false;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username,$department_id)
    {
        return static::findOne(['username' => $username,'department_id'=>$department_id,'status' => self::STATUS_ACTIVE]);
    }
    
    public static function findByMobile($mobile)
    {
        return static::findOne([
            'mobile' => $mobile,
            'status' => self::STATUS_ACTIVE
        ]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }
    

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * Returns static class instance, which can be used to obtain meta information.
     * @param bool $refresh whether to re-create static instance even, if it is already cached.
     * @return static class instance.
     */
    public static function instance ($refresh = false)
    {
        // TODO: Implement instance() method.
    }
    
    /**
     * 根据ID所属id查询用户新明
     */
    public static function getCreateName($id) {
        $data = self::find()->where(['id' => $id])->select(['username'])->asArray()->one();
        return $data['username'];
    }
    /**
     * 根据ID所属id查询用户  2018-5-9
     */
    public static function getCreateNames($ids_arr) {
        $ids = array_unique($ids_arr);
        $userRes = self::find()->where(['id'=>$ids])->select('id,company,username,nick_name,mobile')->asArray()->all();
        $userArr = [];
        foreach ($userRes as $val){
            if ($val['username']) {
                $userArr[$val['id']] = $val['username'];
            } else {
                $userArr[$val['id']] = $val['mobile'];
            }
        }
        return $userArr;
    }

    /**
     * 根据ids 查创建人信息
     * @param $ids
     * @return array
     */
    public static function getCreateNameByIds($ids)
    {
        $data = self::find()->where(['id' => $ids])->select('id,username')->asArray()->all();
        if($data){
            return array_column($data,'username','id');
        }
        return [];
    }

    public static function getOne($where){

        return static::find()->where($where)->asArray()->one();
    }
    public static function getList($where){

        return static::find()->where($where)->asArray()->all();
    }

    public function checkUserCredentials($username, $password)
    {
        $user = static::findByMobile($username);
        if(!$user)
            return false;
        return $user->validatePassword($password);
    }

    public function getUserDetails($username)
    {
        $user = static::findByMobile($username);
        return ['user_id'=>$user->getId()];
    }

}
