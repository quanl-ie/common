<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;

class  BaseModel extends ActiveRecord
{
    //商家
    const SRC_SJ  = 14;
    //服务商
    const SRC_FWS = 13;
    //门店
    const SRC_MD  = 12;
    //技师
    const SRC_JS  = 11;

    // 1 商家 2 服务商 3 门店 4 小程序
    const SOURCE_SJ  = 1;
    const SOURCE_FWS = 2;
    const SOURCE_MD  = 3;
    const SOURCE_XCX = 4;
    /**
     * 根据条件查询所有数据
     * @param string/array $where
     * @param string $select
     * @return array
     * @author xi
     * @date 2017-12-15
     */
    public static function findAllByAttributes($where, $select="*", $index = false,$limit = 1000,$group='',$order='')
    {
        $db = static::find();
        $db->where($where);
        $db->select($select);

        if($group!=''){
            $db->groupBy($group);
        }
        if($order!=''){
            $db->orderBy($order);
        }
        $db->limit($limit);
        $db->asArray();
        $query = $db->all();;

        if($query)
        {
            if( is_string($index) && in_array($index,array_keys((new static())->getAttributes())) )
            {
                $result = [];
                foreach ($query as $val){
                    $result[$val[$index]] = $val;
                }
                return $result;
            }
    
            return $query;
        }
        return [];
    }
    
    /**
     * 根据条件查出一条数据
     * @param array/string $where
     * @param string $returnAttr
     * @return \yii\db\ActiveRecord|NULL|string
     * @author xi
     * @date 2017-12-15
     */
    public static function findOneByAttributes($where, $select="*", $returnAttr='')
    {
        $query = static::find()
        ->where($where)
        ->select($select)
        ->asArray()
        ->one();
        if($query)
        {
            if($returnAttr!='' && is_string($returnAttr) && in_array($returnAttr,array_keys((new static())->getAttributes())) )
            {
                return $query[$returnAttr];
            }
            return $query;
        }
        return [];
    }
}