<?php

namespace common\models;

use Yii;

class GlobalAccount extends BaseModel
{

    //机构用户
    const TYPE_JG = 1;
    //客户
    const TYPE_KH = 2;
    //技师
    const TYPE_JS = 3;

    public static function tableName()
    {
        return 'global_account';
    }
}
