<?php
namespace common\models;

use common\helpers\Helper;
use webapp\logic\BaseLogic;
use Yii;
use yii\db\ActiveRecord;

class WorkRelTechnician extends ActiveRecord
{
    public static function getDb() {
        return Yii::$app->order_db;
    }
    
    public static function tableName()
    {
        return 'work_rel_technician';
    }

    /**
     * 查出技师
     * @param $workNo
     * @return int|mixed
     */
    public static function getOldTechnicianId($workNo)
    {
        $query = self::find()
            ->where(['work_no'=>$workNo,'type'=>2,'push_status'=>1])
            ->select('technician_id,id')
            ->orderBy('id desc')
            ->asArray()
            ->all();
        if($query)
        {
            return $query;
        }
        return 0;
    }
    /**
     * 查出工单负责人
     * @param $workNo
     * @return int|mixed
     */
    public static function getTechnicianInfo($workNo)
    {
        $query = self::find()
            ->where(['work_no'=>$workNo,'is_self'=>[1,2]])
            ->select('work_no,technician_id,is_self')
            ->asArray()
            ->all();

        if($query)
        {
            $arr1 = [];
            foreach ($query as $val){
                $arr1[$val['work_no']][$val['is_self']] = $val;
            }

            $result = [];
            foreach ($arr1 as $val)
            {
                if(isset($val[1])){
                    $result [] = $val[1];
                }
                else if(isset($val[2])){
                    $result[] = $val[2];
                }
            }
            unset($arr1);
            return $result;
        }
        return [];
    }
    public static function chagePushStatus($id,$status=2)
    {
        return self::updateAll(['push_status'=>2],['id'=>$id]);
    }

    /**
     * 获取工单指派情况
     * @param $workNo
     * @return array
     */
    public static function getTechnicianIdByWorkNo($workNo,$andWhere='')
    {
        $result = [
            'groupLender' => [],
            'lender' => 0,
            'groupingId' => 0,
            'member' => []
        ];

        $db = self::find();
        $db->where(['work_no'=>$workNo,'type'=>1]);
        if($andWhere){
            $db->andWhere($andWhere);
        }
        $db->select('technician_id,is_self');
        $db->asArray();
        $query = $db->all();
        if($query)
        {

            foreach ($query as $val){
                if($val['is_self'] == 1){
                    $result['lender'] = $val['technician_id'];
                }
                if($val['is_self'] == 2){
                    $result['groupLender'][] = $val['technician_id'];
                }
                if($val['is_self'] == 3 || $val['is_self'] == 1){
                    $result['member'][] = $val['technician_id'];
                }
            }

            if($result['member']){
                 $result['groupingId'] = Common::getGroupingIdByJsId(BaseModel::SRC_FWS,BaseLogic::getManufactorId() ,$result['member'][0]);
            }
            else if($result['groupLender']) {
                $result['groupingId'] = Common::getGroupingIdByJsId(BaseModel::SRC_FWS,BaseLogic::getManufactorId(),$result['groupLender'][0]);
            }
        }
        return $result;
    }
}