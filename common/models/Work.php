<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;

class Work extends BaseModel
{
    public static function getDb() {
        return Yii::$app->order_db;
    }
    
    public static function tableName()
    {
        return 'work';
    }
}