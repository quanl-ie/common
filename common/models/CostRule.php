<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;

class CostRule extends ActiveRecord
{
    
    public static function getDb() {
        return Yii::$app->order_db;
    }
    public static function tableName()
    {
        return 'cost_rule';
    }
    public static function getOne($where = []) {
        return self::find()->where($where)->asArray()->one();
    }

    /**
     * 获取商家规则列表
     * @author liuxingqi <lxq@c-ntek.com>
     * @param type $where
     * @return type
     */
    public static function getManufactorList($where = [])
    {
        return self::find()->where($where)->asArray()->all();
    }
    
    public static function getCostIdList($where = [],$andWhere = [])
    {
        return self::find()->where($where)->andWhere($andWhere)->asArray()->all();
    }

    /**
     * 查出规则详情id
     * @param $costRuleId
     * @param $brandId
     * @param $classId
     * @param $typeId
     * @param $area
     * @return int|mixed
     */
    public static function getCostRuleDetailId($costRuleId,$brandId,$classId,$typeId,$area)
    {
        $where = [
            'a.id'       => $costRuleId,
            'a.status'   => 1,
            'b.status'   => 1,
            'b.brand_id' => $brandId,
            'b.class_id' => $classId,
            'b.type_id'  => $typeId
        ];
        $query = self::find()
            ->from( self::tableName() . ' as a')
            ->innerJoin(['`' . CostRuleDetail::tableName() . '` as b'], ' a.id = b.cost_rule_id ')
            ->where($where)
            ->andWhere("FIND_IN_SET('".$area."',service_area)")
            ->select('b.id')
            ->asArray()
            ->one();
        if($query)
        {
            return $query['id'];
        }
        return 0;
    }
}