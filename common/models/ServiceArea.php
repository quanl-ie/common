<?php

namespace common\models;

use Yii;
use webapp\models\ManufactorArea;
use webapp\logic\BaseLogic;


class ServiceArea extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'service_area';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'department_id', 'area', 'status', 'del_status', 'oprate_id', 'create_time', 'update_time'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'            => '服务区域主键',
            'department_id' => '（部门表）department 表id',
            'area'          => '地区字符串   省id_市id_区id   逗号分隔  默认为全部省市区   0_0_0',
            'status'        => '状态，1-正常 | 2-禁用',
            'del_status'    => '删除状态，2-删除 | 1-正常',
            'oprate_id'     => '操作人id',
            'create_time'   => '创建时间',
            'update_time'   => '更新时间',
        ];
    }
    
    public static function getList($where)
    {
        return self::find()->where(['del_status'=>1])->andFilterWhere($where)->asArray()->all();
    }

    /**
     * 编辑服务区域
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/5
     * Time: 17:55
     * @param $data
     * @return bool
     * @throws \Exception
     */
    public static function add($data)
    {
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();
        try {

            //添加服务区域
            if (isset($data['add']) && !empty($data['add']))
            {
                //添加服务区域
                foreach ($data['add'] as $key => $val) {
                    $model = new self();
                    $model->department_id = $val['department_id'];
                    $model->area          = $val['area'];
                    $model->status        = $val['status'];
                    $model->del_status    = $val['del_status'];
                    $model->oprate_id     = $val['oprate_id'];
                    $model->create_time   = $val['create_time'];
                    $model->update_time   = $val['update_time'];
                    
                    if ($model->save(false))
                    {
                        //添加服务区域关系
                        
                        $manArea = new ManufactorArea();
                        $manArea->department_id     = BaseLogic::getDepartmentId();
                        $manArea->direct_company_id = $val['department_id'];
                        $manArea->area_id           = $model->id;
                        $manArea->oprate_id         = $val['oprate_id'];
                        $manArea->create_time       = $val['create_time'];
                        $manArea->update_time       = $val['update_time'];
                        
                        if (!$manArea->save(false)) {
                            $transaction->rollBack();
                            return false;
                        }
                        
                    }
                    else
                    {
                        $transaction->rollBack();
                        return false;
                    }
                }
            }
            
            //删除服务区域
            if (isset($data['del']) && !empty($data['del']))
            {
                foreach ($data['del'] as $val) {
                    $model = self::findOne(['id'=>$val['id']]);
                    $model->status      = $val['status'];
                    $model->oprate_id   = $val['oprate_id'];
                    $model->update_time = $val['update_time'];
    
                    if (!$model->save(false)) {
                        $transaction->rollBack();
                        return false;
                    }
                    
                }
            }
    
            //修改服务区域
            if (isset($data['edit']) && !empty($data['edit']))
            {
                foreach ($data['edit'] as $val) {
                    $model = self::findOne(['id'=>$val['id']]);
                    $model->status      = $val['status'];
                    $model->oprate_id   = $val['oprate_id'];
                    $model->update_time = $val['update_time'];
            
                    if (!$model->save(false)) {
                        $transaction->rollBack();
                        return false;
                    }
            
                }
            }
            
            $transaction->commit();
            return true;
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;

        }
    }

}
