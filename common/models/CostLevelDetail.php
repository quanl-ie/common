<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;

class CostLevelDetail extends ActiveRecord
{
    public static function getDb() {
        return Yii::$app->order_db;
    }
    
    public static function tableName()
    {
        return 'cost_level_detail';
    }
}