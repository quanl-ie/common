<?php
/**
 * Created by PhpStorm.
 * 消息推送APP 
 * User: quanl
 * Date: 2018/6/26
 * Time: 15:36
 */
namespace common\models;
use common\lib\Push;
use webapp\modules\v1\models\SaleOrderView;
use webapp\modules\v1\models\WorkOrderTechnician;
use yii;
use yii\db\ActiveRecord;

class PushMessage extends ActiveRecord
{
      public static function getDb() {
            return Yii::$app->order_db;
      }
      public static function tableName()
      {
            return 'work_order';
      }
      /**
       * 处理业务逻辑
       * @param $order_no $flag 标记来源
       */
      public static function push($order_no,$flag = '',$work_no='',$technicianIds=array())
      {
            $databaseName = explode('dbname=',ServiceType::getDb()->dsn)[1];

            if($flag == ''){
                  return false;
            }
            $where = "a.order_no =".$order_no;
            $query = self::find()
                  ->from(self::tableName() . ' as a ')
                  ->leftJoin(['`'.Work::tableName().'` as d'], 'a.order_no = d.order_no')
                  ->leftJoin(['`'.WorkOrderDetail::tableName().'` as c'], 'a.order_no = c.order_no')
                  ->leftJoin(['`'.$databaseName.'`.`'.ServiceType::tableName().'` as b'], 'a.work_type = b.id')
                  ->where($where)
                  ->groupBy('d.work_no')
                  ->select('a.order_no,a.plan_time,a.plan_time_type,c.sale_order_id,d.work_no,b.title,count(*) as count')
                  ->asArray()->all();
            if ($query) {
                  foreach ($query as $val) {
                        //查询技师
                        $tech_ids = array();
                        $tech_info = WorkOrderTechnician::getTechnicianByWorkId($val['work_no']);
                        if(!empty($tech_info))
                        {
                              if(in_array($flag,['zp','ed','qx'])){
                                    foreach($tech_info as $v)
                                    {
                                          //有组长的情况
                                          if($v['is_self'] == 2)
                                          {
                                                //判断有没有指派
                                                $resa =  WorkOrderTechnician::getTechnicianByWorkId($v['work_no']);
                                                $zp_arr_ids = array_column($resa,'is_self');
                                                // 有技师
                                                if(in_array(3,$zp_arr_ids))
                                                {
                                                      if($flag == 'qx'){
                                                            foreach($resa as $vak)
                                                            {
                                                                  // 取消订单 关闭订单 三个类型都发
                                                                  $tech_ids[] = $vak['technician_id'];
                                                            }
                                                      }else{
                                                            foreach($resa as $vak)
                                                            {
                                                                  // 有组长 有技师  发送给负责人和技师
                                                                  if($vak['is_self'] == 1 || $vak['is_self'] == 3){
                                                                        $tech_ids[] = $vak['technician_id'];
                                                                  }
                                                            }
                                                      }

                                                }else  //无技师 发给组长
                                                {
                                                      foreach($resa as $vak)
                                                      {
                                                            if($vak['is_self'] == 2){
                                                                  $tech_ids[] = $vak['technician_id'];
                                                            }
                                                      }
                                                }
                                          }else{
                                                // 无组长的情况 肯定有负责人或者技师  直接遍历
                                                //（$tech_info 已经包含了 所有type类型 1,2,3，排除 2 组长 剩下的就是 1,3 直接遍历）
                                                $tech_ids[] = $v['technician_id'];
                                          }
                                    }
                              }else{
                                    $tech_ids = $technicianIds;
                              }
							  $order_status = 2;
                              if($flag == 'zp'){								  
                                    $title = "您有一个新的工单！请点击查看！";
                              }else if($flag == 'ed'){
                                    $title = "预约服务时间已修改，请尽快查看";
                              }else if($flag == 'gp'){
                                    $order_status = 7;
                                    $title = "工单已改派，请点击查看！";
                              }else if($flag == 'qx'){
								    $order_status = 7;
                                    $title = "工单已取消，请点击查看！";
                              }else{
                                    $title = '';
                              }

                        }else{  //如果没有技师 直接查询下一个工单
                              continue;
                        }
                        //（ 去重）如果是同一个人 只发一遍
                        $tech_ids = array_unique($tech_ids);
                        $type = 1;
                        if($flag == 'gp'){
                              $type_content['order_no'] = $order_no;
                              $type_content['work_no'] = $work_no;
                        }else{
                              $type_content['order_no'] = $val['order_no'];
                              $type_content['work_no'] = $val['work_no'];
                        }
                        //查询产品信息
                        $prod = SaleOrderView::getProdInfo($val['sale_order_id']);
                        //拼接内容
                        $content = '';
                        if($val['plan_time'] != null){
                              if($val['plan_time_type'] == 1){
                                  $content .= date("Y-m-d", $val['plan_time'])."  全天";
                              }else if($val['plan_time_type'] == 2){
                                  $content .= date("Y-m-d", $val['plan_time'])."  上午";
                              }else if($val['plan_time_type'] == 3){
                                  $content .= date("Y-m-d", $val['plan_time'])."  下午";
                              }else if($val['plan_time_type'] == 4){
                                  $content .= date("Y-m-d", $val['plan_time'])."  晚上";
                              }else{
                                  $content .= date("Y-m-d H:i", $val['plan_time']);
                              }
                        }
                        //服务类型
                        $content .=  " | " . $val['title'];
                        //产品名称
                        if($prod['prod_name']){
                              if($val['count']>1){
                                  $content .= " | " . $prod['prod_name']."等".$val['count']."个产品";
                              }else{
                                  $content .= " | " . $prod['prod_name'];
                              }
                        }
                        foreach ($tech_ids as $m) {
                              Push::pushMsg($title, $content, $m, $type,(object)$type_content,$order_status);
                        }
                  }
            }
      }
}