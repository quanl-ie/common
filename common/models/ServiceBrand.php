<?php

namespace common\models;

use webapp\models\ManufactorBrand;
use Yii;

class ServiceBrand extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'service_brand';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'department_id', 'status', 'del_status', 'create_id', 'create_time', 'update_id', 'update_time'], 'required'],
            [['id', 'department_id', 'status', 'del_status', 'create_id', 'create_time', 'update_id', 'update_time'], 'integer'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'            => '服务品牌主键',
            'title'         => '品牌名称',
            'department_id' => '（部门表）department 表id     0为公有',
            'status'        => '状态，1-正常 | 2-禁用',
            'del_status'    => '删除状态，2-删除 | 1-正常',
            'create_id'     => '创建人id',
            'create_time'   => '创建时间',
            'update_id'     => '修改人id',
            'update_time'   => '更新时间',
        ];
    }

    /**
     * 根据id获取品牌名称
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/5
     * Time: 15:45
     * @param $id
     * @return array 一维数组
     */
    public static function getName($id)
    {
        $brand = self::find()->where(['id'=>$id])->asArray()->all();
        $data = [];
        if (!empty($brand)) {
            $data = array_column($brand,'title','id');
        }
        return $data;
    }

    /**
     * 获取所有品牌
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/5
     * Time: 15:45
     * @return array 一维数组
     */
    public static function getList($where = [])
    {
        return self::find()->where(['del_status'=>1])->andFilterWhere($where)->asArray()->all();
    }

    /**
     * 编辑品牌
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/5
     * Time: 17:55
     * @param $data
     * @return bool
     * @throws \Exception
     */
    public static function add($data,$brandIds,$departmentId)
    {
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();
        try {

            //添加品牌
            if (isset($data) && !empty($data))
            {
                foreach ($data as $key => $val) {
                    //添加品牌
                    $model = new self();
    
                    $model->title         = $val['title'];
                    $model->department_id = $val['department_id'];
                    $model->sort          = $val['sort'];
                    $model->status        = $val['status'];
                    $model->del_status    = $val['del_status'];
                    $model->create_id     = $val['create_id'];
                    $model->create_time   = $val['create_time'];
                    $model->update_id     = $val['update_id'];
                    $model->update_time   = $val['update_time'];
    
                    if (!$model->save(false)) {
                        $transaction->rollBack();
                        return false;
                    }
    
                    //添加品牌关系
                    $manBrand = new ManufactorBrand();
    
                    $manBrand->department_id     = $departmentId;
                    $manBrand->direct_company_id = $val['department_id'];
                    $manBrand->brand_id          = $model->id;
                    $manBrand->status            = $val['status'];
                    $manBrand->del_status        = $val['del_status'];
                    $manBrand->create_time       = $val['create_time'];
                    $manBrand->update_time       = $val['update_time'];
    
                    if (!$manBrand->save(false)) {
                        $transaction->rollBack();
                        return false;
                    }
                    
                }
                
            }

            $transaction->commit();
            return true;
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;

        }
    }

    /**
     * 修改品牌状态（禁用、启用）
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/5
     * Time: 13:39
     * @param $id
     * @param $status
     * @return bool
     */
    public static function changeStatus($data)
    {
        $db   = Yii::$app->db;
        $transaction = $db->beginTransaction();
        try {
            
            if (!empty($data))
            {
                
                //修改品牌状态
                if (isset($data['id']))
                {
                    
                    $model = self::findOne(['id'=>$data['id']]);
                    
                    if (empty($model)) {
                        return false;
                    }
    
                    $model->status      = $data['status'];
                    $model->update_id   = $data['update_id'];
                    $model->update_time = $data['update_time'];
    
                    if (!$model->save(false)) {
                        $transaction->rollBack();
                        return false;
                    }
    
                    //修改产品状态
                    if (isset($data['topId']))
                    {
                        
                        //禁用产品
                        if (isset($data['status']) && $data['status'] == 2) {
                            $product = Product::getList(['department_top_id'=>$data['topId'],'brand_id'=>$data['id'],'status'=>1]);
                            
                            if (!empty($product))
                            {
                                $prodId = array_column($product,'id');
                                
                                $attr = [
                                    'status'      => 2,
                                    'update_time' => $data['update_time'],
                                ];
    
                                Product::updateAll($attr,['id' =>$prodId,'status'=>1]);
                            }
                            
                        }
                        
                        //启用产品
                        if (isset($data['status']) && $data['status'] == 1)
                        {
                            $product = Product::getList(['department_top_id'=>$data['topId'],'brand_id'=>$data['id'],'status'=>2]);
    
                            if (!empty($product))
                            {
        
                                //获取类目id
                                $classId = array_unique(array_column($product,'class_id'));
                                $class   = ServiceClass::getList(['id'=>$classId,'status'=>1,'department_id'=>$data['topId']]);
        
                                if (!empty($class))
                                {
                                    $classId = array_unique(array_column($class,'id'));
                                    $product = Product::getList(['department_top_id'=>$data['topId'],'brand_id'=>$data['id'],'class_id'=>$classId,'status'=>2]);
                                    
                                    if (!empty($product))
                                    {
                                        
                                        $prodId = array_column($product,'id');
    
                                        $attr = [
                                            'status'      => 1,
                                            'update_time' => $data['update_time'],
                                        ];
    
                                        Product::updateAll($attr,['id' =>$prodId,'status'=>2]);
   
                                    }
                                    
                                }
    
                            }
                        }

                    }
                }
                $transaction->commit();
                return true;
            }
            return false;
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        
        }
    }
}
