<?php

namespace common\models;
use yii\db\ActiveRecord;

class Push extends ActiveRecord
{
    /**
     * 拼接订单信息
     */
    public static function orderData($order) {
        //客户对应地址
        $order['address'] = AccountAddress::getAddress($order['account_id'],1);
        //客户对应服务
        $order['service'] = SaleOrder::getServiceBrand($order['account_id']);
        //查询二级分类
        $sale_order = \common\models\SaleOrder::find()->where(['id' => $order['workDetail'][0]['sale_order_id']])->asArray()->one();
        $order['class_id'] = $sale_order['class_id'];
        //查询当日工作的技师且当前时间没有服务项目
        $tecArr = self::checkTime($order);
        //查询接单范围内有当前订单位置的技师
        $tecArr = self::checkDistance($order, $tecArr);
        //查询当前时间前后半小时内没有服务订单的技师
        $tecArr = self::checkStatus($order, $tecArr);
        //匹配服务项
        $tecArr = self::checkService($order, $tecArr);
        //设置消息内容
        $data   = self::setMessage($order, $tecArr, 1);
        return $data;
    }
    
     /**
     * 检索当前时间上班且没有服务的技师
     */
    public static function checkTime($order) {
        //获取服务时间
        $service_wook = date("w", $order['plan_time']);
        $service_time = date("H", $order['plan_time']);
        //根据服务时间查询当前时间的技师
//        $tecArr = DB::name('technician_mode')->where("service_work like '%$service_wook%'")->select();
        $tecArr = \common\models\TechnicianMode::find()->where("service_work like '%$service_wook%'")->asArray()->all();
        //排除当前时间不上班的技师
        $newTecArr = [];
        foreach ($tecArr as $k => $v) {
            $tec = Technician::find()->where(['id' => $v['technician_id']])->asArray()->one();
            if(!empty($v['service_time']) && $tec['audit_time'] == 5) {
                $time = json_decode($v['service_time'], true);
                if($service_time >= $time[0] && $service_time <= $time[1]) {
                    $newTecArr[] = $v;
                }
            }
        }
        $newTecArr = self::getTecInfo($newTecArr);
        return $newTecArr;
    }
    
    /**
     * 检索订单合适范围内的技师
     */
    public static function checkDistance($order, $tecArr) {
        $newTecArr = [];
        //筛选技师接单范围合适的即使
        foreach ($tecArr as $k => $v) {
            $distance = \common\helpers\Helper::getDistance($v['lon'], $v['lat'], $order['address']['lon'], $order['address']['lat']);
            if($distance <= $v['service_radius']) {
                $newTecArr[] = $v;
            }
        }
        return $newTecArr;
    }
    
    /**
     * 筛选没有在服务中的技师
     */
    public static function checkStatus($order, $tecArr) {
        $front_time   = $order['plan_time']-1800;
        $end_time   = $order['plan_time']+1800;
        $tecIds = $orderNo = $newTec = [];
        foreach ($tecArr as $v) {
            $tecIds[] = $v['technician_id'];
        }
        //查询技师时间段是否有订单
        $orderData = \common\models\Order::find()
                ->where("plan_time >= $front_time and plan_time <= $end_time and status=1")
                ->asArray()->all();
        //查询技师
        foreach($orderData as $v) {
            $orderNo[] = $v['order_no'];
        }
        $orderData = \common\models\OrderDetail::find()->where(['order_no' => $orderNo, 'technician_id' => $tecIds])->asArray()->all();
        if(empty($orderData)) {
            return $tecArr;
        }
        foreach ($tecArr as $k => $v) {
            $newTec[$v['technician_id']] = $v;
        }
        foreach ($orderData as $k => $v) {
            if($newTec[$v['technician_id']]) {
                unset($newTec[$k]);
            }
        }
        return array_values($newTec);
    }
    
    /**
     * 获取技师 信息
     */
    public static function getTecInfo($tecArr) {
        $tecIds = $newTecSkill = [];
        foreach ($tecArr as $v) {
            $tecIds[] = $v['technician_id'];
        }
        //查询技师技能
        $tecSkill = \common\models\TechnicianSkill::find()->where(['technician_id' => $tecIds])->asArray()->all();
        foreach ($tecSkill as $v) {
            $newTecSkill[$v['technician_id']]['class_id'][] = $v['class_id'];
            $newTecSkill[$v['technician_id']]['service_id'][] = $v['service_id'];
        }
        foreach ($tecArr as $k => $v) {
            if(isset($newTecSkill[$v['technician_id']])) {
                $tecArr[$k]['class_id'] = $newTecSkill[$v['technician_id']]['class_id'];//service_id
                $tecArr[$k]['service_id'] = array_unique($newTecSkill[$v['technician_id']]['service_id']);
            }else {
                unset($tecArr[$k]);
            }
        }
        return array_values($tecArr);
    }
    
    /**
     * 匹配服务项
     */
    public static function checkService($order, $tecArr) {
        
        foreach ($tecArr as $k => $v) {
            $skillArr = ServiceClass::find()->select(['id'])->where(['pid' => $v['class_id']])->asArray()->all();
            $classIds = [];
            foreach($skillArr as $vv) {
                $classIds[] = $vv['id'];
            }
            $classIds = array_unique($classIds);
            if(!in_array($order['class_id'], $classIds) || !in_array($order['work_type'], $v['service_id'])){
                unset($tecArr[$k]);
            }
        }
        return $tecArr;
    }
    
    /**
     * 重组发送内容
     */
    public static function setMessage($order, $tecArr) {
//        print_r("<pre>");
//        print_r($order);
        $getClassName = ServiceClass::getClassName($order['class_id']);
        $getTitle     = ServiceType::getTitle($order['work_type']);
        $content = "【新订单】[".date("Y-m-d H:i:s",$order['plan_time'])."][".$getClassName."][".$getTitle."]";
        $data = [];
        foreach($tecArr as $k => $v) {
            $data[$k]['technician_id'] = $v['technician_id'];
            $data[$k]['content'] = $content;
            $data[$k]['title'] = '新的订单，快来抢！！！';
            $data[$k]['order_code'] = $order['order_no'];
            $data[$k]['order_status'] = $order['status'];
        }
        return $data;
    }
}