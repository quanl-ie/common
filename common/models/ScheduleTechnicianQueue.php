<?php
namespace common\models;

use Yii;

class ScheduleTechnicianQueue extends BaseModel
{

    public static function getDb()
    {
        return Yii::$app->db;
    }

    public static function tableName()
    {
        return 'schedule_technician_queue';
    }

    /**
     * 填加
     * @param $workNo
     * @param $type
     * @param string $processDate
     */
    public static function push($workNo,$type,$processDate ,$planTime,$params = '')
    {
        if($processDate == ''){
            $processDate = date('Y-m-d');
        }
        $model = new self();
        $model->work_no      = $workNo;
        $model->process_date = $processDate;
        $model->plan_time    = $planTime;
        $model->type         = $type;
        $model->params       = $params;
        $model->save();
    }
   

}