<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * 客户管理
 * @author liuxingqi <lxq@c-ntek.com>
 * @date 2017-12-15
 */
class  Account extends BaseModel
{

    public static function tableName()
    {
        return 'account';  //客户表
    }
}