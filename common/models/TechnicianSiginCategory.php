<?php

namespace common\models;

class TechnicianSiginCategory extends BaseModel
{
    public static function tableName()
    {
        return 'technician_sign_in_category';
    }

    /**
     * 初始化数据
     * @author xi
     */
    public static function getInitData()
    {
        $data = [
            [
                'c_id'  => 1,
                'title' => '考勤',
                'check_deviation' => 0
            ],
            [
                'c_id'  => 2,
                'title' => '开始服务',
                'check_deviation' => 1
            ],
            [
                'c_id'  => 3,
                'title' => '出车',
                'check_deviation' => 0
            ],
            [
                'c_id'  => 4,
                'title' => '完工',
                'check_deviation' => 1
            ],
            [
                'c_id'  => 5,
                'title' => '出工',
                'check_deviation' => 0
            ]
        ];

        return $data;
    }

    /**
     * 填加
     * @param $departmentId
     * @return bool
     */
    public static function add($departmentId)
    {
        foreach (self::getInitData() as $key=>$val)
        {
            $arr = self::findOneByAttributes(['direct_company_id'=>$departmentId,'c_id' => $val['c_id'],'title'=>$val['title']],'id');
            if(!$arr)
            {
                $model = new self();
                $model->direct_company_id = $departmentId;
                $model->c_id              = $val['c_id'];
                $model->title             = $val['title'];
                $model->check_deviation   = $val['check_deviation'];
                $model->save();
            }
        }

        return true;
    }

}