<?php
namespace common\models;


class Counter extends BaseModel
{
    public static function tableName()
    {
        return 'counter';
    }

    /**
     * 获取计数
     * @param $type
     * @return bool|int|mixed
     */
    public static function getCounter($type)
    {
        $model = self::findOne(['type' => $type]);
        if($model)
        {
            $model->count+=1;
            if($model->save(false)){
                return $model->count;
            }
        }

        return false;
    }

}