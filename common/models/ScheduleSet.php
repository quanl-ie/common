<?php
/**
 * Created by PhpStorm.
 * User: xingq
 * Date: 2018/5/14
 * Time: 14:47
 */

namespace common\models;

use webapp\logic\BaseLogic;
use Yii;
class ScheduleSet extends BaseModel
{
    
    public static function tableName()
    {
        return 'schedule_set';
    }
    
    /**
     * 获取单条数据
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/5/14
     * Time: 14:49
     * @param array $where
     * @return array|null|\yii\db\ActiveRecord
     */
    public static function getOne($where = [])
    {
        return self::find()->where($where)->asArray()->one();
        
    }
    
    /**
     * 获取多条数据
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/5/14
     * Time: 14:49
     * @param array $where
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getList($where = [])
    {
        return self::find()->where($where)->asArray()->all();
    }
    
    /**
     * 添加与修改
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/5/18
     * Time: 16:02
     * @param $data
     * @return bool
     * @throws \Exception
     */
    public static function add($data)
    {
        
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();
        try {
            
            $addAttr = ['name','start_time','end_time','direct_company_id','department_top_id','is_default','is_delete','create_time','oprate_id','color'];
            
            //添加
            if (isset($data['add'])) {
                $db->createCommand()->batchInsert(self::tableName(),$addAttr,$data['add'])->execute();
            }
    
            //修改非默认数据时添加
            if (isset($data['addDef'])) {
                $db->createCommand()->batchInsert(self::tableName(),$addAttr,$data['addDef'])->execute();
            }
            
            //修改默认数据时添加
            if (isset($data['edit']))
            {
                foreach ($data['edit'] as $key => $val) {
                    $model = new self();
                    $model->name              = $val['name'];
                    $model->start_time        = $val['start_time'];
                    $model->end_time          = $val['end_time'];
                    $model->direct_company_id = $val['direct_company_id'];
                    $model->department_top_id = $val['department_top_id'];
                    $model->is_default        = $val['is_default'];
                    $model->is_delete         = $val['is_delete'];
                    $model->create_time       = $val['create_time'];
                    $model->oprate_id         = $val['oprate_id'];
                    $model->color             = $val['color'];
                    
                    if (!$model->save(false)) {
                        $transaction->rollBack();
                        return false;
                    }
                }
            }
            
            //删除数据
            if (isset($data['del'])) {
                $attr = [
                    'is_delete' => 2,
                    'oprate_id' => Yii::$app->user->id,
                ];
                
                self::updateAll($attr,['id' =>$data['del']]);
            }

            $transaction->commit();
            return true;
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
        
    }

    /**
     * 初始化数据
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/29
     * Time: 19:59
     * @param $dcId  直属公司id
     * @param $user  添加人
     * @return bool
     * @throws \Exception
     */
    public static function initScheduleSet($dcId,$user)
    {
        $topId = BaseLogic::getTopId() > 0 ? BaseLogic::getTopId() : 0;
        
        if ($topId == 0) {
            $topId = $dcId;
        }
        
        $time = date('Y-m-d H:i:s',time());
        $data[1]['name']              = '全天班';
        $data[1]['start_time']        = '08:00';
        $data[1]['end_time']          = '18:00';
        $data[1]['direct_company_id'] = $dcId;
        $data[1]['department_top_id'] = $topId;
        $data[1]['is_default']        = 1;
        $data[1]['is_delete']         = 1;
        $data[1]['create_time']       = $time;
        $data[1]['oprate_id']         = $user;
        $data[1]['color']             = '';
        
        $data[2]['name']              = '上午班';
        $data[2]['start_time']        = '08:00';
        $data[2]['end_time']          = '22:00';
        $data[2]['direct_company_id'] = $dcId;
        $data[2]['department_top_id'] = $topId;
        $data[2]['is_default']        = 1;
        $data[2]['is_delete']         = 1;
        $data[2]['create_time']       = $time;
        $data[2]['oprate_id']         = $user;
        $data[2]['color']             = '#d8edff';
        
        $data[3]['name']              = '下午班';
        $data[3]['start_time']        = '12:00';
        $data[3]['end_time']          = '18:00';
        $data[3]['direct_company_id'] = $dcId;
        $data[3]['department_top_id'] = $topId;
        $data[3]['is_default']        = 1;
        $data[3]['is_delete']         = 1;
        $data[3]['create_time']       = $time;
        $data[3]['oprate_id']         = $user;
        $data[3]['color']             = '#d8ffdb';
        
        $data[4]['name']              = '晚班';
        $data[4]['start_time']        = '18:00';
        $data[4]['end_time']          = '23:59';
        $data[4]['direct_company_id'] = $dcId;
        $data[4]['department_top_id'] = $topId;
        $data[4]['is_default']        = 1;
        $data[4]['is_delete']         = 1;
        $data[4]['create_time']       = $time;
        $data[4]['oprate_id']         = $user;
        $data[4]['color']             = '#ffd7fb';
        
        $data[5]['name']              = '休息';
        $data[5]['start_time']        = '00:00';
        $data[5]['end_time']          = '23:59';
        $data[5]['direct_company_id'] = $dcId;
        $data[5]['department_top_id'] = $topId;
        $data[5]['is_default']        = 1;
        $data[5]['is_delete']         = 1;
        $data[5]['create_time']       = $time;
        $data[5]['oprate_id']         = $user;
        $data[5]['color']             = '#ffbebe';
        
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();
        try {
            $attr = ['name','start_time','end_time','direct_company_id','department_top_id','is_default','is_delete','create_time','oprate_id','color'];
            $db->createCommand()->batchInsert(self::tableName(), $attr, $data)->execute();
            
            $transaction->commit();
            return true;
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
        
        
    }
}
