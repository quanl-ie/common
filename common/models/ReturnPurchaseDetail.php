<?php
namespace common\models;

use Yii;


class ReturnPurchaseDetail extends BaseModel
{
/*    public static function getDb ()
    {
        return Yii::$app->order_db;
    }*/

    public static function tableName ()
    {
        return 'return_purchase_detail';
    }

    public static function getOne($where = [])
    {
        return self::find()->where($where)->asArray()->one();
    }



    public static function getList($where = [],$flag=0)
    {
        if($flag){
            return self::find()->where($where)->select("*,sum(`prod_num`) as total_prod_num")->groupBy("parent_id")->asArray()->all();
        }
        return self::find()->where($where)->asArray()->all();
    }
    
    //获取产品明细
    public static function getProduct($where = [])
    {
        
        $db = self::find();
        $db->from(self::tableName() . ' as a');
        //判断where
        if (!empty($where)) {
            foreach ($where as $val) {
                $db->andFilterWhere($val);
            }
        }
        $db->leftJoin([Product::tableName() . ' as b'],' a.prod_id = b.id');
        $db->select('a.id,a.prod_id,a.prod_num,a.finish_num,b.prod_name,b.prod_no,b.brand_id,b.class_id,b.type_id,b.model,b.unit_id,b.suggest_sale_price,b.status');
        $list = $db->asArray()->all();
        return $list;
        
    }
    
}