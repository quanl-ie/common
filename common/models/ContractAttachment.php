<?php
namespace common\models;

use Yii;


class ContractAttachment extends BaseModel
{
/*    public static function getDb ()
    {
        return Yii::$app->order_db;
    }*/

    public static function tableName ()
    {
        return 'contract_attachment';
    }

    public static function getOne($where = [])
    {
        return self::find()->where($where)->asArray()->one();
    }


    public static function getList($where = [])
    {
        return self::find()->where($where)->asArray()->all();
    }
}