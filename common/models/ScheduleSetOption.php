<?php
    /**
     * Created by PhpStorm.
     * User: xingq
     * Date: 2018/5/14
     * Time: 14:47
     */
    
    namespace common\models;
    
    use Yii;
    
    class ScheduleSetOption extends BaseModel
    {
    
        public static function tableName()
        {
            return 'schedule_set_option';
        }
    
        /**
         * 获取单条数据
         * User: xingq
         * Email: liuxingqi@services.cn
         * Date: 2018/5/14
         * Time: 14:49
         * @param array $where
         * @return array|null|\yii\db\ActiveRecord
         */
        public static function getOne($where = [])
        {
            return self::find()->where($where)->asArray()->one();
            
        }
    
        /**
         * 获取多条数据
         * User: xingq
         * Email: liuxingqi@services.cn
         * Date: 2018/5/14
         * Time: 14:49
         * @param array $where
         * @return array|\yii\db\ActiveRecord[]
         */
        public static function getList($where = [])
        {
            return self::find()->where($where)->asArray()->all();
        }
        
        public static function add($setOption,$techId)
        {
            $db = Yii::$app->db;
            $transaction = $db->beginTransaction();
            try {
        
                //添加设置
                $model = new self();
                foreach ($setOption as $key => $val) {
                    $model->$key = $val;
                }
                $model->save(false);
                
                
                //绑定技师
                $data = [];
                foreach ($techId as $key => $val) {
                    $data[$key]['option_id']   = $model->id;
                    $data[$key]['tech_id']     = $val;
                    $data[$key]['create_time'] = date('Y-m-d H:i:s',time());
                }
                
                if ($data) {
                    $db->createCommand()->batchInsert(ScheduleSetOptionDetail::tableName(),['option_id','tech_id','create_time'], $data)->execute();
                }
        
                $transaction->commit();
                return true;
            } catch(\Exception $e) {
                $transaction->rollBack();
                throw $e;
            }
            
        }
    }