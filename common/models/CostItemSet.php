<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;

class CostItemSet extends ActiveRecord
{
    public static function tableName()
    {
        return 'cost_item_set';
    }
    
    public static function getName($id)
    {
        $item = self::find()->where(['id'=>$id])->asArray()->all();
        $data = [];
        if (!empty($item)) {
            $data = array_column($item,'name','id');
        }
        return $data;
    }
    
    /**
     * 获取多个收费项目
     * @author liuxingqi <lxq@c-ntek.com>
     * @param type $where
     * @return type
     */
    public static function getList($where = [])
    {
        return self::find()->where(['del_status'=>1])->andFilterWhere($where)->asArray()->all();
    }
    
    /**
     * 收费项目添加
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/6
     * Time: 14:02
     * @param $data
     * @return bool
     * @throws \Exception
     */
    public static function add($data,$itmeIds,$departmentId)
    {
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();
        try {
            
            //添加收费项目
            if (isset($data) && !empty($data))
            {
                foreach ($data as $key => $val) {
                    //添加收费项目
                    $model = new self();
    
                    $model->name          = $val['name'];
                    $model->department_id = $val['department_id'];
                    $model->sort          = $val['sort'];
                    $model->status        = $val['status'];
                    $model->del_status    = $val['del_status'];
                    $model->oporate_id    = $val['oporate_id'];
                    $model->create_time   = $val['create_time'];
                    $model->update_time   = $val['update_time'];
                    $model->quote_id      = $val['quote_id'];
    
                    if (!$model->save(false)) {
                        $transaction->rollBack();
                        return false;
                    }
                    
                    //添加收费项目关系
                    $costTransaction = Yii::$app->order_db->beginTransaction();
                    try
                    {
                        $itemModel = new \webapp\models\CostItem();
                        
                        $itemModel->department_id     = $departmentId;
                        $itemModel->direct_company_id = $val['department_id'];
                        $itemModel->cost_item_set_id  = $model->id;
                        $itemModel->status            = $val['status'];
                        $itemModel->del_status        = $val['del_status'];
                        $itemModel->create_time       = $val['create_time'];
                        $itemModel->update_time       = $val['update_time'];
   
                        if (!$itemModel->save(false)) {
                            $transaction->rollBack();
                            $costTransaction->rollBack();
                            return false;
                        }
                        $costTransaction->commit();
                    }
                    catch(\Exception $orderException)
                    {
                        $transaction->rollBack();
                        $costTransaction->rollBack();
                        return false;
                    }
                    
                }

            }
            $transaction->commit();
            return true;
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
            
        }
    }
    
    /**
     * 禁用启用
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/6
     * Time: 14:03
     * @param $id
     * @param $status
     * @return bool
     */
    public static function changeStatus($id,$status)
    {
        $model = self::findOne(['id'=>$id]);
        $model->status = $status;
        
        if ($model->save(false)) {
            return true;
        }
        return false;
    }
}