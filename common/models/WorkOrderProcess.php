<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;

class WorkOrderProcess extends ActiveRecord
{
    public static function getDb() {
        return Yii::$app->order_db;
    }
    
    public static function tableName()
    {
        return 'work_order_process';
    }
}