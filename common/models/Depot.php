<?php

namespace common\models;

use Yii;
use webapp\logic\BaseLogic;
class Depot extends BaseModel
{
    /**
     * @inheritdoc 库房表
     */
    public static function tableName()
    {
        return 'depot';
    }
    public static function getOne($where = [])
    {
        return self::find()->where($where)->asArray()->one();
    }
    public static function getList($where = [])
    {
        return self::find()->where($where)->asArray()->all();
    }
    public static function getIndexList ($maps,$where,$page,$pageSize)
    {
        $db = self::find();
        $db->where($maps);
        //判断where
        if (!empty($where)) {
            foreach ($where as $val) {
                $db->andFilterWhere($val);
            }
        }
        //总数
        $totalNum = $db->count();
        //当有结果时进行组合数据
        if ($totalNum>0) {
            //总页数
            $totalPage = ceil($totalNum/$pageSize);
            if($page<1)
            {
                $page = 1;
            }
            else if($page>$totalPage)
            {
                $page = $totalPage;
            }
            $db->offset(($page-1)*$pageSize);
            $db->orderBy(['id'=> SORT_DESC]);
            $db->limit($pageSize);
            //print_r($db->createCommand()->getRawSql());die;
            $db->asArray();
            $list = $db->all();
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => $totalPage,
                'list'       => $list
            ];
        }
        else
        {
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => 0,
                'list'       => []
            ];
        }
    }
    //添加数据
    public static function add($data){
        $model = new self();
        //处理数据
        foreach ($data as $key => $val) {
            $model->$key = $val;
        }
        //print_r($model);exit;
        //开启事务
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();
        try {
            //库房表添加数据
            $model->save(false);
            $transaction->commit();
            return true;
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }

    }
    //编辑修改数据
    public static function edit($data){
        //查询库房数据
        $model = self::findOne(['id'=>$data['id']]);
        //处理数据
        foreach ($data as $key => $val) {
            $model->$key = $val;
        }
        //开启事务
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();
        try {
            $model->save(false);
            $transaction->commit();
            return true;
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }

    }
    public static function search($where)
    {
        $db = self::find();
        if (!empty($where)) {
            foreach ($where as $val) {
                $db->andFilterWhere($val);
            }
        }
        return $db->asArray()->all();
    }
}
