<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;

class WorkOrderAssign extends ActiveRecord
{
    public static function getDb()
    {
        return Yii::$app->order_db;
    }

    public static function tableName()
    {
        return 'work_order_assign';
    }

    /**
     * 查询指派是否有机构
     * @param $orderNo
     * @param $departmentId
     * @return int|string
     * @author xi
     */
    public static function hasDepartment($orderNo,$departmentId)
    {
        $where = [
            'order_no'              => $orderNo,
            'service_department_id' => $departmentId
        ];
        $query = self::find()
            ->where($where)
            ->andWhere('`type` <> 2')
            ->count();

        return $query > 0?true:false;
    }
   
}