<?php

namespace common\models;

use Yii;
class Purchase extends BaseModel
{
    /**
     * @inheritdoc 采购单表
     */
    public static function tableName()
    {
        return 'purchase';
    }
    public static function getOne($where = [],$falg = '')
    {
        if ($falg) {
            return self::find()->where($where)->one();
        }
        return self::find()->where($where)->asArray()->one();
    }
    public static function getList($where = [])
    {
        return self::find()->where($where)->asArray()->all();
    }
    public static function getIndexList ($maps,$where,$page,$pageSize)
    {
        $db = self::find();
        $db->where($maps);
        //判断where
        if (!empty($where)) {
            foreach ($where as $val) {
                $db->andFilterWhere($val);
            }
        }
        //总数
        $totalNum = $db->count();
        //当有结果时进行组合数据
        if ($totalNum>0) {
            //总页数
            $totalPage = ceil($totalNum/$pageSize);
            if($page<1)
            {
                $page = 1;
            }
            else if($page>$totalPage)
            {
                $page = $totalPage;
            }
            $db->offset(($page-1)*$pageSize);
            $db->orderBy(['id'=> SORT_DESC]);
            $db->limit($pageSize);
            //print_r($db->createCommand()->getRawSql());die;
            $db->asArray();
            $list = $db->all();
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => $totalPage,
                'list'       => $list
            ];
        }
        else
        {
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => 0,
                'list'       => []
            ];
        }
    }
    //添加数据
    public static function add($data){
        $model = new self();
        //处理数据
        if($data['main']){
            foreach ($data['main'] as $key => $val) {
                $model->$key = $val;
            }
            //开启事务
            $db = Yii::$app->db;
            $transaction = $db->beginTransaction();
            try {
                //库房表添加数据
                $model->save(false);
                $total_amount = 0; //此采购单的总额
                if(!empty($data['addition'])){
                    //print_r($data['addition']);die;
                    //处理需要添加的产品数据
                    foreach ($data['addition'] as $key => $val) {
                        $producs[$key]['parent_id']     = $model->id;
                        $producs[$key]['prod_id']       = $val['prod_id'];
                        $producs[$key]['prod_num']      = $val['prod_num'];
                        $producs[$key]['activity_prod_num'] = $val['prod_num'];
                        $producs[$key]['price']         = $val['price'];
                        $producs[$key]['total_amount']  = $val['price']*$val['prod_num'];
                        $producs[$key]['direct_company_id']      = $data['main']['direct_company_id'];
                        //此采购单的总额
                        $total_amount += (int)$producs[$key]['total_amount'];
                    }
                    $field = ['parent_id','prod_id','prod_num','activity_prod_num','price','total_amount','direct_company_id'];
                    $totalnum = Yii::$app->db->createCommand()
                        ->batchInsert(PurchaseDetail::tableName(),$field,$producs)
                        ->execute();
                }
                //将此采购单的总额更新到主表去
                $model->total_amount = $total_amount;
                $model->save(false);
                //提交保存数据
                $re = $transaction->commit();
                return true;
            } catch(\Exception $e) {
                $transaction->rollBack();
                throw $e;
            }
        }
    }
    //编辑修改数据
    public static function edit($data){
        //查询采购单主表数据
        $model = self::findOne(['id'=>$data['main']['id']]);
        //处理数据
        foreach ($data['main'] as $key => $val) {
            $model->$key = $val;
        }
        //开启事务
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();
        try {
            //$model->save(false);
            $total_amount = 0; //此采购单的总额
            //删除原有的采购单数据
            $delDetail = PurchaseDetail::deleteAll(['direct_company_id'=>$data['main']['direct_company_id'],'parent_id'=>$data['main']['id']]);
            if (isset($data['addition']))
            {
                foreach ($data['addition'] as $key => $val) {
                    $producs[$key]['parent_id']     = $data['main']['id'];
                    $producs[$key]['prod_id']       = $val['prod_id'];
                    $producs[$key]['prod_num']      = $val['prod_num'];
                    $producs[$key]['price']         = $val['price'];
                    $producs[$key]['total_amount']  = $val['price']*$val['prod_num'];
                    $producs[$key]['direct_company_id']      = $data['main']['direct_company_id'];
                    //此采购单的总额
                    $total_amount += (int)$producs[$key]['total_amount'];
                }
                $field = ['parent_id','prod_id','prod_num','price','total_amount','direct_company_id'];
                $totalnum = Yii::$app->db->createCommand()
                    ->batchInsert(PurchaseDetail::tableName(),$field,$producs)
                    ->execute();
            }
            //将此采购单的总额更新到主表去
            $model->total_amount = $total_amount;
            $model->save(false);
            //提交保存数据
            $transaction->commit();
            return true;
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }

    }
    
    /**
     * 获取入库单详情数据
     */
    public static function getDetail($map,$where)
    {
        $db = self::find();
        $db->from(self::tableName() . ' as a');
        $db->where($map);
        $db->andFilterWhere($where);
        $db->leftJoin([PurchaseDetail::tableName() . ' as b'],' a.id = b.parent_id');
        $db->select('a.id,a.direct_company_id,a.audit_status,b.prod_id,b.prod_num,b.activity_prod_num,b.finish_num');
        $db->asArray();
        return $db->all();

    }
}
