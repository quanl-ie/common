<?php

namespace common\models;

use Yii;
/**
 * This is the model class for table "tp_region".
 *
 * @property int $region_id
 * @property string $region_code
 * @property int $parent_id
 * @property string $region_name
 * @property int $region_type
 * @property int $agency_id
 * @property int $is_show
 */
class Region extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'region';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'region_type', 'agency_id', 'is_show'], 'integer'],
            [['region_code'], 'string', 'max' => 255],
            [['region_name'], 'string', 'max' => 120],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'region_id' => 'Region ID',
            'region_code' => 'Region Code',
            'parent_id' => 'Parent ID',
            'region_name' => 'Region Name',
            'region_type' => 'Region Type',
            'agency_id' => 'Agency ID',
            'is_show' => 'Is Show',
        ];
    }
   
    //获取所有已开通城市列表
    public static function getOpenCityList()
    {
        $data =  self::find()
                ->select(['region_id','region_name'])
                ->where(['is_open'=>1,])
                ->asArray()->all();
        $cityArr[0] = '请选择';
        foreach ($data as $v) {
            $cityArr[$v['region_id']] = $v['region_name'];
        }
        return $cityArr;
    }
    /**
     * 根据城市id返回城市名称
     * @author liuxingqi <lxq@c-ntek.com>
     * @param type $where
     * @return array
     */
    public static function getCityName($where=[]){
        return self::find()->select('parent_id,region_id,region_name')->where($where)->asArray()->all();
    }
    public static function getCityNameOne($where=[]){
        return self::find()->select('parent_id,region_id,region_name')->where($where)->asArray()->one();
    }
    public static function getCityNameById($id){
        $res = self::find()->select('region_name')->where(['region_id'=>$id])->asArray()->one();
        if($res){
            return $res['region_name'];
        }else{
            return '';
        }
    }
    /**
     * 获取地区列表
     * @author liuxingqi <lxq@c-ntek.com>
     * @param type $where
     * @return type
     */
    public static function getList($where = [])
    {
        return self::find()->select('region_id,parent_id,region_name')->where($where)->asArray()->all();
    }

    /**
     * 菜单获取
     * @paramint $module 模块id
     * @param array $menu_ids
     * @return array
     * @author xi
     */
    public static function listAll()
    {
        $query = self::find()->select('region_id,parent_id,region_name')->asArray()->all();

        $items = array();
        foreach ($query as $row){
            $items[$row['region_id']] = $row;
        }
        foreach ($items as $item){
            $items[$item['parent_id']]['items'][$item['region_id']] = &$items[$item['region_id']];
        }
        $data = isset($items[0]['items']) ? $items[0]['items'] : array();
        $data = $data[1]['items'];
        return self::formatMenu($data);
    }

    /**
     *格式化输出
     */
    public static function formatMenu($data)
    {
        $result = [];
        foreach($data as $val)
        {
            $items  = [];
            if(isset($val['items']) && is_array($val['items']) && $val['items'])
            {
                $items = self::formatMenu($val['items']);
            }

            $temp = [
                'region_id'   => $val['region_id'],
                'region_name' => $val['region_name'],
                'items' => $items
            ];
            if(!$items){
                unset($temp['items']);
            }
            $result[]= $temp;

        }
        return $result;
    }
    
    public static function getServiceArea($area)
    {
        $res = [];
        foreach ($area as $key => $val) {
            $res[$key] = explode('_', $val);
        }
        foreach ($res as $key => $val) {
            $provinceIds[] = $val[0];
            $cityIds[]     = $val[1];
            $districtIds[] = $val[2];
        }
        $data['provinceId'] = array_unique($provinceIds);
        $data['cityId']     = array_unique($cityIds);
        $data['districtId'] = array_unique($districtIds);
        return $data;
    }
    /**
     * 收费项目获取地区列表数据
     * @author sxz 2018-06-12
     * @param type $where
     * @return type
     */
    public static function getCostAreaList($where = '')
    {
        $data =  self::find()
            ->select(['parent_id','region_id','region_name'])
            ->where([])
            ->andWhere("parent_id in('$where')")
            ->asArray()->all();
        return $data;
    }
}
