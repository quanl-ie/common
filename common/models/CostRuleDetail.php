<?php
namespace common\models;

use Yii;

class CostRuleDetail extends BaseModel
{
    public static function getDb ()
    {
        return Yii::$app->order_db;
    }

    public static function tableName ()
    {
        return 'cost_rule_detail';
    }

    public static function getOne($where = [])
    {
        return self::find()->where($where)->one();
    }
    public static function getList($where = [])
    {
        return self::find()->where($where)->asArray()->all();
    }
}