<?php

namespace common\models;
use yii\db\ActiveRecord;

class TechnicianMode extends ActiveRecord
{
    public static function tableName()
    {
        return 'technician_mode';
    }
}