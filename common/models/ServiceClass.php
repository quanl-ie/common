<?php

namespace common\models;

use webapp\models\ManufactorClass;
use Yii;


class ServiceClass extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'service_class';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'department_id', 'status', 'del_status', 'create_id', 'create_time', 'update_id', 'update_time'], 'integer'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'            => '主键',
            'title'         => '产品类型名称',
            'parent_id'     => '父级id',
            'parent_ids'    => '所有父级id , 以逗号分隔',
            'department_id' => '（部门表）department 表id     0为公有',
            'status'        => '状态，1-正常 | 0-禁用',
            'del_status'    => '删除状态，1-删除 | 0-正常',
            'create_id'     => '添加人id',
            'create_time'   => '创建时间',
            'update_id'     => '修改人id',
            'update_time'   => '更新时间',
        ];
    }
    
    /**
     * 根据id获取产品类型名称
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/6
     * Time: 15:14
     * @param $id
     * @return array
     */
    public static function getName($id)
    {
        $class = self::find()->where(['id'=>$id])->asArray()->all();
        $data = [];
        if (!empty($class)) {
            $data = array_column($class,'title','id');
        }
        return $data;
    }
    
    /**
     * 获取所有产品类型
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/6
     * Time: 15:15
     * @param array $where
     * @return array
     */
    public static function getList($where = [])
    {
        return self::find()->where(['del_status'=>1])->andFilterWhere($where)->asArray()->all();
    }
    
    /**
     * 获取所有子级
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/11
     * Time: 17:40
     * @param $classId
     * @return array
     */
    public static function getChilds($classId)
    {
        $class = self::find()->where("find_in_set($classId,parent_ids)")->andWhere(['<>','parent_id',0])->asArray()->all();
        
        if (!empty($class)) {
            return array_column($class,'id');
        }
        return [-1];
        
    }
    
    /**
     * 添加类目
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/5
     * Time: 17:55
     * @param $data
     * @return bool
     * @throws \Exception
     */
    public static function add($data)
    {
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();
        try {
            
            //添加类目
            if (isset($data['class']) && !empty($data['class']))
            {
                //添加类目
                $model = new self();
                foreach ($data['class'] as $key => $val) {
                    $model->$key = $val;
                }
                $model->save(false);
              
            }
            
            if (isset($data['mClass']) && !empty($data['mClass']))
            {
                //添加类目
                $manfactorClass = new ManufactorClass();
                $data['mClass']['class_id'] = $model->id;
                foreach ($data['mClass'] as $key => $val) {
                    $manfactorClass->$key = $val;
                }
                $manfactorClass->save(false);
            }
            
            $transaction->commit();
            return true;
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
            
        }
    }
    
    /**
     * 修改类目
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/11
     * Time: 17:47
     * @param $data
     * @return bool
     * @throws \Exception
     */
    public static function edit($data)
    {
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();
        try {
        
            //修改类目
            if (isset($data['class']) && !empty($data['class']))
            {
                //修改类目
                $model = self::findOne(['id'=>$data['class']['id']]);
                unset($data['class']['id']);
                foreach ($data['class'] as $key => $val) {
                    $model->$key = $val;
                }
                $model->save(false);
            
            }
            
            if (isset($data['changePids']) && !empty($data['changePids']))
            {
                //修改类目父级id
                foreach ($data['changePids'] as $key => $val) {
                    $changeClass = self::findOne(['id' => $key]);
                    $changeClass->parent_ids  = $val['parent_ids'];
                    $changeClass->update_id   = $val['update_id'];
                    $changeClass->update_time = $val['update_time'];
                    $changeClass->save(false);
                }
                
            }
            
        
            if (isset($data['mClass']) && !empty($data['mClass']))
            {
                //修改类目关系
                $manfactorClass = ManufactorClass::getList(['direct_company_id'=>$data['mClass']['direct_company_id'],'class_id'=>$data['mClass']['class_id']]);
                
                foreach ($manfactorClass as $key => $val) {
                    $mClass = ManufactorClass::findOne(['id'=>$val['id']]);
                    $mClass->class_pid   = $data['mClass']['class_pid'];
                    $mClass->update_time = $data['mClass']['update_time'];
                    
                    $mClass->save(false);
                }
                
            }
        
            $transaction->commit();
            return true;
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        
        }
    }
    
    /**
     * 更改类目状态
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/11
     * Time: 17:47
     * @param $data
     * @return bool
     * @throws \Exception
     */
    public static function changeStatus($data)
    {
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();
        try {
            if (isset($data) && !empty($data))
            {
                //操作自身类目
                $model = self::findOne(['id'=>$data['id']]);
    
                $model->status      = $data['status'];
                $model->update_id   = $data['user'];
                $model->update_time = $data['time'];
                
                if (!$model->save(false))
                {
                    $transaction->rollBack();
                    return false;
                }
                
                //启用
                if (isset($data['status']) && $data['status'] == 1)
                {
                    $serviceClass = self::getList(['id'=>explode(',',$model->parent_ids),'department_id'=>$data['top_id'],'status'=>2]);
                    
                    if (!empty($serviceClass))
                    {
                        $classId = array_unique(array_column($serviceClass,'id'));
                        
                        $attr = [
                            'status'      => 1,
                            'update_id'   => $data['user'],
                            'update_time' => $data['time'],
                        ];
                        
                        self::updateAll($attr,['id' =>$classId,'status'=>2]);
                        
                        array_push($classId,$data['id']);
    
                        $product = Product::getList(['department_top_id'=>$data['top_id'],'class_id'=>$classId,'status'=>2]);
                        if (!empty($product))
                        {
                            $brandId = array_unique(array_column($product,'brand_id'));
                            $brand   = ServiceBrand::getList(['id'=>$brandId,'status'=>1,'department_id'=>$data['top_id']]);
                            $prod    = Product::getList(['department_top_id'=>$data['top_id'],'class_id'=>$classId,'brand_id'=>array_column($brand,'id'),'status'=>2]);
                            
                            if (!empty($prod))
                            {
                                $prodId = array_column($prod,'id');
                                $attr   = [
                                    'status'      => 1,
                                    'update_time' => $data['time'],
                                ];
    
                                Product::updateAll($attr,['id' =>$prodId,'status'=>2]);
                            }
                        }
                    }
                }
                
                //禁用
                if (isset($data['status']) && $data['status'] == 2)
                {
                    $childClassIds = self::getChilds($data['id']);
                    $serviceClass  = self::getList(['id'=>$childClassIds,'department_id'=>$data['top_id']]);

                    if (!empty($serviceClass))
                    {
                        $classId = array_unique(array_column($serviceClass,'id'));
        
                        $attr = [
                            'status'      => 2,
                            'update_id'   => $data['user'],
                            'update_time' => $data['time'],
                        ];
        
                        self::updateAll($attr,['id' =>$classId,'status'=>1]);
        
                        array_push($classId,$data['id']);
     
                        $product = Product::getList(['department_top_id'=>$data['top_id'],'class_id'=>$classId,'status'=>1]);
                        
                        if (!empty($product))
                        {
    
                            $prodId = array_column($product,'id');
                            $attr   = [
                                'status'      => 2,
                                'update_time' => $data['time'],
                            ];
    
                            Product::updateAll($attr,['id' =>$prodId,'status'=>1]);
                            
                        }
                    }
                }
            }
            
            $transaction->commit();
            return true;
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
    
        }
    }

    public static function getTitle($id=null)
    {
        $goodsData = self::find()
            ->select('title')
            ->where(['id'=>$id])->asArray()->one();
        return $goodsData['title'];
    }
}
