<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

class CostLevelItem extends ActiveRecord
{
    public static function getDb() {
        return Yii::$app->order_db;
    }
    
    public static function tableName()
    {
        return 'cost_level_item';
    }

    /**
     * 根据 cost_id 查收费项目名称
     * @param array $costIds
     * @return array
     */
    public static function getCostAttrByIds(array $costIds)
    {
        $query = self::find()
            ->where(['cost_item_id'=>$costIds,'status'=>1])
            ->select('cost_item_id,cost_item_name,unit')
            ->groupBy('cost_item_id')
            ->asArray()->all();
        if($query)
        {
            return ArrayHelper::index($query,'cost_item_id');
        }
        return [];
    }
}