<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;

class CostPayType extends ActiveRecord
{
    const SJ_PAY = 1;
    const FWS_PAY = 2;
    const KH_PAY = 3;
    const MF_PAY = 4;

    public static function getDb() {
        return Yii::$app->order_db;
    }
    
    public static function tableName()
    {
        return 'cost_pay_type';
    }
    /**
     * 获取单个付费方
     * @author sxz
     * @param type $where
     * @return type
     */
    public static function getOne($where = [])
    {
        return self::find()->where(['status'=>1])->andWhere($where)->asArray()->one();
    }
    /**
     * 获取多个付费方
     * @author sxz
     * @param type $where
     * @return type
     */
    public static function getList($where = [])
    {
        return self::find()->where(['status'=>1])->andWhere($where)->asArray()->all();
    }

    /**
     * 查出付费方信息
     * @return array
     */
    public static function getNameRel()
    {
        $query = self::find()
            ->where(['status'=>1])
            ->select('id,type_name')
            ->asArray()->all();

        if($query)
        {
            return array_column($query,'type_name','id');
        }
        return [];
    }
    
}