<?php
    /**
     * Created by PhpStorm.
     * User: xingq
     * Date: 2018/3/22
     * Time: 10:20
     */
    
    namespace common\models;
    
    
    class DictEnum extends BaseModel
    {
        public static function tableName()
        {
            return 'dict_enum';
        }
        /**
         * 获取单条数据
         * User: xingq
         * Email: liuxingqi@services.cn
         * Date: 2018/3/22
         * Time: 9:59
         * @param array $where
         * @return array|null|ActiveRecord
         */
        public static function getOne($where = [])
        {
            return self::find()->where($where)->one();
        }
        /**
         * 获取多条数据
         * User: xingq
         * Email: liuxingqi@services.cn
         * Date: 2018/3/22
         * Time: 10:00
         * @param array $where
         * @return array|ActiveRecord[]
         */
        public static function getList($where = [])
        {
            return self::find()->where($where)->asArray()->all();
        }

        /**
         * 获取值
         * @param $id
         * @return string
         */
        public static function getDesc($key,$id)
        {
            $query = self::findOneByAttributes(['dict_key'=>$key,'dict_enum_id'=>$id]);
            if($query){
                return $query['dict_enum_value'];
            }
            return '';
        }

        /**
         * 获取数据
         * @param $dictKey
         * @return array
         */
        public static function getDataByKey($dictKey)
        {
            $query = self::findAllByAttributes(['dict_key' => $dictKey], 'dict_enum_id as id,dict_enum_value as title');
            if($query){
                return $query;
            }

            return [];
        }
    }