<?php

namespace common\models;

use Yii;

class ManufacturerType extends BaseModel
{
    public static function tableName()
    {
        return 'manufacturer_type';
    }

    /**
     * 获取服务类型
     * @param $where
     * @return array|\yii\db\ActiveRecord[]
     * @author xi
     */
    public static function findTypeByWhere($where)
    {
        $query = self::find()
                ->from(self::tableName() . ' as a')
                ->innerJoin(['`'.ServiceType::tableName().'` as b'],'a.type_id = b.id')
                ->where($where)
                ->select('a.type_id,b.title')
                ->asArray()->all();
        return $query;
    }
}
