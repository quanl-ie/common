<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use common\models\ContractDetail;
use common\models\Product;
use webapp\models\SaleOrder;
use common\models\ContractAttachment;

/**
 * 合同管理
 * @author liuxingqi <lxq@c-ntek.com>
 * @date 2017-12-15
 */
class  Contract extends BaseModel
{

    public static function tableName()
    {
        return 'contract';  //合同表
    }


    public static function showStatus(){

        return ['0'=>'未知','1'=>'待审批','2'=>'审批通过','3'=>'执行中','4'=>'执行完毕','5'=>'审批未通过','6'=>'已终止','7'=>'已删除'];
    }


    public static function showCollectionStatus(){
        return ['0'=>'未知','1'=>'全款','2'=>'未收款','3'=>'定金'];
    }

    public static function showInvoiceType(){
        return ['0'=>'未知','1'=>'不开票','2'=>'企业','3'=>'个人'];
    }

    public static function showExpressType(){
        return ['0'=>'','1'=>'带货安装','2'=>'客户自提','3'=>'物流运输'];
    }

    public static function showStoreOutStatus(){
           return ['0'=>'无需出库','1'=>'待出库','2'=>'部分出库','3'=>'全部出库'];

    }
    //合同类型
    public static function showContractType(){
        return ['1'=>'销售','2'=>'分销','3'=>'售后服务'];
    }
    //付款方式
    public static function showPaymentType(){
        return ['1'=>'现场收费','2'=>'微信','3'=>'支付宝','4'=>'打款公司','5'=>'门店代收'];
    }



    //获取单条信息
    public static function getOne($where,$flag = '')
    {
        if ($flag) {
            return self::find()->where($where)->one();
        }
        return self::find()->where($where)->asArray()->one();
    }



    //获取多条信息
    public static function getList($param)
    {
        if(isset($param['pageSize'])){
            $pageSize = $param['pageSize'];
        }else{
            $pageSize = 10;
        }
        if(!isset($param['currentPage'])){
            $page = 0;
        }else{
            $page = $param['currentPage']-1;
        }

        unset($param['currentPage']);
        unset($param['pageSize']);

        if($param['department_id']){
            $db = self::find();
            if(isset($param['startTime']) && $param['startTime']){
                $db->andFilterWhere(['>','signed_date',$param['startTime']]);
                unset($param['startTime']);
            }
            if(isset($param['endTime']) && $param['endTime']){
                $db->andFilterWhere(['<','signed_date',$param['endTime']]);
                unset($param['endTime']);
            }
            if(isset($param['subject']) && $param['subject']){
                $db->andFilterWhere(['like','subject',$param['subject']]);
                unset($param['subject']);
            }
            foreach($param as $k=>$v){
                if(!empty($v)){
                    $db->andwhere([$k=>$v]);
                }
            }

            $count = $db->count();
            $totalPage = ceil($count/$pageSize);
            $start = $page*$pageSize;
            $limit = $pageSize;
            $list =$db->offset($start)
                ->limit($pageSize)
                ->orderBy('id desc')
                ->asArray()
                ->all();
           //echo $db->createCommand()->getRawSql();exit;

        }

        return [
            'page'       => $page+1,
            'totalCount' => isset($count) ? $count : 0,
            'totalPage'  => isset($totalPage) ? $totalPage : 0,
            'list'       => isset($list) ? $list : ''
        ];

    }



    //添加
    public static function add($contractData,$productData=[],$fileArr=[])
    {
        $model = new self();
        //处理数据
        foreach ($contractData as $key => $val) {
            $model->$key = $val;
        }
        //开启事务
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();
        try {
            //添加数据
          $model->save(false);
            $id = $model->id;
            $productList = $fileArrList = [];
            foreach($productData as &$v){
                if(!isset($v['prod_id'])){
                    continue;
                }
                $v['department_top_id'] = $contractData['department_top_id'];
                $v['department_id']     = $contractData['department_id'];
                $v['parent_id']         = $id;
                $v['finish_num']        = 0;
                $v['total_num']         = $v['prod_num']; //合同剩余数量，默认产品总量
                $productList[]          = $v;
            }
            //插入附件数据
            if($fileArr)
            {
                $time = time();
                foreach($fileArr as &$v){
                    $v['parent_id']    = $id;
                    $v['create_time']  = $time;
                    $fileArrList[]     = $v;
                }
            }
            $db->createCommand()
                ->batchInsert(ContractDetail::tableName(),['prod_id','prod_num','sale_price','rate','amount','total_amount','warranty_date','department_top_id','department_id','parent_id','finish_num','total_num'],
                    $productList)
                ->execute();
            $db->createCommand()
                ->batchInsert(ContractAttachment::tableName(),['name','attachment_url','parent_id','create_time'],
                    $fileArrList)
                ->execute();
            $transaction->commit();
            return [
                'id' => $model->id
            ];
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }
    
    //编辑
    public static function edit($contractData,$productData=[],$fileArr=[])
    {
        $db = new self();
        //处理数据
        $model = $db->findOne([$contractData['id']]);
        foreach ($contractData as $key => $val) {
            if($val){
                $model->$key = $val;
            }

        }
        //开启事务
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();
        try {
            //更改数据
            $model->save(false);
            $id = $model->id;
            $productList = [];
            //删除旧的数据
            $old_data = ContractDetail::deleteAll(['parent_id' => $id]);
            
            foreach($productData as &$v){
                if(!isset($v['prod_id'])){
                    continue;
                }
                $v['department_top_id'] = $contractData['department_top_id'];
                $v['department_id']     = $contractData['department_id'];
                $v['parent_id']         = $id;
                $v['total_num']         = $v['prod_num'];
                $prodInfo = ContractDetail::findOne(['parent_id'=>$id,'prod_id'=>$v['prod_id']]);
                if(!empty($prodInfo->prod_id)){
                    $prodInfo->prod_num = $v['prod_num'];
                    $prodInfo->sale_price = $v['sale_price'];
                    $prodInfo->amount = $v['amount'];
                    $prodInfo->rate = $v['rate'];
                    $prodInfo->total_amount = $v['total_amount'];
                    $prodInfo->total_num = $v['prod_num']; //合同剩余数量，默认产品总量
                    $prodInfo->warranty_date = $v['warranty_date'];
                    $prodInfo->save(false);
                }else{
                    $productList[] = $v;
                }
            }
            //删除旧的数据
            ContractAttachment::deleteAll(['parent_id' => $id]);
            //插入附件数据
            if($fileArr)
            {
                $time = time();
                foreach($fileArr as &$v){
                    $v['parent_id']    = $id;
                    $v['create_time']  = $time;
                    $fileArrList[]     = $v;
                }
            }
            if(!empty($productList)){
                $db->createCommand()
                    ->batchInsert(ContractDetail::tableName(),['prod_id','prod_num','sale_price','rate','amount','total_amount','warranty_date','department_top_id','department_id','parent_id','total_num'],
                        $productList)
                    ->execute();
            }
            //print_r($fileArrList);die;
            if(!empty($fileArrList)){
                $db->createCommand()
                    ->batchInsert(ContractAttachment::tableName(),['name','attachment_url','parent_id','create_time'],
                        $fileArrList)
                    ->execute();
            }
            $transaction->commit();
            return [
                'id' => $model->id
            ];
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }

    }
    


//更改状态
    public static function updateStatus($param,$id){
        $model = self::findOne(['id'=>$id]);
        foreach($param as $k=>$v){
            $model->$k=$v;
        }
        $model->save(false);
        return $model->id;
    }


    /**
     * 审批操作
     * @author Cheng Juanjuan
     * @date   2017-12-21
     */
    public static function checkOption($param,$id,$product=''){

        $model = self::findOne(['id'=>$id]);
        foreach($param as $k=>$v){
            $model->$k=$v;
        }
        $model->save(false);
        //开启事务
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();
        try {
            $id = $model->id;
            $productList = '';
            if($param['audit_status']==1){
                if(!empty($product)){
                    //去除合同审核通过将产品添加到客户中
                    /*foreach($product as $k=>$v){
                        $saleOrder = SaleOrder::add($v);
                    }*/
                }
            }
            $transaction->commit();
            return $id;
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
        return false;
    }


    public static function getContractDetail($where = [])
    {
        $db = self::find()
            ->from(self::tableName() . ' as a')
            ->leftJoin([ContractDetail::tableName() . ' as b'],'a.id = b.parent_id')
            ->select('b.id,b.prod_id,b.parent_id,a.subject')
            ->where($where)
            ->groupBy('a.subject')
            ->asArray();
        
        return $db->all();
    }
}