<?php
namespace common\models;

use Yii;
use yii\base\Model;
use common\helpers\Helper;

/**
 * Login form
 */
class LoginForm extends Model
{
    public $username;
    public $password;
    public $rememberMe = true;
    public $verifyCode;
    private $_user;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
            ['verifyCode', 'captcha','captchaAction'=>'site/captcha'],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'username'   =>'用户名',
            'password'   => '密码',
            'rememberMe' => '记住我',
            'verifyCode' => '',
            'mobile'     => '手机号'
        ];
    }
    
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, '用户名或密码错误');
            }
        }
    }
    
    public function login()
    {   
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        } else {
            return false;
        }
    }
    
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::findByUsername($this->username);
            if($this->_user){
                $this->_user->last_login_time = time();
                $this->_user->login_ip        = Helper::getIp();
                $this->_user->save(false);
            }
        }
        return $this->_user;
    }

}
