<?php
namespace common\models;

use webapp\logic\BaseLogic;
use yii\db\ActiveRecord;
use Yii;

class PickUser extends BaseModel
{
    
    public static function tableName()
    {
        return 'pick_user';
    }

    /**
     * @param array $where
     * @return array|null|ActiveRecord
     * 获取单条数据
     */
    public static function getOne($where = [])
    {
        return self::find()->where($where)->asArray()->one();
    }

    /**
     * @param array $where
     * @return array|ActiveRecord[]
     * 获取多条数据
     */
    public static function getList($where = [])
    {
        return self::find()->where($where)->asArray()->all();
    }

    /**
     * @param $data
     * @return bool|mixed
     * @throws \Exception
     * 添加
     */
    //添加数据
    public static function add($data){
        $model = new self();
        //处理数据
        foreach ($data as $key => $val) {
            $model->$key = $val;
        }
        //开启事务
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();
        try {
            //库房表添加数据
            $res = $model->save(false);
            $transaction->commit();
            if($res){
                return $model->id;
            }

        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }

    }

    //编辑修改数据
    public static function edit($data){
        //查询数据
        $model = self::findOne(['id'=>$data['id']]);
        //处理数据
        foreach ($data as $key => $val) {
            $model->$key = $val;
        }
        //开启事务
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();
        try {
            $model->save(false);
            $transaction->commit();
            return true;
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    public static function upAll($ids){
        $attr = [
            'status'      => 2,
            'update_time' => time(),
        ];
        return self::updateAll($attr,['id' =>$ids,'status'=>1]);
    }
}