<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;


class  AppraisalSetConfig extends BaseModel
{

    public static function tableName()
    {
        return 'sys_switch_config';  //评论开关表
    }
    
    public function attributeLabels()
    {
        return [
            'id' => '',
            'num' => '天数',
        ];
    }
    
    public function rules()
    {
        return [
            [['id','num'],'required']
        ];
    }
    
    
    public static function getOne($where = [],$flag = '')
    {
        if ($flag) {
            return self::find()->where($where)->one();
        }else{
            return self::find()->where($where)->asArray()->one();
        }
        
    }

    /**
     * @param $data
     * @return bool
     * @throws \Exception
     * 添加编辑
     */
    public static function edit($data)
    {
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();
        try {

            if(isset($data['id']) && $data['id']>0){
                $model = self::getOne(['id'=>$data['id'],'type'=>3,'status'=>1],1);
                $model->num = $data['num'];
                $model->update_time = time();
                $model->save(false);
            }else{
                unset($data['id']);
                $model = new self();
                foreach($data as $k=>$v){
                    $model->$k = $v;
                }
                $model->status = 1;
                $model->create_time = time();
                $model->update_time = time();
                $model->save(false);
            }
            $transaction->commit();
            return true;
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
        
    }
}