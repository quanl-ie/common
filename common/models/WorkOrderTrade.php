<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;

class WorkOrderTrade extends BaseModel
{
    public static function getDb() {
        return Yii::$app->order_db;
    }
    
    public static function tableName()
    {
        return 'work_order_trade';
    }
}