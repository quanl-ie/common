<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * UserInfo model
 *
 */
class UserInfo extends ActiveRecord
{
    public static function tableName()
    {
        return 'user_info';
    }
    
    /**
     * 获取厂家资料信息
     * @author liwenyong<liwenyong@c-ntek.com>
     * @date   2017-08-29
     * @return object 
     */
    public static function getData($id) 
    {
        $data = self::find()->where(['user_id' => $id])->asArray()->one();
        return $data;
    }
}
