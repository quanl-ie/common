<?php
namespace common\models;

use Yii;

class WorkAppraisalDict extends BaseModel
{
    public static function getDb()
    {
        return Yii::$app->order_db;
    }

    public static function tableName()
    {
        return 'work_appraisal_dict';
    }

    /**
     * 获取默认标签
     * @return array
     * @author xi
     */
    public static function getInitTags()
    {
        $tagArr = [
            [   'star' => '1',
                'num' => '1',
                'title' => '专业技能棒',
                'icon' => '/images/medal/1-1.png',
                'gray_icon' => '/images/medal/1-0.png',
            ],
            [
                'star' => '1',
                'num' => '2',
                'title' => '态度好服务棒',
                'icon' => '/images/medal/2-1.png',
                'gray_icon' => '/images/medal/2-0.png',
            ],
            [
                'star' => '1',
                'num' => '3',
                'title' => '仪容仪表整洁',
                'icon' => '/images/medal/3-1.png',
                'gray_icon' => '/images/medal/3-0.png',
            ],
            [
                'star' => '1',
                'num' => '4',
                'title' => '上门准时',
                'icon' => '/images/medal/4-1.png',
                'gray_icon' => '/images/medal/4-0.png',
            ],
            [
                'star' => '2',
                'num' => '1',
                'title' => '专业技能一般',
                'icon' => NULL,
                'gray_icon' => NULL,
            ],
            [
                'star' => '2',
                'num' => '2',
                'title' => '态度不友好',
                'icon' => NULL,
                'gray_icon' => NULL,
            ],
            [
                'star' => '2',
                'num' => '3',
                'title' => '忘带工具',
                'icon' => NULL,
                'gray_icon' => NULL,
            ],
            [
                'star' => '2',
                'num' => '4',
                'title' => '没带配件',
                'icon' => NULL,
                'gray_icon' => NULL,
            ],
            [
                'star' => '2',
                'num' => '5',
                'title' => '上门迟到',
                'icon' => NULL,
                'gray_icon' => NULL,
            ],
            [
                'star' => '2',
                'num' => '6',
                'title' => '无工作服',
                'icon' => NULL,
                'gray_icon' => NULL,
            ],
            [
                'star' => '3',
                'num' => '1',
                'title' => '专业技能差',
                'icon' => NULL,
                'gray_icon' => NULL,
            ],
            [
                'star' => '3',
                'num' => '2',
                'title' => '服务态度恶劣',
                'icon' => NULL,
                'gray_icon' => NULL,
            ],
            [
                'star' => '3',
                'num' => '3',
                'title' => '忘带工具',
                'icon' => NULL,
                'gray_icon' => NULL,
            ],
            [
                'star' => '3',
                'num' => '4',
                'title' => '没带配件',
                'icon' => NULL,
                'gray_icon' => NULL,
            ],
            [
                'star' => '3',
                'num' => '5',
                'title' => '上门迟到',
                'icon' => NULL,
                'gray_icon' => NULL,
            ],
            [
                'star' => '3',
                'num' => '6',
                'title' => '无工作服',
                'icon' => NULL,
                'gray_icon' => NULL,
            ],
            [
                'star' => '3',
                'num' => '7',
                'title' => '索要好评',
                'icon' => NULL,
                'gray_icon' => NULL,
            ],
        ];

        return $tagArr;
    }

    /**
     * 填加
     * @param $departmentId
     * @return bool
     */
    public static function add($departmentId)
    {
        foreach (self::getInitTags() as $val)
        {
            $arr = self::findOneByAttributes(['direct_company_id'=>$departmentId,'star' => $val['star'],'title'=>$val['title'],'status'=>1],'id');
            if(!$arr)
            {
                $model = new self();
                $model->department_id     = $departmentId;
                $model->direct_company_id = $departmentId;
                $model->star              = $val['star'];
                $model->num               = $val['num'];
                $model->title             = $val['title'];
                $model->is_custom         = '0';
                $model->icon              = $val['icon'];
                $model->gray_icon         = $val['gray_icon'];
                $model->status            = '1';
                $model->create_time       = time();
                $model->update_time       = time();
                $model->save();
            }
        }

        return true;
    }
    // 获取全部标签
    public static function getAllTags($departmentId){
        $query = self::find()
            ->where(['direct_company_id'=>$departmentId,'status'=>1])
            ->select('star,title')
            ->orderBy("star asc,num asc")
            ->asArray()
            ->all();
        if($query)
        {
            foreach ($query as $k=>$v){
                $new[$v['star']][] = $v['title'];
            }
            return $new;
        }
        return [];
    }
}