<?php

namespace common\models;
use webapp\modules\v5\models\CostLevelDetail;
use webapp\modules\v5\models\CostLevelRelation;
use yii\db\ActiveRecord;
use Yii;
use webapp\modules\v1\models\CostLevelItem;
use webapp\modules\v5\models\Department;

class CostItem extends ActiveRecord
{
    public static function getDb() {
        return Yii::$app->order_db;
    }



    public static function tableName ()
    {
        return 'cost_item';
    }

    public static function getList($srcType,$srcId){
       return  self::find()->select(['id','cs_id','cs_name'])->where(['src_type'=>$srcType,'src_id'=>$srcId])->asArray()->all();
    }

    public static function getData() {
        $data = self::find()->where(['status' => 1])->asArray()->all();
        $itemArr = [];
        foreach ($data as $k => $v) {
            $itemArr[$k]['id'] = $v['id'];
            $itemArr[$k]['cs_name'] = $v['cs_name'];
        }
        return $itemArr;
    }

    public static function getCostName($id) {
        $data = self::findOne(['cs_id' => $id, 'status' => 1]);
        if($data) {
            return $data->cs_name;
        }
        return '';
    }


    /**
     * 查询当前机构的顶级公司设置的收费项目
     * @param  int      $department_id  机构ID
     * @param  int      $type_id        服务类型ID
     * @param  int      $brand_id       品牌ID
     * @param  int      $class_id       产品类目ID
     * @param  string   $service_area   服务城市
     * @author li
     * @date   2018-06-19
     * @return
     */
    public static function getCostLevel($department_id, $type_id, $brand_id, $class_id, $service_area) {
        $cost_item = [];
        //查询当前机构的总公司
//        $top_id = Department::getTopId($department_id);
        //根据机构ID和总公司ID查询收费标准ID
        $cost_level_id = CostLevelRelation::getCostLevelId($department_id);
        //根据收费标准ID  type_id  class_id  brand_id  service_area  筛选需要的收费标准ID
        $costLevelArr = CostLevelDetail::screenCostLevelId($cost_level_id, $brand_id, $class_id, $type_id, $service_area);
        //查询收费项目
        foreach($costLevelArr as $k => $v) {
            $costItemArr = CostLevelItem::find()->where(['cost_level_id' => $v['cost_level_id'], 'level_detail_id' => $v['cost_level_detail_id'], 'status' => 1])->asArray()->all();
            if($costItemArr) {
                foreach($costItemArr as $kk => $vv) {
                    $cost_item[$kk]['cost_item_id']   = $vv['cost_item_id'];
                    $cost_item[$kk]['cost_item_name'] = $vv['cost_item_name'];
                    $cost_item[$kk]['price']          = $vv['price'] ? $vv['price'] : '';
                    $cost_item[$kk]['unit']           = $vv['unit'] ? $vv['unit'] : '';
                }
            }
        }
        return $cost_item;
    }


    /**
     * 根据 cost_id 查收费项目名称
     * @param array $costIds
     * @return array
     */
    public static function getCostNameByIds(array $costIds)
    {
        $query = self::find()
            ->where(['cs_id'=>$costIds])
            ->select('cs_id,cs_name')
            ->groupBy('cs_id')
            ->asArray()->all();
        if($query)
        {
            return array_column($query,'cs_name','cs_id');
        }
        return [];
    }


	/*
     * 数组去重
     */
    public static function array_unset_tt($arr,$key){
        $res = array();
        foreach ($arr as $value) {
            if(isset($res[$value[$key]])){
                unset($value[$key]);
            }
            else{
                $res[$value[$key]] = $value;
            }
         }
         return $res;
    }

    /**
     * 获取收费项目
     * @todo 以后有空可以把收费标准逻辑加上
     * @param $departmentId
     * @return array
     * @author xi
     * @date 2018-36-26
     */
    public static function getItems($departmentId)
    {
        $result = [];
        $query = self::find()
            ->where(['department_id'=>$departmentId,'status'=>1,'del_status'=>1])
            ->select('cost_item_set_id')
            ->asArray()
            ->all();
        if($query)
        {
            $ids = array_column($query,'cost_item_set_id');
            $setArr = CostItemSet::find()
                ->where(['id'=>$ids,'del_status'=>1])
                ->select('id,name,quote_id')
                ->asArray()->all();
            if($setArr)
            {
                foreach ($setArr as $val)
                {
                    $result [] = [
                        'cost_item_id'   => $val['id'],
                        'cost_item_name' => $val['name'],
                        'unit'           => '次',
                        'price'          => '',
                        'quote_id'       => $val['quote_id']
                    ];
                }
            }
        }

        return $result;
    }

}