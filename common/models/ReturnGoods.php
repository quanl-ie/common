<?php
namespace common\models;

use webapp\logic\ContractLogic;
use Yii;
use yii\db\ActiveRecord;
use common\models\ContractDetail;


/**
 * 合同管理
 * @author liuxingqi <lxq@c-ntek.com>
 * @date 2017-12-15
 */
class  ReturnGoods extends BaseModel
{

    public static function tableName()
    {
        return 'return';  //退货
    }


    public static function showStatus(){

        return ['0'=>'未知','1'=>'待审批','2'=>'审批通过','3'=>'执行中','4'=>'执行完毕','5'=>'审批未通过','6'=>'已终止'];
    }


    public static function showReasonStatus(){
        return ['1'=>'产品质量','2'=>'服务质量','3'=>'客户原因','4'=>'销售退货','5'=>'其他'];
    }

    public static function showInvoiceType(){
        return ['0'=>'未知','1'=>'不开票','2'=>'企业','3'=>'个人'];
    }

    public static function showExpressType(){
        return ['0'=>'未知','1'=>'客户自提','2'=>'客户自提','3'=>'物流运输'];
    }

/*    //规则
    public function rules()
    {
        return [
            [['org_type','org_id','account_type','account_name','sex','create_user','create_time','status','mobile','source_type'],'required'],
            ['mobile','match','pattern'=>'/^1[3456789]\d{9}$/','message'=>'手机号格式不正确'],
        ];
    }*/

  /*  public function attributeLabels()
    {
        return [
            'id'             => 'ID',
            'code'           => '编码',
            'src_type'       => '来源',  //14 厂商 13 服务商 12 门店 11 技师 20 c端用户',
            'src_id'         => '所有者唯一标识',
            'account_id'     => '用户分类',  //1：普通，2：VIP
            'account_name'   => '用户称呼',
            'sex'            => '性别', //1：先生，2：女士
            'create_user'    => '创建人',  //id
            'create_time'    => '创建时间',
            'update_time'    => '修改时间',
            'status'         => '状态', //1：正常，2：停用。
            'mobile'         => '联系方式',
            'source_type'    => '客户来源', //1：手动新建，2：售后时自动新建，3：其他渠道
        ];
    }*/
    
    //获取单条信息
    public static function getOne($where,$flag='')
    {
        if ($flag) {
            return self::find()->where($where)->one();
        }
        return self::find()->where($where)->asArray()->one();
    }



    //获取多条信息
    public static function getList($param)
    {
        if(isset($pageSize)){
            $pageSize = $param['pageSize'];
        }else{
            $pageSize = 10;
        }
        if(!isset($param['currentPage'])){
            $page = 0;
        }else{
            $page = $param['currentPage']-1;
        }

        unset($param['currentPage']);
        unset($param['pageSize']);

        if($param['department_id']){
            $db = self::find();
            if(isset($param['startTime']) && $param['startTime']){
                $db->andFilterWhere(['>','signed_date',$param['startTime']]);
                unset($param['startTime']);
            }
            if(isset($param['endTime']) && $param['endTime']){
                $db->andFilterWhere(['<','signed_date',$param['endTime']]);
                unset($param['endTime']);
            }
            foreach($param as $k=>$v){
                if(!empty($v)){
                    $db->andwhere([$k=>$v]);
                }
            }
            $count = $db->count();
            $totalPage = ceil($count/$pageSize);
            $start = $page*$pageSize;
            $limit = $pageSize;
            $list =$db->offset($start)
                ->limit($pageSize)
                ->orderBy('id desc')
                ->asArray()
                ->all();
         //echo $db->createCommand()->getRawSql();exit;
        }

        return [
            'page'       => $page+1,
            'totalCount' => isset($count) ? $count : 0,
            'totalPage'  => isset($totalPage) ? $totalPage : 0,
            'list'       => isset($list) ? $list : ''
        ];

    }



    //添加
    public static function add($returnData,$productData)
    {
        

        
        $model = new self();
        //处理数据
        foreach ($returnData as $key => $val) {
            $model->$key = $val;
        }
        //开启事务
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();
        try {
            //添加数据
            $model->save(false);
            $id = $model->id;
            foreach($productData as $k => $v){
                if(!isset($v['prod_id'])){
                    $transaction->rollBack();
                }
                unset($productData[$k]['prod_type']);
                unset($productData[$k]['status']);
                $productData[$k]['parent_id'] = $id;
                if($model->need_stock_in==2){
                    $productData[$k]['finish_num'] =   $productData[$k]['prod_num'];
                }else{
                    $productData[$k]['finish_num'] = 0;
                }

                unset($productData[$k]['send_num']);
            }

            $db->createCommand()
                ->batchInsert(ReturnGoodsDetail::tableName(),['department_id','department_top_id','prod_id','prod_num','price','amount','rate','total_amount','sale_price','parent_id','finish_num'],
                    $productData)
                ->execute();

            $transaction->commit();
            return [
                'id' => $model->id
            ];
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    //编辑
    public static function edit($exchangeData,$productData=[])
    {
        $where['id'] = $exchangeData['id'];
        $model = self::findOne($where);
        foreach ($exchangeData as $key => $val) {
            $model->$key = $val;
        }
        $model->save(false);

        //开启事务
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();
        try {
            $id = $model->id;
            $productList = [];
            if(!empty($productData)){
                foreach($productData as &$v){
                    if(!isset($v['prod_id'])){
                        continue;
                    }
                    $prodId = $v['prod_id'];
                    ReturnGoodsDetail::deleteAll(['parent_id'=>$id]);
                    $v['department_id']     = $exchangeData['department_id'];
                    $v['parent_id']         = $id;
                    if($model->need_stock_in==2){
                        $v['finish_num'] = $v['prod_num'];
                    }else{
                        $v['finish_num'] = 0;
                    }
                    $productList[] = $v;
                }

                $db->createCommand()
                    ->batchInsert(ReturnGoodsDetail::tableName(),['prod_id','prod_num','sale_price','rate','amount','total_amount','warranty_date','department_id','parent_id','finish_num'],
                        $productList)
                    ->execute();
                $transaction->commit();
            }
            return true;
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
        return false;
    }


    
    /**
     * 获取客户信息
     * @author li
     * @date   2017-12-21
     */
   /* public static function getAccountInfo($name, $src_id) {
        $data = self::find()
                ->where(['like', 'account_name', $name])
                ->orWhere(['like', 'mobile', $name])
                ->andWhere(['src_type' => BaseModel::SRC_SJ, 'src_id' => $src_id, 'status' => 1])
                ->select(['id','account_name','mobile'])
                ->asArray()->all();
        return $data;
    }*/


    public static function updateStatus($param,$id){
        $model = self::findOne(['id'=>$id]);
        foreach($param as $k=>$v){
            $model->$k=$v;
        }
        $model->save(false);
        return $model->id;
    }



    /**
     * 审批操作
     * @author Cheng Juanjuan
     * @date   2017-12-21
     */
    public static function checkOption($param,$id,$contractId='',$needUpdate=[]){

        $model = self::findOne(['id'=>$id]);
        foreach($param as $k=>$v){
            $model->$k=$v;
        }
        $model->save(false);

        //开启事务
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();
        try {

            $id = $model->id;
            if($contractId){
                foreach($needUpdate as $v){
                    $model = ContractDetail::findOne(['parent_id'=>$v['parent_id'],'prod_id'=>$v['prod_id']]);
                    $model->total_num=$v['total_num'];
                    $model->save(false);
                }
                ContractLogic::checkContract($contractId,2);
                $transaction->commit();
            }
            return $id;
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
        return false;
    }

}