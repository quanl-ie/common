<?php
namespace common\models;

use Yii;
class  Dynaactionform extends BaseModel
{

    public static function tableName()
    {
        return 'dynaactionform';
    }

    /**
     * 查出动态表单数据
     * @param $departmentId
     * @param int $type
     * @return array|mixed
     * @author xi
     */
    public static function getFormData($departmentId, $type = 1)
    {
        if($departmentId)
        {
            $where = [
                'type' => $type
            ];
            $query = self::find()
                ->where($where)
                ->andWhere("FIND_IN_SET(department_ids,$departmentId)")
                ->select("data")
                ->asArray()->one();
            if($query)
            {
                return json_decode($query['data'] , true);
            }
        }

        return [];
    }
}