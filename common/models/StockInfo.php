<?php
namespace common\models;

use webapp\logic\ContractLogic;
use webapp\models\TechProdStock;
use webapp\models\TechProdStockDetail;
use webapp\models\TechStorage;
use yii\db\ActiveRecord;
use common\models\StockDetail;
use Yii;
/**
 * 出入库模型
 * @author lxq liuxingqi@services.cn
 * Class Product
 * @package webapp\models
 */
class StockInfo extends BaseModel
{

    public static function tableName()
    {
        return 'stock_info';
    }

    /**
     * 获取单条数据
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/3/22
     * Time: 9:59
     * @param array $where
     * @return array|null|ActiveRecord
     */
    public static function getOne($where = [],$flag = '')
    {
        if ($flag) {
            return self::find()->where($where)->asArray()->one();
        }else{
            return self::find()->where($where)->one();
        }
    }

    /**
     * 获取多条数据
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/3/22
     * Time: 10:00
     * @param array $where
     * @return array|ActiveRecord[]
     */
    public static function getList($where = [])
    {
        return self::find()->where($where)->asArray()->all();
    }

    /**
     * 列表（分页，搜索）
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/4/20
     * Time: 16:15
     * @param $map
     * @param $where
     * @param int $page
     * @param int $pageSize
     * @return array
     */
    public static function index($map,$where,$page=1,$pageSize=10)
    {
        $db = self::find();
        $db->where($map);

        //判断where
        if (!empty($where)) {
            foreach ($where as $val) {
                $db->andFilterWhere($val);
            }
        }

        //总数
        $totalNum = $db->count();

        //当有结果时进行组合数据
        if ($totalNum>0) {
            //总页数
            $totalPage = ceil($totalNum/$pageSize);
            if($page<1)
            {
                $page = 1;
            }
            else if($page>$totalPage)
            {
                $page = $totalPage;
            }

            $db->select('id,no,subject,type,status,isship,audit_status,audit_date,stock_type,create_time');

            $db->offset(($page-1)*$pageSize);
            $db->orderBy(['create_time'=> SORT_DESC]);
            $db->limit($pageSize);
            $db->asArray();
            $list = $db->all();
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => $totalPage,
                'list'       => $list
            ];
        }
        else
        {
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => 0,
                'list'       => []
            ];
        }
    }

    /**
     * 添加
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/4/20
     * Time: 16:15
     * @param $stockInfo
     * @param $stockDetail
     * @param $type
     * @return bool
     * @throws \Exception
     */
    public static function add($stockInfo,$stockDetail,$type)
    {
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();
        try {

            if (!$stockInfo) {
                return false;
            }

            if (isset($stockInfo['id'])) {
                $model = self::getOne(['id'=>$stockInfo['id']]);
            }else{
                $model = new self();
            }
            foreach ($stockInfo as $key => $val) {
                $model->$key = $val;
            }
            if (!$model->save(false)) {
                return false;
            }

            if (!$stockDetail) {
                return false;
            }
            foreach ($stockDetail as $key => $val) {
                if (!isset($val['parent_id'])) {
                    $stockDetail[$key]['parent_id'] = $model->id;
                    $stockDetail[$key]['create_date'] = date('Y-m-d H:i:s',time());
                }
            }
            if ($type == 2) {
                $field = ['prod_id','depot_id','prod_num','serial_number','direct_company_id','parent_id','create_date'];
            }else{
                $field = ['prod_id','prod_num','serial_number','depot_id','prod_batch','direct_company_id','parent_id','create_date'];
            }
            if ($stockDetail) {

                $db->createCommand()
                    ->batchInsert(StockDetail::tableName(),$field,
                        $stockDetail)
                    ->execute();
            }
            $transaction->commit();
            return $model->id;
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }


    public static function edit($data)
    {
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();
        try {
            $id = [];

            foreach ($data as $key => $val) {
                $id[] = $val['id'];
                unset($data[$key]['id']);
            }

            StockDetail::deleteAll(['id'=>$id]);

            $field = ['direct_company_id','parent_id','prod_id','prod_num','depot_id','prod_batch','prod_date','effective_date','serial_number','create_date','time_stamp'];

            if ($data) {

                $db->createCommand()
                    ->batchInsert(StockDetail::tableName(),$field,
                        $data)
                    ->execute();
            }
            $transaction->commit();
            return true;
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }

    }

    /**
     * 出入库审批
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/4/20
     * Time: 16:14
     * @param $data
     * $shipSys  发货功能开启状态 0 未开启 1已开启
     * @return bool
     * @throws \Exception
     */
    public static function examine($data,$shipSys)
    {
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();
        try {

            //审批单据
            if (isset($data['stock']['id'])) {
                $stock = self::getOne(['id'=>$data['stock']['id']]);
                foreach ($data['stock'] as $key => $val) {
                    $stock->$key = $val;
                }
                $stock->save(false);
            }

            //采购单数据更改
            if (isset($data['purchase'])) {

                foreach ($data['purchase'] as $val) {
                    $purchaseDetail = PurchaseDetail::findOne(['id'=>$val['id']]);
                    $purchaseDetail->finish_num = $val['finish_num'];
                    $purchaseDetail->save(false);
                    $id[] = $val['parent_id'];
                }

                $id = array_unique($id);
                $purchase = PurchaseDetail::getList(['parent_id'=>$id]);
                if ($purchase) {
                    $num = [];
                    foreach ($purchase as $val) {
                        $num[] = $val['prod_num'] - $val['finish_num'];
                    }
                    $sum = array_sum($num);
                    $Purchase = Purchase::getOne(['id'=>$id],1);
                    if ($sum == 0) {
                        $Purchase->status = 1;
                    }elseif ($sum > 0) {
                        $Purchase->status = 2;
                    }
                    $Purchase->save(false);
                }
            }

            //爆溢入库数据存在时修改原单据详情
            if (isset($data['stockDetail'])) {
                foreach ($data['stockDetail'] as $val) {
                    $stockDetail = StockDetail::findOne(['id'=>$val['id']]);
                    $stockDetail->prod_num = $val['prod_num'];
                    $stockDetail->save(false);
                }
            }

            //判断是否存在爆溢入库数据
            if (isset($data['burst'])) {

                $model = new self();
                foreach ($data['burst']['stockInfo'] as $key => $val) {
                    $model->$key = $val;
                }
                $model->save(false);

                if (isset($data['burst']['stockDetail'])) {

                    foreach ($data['burst']['stockDetail'] as $key => $val) {
                        if (!isset($val['parent_id'])) {
                            $data['burst']['stockDetail'][$key]['parent_id'] = $model->id;
                        }
                    }
                    if ($data['burst']['stockDetail']) {

                        $db->createCommand()
                            ->batchInsert(StockDetail::tableName(),['direct_company_id','prod_id','prod_num','depot_id','prod_batch','prod_date','create_date','time_stamp','parent_id'],
                                $data['burst']['stockDetail'])
                            ->execute();
                    }
                }else{
                    $transaction->rollBack();
                    return false;
                }
            }

            //合同数据修改
            if (isset($data['contract'])) {
                foreach ($data['contract'] as $key => $val) {
                    $contractDetail = ContractDetail::findOne(['id'=>$val['id']]);
                    //系统销售未开启时，合同状态维持以前逻辑，修改合同状态
                    $contractDetail->finish_num = $val['finish_num'];
                    $contractDetail->save(false);
                    $id[] = $val['parent_id'];
                }

                $id = array_unique($id);
                $contractDetail = ContractDetail::getList(['parent_id'=>$id]);
                if ($contractDetail) {
                    $num = [];
                    foreach ($contractDetail as $val) {
                        $num[] = $val['total_num'] - $val['finish_num'];
                    }
                    $sum = array_sum($num);
                    $contract = Contract::getOne(['id'=>$id],1);

                    if ($sum == 0) {
                        //系统销售未开启时，合同状态维持以前逻辑，修改合同状态
                        if($shipSys != 1){
                            $contract->status = 4;
                        }
                        $contract->store_out_status = 3;

                    }elseif ($sum > 0) {
                        $contract->store_out_status = 2;
                    }
                    $contract->save(false);
                }
            }

            //退货数据修改
            if (isset($data['return'])) {
                foreach ($data['return'] as $val) {
                    $returnGoodsDetail = ReturnGoodsDetail::findOne(['id'=>$val['id']]);
                    $returnGoodsDetail->finish_num = $val['finish_num'];
                    $returnGoodsDetail->save(false);
                    $id[] = $val['parent_id'];
                }
                $id = array_unique($id);
                $returnGoodsDetail = ReturnGoodsDetail::getList(['parent_id'=>$id]);
                if ($returnGoodsDetail) {
                    $num = [];
                    foreach ($returnGoodsDetail as $val) {
                        $num[] = $val['prod_num'] - $val['finish_num'];
                    }
                    $sum = array_sum($num);
                    $returnGoods = ReturnGoods::getOne(['id'=>$id],1);
                    if ($sum == 0) {
                        $returnGoods->status = 4;
                        $returnGoods->save(false);
                        ContractLogic::checkContract($returnGoods->contract_id,2);
                    }
                }
            }

            //换货数据修改
            if (isset($data['exchange'])) {

                foreach ($data['exchange'] as $val) {
                    $exchangeGoodsDetail = ExchangeGoodsDetail::findOne(['id'=>$val['id']]);
                    $exchangeGoodsDetail->finish_num = $val['finish_num'];
                    $exchangeGoodsDetail->save(false);
                    $id[] = $val['parent_id'];
                }

                $id = array_unique($id);
                $exchangeGoodsDetail = ExchangeGoodsDetail::getList(['parent_id'=>$id]);
                if ($exchangeGoodsDetail) {
                    $num = [];
                    foreach ($exchangeGoodsDetail as $val) {
                        if($val['type'] == 2){
                            $num[] = $val['prod_num'] - $val['send_num'];
                        }else{
                            $num[] = $val['prod_num'] - $val['finish_num'];
                        }

                    }
                    $sum = array_sum($num);
                    $exchangeGoods = ExchangeGoods::getOne(['id'=>$id],1);
                    if ($sum == 0) {
                        if($stock->type ==1){
                            $exchangeGoods->status = 4;
                            ContractLogic::checkContract($exchangeGoods->contract_id,2);
                        }else{
                            if($shipSys != 1){
                                $exchangeGoods->status = 4;
                                ContractLogic::checkContract($exchangeGoods->contract_id,2);
                            }
                        }
                        //系统销售未开启时，合同状态维持以前逻辑，修改合同状态
                    }
                    $exchangeGoods->save(false);
                }
            }

            //技师申请
            if (isset($data['tech'])) {

                foreach ($data['tech'] as $val) {
                    $techProdStockDetail = TechProdStockDetail::findOne(['id'=>$val['id']]);
                    $techProdStockDetail->finish_num = $val['finish_num'];
                    $techProdStockDetail->save(false);
                    $id[] = $val['parent_id'];
                }

                $id = array_unique($id);
                $techProdStockDetail = TechProdStockDetail::getList(['parent_id'=>$id]);
                if ($techProdStockDetail) {
                    $num = [];
                    foreach ($techProdStockDetail as $val) {
                        $num[] = $val['prod_num'] - $val['finish_num'];
                    }
                    $sum = array_sum($num);
                    $techProdStock = TechProdStock::getOne(['id'=>$id],1);

                    if ($sum == 0) {
                        $techProdStock->audit_status = 3;
                    }
                    $techProdStock->save(false);
                }
            }

            //技师自有备件
            if (isset($data['techStorage'])) {
                //无库存是添加
                if (isset($data['techStorage']['add'])) {
                    if (isset($data['techStorage']['add'])) {
                        $field = ['direct_company_id','tech_id','prod_id','prod_batch','total_num','num','status','time_stamp'];

                        $db->createCommand()
                            ->batchInsert(TechStorage::tableName(),$field,$data['techStorage']['add'])
                            ->execute();
                    }
                }

                //库存存在时修改
                if (isset($data['techStorage']['update'])) {
                    foreach ($data['techStorage']['update'] as $val) {
                        $techStorage = TechStorage::findOne(['id'=>$val['id']]);
                        $techStorage->num       = $val['num'];
                        $techStorage->total_num = $val['total_num'];
                        $techStorage->save(false);
                    }
                }

            }
            //采购退货
            if (isset($data['returnPurchase'])) {
                if($shipSys != 1){
                    foreach ($data['returnPurchase'] as $val) {
                        $returnPurchaseDetail = ReturnPurchaseDetail::findOne(['id'=>$val['id']]);
                        $returnPurchaseDetail->finish_num = $val['finish_num'];
                        $returnPurchaseDetail->save(false);
                        $id[] = $val['parent_id'];
                    }
                    $id = array_unique($id);
                    $returnPurchase = ReturnPurchase::getOne(['id'=>$id],1);
                    $returnPurchase->status = 3;
                    $returnPurchase->save(false);
                }
            }
            //操作库存
            if (isset($data['depot'])) {
                //无库存是添加
                if (isset($data['depot']['add'])) {
                    if (isset($data['depot']['add'])) {
                        $field = ['direct_company_id','prod_id','prod_batch','bad_num','num','total_num','freeze_num','depot_id','create_time','time_stamp'];

                        $db->createCommand()
                            ->batchInsert(DepotProdStock::tableName(),$field,$data['depot']['add'])
                            ->execute();
                    }
                }

                //库存存在时修改
                if (isset($data['depot']['update'])) {
                    foreach ($data['depot']['update'] as $val) {
                        $depot = DepotProdStock::findOne(['id'=>$val['id']]);
                        if(isset($val['bad_num'])){
                            $depot->bad_num   = $val['bad_num'];
                        }
                        if(isset($val['num'])){
                            $depot->num   = $val['num'];
                        }
                        if(isset($val['total_num'])){
                            $depot->total_num   = $val['total_num'];
                        }
                        $depot->create_time = date("Y-m-d H:i:s",time());
                        $depot->save(false);
                    }
                }

            }

            $transaction->commit();
            return true;
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

}