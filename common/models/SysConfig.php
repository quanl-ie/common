<?php
namespace common\models;

use webapp\models\Menu;
use Yii;
use yii\db\ActiveRecord;


class  SysConfig extends BaseModel
{

    public static function tableName()
    {
        return 'sys_config';  //客户表
    }
    
    public function attributeLabels()
    {
        return [
            'id' => '',
            'config_values' => '仓储管理开关：',
        ];
    }
    
    public function rules()
    {
        return [
            [['id','config_values'],'required']
        ];
    }
    
    
    public static function getOne($where = [],$flag = '')
    {
        if ($flag) {
            return self::find()->where($where)->one();
        }else{
            return self::find()->where($where)->asArray()->one();
        }
        
    }
    
    public static function edit($data)
    {
    
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();
        try {
            
            $model = self::getOne(['id'=>$data['id']],1);
            $model->config_values = $data['config_values'];
            $model->save(false);
            
            $menu = Menu::find()->where(['name'=>'仓储管理'])->one();
            $menu->status  = $data['status'];
            $menu->is_show = $data['status'];
            $menu->save(false);
            
            $transaction->commit();
            return true;
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
        
    }
}