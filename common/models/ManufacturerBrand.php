<?php

namespace common\models;

use Yii;

class ManufacturerBrand extends BaseModel
{
    public static function tableName()
    {
        return 'manufacturer_brand';
    }

    /**
     * 获取品牌数据
     * @param $departmentId
     * @return array|\yii\db\ActiveRecord[]
     * @author xi
     */
    public static function getBrandList($departmentId)
    {
        $where = [
            'a.department_id' => $departmentId,
            'b.department_id' => $departmentId,
            'a.status'      => 1,
            'a.del_status'  => 1,
            'b.status'      =>1,
            'b.del_status'  => 1
        ];

        $query = self::find()
            ->from( self::tableName() .' as a' )
            ->innerJoin(['`'. ServiceBrand::tableName() .'` as b'] , 'a.brand_id = b.id')
            ->where($where)
            ->select('b.id,b.title')
            ->asArray()
            ->all();
        if($query)
        {
            return $query;
        }
        return [];
    }
}
