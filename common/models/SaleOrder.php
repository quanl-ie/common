<?php
namespace common\models;

use Yii;

class SaleOrder extends BaseModel
{
    public static function tableName()
    {
        return 'sale_order';
    }
    
    /**
     * 客户对应产品
     * @return array
     * @author liwenyong
     * @date   2017-12-20
     */
    public static function getServiceBrand($account_id) {
        $result = [];
        $query = self::findAllByAttributes(['status'=>1,'src_type'=>14,'account_id'=>intval($account_id)],'id,prod_name,brand_name,serial_number');
        foreach ($query as $val)
        {
            $result[$val['id']] = $val['prod_name']."/".$val['brand_name']."/".$val['serial_number'];
        }
        return $result;
    }
    
    /**
     * 根据条件查询所有数据
     * @param string/array $where
     * @param string $select
     * @return array
     * @author xi
     * @date 2017-12-15
     */
    public static function findAllByAttributes($where, $select="*", $index = false,$limit = 1000)
    {
        $query = static::find()
        ->where($where)
        ->select($select)
        ->limit($limit)
        ->asArray()
        ->all();
        if($query)
        {
            if( is_string($index) && in_array($index,array_keys((new static())->getAttributes())) )
            {
                $result = [];
                foreach ($query as $val){
                    $result[$val[$index]] = $val;
                }
                return $result;
            }
    
            return $query;
        }
        return [];
    }

    /**
     * 根据条件查出一条数据
     * @param array/string $where
     * @param string $returnAttr
     * @return \yii\db\ActiveRecord|NULL|string
     * @author xi
     * @date 2017-12-15
     */
    public static function findOneByAttributes($where, $select="*", $returnAttr='')
    {
        $query = static::find()
            ->where($where)
            ->select($select)
            ->asArray()
            ->one();
        if($query)
        {
            if($returnAttr!='' && is_string($returnAttr) && in_array($returnAttr,array_keys((new static())->getAttributes())) )
            {
                return $query[$returnAttr];
            }
            return $query;
        }
        return [];
    }
    /**
     * 添加
     * @author chengjuanjuan <chengjuanjuan@c-ntek.com>
     * @date 2017-12-15
     */
    public static function add($saleData)
    {
        $model = new self();
        foreach($saleData as $k=>$v){
            $model->$k=$v;
        }
        $res = $model->save(false);
        if($res){
            return $model->id;
        }

    }

}