<?php

namespace common\models;
use yii\db\ActiveRecord;

class AccountAddress extends ActiveRecord
{
    
    public static function tableName()
    {
        return 'account_address';
    }
    
    /**
     * 根据用户ID获取用户地址
     * @date 2017-12-20
     * @author liwenyong<liwenyong@uservices.cn>
     * @param  int  $account_id
     * @return json;
     */
    public static function getAddress($account_id, $type = 0) {
        $address = self::find()->where(['account_id' => intval($account_id)])->asArray()->all();
        
        if(!$address) {
            return array();
        }
        $addressArr = [];
        foreach ($address as $v) {
            if($type != 0) {
                $addressArr['lon'] = $v['lon'];
                $addressArr['lat'] = $v['lat'];
            }else{
                $addressArr[$v['id']] = Region::getCityName($v['province_id']).Region::getCityName($v['city_id']).Region::getCityName($v['district_id']).$v['address'];
            }
        }
        return $addressArr;
    }
    /**
     * 根据accountID获取默认地址
     * @param  int  $account_id
     * @return json;
     */
    public static function getDefultAddressByAccountId($account_id) {
        $address = self::find()->where(['account_id'=>$account_id,'is_default'=>1])->asArray()->all();
        if(!$address) {
            return array();
        }
        $addressArr = [];
        foreach ($address as $v) {
            $addressArr[$v['account_id']] = Region::getCityNameById($v['province_id']).Region::getCityNameById($v['city_id']);
        }
        return $addressArr;
    }
    /**
     * 根据条件查出一条数据
     * @param array/string $where
     * @param string $returnAttr
     * @return \yii\db\ActiveRecord|NULL|string
     * @author xi
     * @date 2017-12-15
     */
    public static function findOneByAttributes($where, $select="*", $returnAttr='')
    {
        $query = static::find()
            ->where($where)
            ->select($select)
            ->asArray()
            ->one();
        if($query)
        {
            if($returnAttr!='' && is_string($returnAttr) && in_array($returnAttr,array_keys((new static())->getAttributes())) )
            {
                return $query[$returnAttr];
            }
            return $query;
        }
        return [];
    }
}