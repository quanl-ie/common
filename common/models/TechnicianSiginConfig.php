<?php

namespace common\models;

class TechnicianSiginConfig extends BaseModel
{
    public static function tableName()
    {
        return 'technician_sigin_config';
    }

    public static function setStatus(){
        return ['1'=>'启用','2'=>'不启用'];
    }


    public static function edit($data){

        if(isset($data['id']) && $data['id']>0){
            $info = self::findOne(['id'=>$data['id']]);
            unset($data['id']);
            foreach($data as $k=>$v){
                $info->$k = $v;
            }
            $res = $info->save(false);
        }else{
            unset($data['id']);
            $model = new self();
            foreach($data as $k=>$v){
                $model->$k = $v;
            }
            $model->created_at = time();
            $model->updated_at = time();
            $res = $model->save(false);

        }
      return $res;
    }
}