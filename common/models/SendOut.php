<?php
namespace common\models;

use yii\db\ActiveRecord;
use Yii;

class SendOut extends BaseModel
{
    
    public static function tableName()
    {
        return 'send_out';
    }
    /**
     * 获取单条数据
     * User: quanl
     * Date: 2018/9/4
     * Time: 14:59
     * @param array $where
     * @param string $return  0 数组 1 对象
     * @return array|null|ActiveRecord
     */
    public static function getOne($where = [],$return = 0)
    {
        if($return == 1){
            return self::find()->where($where)->one();
        }
        $list =  self::find()->where($where)->asArray()->one();
        if(!empty($list))
        {
            $list['send_type_desc'] = '';
            $list['send_method_desc'] = '';
            $sendTypeArr   = DictEnum::findAllByAttributes(['dict_key'=>'send_out_type','dict_enum_id'=>$list['send_type']],'dict_enum_id,dict_enum_value','dict_enum_id');
            $sendMethodArr = DictEnum::findAllByAttributes(['dict_key'=>'send_method_type','dict_enum_id'=>$list['send_method']],'dict_enum_id,dict_enum_value','dict_enum_id');
            if(isset($sendTypeArr[$list['send_type']])){
                $list['send_type_desc'] = $sendTypeArr[$list['send_type']]['dict_enum_value'];
            }
            if(isset($sendMethodArr[$list['send_method']])){
                $list['send_method_desc'] = $sendMethodArr[$list['send_method']]['dict_enum_value'];
            }
        }
        return $list;
    }
    /**
     * 获取多条数据
     * User: quanl
     * Email: liquan@services.cn
     * Date: 2018/9/4
     * Time: 10:00
     * @param array $where
     * @return array|ActiveRecord[]
     */
    public static function getList($where = [])
    {
        return self::find()->where($where)->asArray()->all();
    }
    
    /**
     * 列表（分页，搜索）
     * User: quanl
     * Email: liquan@services.cn
     * Date: 2018/4/20
     * Time: 16:15
     * @param $map
     * @param $where
     * @param int $page
     * @param int $pageSize
     * @return array
     */
    public static function index($map,$where,$page=1,$pageSize=10)
    {
        $db = self::find();
        $db->where($map);
        
        //判断where
        if (!empty($where)) {
            foreach ($where as $val) {
                $db->andFilterWhere($val);
            }
        }

        //总数
        $totalNum = $db->count();
        //当有结果时进行组合数据
        if ($totalNum>0) {
            //总页数
            $totalPage = ceil($totalNum/$pageSize);
            if($page<1)
            {
                $page = 1;
            }
            else if($page>$totalPage)
            {
                $page = $totalPage;
            }
            $db->select("id,send_no,send_type,send_method,rec_name,product_ids,create_time,rel_rec_date");
            $db->offset(($page-1)*$pageSize);
            $db->orderBy(['create_time'=> SORT_DESC]);
            $db->limit($pageSize);
            $db->asArray();
            $list = $db->all();

            $sendTypes = array_column($list, 'send_type');
            $sendMethods = array_column($list,'send_method');

            $sendTypeArr   = DictEnum::findAllByAttributes(['dict_key'=>'send_out_type','dict_enum_id'=>$sendTypes],'dict_enum_id,dict_enum_value','dict_enum_id');
            $sendMethodArr = DictEnum::findAllByAttributes(['dict_key'=>'send_method_type','dict_enum_id'=>$sendMethods],'dict_enum_id,dict_enum_value','dict_enum_id');

            foreach ($list as $key=>$val)
            {
                $list[$key]['send_type_desc'] = '';
                $list[$key]['send_method_desc'] = '';
                if(isset($sendTypeArr[$val['send_type']])){
                    $list[$key]['send_type_desc'] = $sendTypeArr[$val['send_type']]['dict_enum_value'];
                    unset($list[$key]['send_type']);
                }
                if(isset($sendMethodArr[$val['send_method']])){
                    $list[$key]['send_method_desc'] = $sendMethodArr[$val['send_method']]['dict_enum_value'];
                    unset($list[$key]['send_method']);
                }
                $list[$key]['product_num'] = count(explode(",",$val['product_ids']));
                unset($list[$key]['product_ids']);
            }
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => $totalPage,
                'list'       => $list
            ];
        }
        else
        {
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => 0,
                'list'       => []
            ];
        }
    }
    
    /**
     * 添加/修改
     * User: quanl
     * Email: liquan@services.cn
     * Date: 2018/9/4
     * Time: 15:15
     * @return bool
     * @throws \Exception
     */
    public static function add($data)
    {
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();
        try {
            if (!$data) {
                return false;
            }
            if (isset($data['id'])) {
                $model = self::getOne(['id'=>$data['id']],1);
            }else{
                $model = new self();
            }
            foreach ($data as $key => $val) {
                $model->$key = $val;
            }
            if (!$model->save(false)) {
                return false;
            }
            $transaction->commit();
            return $model->id;
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }
}