<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;

class CostLevel extends ActiveRecord
{
    public static function getDb() {
        return Yii::$app->order_db;
    }
    
    public static function tableName()
    {
        return 'cost_level';
    }

    /**
     * 查出收费标准
     * @author xi
     * date 2018-2-9
     */
    public static function getStandardId($srcType,$srcId,$saleOrderId,$accountAddressId,$workType)
    {
        //售后产品信息
        $saleOrderArr = SaleOrder::find()
            ->where(['id'=>$saleOrderId,'status'=>1])
            ->select('type_id,class_id,brand_id')
            ->asArray()->one();
        //用户地址信息
        $addressArr = AccountAddress::find()
            ->where(['id'=>$accountAddressId,'status'=>1])
            ->select('province_id,city_id,district_id')
            ->asArray()->one();
        if($saleOrderArr && $addressArr)
        {
//            $where = [
//                'a.src_type' => $srcType,
//                'a.src_id'   => $srcId,
//                'b.brand_id' => [$saleOrderArr['brand_id'],0],
//                'b.class_id' => [$saleOrderArr['class_id'],0],
//                'b.type_id'  => [$workType,0],
//                'a.status'   => 1,
//                'b.status'   => 1
//            ];

            $area = $addressArr['province_id'].'_'.$addressArr['city_id'].'_'.$addressArr['district_id'];
            $query = self::find()
                ->from(self::tableName() . ' as a ')
                ->innerJoin([CostLevelDetail::tableName() . ' as b'],' a.id = b.cost_level_id')
                ->where(['a.src_type' => $srcType, 'a.src_id' => $srcId, 'b.brand_id' => $saleOrderArr['brand_id'], 'b.class_id' => $saleOrderArr['class_id'],'b.type_id' => $workType,'a.status' => 1, 'b.status' => 1])
                ->orWhere(['a.src_type' => $srcType, 'a.src_id' => $srcId, 'b.brand_id' => 0, 'b.class_id' => $saleOrderArr['class_id'],'b.type_id' => $workType,'a.status' => 1, 'b.status' => 1])
                ->orWhere(['a.src_type' => $srcType, 'a.src_id' => $srcId, 'b.brand_id' => $saleOrderArr['brand_id'], 'b.class_id' => 0,'b.type_id' => $workType,'a.status' => 1, 'b.status' => 1])
                ->orWhere(['a.src_type' => $srcType, 'a.src_id' => $srcId, 'b.brand_id' => $saleOrderArr['brand_id'], 'b.class_id' => $saleOrderArr['class_id'],'b.type_id' => 0,'a.status' => 1, 'b.status' => 1])
                ->orWhere(['a.src_type' => $srcType, 'a.src_id' => $srcId, 'b.brand_id' => 0, 'b.class_id' => 0,'b.type_id' => $workType,'a.status' => 1, 'b.status' => 1])
                ->orWhere(['a.src_type' => $srcType, 'a.src_id' => $srcId, 'b.brand_id' => $saleOrderArr['brand_id'], 'b.class_id' => 0,'b.type_id' => 0,'a.status' => 1, 'b.status' => 1])
                ->orWhere(['a.src_type' => $srcType, 'a.src_id' => $srcId, 'b.brand_id' => 0, 'b.class_id' => $saleOrderArr['class_id'],'b.type_id' => 0,'a.status' => 1, 'b.status' => 1])
//                ->where($where)
                ->andWhere("FIND_IN_SET('$area',service_area)")
                ->select('b.id')
                ->asArray()
                ->one();
            if(!$query)
            {
                $query = self::find()
                    ->from(self::tableName() . ' as a ')
                    ->innerJoin([CostLevelDetail::tableName() . ' as b'],' a.id = b.cost_level_id')
                    ->where(['a.src_type' => $srcType, 'a.src_id' => $srcId, 'b.brand_id' => 0, 'b.class_id' => 0,'b.type_id' => 0,'a.status' => 1, 'b.status' => 1])
//                ->where($where)
                    ->andWhere("FIND_IN_SET('$area',service_area)")
                    ->select('b.id')
                    ->asArray()
                    ->one();
            }
            if($query) {
                return $query['id'];
            }
        }
        return 0;
    }
    /**
     * 查出收费标准
     * @author sxz
     * date 2018-3-1
     */
    public static function getStandardData($account_address_id,$provider_ids)
    {
        //用户地址信息
        $addressArr = AccountAddress::find()
            ->where(['id'=>$account_address_id,'status'=>1])
            ->select('province_id,city_id,district_id')
            ->asArray()->one();
        $area = $addressArr['province_id'].'_'.$addressArr['city_id'].'_'.$addressArr['district_id'];
        $provider_arr = [];
        if($provider_ids){
            $provider_arr = explode(',',$provider_ids);
        }
        //print_r($area);die;
        $allArr = self::find()->where(['src_type'=>13])->andWhere(['in','src_id',$provider_arr])->asArray()->all();
        if($allArr) foreach ($allArr as $key=>$val){
            $val['service_area'] = explode(',',$val['service_area']);
            if(!in_array($area,$val['service_area'])){
                unset($allArr[$key]);
            }
        }
        if($allArr){
            //查出符合规则的服务商ids
            $provider_new_ids = array_column($allArr, 'src_id');
            return $provider_new_ids;
        }else{
            return false;
        }

    }
}