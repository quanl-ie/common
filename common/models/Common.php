<?php
namespace common\models;

use Yii;

class Common
{
    //本人
    const DATAROSE_SELF = 1;
    //本公司
    const DATAROSE_COMPANY = 2;
    //本公司及以下
    const DATAROSE_COMPANY_ALL = 3;
    //本部门
    const DATAROSE_DEPARTMENT = 4;
    //本部门及以下
    const DATAROSE_DEPARTMENT_ALL = 5;


    /**
     * 获取账户姓名
     * @return array
     */
    public static function getAccountName($accountId)
    {
        $name = Account::findOneByAttributes(['id'=>$accountId],'account_name','account_name');

        return $name?$name:'';
    }

    /**
     * 获取客户信息
     * @param $accountId
     * @param string $select
     * @return NULL|string|\yii\db\ActiveRecord
     * @author xi
     */
    public static function getAccountInfo($accountId,$select = 'account_name,mobile')
    {
        if($accountId > 0)
        {
            $arr = Account::findOneByAttributes(['id'=>$accountId],$select);
            return $arr;
        }
        return [];
    }

    /**
     * 获取地址
     * @param $id
     * @return mixed|string
     * @author xi
     */
    public static function getAccountAddress($id)
    {
        if($id > 0 )
        {
            $res = AccountAddressView::findOneByAttributes(['id'=>$id],'new_address');
            return isset($res['new_address'])?$res['new_address']:'';
        }
        return '';
    }

    /**
     * 获取登录账户姓名（商家，服务商，门店登录信息）
     * @return array
     */
    public static function getUserName($userId)
    {
        $user = AdminUser::findOne(['id'=>$userId]);

        return $user?$user['username']:'';
    }

    /**
     * 获取技师姓名
     * @return array
     */
    public static function getTechnicianName($technicianId)
    {

        $name = Technician::findOneByAttributes(['id'=>$technicianId],'name','name');

        return $name?$name:'';
    }

    /**
     * 根据 ids 查出技师信息
     * @param array
     * @param string $select$technicianIds
     * @return array
     */
    public static function getTechniciansInfo(array $technicianIds, $select = "id,name,mobile")
    {
        $arr = Technician::findALLByAttributes(['id'=>$technicianIds,'status'=>1],$select,'id');

        return $arr;
    }

    /**
     * 获取服务商名称
     * @return array
     */
    public static function getStoreName($storeId)
    {
        $name = Manufactor::findOneByAttributes(['id'=>$storeId],'company','company');

        return $name?$name:'';
    }

    /**
     * @desc: 获取部门或机构名称
     * @param integer $departmentId 部门ID
     * @return string
     */
    public static function getDepartmentName($departmentId)
    {
        $name = Department::findOneByAttributes([ 'id' => $departmentId ],'name','name');
        return $name ? $name : '';
    }

    /**
     * 获取类目名称
     * @return array
     */
    public static function getClassName($classId)
    {
        if($classId)
        {
            $res = ServiceClass::findOneByAttributes(['id'=>$classId,'status'=>1],'title');
            if($res){
                return $res['title'];
            }
        }

        return '';
    }

    /**
     * 根据二级类目查出一级类目名称
     * @param $classId
     * @return mixed|string
     */
    public static function getParentClassName($classId)
    {
        if($classId)
        {
            $res = ServiceClass::findOneByAttributes(['id'=>$classId,'status'=>1],'pid');
            if($res){
                $pid = $res['pid'];
                $res = ServiceClass::findOneByAttributes(['id'=>$pid,'status'=>1],'title');
                return isset($res['title'])?$res['title']:'';
            }
        }

        return '';
    }

    /**
     * 获取服务类型
     * @return array
     */
    public static function getWorktypeName($workTypeId)
    {
        $name = ServiceType::getTitle($workTypeId);

        return $name?$name:'';
    }

    /**
     * 根据售后订单id 查出类目名称
     * @return array
     */
    public static function getClassNameBySaleId($saleId)
    {
        $name = SaleOrderView::findOneByAttributes(['id'=>$saleId],'class_name','class_name');

        return $name?$name:'';
    }

    /**
     * 工单服务流程
     * @param $id
     * @return mixed|string
     */
    public static function getServiceFlow($id)
    {
        $query = ServiceFlow::find()
            ->where(['id'=>$id])
            ->select('title')
            ->asArray()
            ->one();

        return $query?$query['title']:'';
    }

    /**
     * 获取登录账户姓名（商家，服务商，门店登录信息）
     * @return array
     */
    public static function getUserIds($srcType,$srcId)
    {
        if($srcType && $srcId)
        {
            $ids = [];
            $query = AdminUser::find()
                ->where(['src_type'=>$srcType,'src_id'=>$srcId,'status'=>1])
                ->select('id')
                ->asArray()
                ->all();
            if($query)
            {
                $ids = array_column($query,'id');
            }

            $data = [
                'ids' => $ids
            ];
            return $data;
        }
        return [];
    }

    /**
     * 根据品牌 id 查出品牌名称
     * @param $brandId
     * @return mixed|string
     */
    public static function getBrandName($brandId)
    {
         if($brandId)
         {
             $query = ServiceBrand::find()
                 ->where(['id'=>$brandId,'status'=>1,'del_status'=>1])
                 ->select('title')
                 ->asArray()
                 ->all();
             if($query)
             {
                 if(count($query) == 1) {
                     return $query[0]['title'];
                 }
                 else {
                     return $query;
                 }
             }
         }
         return '';
    }

    /**
     * 获取技师及所属服务商
     * @param $technicianId
     * @return array|NULL|string|\yii\db\ActiveRecord
     * @author xi
     */
    public static function getTechnicianAndFws($technicianId)
    {
        if($technicianId)
        {
            $technicianArr = Technician::findOneByAttributes(['id'=>$technicianId],'id,name,mobile,store_id');
            //服务商
            if($technicianArr){
                $fwsArr = Manufactor::findOneByAttributes(['id'=>$technicianArr['store_id']],'id,company,mobile');
                if($fwsArr){
                    $technicianArr['fws_name']   = $fwsArr['company'];
                    $technicianArr['fws_mobile'] = $fwsArr['mobile'];
                    return $technicianArr;
                }
            }
        }
        return [];
    }

    /**
     * 获取城市名称
     * @param $id
     * @return mixed|string
     */
    public static function getCityName($id)
    {
        if($id)
        {
            $query = Region::find()
                ->where(['region_id'=>$id])
                ->select('region_name')
                ->asArray()
                ->one();
            if($query){
                return $query['region_name'];
            }
        }
        return '';
    }

    /**
     * 根据区县id 查出省份城市区县信息
     * @param $countyId
     * @return array
     */
    public static function getRegionByCountyId($countyId)
    {
        $countyObj = Region::findOne(['region_id'=>$countyId]);
        if($countyObj){
            $cityObj = Region::findOne(['region_id'=>$countyObj->parent_id]);
            if($cityObj){
                $provinceObj = Region::findOne(['region_id'=>$cityObj->parent_id]);
                if($provinceObj){
                    return [
                        'province' => [
                            'id'   => $provinceObj->region_id,
                            'name' => $provinceObj->region_name
                        ],
                        'city' => [
                            'id'   => $cityObj->region_id,
                            'name' => $cityObj->region_name
                        ],
                        'county' => [
                            'id'   => $countyObj->region_id,
                            'name' => $countyObj->region_name
                        ],
                        'desc' => $provinceObj->region_name.$cityObj->region_name.$countyObj->region_name,
                        'idstr' => $provinceObj->region_id.'_'.$cityObj->region_id.'_'.$countyObj->region_id
                    ];
                }
            }
        }
        return [];
    }

    /**
     * 根据多个分组id 查出分组信息
     * @param array $ids
     * @param int $limit
     * @return array
     * @author xi
     * @date 2018-4-26
     */
    public static function getGroupingByIds(array $ids,$limit = 1000)
    {
        if($ids)
        {
            $query = Grouping::findAllByAttributes(['id'=>$ids,'status'=>1],'id,name,remark','id',$limit);
            if($query){
                return $query;
            }
        }
        return [];
    }

    /**
     * 查出技师的组长id
     * @param $srcType
     * @param $srcId
     * @param $technicianId
     * @return int|mixed
     * @author xi
     * @date 2018-4-26
     */
    public static function getGroupingLeader($departmentTopId,$departmentId,$technicianId)
    {
        if($technicianId>0   && $departmentId>0)
        {

            $where = [
                        'technician_id'=>$technicianId,
                        'department_id'=>$departmentId,
                        'status'=>1
            ];

            if($departmentTopId>0){

                $where['department_top_id']=$departmentTopId;
            }

            $query = GroupingRelTechnician::findOneByAttributes($where,'grouping_id');
            if($query)
            {
                $leaderQuery = GroupingRelTechnician::findOneByAttributes(['grouping_id'=>$query['grouping_id'],'job_title'=>2,'status'=>1],'technician_id');
                if($leaderQuery){
                    return $leaderQuery['technician_id'];
                }
            }
        }
        return 0;
    }

    /**
     * 根据技师id 查出分组id
     * @param $srcType
     * @param $srcId
     * @param $technicianId
     * @return int|mixed
     */
    public static function getGroupingIdByJsId($departmentId,$technicianId)
    {
        if($technicianId>0 && $departmentId>0)
        {
            $query = GroupingRelTechnician::findOneByAttributes(['technician_id'=>$technicianId,'department_id'=>$departmentId,'status'=>1],'grouping_id');
            if($query){
                return $query['grouping_id'];
            }
        }
        return 0;
    }

    /**
     * @desc: 根据用户ID获取直属公司名称
     * @param integer $userId
     * @return array|null
     */
    public static function getDirectCompanyName($userId)
    {
        $query = User::find()
            ->from(User::tableName() . ' as a')
            ->leftJoin([Department::tableName(). ' as b'],'a.department_id = b.id')
            ->innerJoin([Department::tableName(). ' as c'],' b.direct_company_id = c.id')
            ->where(['a.id' =>$userId])
            ->select(['c.name','c.id'])
            ->asArray()->one();
        return !empty($query)? $query:'';
    }

    /**
     * @desc: 获取分类的顶级分类
     * @param integer $classId 分类ID
     * @return mixed|NULL|string|\yii\db\ActiveRecord
     */
    public static function getFirstClass($classId)
    {
        $arr = ServiceClass::find()->select(['parent_id','parent_ids','title'])->where(['id'=>$classId])->asArray()->one();
        if(!empty($arr)){
           if(!empty($arr['parent_id'])){
                $ids = explode(',',$arr['parent_ids']);
                if(count($ids)>1){
                    return ServiceClass::findOneByAttributes(['id'=> $ids[1]],'title','title');
                } else {
                    return '';
                }
           } else {
                return $arr['title'];
           }
        }

        return '';
    }

    /**
     * 查出技师的组长id
     * @param $srcType
     * @param $srcId
     * @param $technicianId
     * @return int|mixed
     * @author xi
     * @date 2018-4-26
     */
    public static function getGroupingLeaderByTechnicianId($technicianId)
    {
        if($technicianId>0)
        {

            $where = [
                'technician_id'=>$technicianId,
                'status'=>1
            ];

            $query = GroupingRelTechnician::findOneByAttributes($where,'grouping_id');
            if($query)
            {
                $leaderQuery = GroupingRelTechnician::findOneByAttributes(['grouping_id'=>$query['grouping_id'],'job_title'=>2,'status'=>1],'technician_id');
                if($leaderQuery){
                    return $leaderQuery['technician_id'];
                }
            }
        }
        return 0;
    }

    /**
     * 根据品牌 id 查出品牌名称
     * @param $brandId
     * @return mixed|string
     */
    public static function getBrandNameByIds(array $brandIds)
    {
        if($brandIds)
        {
            $query = ServiceBrand::find()
                ->where(['id'=>$brandIds,'status'=>1,'del_status'=>1])
                ->select('id,title')
                ->asArray()
                ->all();
            if($query)
            {
                return array_column($query,'title','id');
            }
        }
        return '';
    }

    /**
     * 获取类目名称
     * @return array
     */
    public static function getClassNameByIds(array $classIds)
    {
        if($classIds)
        {
            $res = ServiceClass::findAllByAttributes(['id'=>$classIds,'status'=>1],'id,title');
            if($res){
                return array_column($res,'title','id');
            }
        }

        return '';
    }

    /**
     * 根据技师查部门id
     * @param $technicianId
     * @return array
     * @author xi
     */
    public static function getDepartmentIdByTechnicianId($technicianId)
    {
        $query = Technician::find()
            ->from(Technician::tableName() . " as a")
            ->innerJoin(['`'.Department::tableName().'` as b'] , 'a.store_id = b.id')
            ->where(['a.id'=>$technicianId])
            ->select("b.id,b.parent_ids,direct_company_id")
            ->asArray()->one();

        if($query)
        {
            $departmentTopId  = $query['direct_company_id'];
            if($query['parent_ids']){
                $parentIds = explode(',',$query['parent_ids']);
                $departmentTopId = $parentIds[0];
            }
            $result = [
                'department_id'     => $query['id'],
                'direct_company_id' => $query['direct_company_id'],
                'department_top_id' => $departmentTopId
            ];
            return $result;
        }
        return [
            'department_id'     => 0,
            'direct_company_id' => 0,
            'department_top_id' => 0
        ];
    }

    /**
     * 获取售后产品名称
     * @param $saleOrderIds
     * @return array
     * @author xi
     */
    public static function getSaleOrderName($saleOrderIds)
    {
        $where = [
            'id' => $saleOrderIds
        ];
        $query = SaleOrderView::findAllByAttributes($where,'id,prod_name');
        if($query)
        {
            return array_column($query,'prod_name','id');
        }

        return [];
    }
}