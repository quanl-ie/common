<?php

namespace common\models;

use filsh\yii2\oauth2server\models\OauthAccessTokens;
use Yii;
use yii\web\IdentityInterface;
use OAuth2\Storage\UserCredentialsInterface;

class Technician extends BaseModel implements IdentityInterface,UserCredentialsInterface
{
    const STATUS_ACTIVE = 1;

    public static function tableName()
    {
        return 'technician';
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne([
            'id' => $id,
            'status' => 1
        ]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        $user_token = OauthAccessTokens::findOne(['access_token' => $token]);
        if($user_token)
            return self::findOne(['id' => $user_token->user_id]);
        return false;
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password
     *            password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    public function checkUserCredentials($username, $password)
    {
        $user = static::findByMobile($username);
        if(!$user)
            return false;
        return $user->validatePassword($password);
    }

    public function getUserDetails($username)
    {
        $user = static::findByMobile($username);
        return ['user_id'=>$user->getId()];
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByMobile($mobile)
    {
        return static::findOne([
            'mobile' => $mobile,
            'status' => self::STATUS_ACTIVE
        ]);
    }

    /**
     * 根据技师ID查出技师所属部门
     * @param $technicianId
     * @return int|mixed
     */
    public static function getDepartmentIdByTechnicianId($technicianId)
    {
        $query = self::findOneByAttributes(['id'=>$technicianId], 'store_id');
        if($query){
            return $query['store_id'];
        }

        return 0;
    }
}