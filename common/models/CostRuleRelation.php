<?php
namespace common\models;

use Yii;

class CostRuleRelation extends BaseModel
{
    public static function getDb ()
    {
        return Yii::$app->order_db;
    }

    public static function tableName ()
    {
        return 'cost_rule_relation';
    }

    public static function getOne($where = [])
    {
        return self::find()->where($where)->one();
    }
    public static function getList($where = [])
    {
        return self::find()->where($where)->asArray()->all();
    }

    /**
     * 查出结算信息
     * @param $fwsId
     * @param $sjId
     * @param $orderNo
     * @param $costItemId  1 上门费 2  o2o库的 cost_item_set 表
     * @return array|NULL|string|\yii\db\ActiveRecord
     */
    public static function getCostRuleItemInfo($fwsId,$sjId,$orderNo,$costItemId)
    {
        $where = [
            'src_type' => BaseModel::SRC_SJ,
            'src_id'   => $sjId,
            'status'   => 1,
            'service_provider_id' => $fwsId
        ];
        $query = self::findOneByAttributes($where,'cost_rule_id');
        if($query)
        {
            $costRuleId = $query['cost_rule_id'];

            $brandId = 0;
            $classId = 0;
            $typeId  = 0;
            $area    = '';

            //查出订单的 品牌 id, 类别id, 服务id ， 地址
            $orderArr = Order::findOneByOrderNo($orderNo);
            if($orderArr)
            {
                $typeId =$orderArr['work_type'];

                //查出地址
                $addressArr = AccountAddress::findOneByAttributes(['id'=>$orderArr['address_id']],'province_id,city_id,district_id');
                if($addressArr){
                    $area = $addressArr['province_id'].'_'.$addressArr['city_id'].'_'.$addressArr['district_id'];
                }

                //售后产品
                $saleOrderArr = SaleOrder::findOneByAttributes(['id'=>$orderArr['sale_order_id']],'brand_id,class_id');
                if($saleOrderArr)
                {
                    $classId = $saleOrderArr['class_id'];
                    $brandId = $saleOrderArr['brand_id'];
                }
            }

            //查出结算规则信息
            $ruleDetailId = CostRule::getCostRuleDetailId($costRuleId,$brandId,$classId,$typeId,$area);
            if($ruleDetailId > 0)
            {
                $where = [
                    'cost_rule_id'   => $costRuleId,
                    'rule_detail_id' => $ruleDetailId,
                    'status'         => 1,
                    'cost_item_id'   => $costItemId
                ];
                $itemArr = CostRuleItem::findOneByAttributes($where,'id,cost_type,fixed_amount,pecent');
                if($itemArr){
                    return $itemArr;
                }
            }
        }
        return [
            'id'           => 0,
            'cost_type'    => 0,
            'fixed_amount' => 0,
            'pecent'       => 0
        ];

    }
}