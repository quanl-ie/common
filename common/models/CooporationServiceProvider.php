<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;

class  CooporationServiceProvider extends ActiveRecord
{

    public static function getDb() {
        return Yii::$app->db;
    }

    public static function tableName()
    {
        return 'cooperation_service_provider';
    }


    /**
     * 获取对应服务商信息
     * @author  liwennyong<liwenyong@uservices.cn>
     * @date    2017-12-20
     * @return  array
     */
    public static function getServiceProvider($sjId,$saleOrderId,$addressId,$workType,$fwsId=0)
    {
        $providerArr = [];

        //售后产品
        $saleOrderArr = SaleOrder::findOneByAttributes(['id'=>$saleOrderId],'first_class_id,class_id');
        //用户地址
        $addressArr = AccountAddress::findOneByAttributes(['id'=>$addressId],'province_id,city_id,district_id');
        //查出合作服务商
        $fwsArr = self::find()
            ->where(['src_type'=>BaseModel::SRC_SJ,'src_id'=>$sjId,'status'=>2])
            ->andWhere(['<>','service_provider_id',$fwsId])
            ->select('service_provider_id,service_provider_name')
            ->asArray()
            ->all();

        if($saleOrderArr && $addressArr && $fwsArr)
        {
            $fwsIds = array_column($fwsArr,'service_provider_id');

            //服务区域
            $addressArea = $addressArr['province_id'].'_'.$addressArr['city_id'].'_'.$addressArr['district_id'];
            $where = " src_type = ". BaseModel::SRC_FWS . " and  src_id in( ". implode(',',$fwsIds) .") and  find_in_set('".$addressArea."', area) ";
            $areaArr = ServiceArea::findAllByAttributes($where,'src_id','src_id');

            //服务类型
            $where = [
                'src_type' => BaseModel::SRC_FWS,
                'src_id'   => $fwsIds,
                'status'   => 1,
                'type_id'  => $workType
            ];
            $workTypeArr = ManufacturerTypeQualification::findAllByAttributes($where,'src_id','src_id');

            //服务类目
            $where = [
                'src_type' => BaseModel::SRC_FWS,
                'src_id'   => $fwsIds,
                'status'   => 1,
                'class_pid'=> $saleOrderArr['first_class_id']
            ];
            $classArr = ManufacturerClassQualification::findAllByAttributes($where,'src_id','src_id');

            foreach ($fwsArr as $val)
            {
                if(isset($areaArr[$val['service_provider_id']]) && isset($workTypeArr[$val['service_provider_id']]) && isset($classArr[$val['service_provider_id']])){
                    $providerArr[$val['service_provider_id']] = $val['service_provider_name'];
                }
            }
        }

        return $providerArr;
    }
}