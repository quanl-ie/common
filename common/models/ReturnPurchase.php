<?php
namespace common\models;

use webapp\logic\ContractLogic;
use Yii;
use yii\db\ActiveRecord;
use common\models\ContractDetail;


/**
 * 采购退货
 * @author liquan
 * @date 2018-10-10
 */
class  ReturnPurchase extends BaseModel
{

    public static function tableName()
    {
        return 'return_purchase';
    }


    public static function showStatus(){

        return ['0'=>'未知','1'=>'待审批','2'=>'执行中','3'=>'执行完毕','4'=>'审批未通过','5'=>'已终止'];
    }

    public static function showReasonStatus(){
        return ['1'=>'产品质量','2'=>'服务质量','3'=>'其他'];
    }
    
    //获取单条信息
    public static function getOne($where,$flag='',$select="*")
    {
        if ($flag) {
            return self::find()->where($where)->select($select)->one();
        }
        return self::find()->where($where)->select($select)->asArray()->one();
    }



    //获取多条信息
    public static function getList($param)
    {
        if(isset($pageSize)){
            $pageSize = $param['pageSize'];
        }else{
            $pageSize = 10;
        }
        if(!isset($param['currentPage'])){
            $page = 0;
        }else{
            $page = $param['currentPage']-1;
        }

        unset($param['currentPage']);
        unset($param['pageSize']);

        if($param['department_id']){
            $db = self::find();
            if(isset($param['startTime']) && $param['startTime']){
                $db->andFilterWhere(['>=','create_time',$param['startTime']]);
                unset($param['startTime']);
            }
            if(isset($param['endTime']) && $param['endTime']){
                $db->andFilterWhere(['<=','create_time',$param['endTime']]);
                unset($param['endTime']);
            }
            foreach($param as $k=>$v){
                if(!empty($v)){
                    $db->andwhere([$k=>$v]);
                }
            }
            $count = $db->count();
            $totalPage = ceil($count/$pageSize);
            $start = $page*$pageSize;
            $list =$db->offset($start)
                ->select("id,status,subject,no,create_time,audit_suggest,audit_date")
                ->limit($pageSize)
                ->orderBy('id desc')
                ->asArray()
                ->all();
            if(!empty($list)){
                $ids = array_column($list,'id');
                $detail = ReturnPurchaseDetail::findAllByAttributes(['parent_id'=>$ids],"parent_id,sum(prod_num) as sum",'parent_id',1000,'parent_id');
                foreach ($list as $k=>$v){
                    if(isset($detail[$v['id']])){
                        $list[$k]['prod_num'] = $detail[$v['id']]['sum'];
                    }
                }
            }
        }
        return [
            'page'       => $page+1,
            'totalCount' => isset($count) ? $count : 0,
            'totalPage'  => isset($totalPage) ? $totalPage : 0,
            'list'       => isset($list) ? $list : ''
        ];

    }
    public static function getAllList($where = [],$flag  = 0)
    {
        $list = self::find()->where($where)->asArray()->all();
        if($list){
            $ids = array_column($list,'id');
            $detail = ReturnPurchaseDetail::getList(['parent_id'=>$ids],1);
            $detailsArrNum = array_column($detail,'total_prod_num','parent_id');
            foreach ($list as $k=>$v){
                if(isset($detailsArrNum[$v['id']])){
                    $list[$k]['prod_num'] = $detailsArrNum[$v['id']];
                }
            }
            if($flag){
                return $list;
            }
            $item=array();
            foreach($list as $k=>$v){
                if(!isset($item[$v['purchase_id']])){
                    $item[$v['purchase_id']]=$v;
                }else{
                    $item[$v['purchase_id']]['prod_num']+=$v['prod_num'];
                }
            }
            return isset($item) ? $item : [];
        }
        return [];
    }

    //添加
    public static function add($returnData,$productData)
    {
        $model = new self();
        //处理数据
        foreach ($returnData as $key => $val) {
            $model->$key = $val;
        }
        //开启事务
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();
        try {
            //添加数据
            $model->save(false);
            $id = $model->id;
            foreach($productData as $k => $v){
                if(!isset($v['prod_id'])){
                    $transaction->rollBack();
                }
                $productData[$k]['parent_id'] = $id;
            }
            $db->createCommand()
                ->batchInsert(ReturnPurchaseDetail::tableName(),['department_id','department_top_id','prod_id','prod_num','finish_num','parent_id'],
                    $productData)
                ->execute();

            $transaction->commit();
            return [
                'id' => $model->id
            ];
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    //编辑
    public static function edit($exchangeData,$productData=[])
    {
        $where['id'] = $exchangeData['id'];
        $model = self::findOne($where);
        foreach ($exchangeData as $key => $val) {
            $model->$key = $val;
        }
        $model->save(false);

        //开启事务
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();
        try {
            $id = $model->id;
            $productList = [];
            if(!empty($productData)){
                foreach($productData as &$v){
                    if(!isset($v['prod_id'])){
                        continue;
                    }
                    ReturnPurchaseDetail::deleteAll(['parent_id'=>$id]);
                    $v['parent_id']         = $id;
                    $productList[] = $v;
                }
                $db->createCommand()
                    ->batchInsert(ReturnPurchaseDetail::tableName(),['department_id','department_top_id','prod_id','prod_num','finish_num','parent_id'],
                        $productList)
                    ->execute();
                $transaction->commit();
            }
            return true;
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
        return false;
    }




    public static function updateStatus($param,$id){
        $model = self::findOne(['id'=>$id]);
        foreach($param as $k=>$v){
            $model->$k=$v;
        }
        $model->save(false);
        return $model->id;
    }



    /**
     * 审批操作
     */
    public static function checkOption($param,$id,$purchaseId='',$needUpdate=[]){

        $model = self::findOne(['id'=>$id]);
        foreach($param as $k=>$v){
            $model->$k=$v;
        }
        $model->save(false);

        //开启事务
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();
        try {
            $id = $model->id;
            if($purchaseId){
                foreach($needUpdate as $v){
                    $model = PurchaseDetail::findOne(['parent_id'=>$purchaseId,'prod_id'=>$v['prod_id']]);
                    $model->return_num = $v['return_num'] + intval($model->return_num);
                    $model->save(false);
                }
                $transaction->commit();
            }
            return $id;
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
        return false;
    }

}