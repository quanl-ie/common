<?php
namespace common\models;

use Yii;


class ContractDetail extends BaseModel
{
/*    public static function getDb ()
    {
        return Yii::$app->order_db;
    }*/

    public static function tableName ()
    {
        return 'contract_detail';
    }

    public static function getOne($where = [])
    {
        return self::find()->where($where)->asArray()->one();
    }


    public static function getList($where = [])
    {
        return self::find()->where($where)->asArray()->all();
    }
    
    //获取产品明细
    public static function getProduct($where = [])
    {
        $db = self::find();
        $db->from(self::tableName() . ' as a');
        //判断where
        if (!empty($where)) {
            foreach ($where as $val) {
                $db->andFilterWhere($val);
            }
        }
        $db->leftJoin([Product::tableName() . ' as b'],' a.prod_id = b.id');
        $db->select('a.id,a.prod_id,a.prod_num,a.price,a.amount,a.rate,a.total_amount,a.sale_price,a.warranty_date,a.finish_num,a.total_num,b.prod_name,b.prod_no,b.brand_id,b.class_id,b.type_id,b.model,b.unit_id,b.suggest_sale_price,b.status');
        $db->asArray();
        $list = $db->all();
        return $list;
        
    }
}