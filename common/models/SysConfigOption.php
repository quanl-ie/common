<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;


class  SysConfigOption extends BaseModel
{

    public static function tableName()
    {
        return 'sys_config_option';  //客户表
    }
    
    public static function getList($where = [])
    {
        return self::find()->where($where)->asArray()->all();
        
    }
}