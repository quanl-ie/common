<?php
namespace common\components;


use Yii;
use yii\base\Model;
use yii\web\UploadedFile;
use yii\base\Exception;
use yii\helpers\FileHelper;


/**
 * 文件上传处理
 */
class Upload extends Model
{
    public $file;


    private $_appendRules;


    public function init ()
    {
        parent::init();
        $extensions = Yii::$app->params['webuploader']['baseConfig']['accept']['extensions'];
        $this->_appendRules = [
            [['file'], 'file', 'extensions' => $extensions],
        ];
    }


    public function rules()
    {
        $baseRules = [];
        return array_merge($baseRules, $this->_appendRules);
    }


    /**
     *
     */
    public function upImage ($source=null,$name='file')
    {
        $model = new static;
        $model->file = UploadedFile::getInstanceByName($name);
        //开始调用oss  2017-11-09 21:18
        $file = $_FILES[$name]['tmp_name'];
        $fileType = $_FILES[$name]['type'];
        $url = Yii::$app->params['ossUrl'];
        $model->file = UploadedFile::getInstanceByName($name);
        $file = $_FILES[$name]['tmp_name'];
        $fileType = $_FILES[$name]['type'];
        $cfile = curl_file_create($file,$fileType,'test_name');
        $res = self::ossCurlFile($url, $cfile, $source);
        $st = json_decode($res,true);
        if($st['code'] == '200' && $st['imageUrl']){
            return [
                'code' => 0,
                'url' => $st['imageUrl'],
                'attachment' => $st['imageUrl']
            ];
        }else {
            $errors = $model->errors;
            return [
                'code' => 1,
                'msg' => current($errors)[0]
            ];
        }
    }


    //上传其他格式文件
    public function upOtherType ($source=null,$name='file')
    {
        $model = new static;
        $model->file = UploadedFile::getInstanceByName($name);
        //开始调用oss  2017-11-09 21:18
        $file = $_FILES[$name]['tmp_name'];
        //$fileType = $_FILES[$name]['type'];
        $fileFullName = isset($_FILES[$name]['name'])?$_FILES[$name]['name']:'';
        $arr = explode('.',$_FILES[$name]['name']);
        $fileType = '/'.$arr[1];
        $url = Yii::$app->params['ossUrl'];
        $model->file = UploadedFile::getInstanceByName($name);
        $cfile = curl_file_create($file,$fileType,'test_name');
        $res = self::ossCurlFile($url, $cfile, $source);
        $st = json_decode($res);
        if($st->code == '200' && $st->imageUrl){
            return [
                'code' => 0,
                'url' => $st->imageUrl,
                'attachment' => $st->imageUrl,
                'file_full_name'=>$fileFullName
            ];
        }else {
            $errors = $model->errors;
            return [
                'code' => 1,
                'msg' => current($errors)[0]
            ];
        }
    }

    /**
     * 文件上传
     */
    public static function upload($source, $files) {
        $image = $errors = [];
        $imageTyps = ['image/jpg','image/jpeg','image/png'];

        for($i=0;$i<count($files['name']);$i++) {
            if(!in_array($files['type'][$i],$imageTyps)) {
                $errors[] = '类型错误';
                break;
            }
            $filename = [];
            // 移动到框架应用根目录/public/uploads/ 目录下
            $file = $files['tmp_name'][$i];
            $fileType = $files['type'][$i];
            $url = Yii::$app->params['ossUrl'].'/index/oss/index';

            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_TIMEOUT, 120);// 设置超时限制防止死循环
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);// 获取的信息以文件流的形式返回
            $cfile = curl_file_create($file,$fileType,'test_name');
            $data = array('file' => $cfile,'fileSource'=>$source.uniqid());
            curl_setopt($ch, CURLOPT_POST,1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            $st = curl_exec($ch);
            curl_close($ch);
            $st = json_decode($st);
            if($st->code == '200' && $st->imageUrl){
                $image[] = $st->imageUrl;
            }else {
                $errors[] = $st->errors;
            }
        }
        if(!$errors) {
            return ['code' => 1, 'data' => $image];
        }
        return ['code' => 0, 'data' => $errors];

    }

    /**
     * 从本地上传到 oss
     * @param $localFile
     * @param $source
     * @return array
     */
    public static function uploadLocalFile($localFile,$source)
    {
        $url = Yii::$app->params['ossUrl'];
        $cfile = curl_file_create($localFile,'image/png','test_name');

        $res = self::ossCurlFile($url, $cfile, $source);
        $st = json_decode($res);
        if($st->code == '200' && $st->imageUrl){
            return [
                'code' => 0,
                'url' => $st->imageUrl,
            ];
        }
        else {
            return [
                'code' => 1,
                'msg' => '上传失败'
            ];
        }
    }


    public static function ossCurlFile($url,$cfile,$source)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);// 设置超时限制防止死循环
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);// 获取的信息以文件流的形式返回        
        // Assign POST data
        $data = array('file' => $cfile,'fileSource'=>$source);
        curl_setopt($ch, CURLOPT_POST,1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        // Execute the handle
        $st = curl_exec($ch);
        curl_close($ch);
        return $st;

    }
}