<?php
namespace common\helpers;
/**
 * 华信短信接口
 */
use Yii;
use yii\db\ActiveRecord;
use common\helpers\Helper;

class SendSMS extends ActiveRecord
{
    //发送地址 对应UTF-8(返回值为json格式)
//     const SEND_SMS_URL = 'http://dx.ipyy.net/smsJson.aspx';
//     const SN = 'AC00105';
//     const PWD = 'AC0010552';
    
    // 验证码短信帐号
    const SEND_SMS_URL = 'https://dx.ipyy.net/smsJson.aspx';
    const SN = 'AC00239';
    const PWD = '796283';

    // 营销短信帐号
    const YX_SEND_SMS_URL  = 'http://dx110.ipyy.net/smsJson.aspx';
    const YXSN = 'ACYX00160';
    const YXPWD = 'ACYX0016044';

    public static function tableName()
    {
        return 'club_sms_log';
    }

    /**
     * 发送短信短信
     * @param string $mobile 手机号，如多个手机号用逗号隔开
     * @param string $content  内容必须加签名【俱牛】
     * @param boolen $type 判断手机号是否进行check110()验证
     * @author xi
     */
    public static function send($mobile,$content,$type=true)
    {
        if(!preg_match('/^1[2-9]\d{9}(\,1[2-9]\d{9})*$/', $mobile) || trim($content) == '')
        {
            return false;
        }
        try
        {
            if($type){ 
                
                if(self::check110($mobile) == false)
                {
                    file_put_contents(__DIR__.'/sms.log', "[0]time:".date('Y-m-d H:i:s')."mobile:".$mobile." content:".$content ." ip:". Helper::getIp() ."\r\n",FILE_APPEND);
                    return false;
                }
            }

            header("Content-type:text/html;charset=utf-8");
            $sendQuery = 'action=send&userid=%s&account=%s&password=%s&mobile=%s&content=%s&sendTime=%s&extno=%s';
            $sendTime = '';//date('Y-m-d H:i:s');
            $userId = '';
            $user_id = '';
            $password = strtoupper(md5(self::PWD));
            $content = self::strToUtf8($content);
            $extNo = '';
            $sendQuery = sprintf($sendQuery,$userId,self::SN,$password,$mobile,$content,$sendTime,$extNo);
            $jsonData = Helper::curlPost(self::SEND_SMS_URL,$sendQuery);
            $result = json_decode($jsonData,true);

            if(isset($result['returnstatus']) && $result['returnstatus'] == 'Success')
            {
                //self::checkRemainpoint($result['remainpoint'],0);
                return self::saveDB($mobile, $content);
            }
            else
            {
                file_put_contents(__DIR__.'/sms.log', "[1]time:".date('Y-m-d H:i:s')."mobile:".$mobile." content:".$content." error:".var_export($jsonData,true) ."\r\n",FILE_APPEND);
                return false;
            }
        }
        catch (\Exception $e)
        {
            file_put_contents(__DIR__.'/sms.log', "[2]time:".date('Y-m-d H:i:s')."mobile:".$mobile." content:".$content." error:".var_export($e->getMessage(),true) ."\r\n",FILE_APPEND);
            return false;
        }
    }

    /**
     * 发送短信短信 并返回短信运营商的结果
     * @param string $mobile 手机号，如多个手机号用逗号隔开
     * @param string $content 内容必须加签名【俱牛】
     * @author xi
     */
    public static function sendToResult($mobile,$content,$isyx = 1,$type = true)
    {
        if(!preg_match('/^1[2-9]\d{9}(\,1[2-9]\d{9})*$/', $mobile) || trim($content) == '')
        {
            return false;
        }
        try
        {
			if($type && self::check110($mobile) == false)
			{
				file_put_contents(__DIR__.'/sms.log', "[0]time:".date('Y-m-d H:i:s')."mobile:".$mobile." content:".$content ." ip:". Helper::getIp() ."\r\n",FILE_APPEND);
				return false;
			}
			
            $msmUrl     = $isyx == 1 ?  self::YX_SEND_SMS_URL : self::SEND_SMS_URL;
            $pwd        = $isyx == 1 ?  self::YXPWD : self::PWD;
            $sn         = $isyx == 1 ?  self::YXSN : self::SN;

            header("Content-type:text/html;charset=utf-8");
            $sendQuery = 'action=send&userid=%s&account=%s&password=%s&mobile=%s&content=%s&sendTime=%s&extno=%s';
            $sendTime = '';//date('Y-m-d H:i:s');
            $userId = '';
            $user_id = '';
            $password = strtoupper(md5($pwd));
            $content = self::strToUtf8($content);
            $extNo = '';
            $sendQuery = sprintf($sendQuery,$userId,$sn,$password,$mobile,urlencode($content),$sendTime,$extNo);
            $jsonData = Helper::curlPost($msmUrl,$sendQuery);
            $result = json_decode($jsonData,true);
            if(isset($result['returnstatus']) && $result['returnstatus'] == 'Success')
            {
                //self::checkRemainpoint($result['remainpoint'],$isyx);
                $res = self::saveDB($mobile, $content);
            }  else {
                file_put_contents(__DIR__.'/sms.log', "[1]time:".date('Y-m-d H:i:s')."mobile:".$mobile." content:".$content." error:".var_export($result,true) ."\r\n",FILE_APPEND);
            }
            return $result;
        }
        catch (\Exception $e)
        {
            file_put_contents(__DIR__.'/sms.log', "[2]time:".date('Y-m-d H:i:s')."mobile:".$mobile." content:".$content." error:".var_export($e->getMessage(),true) ."\r\n",FILE_APPEND);
            return false;
        }
    }

    /**
     *  检查短信剩余条数
     */
    public static function checkRemainpoint($remainpoint ,$isyx = 1)
    {
        $messType   = $isyx == 1 ? '营销短信' : '验证码短信';
        if($remainpoint < 1000 && ($remainpoint % 100 == 0)) {
            $mobile = '13522742304';
            $content = '【俱牛】您的'.$messType.'短信条数不足 '.$remainpoint.' ,请及时充值';
            header("Content-type:text/html;charset=utf-8");
            $sendQuery = 'action=send&userid=%s&account=%s&password=%s&mobile=%s&content=%s&sendTime=%s&extno=%s';
            $sendTime = '';//date('Y-m-d H:i:s');
            $userId = '';
            $user_id = '';
            $password = strtoupper(md5(self::PWD));
            $content = self::strToUtf8($content);
            $extNo = '';
            $sendQuery = sprintf($sendQuery,$userId,self::SN,$password,$mobile,$content,$sendTime,$extNo);
            $jsonData = Helper::curlPost(self::SEND_SMS_URL,$sendQuery);
            $result = json_decode($jsonData,true);
            return $result;
            // 避免造成死循环
            //self::send($mobile, $content);
        }
    }

    /**
     * 保存到数据库里
     * @param string $moblie
     * @param string $content
     * @return boolean
     * @author xi
     */
    public static function saveDB($moblie,$content)
    {
        $model = new self();
        $model->mobile = $moblie;
        $model->content = $content;
        $model->create_time = date('Y-m-d H:i:s');
        $model->ip = Helper::getIp();
        $model->log = var_export($_SERVER,true);
        return $model->save();
    }
    /**
     * 生成验证码的key
     * @param $mobile
     * @param null $type
     * @return string
     */
    public static function createAuthCodeKey($mobile,$type = null)
    {
        return !empty($type) ? $mobile . 'M' . str_pad($type,3,'0',STR_PAD_LEFT) : $mobile;
    }
    /**
     * 设置验证码
     * @return void
     * @author zhangjunliang
     * @version 1.0.7
     **/
    public static function setAuthCode($mobile,$type = null)
    {
        $code = Helper::getRandNum();
        $key = self::createAuthCodeKey($mobile,$type);
        Yii::$app->redis_cache->set('flag' . $key, $code , 59);
        Yii::$app->redis_cache->set($key , $code , 1800);
        return $code;
    }
    /**
     * 删除验证码
     * @author zhangjunliang
     * @param $mobile
     * @param null $type
     * @date 2015-10-19
     */
    public static function deleteAuthCode($mobile,$type = null)
    {
        $key = self::createAuthCodeKey($mobile,$type);
        if(Yii::$app->redis_cache->exists('flag' . $key))
        {
            Yii::$app->redis_cache->delete('flag' . $key);
        }
        if(Yii::$app->redis_cache->exists($key))
        {
            Yii::$app->redis_cache->delete($key);
        }
        return true;
    }
    /**
     * 检测验证码
     * @return void
     * @author hcj
     * @version 1.0
     **/
    public static function checkAuthCode($mobile,$check_code,$type = null)
    {
        $code = self::getCode($mobile,$type);
        if($code == $check_code)
        {
            return true;
        }
        return false;
    }
    /**
     * 获取验证码
     * @return int
     * @author hcj
     * @version 1.0
     **/
    public static function getCode($mobile,$type = null)
    {
        $key = self::createAuthCodeKey($mobile,$type);
        $code = Yii::$app->redis_cache->get($key);
        if(!$code)
        {
            $code = self::setAuthCode($mobile,$type);
        }
        return $code;
    }

    public static function strToUtf8($data)
    {
        if( !empty($data) )
        {
            $fileType = mb_detect_encoding($data , array('UTF-8','GBK','LATIN1','BIG5')) ;
            if( $fileType != 'UTF-8')
            {
                $data = mb_convert_encoding($data ,'utf-8' , $fileType);
            }
        }
        return $data;
    }
	
	/**
	 * 短信发送验证
	 * 一个手机号一天只能发5条
	 * 一个 ip 一天只能发10条
	 * 同一个手机号发送间隔60秒
	 * @author xi
	 * @date 2017-2-9
	 */
	public static function check110($mobile)
	{
		$ip         = \common\helpers\Helper::getIp();
		$requestUrl = isset($_SERVER['REQUEST_URI'])? $_SERVER['REQUEST_URI'] : '****';
		
		$ipStr  = @file_get_contents( Yii::getAlias('@common').'/config/iplist.txt');
		$urlStr = @file_get_contents( Yii::getAlias('@common').'/config/urllist.txt');
		//白名单 whitelist.txt
		$whiteStr = @file_get_contents( Yii::getAlias('@common').'/config/whitelist.txt');
		
		
		$ipList  = explode(',',str_replace(["\r","\n","\r\n"],',', trim($ipStr)));
		$urlList = explode(',',str_replace(["\r","\n","\r\n"],',', trim($urlStr)));
		$whiteList = explode(',',str_replace(["\r","\n","\r\n"],',', trim($whiteStr)));
		
		if(in_array($mobile,$whiteList)){
			return true;
		}

		if(in_array($ip,$ipList) || in_array($requestUrl,$urlList)){
			return false;
		}

		$mobile24Num = 5;
		$ip24Num     = 100;

		$cache_60s       = 'check_60s_'.$mobile;
		$cache_24_mobile = 'check_24h_mobile_'.$mobile;
		$cache_24_ip     = 'check_24h_ip_'. ip2long($ip);

		if(Yii::$app->redis_cache->exists($cache_60s)){
			return false;
		}

		$mobile24SendOkNum = 1;
		if( Yii::$app->redis_cache->exists($cache_24_mobile))
		{
			$mobile24SendOkNum = Yii::$app->redis_cache->get($cache_24_mobile);
			if($mobile24SendOkNum > $mobile24Num){
				return false;
			}
		}

		$ip24SendOkNum = 1;

		if( Yii::$app->redis_cache->exists($cache_24_ip ))
		{
			$ip24SendOkNum = Yii::$app->redis_cache->get($cache_24_ip);
			if($ip24SendOkNum > $ip24Num){
				return false;
			}
		}

		Yii::$app->redis_cache->set($cache_60s, 1 ,60);
		Yii::$app->redis_cache->set($cache_24_mobile, $mobile24SendOkNum+1, strtotime('tomorrow')-time());
		Yii::$app->redis_cache->set($cache_24_ip, $ip24SendOkNum+1, strtotime('tomorrow')-time());
		
		return true;
	}

}