<?php
namespace common\helpers;

use Yii;
use yii\db\ActiveRecord;
//阿里云短信类包
use Aliyun\Core\Config;
use Aliyun\Core\Profile\DefaultProfile;
use Aliyun\Core\DefaultAcsClient;
use Aliyun\Api\Sms\Request\V20170525\SendSmsRequest;

class SendSmg extends ActiveRecord
{
    
    /**
     * 发送短信短信
     * @param string $mobile 手机号
     * @param string $content  内容
     * @author liwenyong<liwenyong@c-ntek.com>
     */
    public static function send($phone,$content,$type=true)
    {
        if (!preg_match('/^1[2-9]\d{9}(\,1[2-9]\d{9})*$/', $phone) || trim($content) == '') {
            return false;
        }
        try
        {
//            if ($type) { 
//                if (self::check110($phone) == false) {
//                    file_put_contents(__DIR__.'/sms.log', "[0]time:".date('Y-m-d H:i:s')."mobile:".$phone." content:".$content ." ip:". Helper::getIp() ."\r\n",FILE_APPEND);
//                    return false;
//                }
//            }
            Config::load();             //加载区域结点配置
            $accessKeyId = Yii::$app->params['accessKeyId'];
            $accessKeySecret = Yii::$app->params['accessKeySecret'];
            $templateCode = Yii::$app->params['templateCode'];   //短信模板ID
            //短信API产品名（短信产品名固定，无需修改）
            $product = "Dysmsapi";
            //短信API产品域名（接口地址固定，无需修改）
            $domain = "dysmsapi.aliyuncs.com";
            //暂时不支持多Region（目前仅支持cn-hangzhou请勿修改）
            $region = "cn-hangzhou";
            // 初始化用户Profile实例
            $profile = DefaultProfile::getProfile($region, $accessKeyId, $accessKeySecret);
            // 增加服务结点
            DefaultProfile::addEndpoint("cn-hangzhou", "cn-hangzhou", $product, $domain);
            // 初始化AcsClient用于发起请求
            $acsClient = new DefaultAcsClient($profile);
            // 初始化SendSmsRequest实例用于设置发送短信的参数
            $request = new SendSmsRequest();
            // 必填，设置短信接收号码
            $request->setPhoneNumbers($phone);
            // 必填，设置签名名称
            $request->setSignName(Yii::$app->params['SignName']);
            // 必填，设置模板CODE
            $request->setTemplateCode(Yii::$app->params['templateCode']);
            $smsData = array('code'=>$content);    //所使用的模板若有变量 在这里填入变量的值  我的变量名为username此处也为username
            //选填-假如模板中存在变量需要替换则为必填(JSON格式),友情提示:如果JSON中需要带换行符,请参照标准的JSON协议对换行符的要求,比如短信内容中包含\r\n的情况在JSON中需要表示成\\r\\n,否则会导致JSON在服务端解析失败
            $request->setTemplateParam(json_encode($smsData));
            //发起访问请求
            $acsResponse = $acsClient -> getAcsResponse($request);
            //返回请求结果
            $result = json_decode(json_encode($acsResponse), true);
            $resp = $result['Code'];
            return $resp;
        }
        catch (\Exception $e)
        {
            file_put_contents(__DIR__.'/sms.log', "[2]time:".date('Y-m-d H:i:s')."mobile:".$phone." content:".$content." error:".var_export($e->getMessage(),true) ."\r\n",FILE_APPEND);
            return false;
        }
    }
}