<?php

namespace common\helpers;

use Yii;
use Aliyun\Core\Config;
use Aliyun\Core\Profile\DefaultProfile;
use Aliyun\Core\DefaultAcsClient;
use Aliyun\Api\Sms\Request\V20170525\SendSmsRequest;

/**
 * @desc 阿里云短信接口
 * @author chengjuanjuan <chengjuanjuan@c-ntek.com>
 * @date 2017-09-01
 * @return mixed
 *
 **/
class SendMsg
{
    const ACCESS_KEY_ID = 'LTAIcm99C3Jn8zIo';
    const ACCESS_KEY_SECRET = 'nXPWqNkYxNs2CkWXf18fJ2kVkBnqlN';
    const PRODUCT = 'Dysmsapi';               //短信API产品名（短信产品名固定，无需修改）
    const DOMAIN = 'dysmsapi.aliyuncs.com';  //短信API产品域名（接口地址固定，无需修改）
    const REGION = 'cn-hangzhou';           //暂时不支持多Region（目前仅支持cn-hangzhou请勿修改）
    const SIGN_NAME = '北京智能云集';       //设置签名名称

    /**
     * 发送验证码
     * author chengjuanjuan <chengjuanjuan@c-ntek.com>
     * date 2017-09-03
     * @param string $mobile 接收手机号
     * $param intval type       短信类型:1厂家注册验证码;2待补充
     * @param array $content 短信内容
     * @param templateCode  短信模板id
     * @return code
     */
    public static function sendMsg ($phone = '13436841028', $type = 1, $content = array(), $templateCode = 'SMS_89850129')
    {
        //加载区域结点配置
        Config::load();
        //判断短信类型
        if ($type == 1) {
            $action ='register';
            $code    = rand(100000, 999999);
            $content = array('code' => $code);
            #Yii::$app->session[$action.$phone]=$code;
            Yii::$app->cache->set($action.$phone,$code,60*10);
        }
        //exit;
        // 初始化用户Profile实例
        $profile = DefaultProfile::getProfile(self::REGION, self::ACCESS_KEY_ID, self::ACCESS_KEY_SECRET);

        // 增加服务结点
        DefaultProfile::addEndpoint(self::REGION, self::REGION, self::PRODUCT, self::DOMAIN);

        // 初始化AcsClient用于发起请求
        $acsClient = new DefaultAcsClient($profile);

        // 初始化SendSmsRequest实例用于设置发送短信的参数
        $request = new SendSmsRequest();

        // 必填，设置短信接收号码
        $request->setPhoneNumbers($phone);

        // 必填，设置签名名称
        $request->setSignName(self::SIGN_NAME);

        // 必填，设置模板CODE
        $request->setTemplateCode($templateCode);

        //所使用的模板若有变量 在这里填入变量的值  我的变量名为username此处也为username
        $smsData = $content;

        //选填-假如模板中存在变量需要替换则为必填(JSON格式),友情提示:如果JSON中需要带换行符,请参照标准的JSON协议对换行符的要求,比如短信内容中包含\r\n的情况在JSON中需要表示成\\r\\n,否则会导致JSON在服务端解析失败
        $request->setTemplateParam(json_encode($smsData));

        //发起访问请求
        $acsResponse = $acsClient->getAcsResponse($request);
        //返回请求结果
        $result = json_decode(json_encode($acsResponse), true);
        $responceCode = $result['Code'];
        if($responceCode=='OK'){
            $responceData = 1;
        }else{
            //短信发送失败
            $responceData = -2;
        }
        return $responceData;
    }

    /*
     * @desc 短信剩余条数查询
     * @date 2017-09-03
     * @param
     * return mixed
     * */
    public static function getMsgCount ()
    {


    }


}

?>