<?php
namespace common\helpers;
/**
 * 华信短信接口
 */
use Yii;
use yii\db\ActiveRecord;
use common\helpers\alidayu\AliSendSMS;
use common\models\Mq;

class LibrarySMS extends ActiveRecord
{
    //确定短信模板
    const template_head     = "【俱牛】验证码：";
    const template_login    = "，请在验证页面输入该数字，并点击登录完成操作。"; //登录
    const template_register = "，请在验证页面输入该数字，并点击注册完成操作。"; //主册
    const template_back     = "，您正在找回密码，如非本人操作，请忽略。";       //密码找回
    const template_next     = "，请在验证页面输入该数字，并点击下一步完成操作。";//下一步

    /**
     * 短信发送
     * @param $mobile
     * @param $template
     * @return array $result
     * @author lzg
     * @date 2017-03-02
     */
    public static function send($mobile,$template){//短信验证码
        //判断手机号
        if(!preg_match('/^1[2-9]\d{9}(\,1[2-9]\d{9})*$/', $mobile))
        {
            $result['success'] = false;
            $result['message'] = '手机号格式不正确';
            return $result;
        }
        $code = (string)rand( 100000 , 999999 );
        //判断优先发送那个短信-- 华信与阿里大于
        $content     = self::template_head.$code;
        $content_ali = [];
        switch ($template){
            case "template_login":
                $content = $content.self::template_login;
                $content_ali['param']['code']   = $code;
                $content_ali['template_id']     = "SMS_51395002";
                break;
            case "template_register":
                $content = $content.self::template_register;
                $content_ali['param']['code']   = $code;
                $content_ali['template_id']     = "SMS_51410001";
                break;
            case "template_back":
                $content = $content.self::template_back;
                $content_ali['param']['code']  = $code;
                $content_ali['template_id']    = "SMS_51240034";
                break;
            case "template_next":
                $content = $content.self::template_next;
                $content_ali['param']['code']   = $code;
                $content_ali['template_id']     = "SMS_51405016";
                break;
            default:
                $result['success'] = false;
                $result['message'] = '短信模板类型参数不正确';
                return $result;
                break;
        }
        //处理短信发送次数
        $mobile_check = SendSMS::check110($mobile,$type=false);
        if(!$mobile_check){
            @file_put_contents("/tmp/sms_success.log","[短信频繁获取]time:".date("Y-m-d H:i:s")."mobile:".$mobile."\r\n",FILE_APPEND);
            $result['success'] = false;
            $result['message'] = '您获取验证码过于频繁，请稍后再试。';
            return $result;
        }
        $cache_24_mobile = 'check_24h_mobile_count'.$mobile;
        $mobileCount = 1;
        if( Yii::$app->redis_cache->exists($cache_24_mobile)){
            $mobileCount = Yii::$app->redis_cache->get($cache_24_mobile);
            Yii::$app->redis_cache->set($cache_24_mobile, $mobileCount+1, strtotime('tomorrow')-time());
        }else{
            Yii::$app->redis_cache->set($cache_24_mobile, $mobileCount+1, strtotime('tomorrow')-time());
        }

        //取余
        $num = $mobileCount % 2 ; // 0 发主 1 发从
        $sms_priority = "huaxin"; //默认发华信
        if( isset( Yii::$app->params['sms_priority'])  ){
            if( isset( Yii::$app->params['sms_priority']['main'] ) && $num == 1 ){
                $sms_priority = Yii::$app->params['sms_priority']['main'];
            }
            if( isset( Yii::$app->params['sms_priority']['slave'] ) && $num == 0 ){
                $sms_priority = Yii::$app->params['sms_priority']['slave'];
            }
        }
        //判断调用哪个短信服务商
        //判断优先发送那个短信-- 华信与阿里大于 --> 主从
        $status  = false;
        $message = "获取失败，请重新获取";
        if($sms_priority == "alidayu"){
            $res = AliSendSMS::send($mobile,$content_ali);
            //处理短信结果
            if(isset($res['success']) && $res['success'] == true ){//成功
                @file_put_contents("/tmp/sms_success.log","[阿里大于".$mobileCount.$sms_priority."]time:".date("Y-m-d H:i:s")."mobile:".$mobile." content:".var_export($content_ali,true)."\r\n",FILE_APPEND);
                $status = true;
                $message = '发送成功';
            }else{//失败
                @file_put_contents("/tmp/sms_error.log","[阿里大于".$mobileCount.$sms_priority."]time:".date("Y-m-d H:i:s")."mobile:".$mobile." content:".var_export($content_ali,true)."error".var_export($res,true)."\r\n",FILE_APPEND);
                //发送邮件
                if(isset(Yii::$app->params['sms_error_email'])){
                    $params = [
                        'to' => Yii::$app->params['sms_error_email'],
                        'cc' => '',
                        'subject' => "阿里大于短信验证码发送失败",
                        'body' => json_encode($res),
                    ];
                    Mq::publish('mail', 'mail', $params,'exchange');
                }
            }
        }
        else{
            $res = SendSMS::send($mobile,$content,$type=false);
            //处理短信结果 -- 记录日志
            if($res){ //发送成功
                @file_put_contents("/tmp/sms_success.log","[华信".$mobileCount.$sms_priority."]time:".date("Y-m-d H:i:s")."mobile:".$mobile." content:".$content."\r\n",FILE_APPEND);
                $status = true;
                $message = '发送成功';
            }else{//发送失败
                @file_put_contents("/tmp/sms_error.log","[华信".$mobileCount.$sms_priority."]time:".date("Y-m-d H:i:s")."mobile:".$mobile." content:".$content."\r\n",FILE_APPEND);
                //发送邮件 -- 邮件信息
                if(isset(Yii::$app->params['sms_error_email'])){
                    $params = [
                        'to' => Yii::$app->params['sms_error_email'],
                        'cc' => '',
                        'subject' => "华信短信验证码发送失败",
                        'body' => "[华信]time:".date("Y-m-d H:i:s")."mobile:".$mobile." content:".$content,
                    ];
                    Mq::publish('mail', 'mail', $params,'exchange');
                }
            }
        }

        $result['success'] = $status;
        $result['message'] = $message;
        $result['code']    = $code;
        return $result;
    }

}