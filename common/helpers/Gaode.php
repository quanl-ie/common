<?php
namespace common\helpers;

use Yii;

class Gaode
{
    /**
     * 查坐标
     * @param string $address
     * @return array
     * @author xi
     * @date 2017-12-25
     */
    public static function getLocation($address,$city = '')
    {
        $url = "http://restapi.amap.com/v3/geocode/geo?key=".Yii::$app->params['gaode_key']."&address=".$address."&city=".$city;
        $jsonStr = Helper::curlGet($url);
        $arr = json_decode($jsonStr,true);
        if(isset($arr['status']) && $arr['status'] == 1 && isset($arr['geocodes'][0]['location'])){
            $location = $arr['geocodes'][0]['location'];
            $locationArr = explode(',',$location);
            return [
                'lon' => $locationArr[0],
                'lat' => $locationArr[1]
            ];
        }
        else {
            return [
                'lon' => 0,
                'lat' => 0
            ];
        }
    }
    //获取省市区名称
    public static function getProvinceName($address,$city = '')
    {
        $address = urlencode($address);
        $url = "http://restapi.amap.com/v3/geocode/geo?key=".Yii::$app->params['gaode_key']."&address=".$address."&city=".$city;
        $jsonStr = Helper::curlGet($url);
        $arr = json_decode($jsonStr,true);
        if(isset($arr['status']) && $arr['status'] == 1 && $arr['count'] > 0){
            if(isset($arr['geocodes'][0]['province'])){
                $province = $arr['geocodes'][0]['province'];
            }
            if(isset($arr['geocodes'][0]['city'])){
                $city = $arr['geocodes'][0]['city'];
            }
            if(isset($arr['geocodes'][0]['district'])){
                $district = $arr['geocodes'][0]['district'];
            }
			
			$location = $arr['geocodes'][0]['location'];
			$locationArr = explode(',',$location);
			return [
				'province' => $province,
				'city' => $city,
				'district' => $district,
				'lon' =>$locationArr[0],
				'lat' =>$locationArr[1],
			];
			
        }else {
            return [];
        }
    }

    /**
     * 根据经纬度查地址
     * @param $lon
     * @param $lat
     * @return string
     */
    public static function getAddressByLatLon($lon,$lat)
    {
        if($lon && $lat)
        {
            $url = "http://restapi.amap.com/v3/geocode/regeo?key=".Yii::$app->params['gaode_key']."&location=$lon,$lat&poitype=&radius=&extensions=base&batch=false&roadlevel=0";
            $jsonStr = Helper::curlGet($url);
            $arr = json_decode($jsonStr,true);
            if(isset($arr['status']) && $arr['status'] == 1 && isset($arr['regeocode']) && $arr['regeocode'])
            {
                if($arr['regeocode']['formatted_address']){
                    return $arr['regeocode']['formatted_address'];
                }
            }
        }

        return '';
    }

    /**
     * 坐标转换
     * @param $lon
     * @param $lat
     * @param string $coordsys
     * @return array
     * @author xi
     */
    public static function convert($lon,$lat,$coordsys = 'baidu')
    {
        if($lon > 0 && $lat > 0)
        {
            $url = "https://restapi.amap.com/v3/assistant/coordinate/convert?locations=$lon,$lat&coordsys=$coordsys&output=json&key=".Yii::$app->params['gaode_key'];
            $jsonStr = Helper::curlGet($url);
            $arr = json_decode($jsonStr,true);

            if(isset($arr['locations']) && $arr['locations'])
            {
                $locationArr = explode(',',$arr['locations']);
                if($locationArr){
                    return [
                        'lon' => $locationArr[0],
                        'lat' => $locationArr[1]
                    ];
                }

            }
        }
        return [
            'lon' => 0,
            'lat' => 0
        ];
    }
}