<?php
namespace common\helpers;

use common\helpers\Baidu;
use Yii;
use linslin\yii2\curl;
use common\models\Log;
class Helper
{

	/**
	 *
	 * curl 功能简单封装
	 * @param string $url
	 * @author xi
	 * @since 2014-5-20
	 */
	public static function curlPost($url,$post_data,$headers=[])
	{
		$ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);

        if($headers){
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_HTTPHEADER , $headers);
        }
        else {
            curl_setopt($ch, CURLOPT_HEADER, false);
        }
        curl_setopt($ch, CURLOPT_NOBODY, false); // remove body
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
//        curl_setopt($ch, CURLOPT_REFERER, Yii::$app->params['sh_url']);
        curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		curl_setopt($ch, CURLOPT_SSLVERSION, 1);

        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
	}

	/**
	 *
	 * curl 功能简单封装
	 * @param string $url
	 * @author xi
	 * @since 2014-5-20
	 */
	public static function curlGet($url)
	{
	    try{
            return file_get_contents($url);
	    }
	    catch (\Exception $e){
	        try{
	            $ch = curl_init();
	            curl_setopt($ch, CURLOPT_URL, $url);
	            curl_setopt($ch, CURLOPT_HEADER, false);
	            curl_setopt($ch, CURLOPT_NOBODY, false); // remove body
	            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
	            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
	            curl_setopt($ch, CURLOPT_SSLVERSION, 1);

	            $result = curl_exec($ch);
	            curl_close($ch);
	            return $result;
	        }
	        catch (\Exception $e1){
	            Log::mail('file_get_content 取数据失败', "请求地址:$url");
	            return '';
	        }
	    }
	}

	/**
	 *
	 * @param unknown $url
	 * @param unknown $vars
	 * @param number $second
	 * @param unknown $aHeader
	 * @return mixed|boolean
	 */
	public static function curlPostSsl($url, $vars, $second=30,$aHeader=array())
	{
	    $ch = curl_init();
	    //超时时间
	    curl_setopt($ch,CURLOPT_TIMEOUT,$second);
	    curl_setopt($ch,CURLOPT_RETURNTRANSFER, 1);
	    //这里设置代理，如果有的话
	    //curl_setopt($ch,CURLOPT_PROXY, '10.206.30.98');
	    //curl_setopt($ch,CURLOPT_PROXYPORT, 8080);
	    curl_setopt($ch,CURLOPT_URL,$url);
	    curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
	    curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,false);

	    //以下两种方式需选择一种

	    //第一种方法，cert 与 key 分别属于两个.pem文件
	    //默认格式为PEM，可以注释
	    $dirname = dirname(__FILE__);
	    curl_setopt($ch,CURLOPT_SSLCERTTYPE,'PEM');
	    curl_setopt($ch,CURLOPT_SSLCERT, $dirname.'/wechat/cert/apiclient_cert.pem');
	    //默认格式为PEM，可以注释
	    curl_setopt($ch,CURLOPT_SSLKEYTYPE,'PEM');
	    curl_setopt($ch,CURLOPT_SSLKEY, $dirname.'/wechat/cert/apiclient_key.pem');
	    curl_setopt($ch,CURLOPT_CAINFO, $dirname.'/models/wechat/cert/rootca.pem');

	    //第二种方式，两个文件合成一个.pem文件
	    //curl_setopt($ch,CURLOPT_SSLCERT,'/models/wechat/rootca.pem');

	    if( count($aHeader) >= 1 ){
	        curl_setopt($ch, CURLOPT_HTTPHEADER, $aHeader);
	    }

	    curl_setopt($ch,CURLOPT_POST, 1);
	    curl_setopt($ch,CURLOPT_POSTFIELDS,$vars);
	    $data = curl_exec($ch);

	    if($data){
	        curl_close($ch);
	        return $data;
	    }
	    else {
	        $error = curl_errno($ch);
	        echo "call faild, errorCode:$error\n";
	        curl_close($ch);
	        return false;
	    }
	}

	/**
	 *
	 * 取ip 地址
	 * @author xi
	 * @since 2014-5-23
	 */
	public static function getIp()
	{
		 $ip = '';
        if (isset($_SERVER)) {
            if (isset($_SERVER["HTTP_X_FORWARDED_FOR"])) {
                $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
                if (strstr($ip, ",")) {
                    $x = explode(',', $ip);
                    $ip = trim(end($x));
                }
            } elseif (isset($_SERVER["HTTP_CLIENT_IP"])) {
                $ip = $_SERVER["HTTP_CLIENT_IP"];
            } elseif (isset($_SERVER["REMOTE_ADDR"])) {
                $ip = $_SERVER["REMOTE_ADDR"];
            } else {
                $ip = isset($_SERVER["SSH_CLIENT"])?$_SERVER["SSH_CLIENT"]:'';
            }
        } else {
            if (getenv("HTTP_X_FORWARDED_FOR")) {
                $ip = getenv("HTTP_X_FORWARDED_FOR");
            } elseif (getenv("HTTP_CLIENT_IP")) {
                $ip = getenv("HTTP_CLIENT_IP");
            } else {
                $ip = getenv("REMOTE_ADDR");
            }
        }
        return $ip;
	}
    /**
     * 计算当前所在城市名称,不带市 默认北京
     * @param string $city 城市名称
     * @param double $lng 企业百度经度
     * @param double $lat 企业百度纬度
     * @return string
     * @author zhangjunliang
     * @dete 2015-08-19
     */
    public static function getAddressByLocation($city , $lng = '' ,$lat = '')
    {
        $city = str_ireplace('市','',trim($city));
        if(empty($city))
        {
            if($lng && $lat)
            {
                $addressInfo = Baidu::getAddressByLocation($lng , $lat);
                if(empty($addressInfo['city']))
                {
                    return $addressInfo['city'];
                }
            }
            $ip = static::getIp();
            $addressInfo = static::getAddressByIp($ip);
            return !empty($addressInfo['city']) ? $addressInfo['city'] : '';
        }
        else
        {
            return $city;
        }
    }
	/**
	 * 根据ip查询地址
	 * @param string $ip
	 * @return unknown
	 * @author xi
	 * @date 2015-3-18
	 */
	public static function getAddressByIp($ip)
	{
	    $urls [] = "http://int.dpool.sina.com.cn/iplookup/iplookup.php?format=json&ip=$ip";
	    $urls [] = "http://ip.taobao.com/service/getIpInfo.php?ip=$ip";

	    $rand = rand(0, 1);
	    $result = self::curlGet($urls[$rand]);
	    $result = json_decode($result,true);

	    if(isset($result['data'])){
	        return [
	            'country' => isset($result['data']['country']) ?  $result['data']['country'] : '',
	            'province' => isset($result['data']['region']) ? $result['data']['region'] : '',
	            'city' => isset($result['data']['city']) ? str_replace('市', '',  $result['data']['city']) : ''
	        ];
	    }
	    else{
	        return [
	            'country' => isset($result['country']) ? $result['country'] : '',
	            'province' => isset($result['province']) ? $result['province'] : '',
	            'city' => isset($result['city']) ? str_replace('市', '', $result['city']) : ''
	        ];
	    }
	}

	/**
	 * 计算字符串长度
	 * @param string $str
	 * @return int
	 */
	public static function strCount($str)
	{
	    if(empty($str)){
	        return 0;
	    }
	    if(function_exists('mb_strlen')){
	        return mb_strlen($str,'utf-8');
	    }
	    else {
	        preg_match_all("/./u", $str, $ar);
	        return count($ar[0]);
	    }
	}

	/**
	 * 字符串截取
	 * @param string $str
	 * @param number $start
	 * @param int $length
	 * @param string $charset
	 * @param string $suffix
	 */
	public static function csubstr($str, $start=0, $length, $charset="utf-8", $suffix=true)
	{
	    if(function_exists("mb_substr"))
	    {
	        $slice = mb_substr($str, $start, $length, $charset);
	    }
	    else
	    {
	        $re['utf-8']   = "/[\x01-\x7f]|[\xc2-\xdf][\x80-\xbf]|[\xe0-\xef][\x80-\xbf]{2}|[\xf0-\xff][\x80-\xbf]{3}/";
	        $re['gb2312'] = "/[\x01-\x7f]|[\xb0-\xf7][\xa0-\xfe]/";
	        $re['gbk']          = "/[\x01-\x7f]|[\x81-\xfe][\x40-\xfe]/";
	        $re['big5']          = "/[\x01-\x7f]|[\x81-\xfe]([\x40-\x7e]|\xa1-\xfe])/";

	        preg_match_all($re[$charset], $str, $match);
	        if(count($match[0]) <= $length) return $str;
	        $slice = join("",array_slice($match[0], $start, $length));
	    }
	    if($suffix) return $slice."…";
	    return $slice;
	}

	/**
	 * substr 方法可以截取UTF-8 中文字符
	 * @param string $string 是必须的，是被截取的
	 * @param integer $length 是必须的，截多少个字
	 * @param string $etc 可选，默认为...
	 * @return string  返回一个新的串
	 * @author xi
	 * @since 2014-5-13
	 */
	public static function subString($string, $length, $etc = '...') {
			$result = '';
			$string = html_entity_decode ( trim ( strip_tags ( $string ) ), ENT_QUOTES, 'UTF-8' );
			$strlen = strlen ( $string );
			for($i = 0; (($i < $strlen) && ($length > 0)); $i ++) {
				if ($number = strpos ( str_pad ( decbin ( ord ( substr ( $string, $i, 1 ) ) ), 8, '0', STR_PAD_LEFT ), '0' )) {
					if ($length < 1.0) {
						break;
					}
					$result .= substr ( $string, $i, $number );
					$length -= 1.0;
					$i += $number - 1;
				} else {
					$result .= substr ( $string, $i, 1 );
					$length -= 0.5;
				}
			}
			$result = htmlspecialchars ( $result, ENT_QUOTES, 'UTF-8' );
			if ($i < $strlen) {
				$result .= $etc;
			}
			return $result;
	}

	/**
	 * 取随机ip
	 * @return string
	 * @author xi
	 * @date 2015-3-20
	 */
	public static function getRandIp()
	{
	    return rand(10,255).'.'.rand(10,255).'.'.rand(10,255).'.'.rand(10,255);
	}

	/**
	 * 调试方法
	 * @param minx $val
	 * @return void
	 * @author xi
	 * @date 2015-1-18
	 */
	public static function dump($val)
	{
	    header("Content-type: text/html; charset=utf-8");
	    echo '<pre>';
	    print_r($val);
	    echo '</pre>';
	    die;
	}

	/**
	 * 米转换成公里
	 * @param int $m
	 * @return string
	 */
	public static function m2km($m)
	{
	    if($m<1000 && $m>0){
	        return $m.'米';
	    }
	    else if($m<50000){
	        return round($m/1000,1).'公里';
	    }
	    return '';
	}

	/**
	 * 随机概率
	 * @param unknown $proArr
	 * @return int
	 */
	public static function getRand($proArr) {
	    $result = '';
	    $proSum = array_sum($proArr);
	    foreach ($proArr as $key => $proCur) {
	        $randNum = rand(1, $proSum);             //抽取随机数
	        if ($randNum <= $proCur) {
	            $result = $key;                         //得出结果
	            break;
	        } else {
	            $proSum -= $proCur;
	        }
	    }
	    unset ($proArr);
	    return $result;
	}

	/**
	 * 获取操作系统
	 * @return number
	 * @author xi
	 */
	public static function getOS()
	{
	    if(stristr($_SERVER['HTTP_USER_AGENT'],'Android')) {
	        return 2;
	    }
	    else if(stristr($_SERVER['HTTP_USER_AGENT'],'iPhone')){
	        return 1;
	    }
	    else if(stristr($_SERVER['HTTP_USER_AGENT'],'Windows')){
	        return 3;
	    }
	    else{
	        return 4;
	    }
	}

	/**
	 * 获取手机归属地
	 * @param string $mobile
	 * @return string
	 * @date 2015-07-22
	 */

	public static function getMobileArea($mobile){
	    $url = "http://tcc.taobao.com/cc/json/mobile_tel_segment.htm?format=json&tel=".$mobile."&t=".time();
	    $content = file_get_contents($url);
	    $content = @mb_convert_encoding($content, 'utf8' , 'gbk');
	    preg_match_all("/(\w+):'([^']+)/", $content, $m);
	    $result = array_combine($m[1], $m[2]);
	    return $result;
	}


	/**
 	 * 返回json统一数据格式
 	 * @author hcj
 	 * @param $result int 消息编号 默认为 0 （错误）
 	 * @param $message string 消息内容
 	 * @param $data string|int|array 扩张数据 会覆盖result、message
 	 */
	public static function result($result, $message="", $data=array()){
		$res = array(
			'success'=>$result,
			'message'=>$message,
		);
		if($data){
		    $data = self::toString($data);
			$res = array_merge($res,$data);
		}
	    if(!$res['success'] || $res['success']<0){
	        $res['code'] = 1000 - $res['success'];
	        $res['code'] = (string)$res['code'];
	        $res['success'] = false;
	    }else{
	        $res['success'] = true;
	        unset($res['message']);
	    }
		return $res;
	}

	/**
	 * 生成access_token
	 *
	 * @return string
	 * @author hcj
	 **/
	public static function getAccessToken()
	{
		return md5(uniqid(time(),true));
	}


	/**
	 * 验证手机号
	 *
	 * @return void
	 * @author hcj
	 **/
	public static function verifyMobile($mobile)
	{
		return preg_match('/^1(\d){10}$/u', trim($mobile));
	}

    /**
     * 验证邮箱
     *
     * @return void
     * @author zhuangjunliang
     **/
    public static function verifyEmail($email)
    {
        return (bool)filter_var($email, FILTER_VALIDATE_EMAIL);
    }


	/**
	 * 生成随机数
	 *
	 * @return void
	 * @author hcj
	 * @version 1.0
	 **/
	public static function getRandNum($num=6)
	{
		if($num>9)
			return self::getRandNum(9).self::getRandNum($num-9);
		$bn = "1".str_repeat("0", $num-1);
		return rand($bn,$bn*10-1);
	}

	/**
	 * 写入日志文件
	 *
	 * @return void
	 * @author hcj
	 * @version 1.0
	 **/
	public static function console()
	{
		foreach (func_get_args() as $k => $v) {
			Log::save($v,'app.log');
		}
	}

	/**
	 * 生成二维码
	 * @param string $content
	 * @return string
	 * @author xi
	 * @date 2015-8-24
	 */
	public static function getQR($content)
	{
	    return  base64_encode( static::curlGet('http://qr.liantu.com/api.php?text='.$content));
	}


	/**
	 * 把给定字符串转换成蛇形
	 *
	 * @param string $value 如 fooBar =>foo_bar
	 * @return string
	 * @author hcj
	 * @version 1.0
	 **/
	public static function snake($value,$delimiter="_")
	{
		if(ctype_lower($value)) return $value;
		$replace = '$1'.$delimiter.'$2';
		return strtolower(preg_replace('/(.)([A-Z])/', $replace, $value));
	}

	/**
	 * 把给定字符串转换成首字母大写
	 *
	 * @param string $value 如 foo_bar=>FooBar
	 * @return string
	 * @author hcj
	 * @version 1.0
	 **/
	public static function studly($value)
	{
		$value = ucwords(str_replace(array('-','_'), ' ', $value));
		return str_replace(' ', '', $value);
	}

	/**
	 * 转换成字符串
	 * @param array $arr
	 * @return array
	 * @author xi
	 * @date 2015-8-26
	 */
	public static function toString($arr)
	{
	    foreach($arr as $key=>$val)
	    {
	        if(is_array($val)){
	            $arr[$key] = self::toString($val);
	        }
	        else if(is_int($val) || is_float($val) || is_double($val)){
	            $arr[$key] = (string)$val;
	        }
	        else if(is_null($val)){
	            $arr[$key] = '';
	        }
	    }
	    return $arr;
	}
    /**
     * 获取手机类型
     * @return string
     * @author zhangjunliang
     * @date 2015-9-15
     */
    public static function getMobileType()
    {
        $userAgent = $_SERVER['HTTP_USER_AGENT'];
        $mobileType = '';
        if(stristr($userAgent,'android'))
        {
            $mobileType = 'android';
        }
        else if(stristr($userAgent,'iphone'))
        {
            $mobileType = 'ios';
        }
        else if(stristr($userAgent,'windows phone'))
        {
            $mobileType = 'windows phone';
        }
        else {
            $mobileType = 'PC';
        }
        return strtolower($mobileType);
    }

    /**
     * 获取浏览器，是手机浏览器，微信浏览器，pc
     * @return int
     */
    public static function getBrowser()
    {
        $userAgent = isset($_SERVER['HTTP_USER_AGENT'])?$_SERVER['HTTP_USER_AGENT']:'';

        if ( strpos($userAgent, 'MicroMessenger') !== false ) {
            return 1;
        }
        else if(stristr($userAgent,'android') || stristr($userAgent,'iphone') || stristr($userAgent,'windows phone')){
            return 2;
        }
        else {
            return 3;
        }
    }

    /**
     * 计算当前日期，位于上半年或下半年；
     * 返回起始日期点
     *
     * @param int $month
     * @return array
     * @author lijing
     * @date 2016-03-02
     */
    public static function getMonthYear($month)
    {
    	$month_year = [];
    	$month = $month>0 ? $month : $month+12;

    	//下半年
    	if( 6 <= $month && $month  < 12 )
    	{
    		$month_year['start_time'] = date('Y').'-06-01';
    		$month_year['end_time'] = date('Y').'-11-31';
    	}
    	elseif( $month == 12 )
    	{
    		$month_year['start_time'] = date('Y').'-12-01';
    		$month_year['end_time'] = (date('Y')+1).'-05-31';
    	}
    	else
    	{
    		$month_year['start_time'] = (date('Y')-1).'-12-01';
    		$month_year['end_time'] = date('Y').'-05-31';
    	}

    	return $month_year;
    }

    /**
     * 删除数组中元素，并重新设置索引
     *
     * @param array 	&$arr 数组的引用，在原数组上操作
     * @param int|array $keys  要删除的数组的索引值
     * @return null
     * @author lijing
     * @date 2016-04-26
     */
    public static function array_remove_value(&$arr, $keys)
    {
    	if (!is_array($keys)) {
    		$keys = array($keys);
    	}
    	foreach ($keys as $k) {
    		//unset($array[$k]);
    		array_splice($arr, $k, 1);
    	}
    	$arr = array_values($arr);
    }

    /**
     * 根据身份证获取生日
     * @param string $id_card
     * @return string
     * @author xi
     * @date 2016-5-27
     */
    public static function getBirthdayByIdCard($id_card)
    {
        if(in_array(strlen($id_card), [15,18]) ){
            return substr($id_card,6,4).'-'.substr($id_card,10,2).'-'.substr($id_card,12,2);
        }
        return '';
    }

    /**
     * 生成uuid
     * @return string
     * @author xi
     */
    public static function createUUID(){
        $str = md5(uniqid(mt_rand(), true));
        return $str;
    }

    /**
     * 替换新的尺寸
     * @example  /2016/09/11/20161234.jpg  返回/2016/09/11/20161234_**_**.jpg
     * @param string $srcFile
     * @param string $replaceSize
     * @return string
     */
    public static function toSizeImagePath($srcFile,$replaceSize)
    {
        if($srcFile && $replaceSize)
        {
            $extension = pathinfo($srcFile, PATHINFO_EXTENSION);
            $dictFile  = $dstpath = str_replace('.'.$extension,  '_'.$replaceSize.'.'.$extension,$srcFile);
            return $dictFile;
        }
        return $srcFile;
    }

    /**
     * 获取身份证性别
     * @param string $idCard
     * @return int
     */
    public static function getSexByIdCard($idCard)
    {
        if( strlen($idCard) ==15 ){
            return $idCard[13]%2 ==0 ?2:1;
        }
        else if(strlen($idCard) == 18){
            return $idCard[16]%2 ==0 ?2:1;
        }
        return 0;
    }
    /**
	* 活动力加密
	*
    **/
	public static function think_ucenter_md5($str, $key = 'vk0lQBLK`6"c&d-AbDtu[F47$;i~%!#YCZGRh^>?')     //活动力加密
    {
        return '' === $str ? '' : md5(sha1($str) . $key);
    }

	/**
	 * 时间差计算
	 * @param $time
	 * @return string
	 * @author guoyashuai
	 * @date 2016-11-28
	 */
	public static function time2Units ($time)
	{
	   $now  = time();     // Current timestamp
	   $diff = $now - $time;
	   $year   = floor($diff / 60 / 60 / 24 / 365);
	   $time  -= $year * 60 * 60 * 24 * 365;
	   $month  = floor($diff / 60 / 60 / 24 / 30);
	   $time  -= $month * 60 * 60 * 24 * 30;
	   $week   = floor($diff / 60 / 60 / 24 / 7);
	   $time  -= $week * 60 * 60 * 24 * 7;
	   $day    = floor($diff / 60 / 60 / 24);
	   $time  -= $day * 60 * 60 * 24;
	   $hour   = floor($diff / 60 / 60);
	   $time  -= $hour * 60 * 60;
	   $minute = floor($diff / 60);
	   $time  -= $minute * 60;
	   $second = $diff;
	   $elapse = '';

	   $unitArr = [
			'年'   => 'year',
			'个月' => 'month',
			'周'   => 'week',
			'天'   => 'day',
			'小时' => 'hour',
			'分钟' => 'minute',
			'秒'   => 'second'
	   ];

	   foreach ( $unitArr as $cn => $u )
	   {
		  if ( $$u > 0 )
		  {
			 $elapse = $$u . $cn;
			 break;
		  }
	   }

	   return $elapse;
	}

	/**
     * 获取主域
     * @param string $url
     * @return null
     */
    public static function getBaseDomain($domain){
        $re_domain = '';
        $domain_postfix_cn_array = array("com", "net", "org", "gov", "edu", "com.cn", "cn");
        $array_domain = explode(".", $domain);
        $array_num = count($array_domain) - 1;
        if ($array_domain[$array_num] == 'cn') {
            if (in_array($array_domain[$array_num - 1], $domain_postfix_cn_array)) {
                $re_domain = $array_domain[$array_num - 2] . "." . $array_domain[$array_num - 1] . "." . $array_domain[$array_num];
            } else {
                $re_domain = $array_domain[$array_num - 1] . "." . $array_domain[$array_num];
            }
        } else {
            $re_domain = $array_domain[$array_num - 1] . "." . $array_domain[$array_num];
        }
        return $re_domain;
    }

    /**
     * 获取客户端浏览器信息 添加win10 edge浏览器判断
     */
    public static function get_broswer(){
        $sys = isset($_SERVER['HTTP_USER_AGENT'])?$_SERVER['HTTP_USER_AGENT']:'';  //获取用户代理字符串
        if (stripos($sys, "Firefox/") > 0) {
            preg_match("/Firefox\/([^;)]+)+/i", $sys, $b);
            $exp[0] = "Firefox";
            $exp[1] = $b[1];  //获取火狐浏览器的版本号
        } elseif (stripos($sys, "Maxthon") > 0) {
            preg_match("/Maxthon\/([\d\.]+)/", $sys, $aoyou);
            $exp[0] = "傲游";
            $exp[1] = $aoyou[1];
        } elseif (stripos($sys, "MSIE") > 0) {
            preg_match("/MSIE\s+([^;)]+)+/i", $sys, $ie);
            $exp[0] = "IE";
            $exp[1] = $ie[1];  //获取IE的版本号
        } elseif (stripos($sys, "OPR") > 0) {
                preg_match("/OPR\/([\d\.]+)/", $sys, $opera);
            $exp[0] = "Opera";
            $exp[1] = $opera[1];
        } elseif(stripos($sys, "Edge") > 0) {
            //win10 Edge浏览器 添加了chrome内核标记 在判断Chrome之前匹配
            preg_match("/Edge\/([\d\.]+)/", $sys, $Edge);
            $exp[0] = "Edge";
            $exp[1] = $Edge[1];
        } elseif (stripos($sys, "Chrome") > 0) {
                preg_match("/Chrome\/([\d\.]+)/", $sys, $google);
            $exp[0] = "Chrome";
            $exp[1] = $google[1];  //获取google chrome的版本号
        } elseif(stripos($sys,'rv:')>0 && stripos($sys,'Gecko')>0){
            preg_match("/rv:([\d\.]+)/", $sys, $IE);
                $exp[0] = "IE";
            $exp[1] = $IE[1];
        }else {
           $exp[0] = "未知浏览器";
           $exp[1] = "";
        }
        return $exp[0].'('.$exp[1].')';
    }

    /**
     * 获取客户端操作系统信息包括win10
     * @param  null
     */
    public static function get_os(){
        $agent = isset($_SERVER['HTTP_USER_AGENT'])?$_SERVER['HTTP_USER_AGENT']:'';
        $os = false;

        if (preg_match('/win/i', $agent) && strpos($agent, '95'))
        {
            $os = 'Windows 95';
        }
        else if (preg_match('/win 9x/i', $agent) && strpos($agent, '4.90'))
        {
          $os = 'Windows ME';
        }
        else if (preg_match('/win/i', $agent) && preg_match('/98/i', $agent))
        {
          $os = 'Windows 98';
        }
        else if (preg_match('/win/i', $agent) && preg_match('/nt 6.0/i', $agent))
        {
          $os = 'Windows Vista';
        }
        else if (preg_match('/win/i', $agent) && preg_match('/nt 6.1/i', $agent))
        {
          $os = 'Windows 7';
        }
        else if (preg_match('/win/i', $agent) && preg_match('/nt 6.2/i', $agent))
        {
          $os = 'Windows 8';
        }
        else if(preg_match('/win/i', $agent) && preg_match('/nt 10.0/i', $agent))
        {
          $os = 'Windows 10';#添加win10判断
        }
        else if (preg_match('/win/i', $agent) && preg_match('/nt 5.1/i', $agent))
        {
          $os = 'Windows XP';
        }
        else if (preg_match('/win/i', $agent) && preg_match('/nt 5/i', $agent))
        {
          $os = 'Windows 2000';
        }
        else if (preg_match('/win/i', $agent) && preg_match('/nt/i', $agent))
        {
          $os = 'Windows NT';
        }
        else if (preg_match('/win/i', $agent) && preg_match('/32/i', $agent))
        {
          $os = 'Windows 32';
        }
        else if (preg_match('/linux/i', $agent))
        {
          $os = 'Linux';
        }
        else if (preg_match('/unix/i', $agent))
        {
          $os = 'Unix';
        }
        else if (preg_match('/sun/i', $agent) && preg_match('/os/i', $agent))
        {
          $os = 'SunOS';
        }
        else if (preg_match('/ibm/i', $agent) && preg_match('/os/i', $agent))
        {
          $os = 'IBM OS/2';
        }
        else if (preg_match('/Mac/i', $agent) && preg_match('/PC/i', $agent))
        {
          $os = 'Macintosh';
        }
        else if (preg_match('/PowerPC/i', $agent))
        {
          $os = 'PowerPC';
        }
        else if (preg_match('/AIX/i', $agent))
        {
          $os = 'AIX';
        }
        else if (preg_match('/HPUX/i', $agent))
        {
          $os = 'HPUX';
        }
        else if (preg_match('/NetBSD/i', $agent))
        {
          $os = 'NetBSD';
        }
        else if (preg_match('/BSD/i', $agent))
        {
          $os = 'BSD';
        }
        else if (preg_match('/OSF1/i', $agent))
        {
          $os = 'OSF1';
        }
        else if (preg_match('/IRIX/i', $agent))
        {
          $os = 'IRIX';
        }
        else if (preg_match('/FreeBSD/i', $agent))
        {
          $os = 'FreeBSD';
        }
        else if (preg_match('/teleport/i', $agent))
        {
          $os = 'teleport';
        }
        else if (preg_match('/flashget/i', $agent))
        {
          $os = 'flashget';
        }
        else if (preg_match('/webzip/i', $agent))
        {
          $os = 'webzip';
        }
        else if (preg_match('/offline/i', $agent))
        {
          $os = 'offline';
        }else if(stristr($agent,'Android')) {
	        return "Android";
	    }
	    else if(stristr($agent,'iPhone')){
	        return "iPhone";
	    }
        else
        {
          $os = '未知操作系统';
        }
        return $os;
    }

    /**
     * 毫秒数
     * @return float
     * @author xi
     * @date 2017-05-17
     */
    public static function microtimeFloat()
    {
        list($usec, $sec) = explode(" ", microtime());
        return ((float)$usec + (float)$sec);
    }

    /**
     * curl 提交 json
     * @param string $url
     * @param array $data
     * @author xi
     * @date 2017-12-18
     */
    public static function curlPostJson($url, array $data)
    {
        $jsonStr = json_encode($data);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonStr);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT,15);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json; charset=utf-8',
            'Content-Length: ' . strlen($jsonStr)
        ));
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

    /**
     * 获取两个经纬度之间的距离
     * @param string $longitude1  A点经度
     * @param string $latitude1   A点纬度
     * @param string $longitude2  B点经度
     * @param string $latitude2   B点纬度
     * @param int $unit           单位
     * @param int $decimal        小数后面保留几位
     * @return type
     */
    public static function getDistance($longitude1, $latitude1, $longitude2, $latitude2, $unit=2, $decimal=2){

		$EARTH_RADIUS = 6370.996; // 地球半径系数
		$PI = 3.1415926;//圆周率

		$radLat1 = $latitude1 * $PI / 180.0;
		$radLat2 = $latitude2 * $PI / 180.0;

		$radLng1 = $longitude1 * $PI / 180.0;
		$radLng2 = $longitude2 * $PI /180.0;

		$a = $radLat1 - $radLat2;
		$b = $radLng1 - $radLng2;

		$distance = 2 * asin(sqrt(pow(sin($a/2),2) + cos($radLat1) * cos($radLat2) * pow(sin($b/2),2)));
		$distance = $distance * $EARTH_RADIUS * 1000;
		//设置 单位为KM
		if($unit==2){
			$distance = $distance / 1000;
		}
		//设置小数点后面保留几位
		return round($distance, $decimal);

	}



    /**
     * 获取本周一周的日期，如：
     * $start_date=2017-12-11 00:00:00
     * $start_date=2017-12-18 00:00:00
     * return int  时间戳
     */
    public static function getSearchDate(){
        $date=date('Y-m-d');  //当前日期
        $first=1; //$first =1 表示每周星期一为开始日期 0表示每周日为开始日期
        $w=date('w',strtotime($date));  //获取当前周的第几天 周日是 0 周一到周六是 1 - 6
        $now_start=date('Y-m-d',strtotime("$date -".($w ? $w - $first : 6).' days')); //获取本周开始日期，如果$w是0，则表示周日，减去 6 天
        $now_end=date('Y-m-d',strtotime("$now_start +6 days"));  //本周结束日期

        //获取本周起始日期
        return $week = array('now_srart' => strtotime($now_start."00:00:00"), 'now_end' => strtotime($now_end."23:59:59"));

    }



    /**
     * 生成技师编号
     *
     * @param int $length
     * @author liuxingqi <lxq@c-ntek.com>
     * @return string
     */
    public static function get_technicianNum()
    {
        $start = rand(0000,9999);
        $middle = substr(time(), 4,9);
        $end = rand(000, 999);
        return $start.$middle.$end;
    }


    /**
     * 获取日期为今天明天其他日期
     */
    public static function setDateTime($time) {
        $now_time = date("Y-m-d");
        $next_time = date("Y-m-d",strtotime("+1 day"));
        $new_time     = date("Y-m-d",$time);
        $service_time = date("H:i",$time);

        if($now_time == $new_time) {
            return '今天 '.$service_time;
        }else if($next_time == $new_time) {
            return '明天 '.$service_time;
        }else {
            return date("m月d日",$time).$service_time;
        }
    }


    /**
     * 获取日期为今天明天其他日期
     */
    public static function setDate($time,$type)
    {
        if($time>0)
        {
            $now_time     = date("Y-m-d");
            $next_time    = date("Y-m-d",strtotime("+1 day"));
            $new_time     = date("Y-m-d",$time);
            $service_time = date("H:i",$time);

            if($type == 1) {
                $service_time = '全天';
            }
            else if($type == 2) {
                $service_time = '上午';
            }
            else if($type == 3) {
                $service_time = '下午';
            }
            else if($type == 4) {
                $service_time = '晚上';
            }

            if($now_time == $new_time) {
                return '今天 '.$service_time;
            }
            else if($next_time == $new_time) {
                return '明天 '.$service_time;
            }
            else {
                return date("m月d日",$time).$service_time;
            }
        }
        else {
            return "待定";
        }
    }
    /**
     * 验证手机号是否符合正则
     * @author liuxingqi <lxq@c-ntek.com>
     * @param type $mobile
     */
    public static function verifyPregMobile($mobile)
    {
        $pre = '/^1(3[0-9]|47|5[0,1,2,3,5,6,7,8,9]|66|7[0,3,6,7]|8[0,2,3,5,6,7,8,9]|9[8,9])[0-9]{8}$/';
        if (!preg_match($pre,$mobile)) {
            return false;
        }
        return true;
    }
    /**
     * 验证手机号是否符合正则
     * @author liuxingqi <lxq@c-ntek.com>
     * @param type $mobile  2018-3-7
     */
    public static function verifyPregMobiles($mobile)
    {
        $pre = '/^[1][3,4,5,7,8][0-9]{9}$/';
        if (!preg_match($pre,$mobile)) {
            return false;
        }
        return true;
    }


    /**
     * 把时间戳转换为几分钟或几小时前或几天前
     */
    public static function wordTime($time) {
        $time = (int) substr($time, 0, 10);
        $int = time() - $time;
        $str = '';
        if ($int <= 30){
            $str = sprintf('刚刚', $int);
        }elseif ($int < 60){
            $str = sprintf('%d秒前', $int);
        }elseif ($int < 3600){
            $str = sprintf('%d分钟前', floor($int / 60));
        }elseif ($int < 86400){
            $str = sprintf('%d小时前', floor($int / 3600));
        }elseif ($int < 2592000){
            $str = sprintf('%d天前', floor($int / 86400));
        }else{
            $str = sprintf('%d天前', floor($int / 86400));
            //$str = date('Y-m-d H:i:s', $time);
        }
        return $str;
    }

    /**
     * $desc 各种特殊字符过滤
     */

    public  static function filterWord($word){
        return $word = htmlspecialchars(str_replace(array('*','select','table','"',"'"),'',strip_tags(trim($word))));
    }

    /**
     * 计算出时间差
     * @param $date
     * @return string
     */
    public static function getDiffDateDesc($date)
    {
        $diff = time() - $date;

        if( $diff < 60){
            return $diff.'秒';
        }
        else if($diff < 3600){
            return floor($diff/60).'分钟前';
        }
        else if($diff < 86400){
            return floor($diff/3600) . '小时前';
        }
        else if($diff< 86400*30){
            return floor($diff/86400) . '天前';
        }
        else if($diff < 86400*365){
            return floor($diff/(86400*30)) . '月前';
        }
        else {
            return floor($diff/(86400*365)) . '年前';
        }
    }
    
    /**
     * 整点半点定义
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/5/24
     * Time: 14:39
     * @return array
     */
    public static function setTime(){
        return [
            '0'  => '00:00',
            '1'  => '00:30',
            '2'  => '01:00',
            '3'  => '01:30',
            '4'  => '02:00',
            '5'  => '02:30',
            '6'  => '03:00',
            '7'  => '03:30',
            '8'  => '04:00',
            '9'  => '04:30',
            '10' => '05:00',
            '11' => '05:30',
            '12' => '06:00',
            '13' => '06:30',
            '14' => '07:00',
            '15' => '07:30',
            '16' => '08:00',
            '17' => '08:30',
            '18' => '09:00',
            '19' => '09:30',
            '20' => '10:00',
            '21' => '10:30',
            '22' => '11:00',
            '23' => '11:30',
            '24' => '12:00',
            '25' => '12:30',
            '26' => '13:00',
            '27' => '13:30',
            '28' => '14:00',
            '29' => '14:30',
            '30' => '15:00',
            '31' => '15:30',
            '32' => '16:00',
            '33' => '16:30',
            '34' => '17:00',
            '35' => '17:30',
            '36' => '18:00',
            '37' => '18:30',
            '38' => '19:00',
            '39' => '19:30',
            '40' => '20:00',
            '41' => '20:30',
            '42' => '21:00',
            '43' => '21:30',
            '44' => '22:00',
            '45' => '22:30',
            '46' => '23:00',
            '47' => '23:30'
        ];

    }

    /**
     * 时间类型定义
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/5/24
     * Time: 14:39
     * @return array
     */
    public  static function setTimeType(){
         return  [
                    '1' => '全天0:00-23:59',
                    '2' => '上午0:00-11:59',
                    '3' => '下午12:00-17:59',
                    '4' => '晚上18:00-23:59',
                    '5' => '具体时间'
                ];
        }

    public static function changeTime($type,$date,$time='')
    {
        switch ($type){
            case 1:
                $timeFomate =  strtotime($date.' 23:59:59');
                break;

            case 2:
                $timeFomate =  strtotime($date.' 11:59:59');
                break;

            case 3:
                $timeFomate =  strtotime($date.'  17:59:59');
                break;

            case 4:
                $timeFomate =  strtotime($date.' 23:59:59');
                break;

            default:
                $timeFomate =  strtotime($date.$time);

        }
        return $timeFomate;

    }

    /**
     * 单位转换 b 转 kb  MB  GB
     * @param  $size  大小
     * @return string
     */
    public static function formatBytes($size) {
        $units = array(' B', ' KB', ' MB', ' GB', ' TB');
        for ($i = 0; $size >= 1024 && $i < 4; $i++) $size /= 1024;
        return round($size, 2).$units[$i];
    }


}
