<?php
namespace common\helpers;

use Yii;
use common\models\Mq;
class Log
{
    /**
     * 自定义保存文本
     * @param string $message
     * @param string $filename
     * @author xi
     */
    public static function add($message,$filename='')
    {
        $runtimePath = Yii::$app->getBasePath().'/../../runtime/logs/';
        if(!file_exists($runtimePath)){
            @mkdir($runtimePath,0777,true);
        }
        $filename = $filename!=''?$filename:'mylog.log';
        $filePath = $runtimePath.$filename;
        
        @file_put_contents($filePath, $message ,FILE_APPEND);
    }

    /**
     * 发送邮件
     * @param string $subject 邮件主题 
     * @param string $body 邮件内容
     * @author xi
     */
    public static function mail($subject,$body,$to='xiyi_pp@qq.com',$cc='')
    {
        $params = [
            'to'      => $to,
            'cc'      => $cc,
            'subject' => $subject,
            'body'    => $body
        ];
        //Mq::publish('mail', 'mail', $params,'exchange');
    }
    
}