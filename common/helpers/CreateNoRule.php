<?php
    /**
     * Created by PhpStorm.
     * User: xingq
     * Date: 2018/3/22
     * Time: 13:21
     */
    
    namespace common\helpers;
    
    use Yii;
    
    class CreateNoRule
    {
        /**
         * 入库单号
         * User: xingq
         * Email: liuxingqi@services.cn
         * Date: 2018/3/22
         * Time: 15:53
         * @return string
         */
        public static function stockIn($type)
        {
            $data = self::getNo($type);
            if (!$data) {
                return self::getNo($type);
            }
            return $data;
        }
    
        /**
         * 出库单号
         * User: xingq
         * Email: liuxingqi@services.cn
         * Date: 2018/3/22
         * Time: 15:54
         * @return string
         */
        public static function stockOut($type)
        {
            $data = self::getNo($type);
            if (!$data) {
                return self::getNo($type);
            }
            return $data;
        }
        /**
         * 采购单号
         * User: xingq
         * Email: liuxingqi@services.cn
         * Date: 2018/3/22
         * Time: 15:54
         * @return string
         */
        public static function purchaseNo()
        {
            return time().rand(0000,9999);
        }




        /**
         * 合同单号
         * User: chengjuanjuan
         * Email: chengjuanjuan@services.cn
         * Date: 2018/3/22
         * Time: 15:54
         * @return string
         */
        public static function contractNo()
        {
            return time().rand(0000,9999);
        }


        /**
         * 退货单号
         * User: chengjuanjuan
         * Email: chengjuanjuan@services.cn
         * Date: 2018/3/22
         * Time: 15:54
         * @return string
         */
        public static function returnGoodsNo()
        {
            return time().rand(0000,9999);
        }
    
        /**
         * 公共调用编号自增
         * User: xingq
         * Email: liuxingqi@services.cn
         * Date: 2018/4/23
         * Time: 14:26
         * @param string $type
         * @return string
         */
        public static function getNo($type)
        {
            if ($type) {
                $key = $type.date('Ymd',time());
                
                $res = Yii::$app->redisNo->hincrby($key,$key,1);
                if ($res) {
                    $no = $key.sprintf('%03s',$res);
                }else{
                    $no = $key.'001';
                }
                return $no;
            }
            return false;
        }
    }