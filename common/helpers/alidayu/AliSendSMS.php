<?php
namespace common\helpers\alidayu;

use Yii;
use common\helpers\SendSMS;

header("Content-Type:text/html;charset=UTF-8");

class AliSendSMS
{ 
    const appKey     = "23649070";//app key值
    const appSecret  = "908bdacf51bb2649a76bf1ab3f131468";//秘钥
    const url        = "http://gw.api.taobao.com/router/rest"; //正式环境
    const format     = "json";//响应格式。默认为xml格式，可选值：xml，json
    const signMethod = "md5";//签名的摘要算法，可选值为：hmac，md5。
    const apiVersion = "2.0";

    /**
     * 验证码
     * @param $mobile
     * @param $param
     * @param $entend
     * @author lzg
     * @date 2016-03-01
     */
    public static function send($mobile,$param = [],$entend=''){
        //判端参数
        if(!preg_match('/^1[2-9]\d{9}(\,1[2-9]\d{9})*$/', $mobile) || empty($param) )
        {
            return false;
        }
        //公共参数
        $method      = "alibaba.aliqin.fc.sms.num.send";//API接口名称
        //接口参数
        $sms_type           = "normal";
        $sms_free_sign_name = "俱牛";//短信签名
        $sms_param          = json_encode($param['param']);//短信模板变量 Json
        $rec_num            = $mobile;//短信接收号码
        $sms_template_code  = $param['template_id'];//短信模板ID

        //组装系统参数
        $sysParams['app_key']      = self::appKey;
        $sysParams['v']            = self::apiVersion;
        $sysParams['format']       = self::format;
        $sysParams['sign_method']  = self::signMethod;
        $sysParams['method']       = $method;
        $sysParams['timestamp']    = date('Y-m-d H:i:s');

        if($entend){
            $apiParams['entend']      = $entend;
        }
        $apiParams['sms_type']           = $sms_type;
        $apiParams['sms_free_sign_name'] = $sms_free_sign_name;
        $apiParams['sms_param']          = $sms_param;
        $apiParams['rec_num']            = $rec_num;
        $apiParams['sms_template_code']  = $sms_template_code;

        $requestUrl = self::url."?";
        //签名
        $sign       = self::createSign(array_merge($apiParams,$sysParams));//签名MD5
        $sysParams['sign']         = $sign;

        foreach ($sysParams as $sysParamKey => $sysParamValue)
        {
            // if(strcmp($sysParamKey,"timestamp") != 0)
            $requestUrl .= "$sysParamKey=" . urlencode($sysParamValue) . "&";
        }

        $fileFields = array();
        foreach ($apiParams as $key => $value) {
            if(is_array($value) && array_key_exists('type',$value) && array_key_exists('content',$value) ){
                $value['name'] = $key;
                $fileFields[$key] = $value;
                unset($apiParams[$key]);
            }
        }
        $requestUrl = substr($requestUrl, 0, -1);

        //发起HTTP请求
        try
        {
            if(count($fileFields) > 0){
                $resp = self::curl_with_memory_file($requestUrl, $apiParams, $fileFields);
            }else{
                $resp = self::curl($requestUrl, $apiParams);
            }
        }
        catch (\Exception $e)
        {
            $result['code'] = $e->getCode();
            $result['message']= $e->getMessage();
            return $result;
        }
        $jsonData = $resp ;
        $result = json_decode($jsonData,true);
        if(isset($result['alibaba_aliqin_fc_sms_num_send_response'])){
            //判断短信是否发送成功
            //Array ( [alibaba_aliqin_fc_sms_num_send_response] => Array ( [result] => Array ( [err_code] => 0 [model] => 106150460928^1108330741107 [success] => 1 ) [request_id] => zt9t6yov6eg5 ) )
            SendSMS::saveDB($mobile,'阿里大于'.$sms_param);
            $res["success"] = true;
            $res['data']    = $result['alibaba_aliqin_fc_sms_num_send_response'];
            return $res;
        }else{
            file_put_contents(__DIR__.'/sms.log', "[阿里大于]time:".date('Y-m-d H:i:s')."mobile:".$mobile." error:".var_export($result,true) ."\r\n",FILE_APPEND);
            //{"error_response":{"code":50,"msg":"Remote service error","sub_code":"isv.invalid-parameter","sub_msg":"非法参数"}}
            //发送邮件通知
            $res["success"] = false;
            $res['data']    = $result['error_response'];
            return $res;
        }
    }

    /**
     * 查询发送短信查询
     * @param $mobile
     * @param $query_date
     * @param $current_page
     * @param $page_size 最大50
     * @author lzg
     * @date 2017-03-01
     */
    public static function query($mobile,$query_date,$current_page=1,$page_size=50){
        //判端参数
        if(!preg_match('/^1[2-9]\d{9}(\,1[2-9]\d{9})*$/', $mobile) )
        {
            return false;
        }

        //公共参数
        $method      = "alibaba.aliqin.fc.sms.num.query";//API接口名称

        //组装系统参数
        $sysParams['app_key']      = self::appKey;
        $sysParams['v']            = self::apiVersion;
        $sysParams['format']       = self::format;
        $sysParams['sign_method']  = self::signMethod;
        $sysParams['method']       = $method;
        $sysParams['timestamp']    = date('Y-m-d H:i:s');

        //接口参数
        $apiParams['rec_num']      = $mobile;
        $apiParams['query_date']   = $query_date;
        $apiParams['current_page'] = (string)$current_page;
        $apiParams["page_size"]    = (string)$page_size;
        $requestUrl = self::url."?";
        //签名
        $sign       = self::createSign(array_merge($apiParams,$sysParams));//签名MD5
        $sysParams['sign']         = $sign;

        foreach ($sysParams as $sysParamKey => $sysParamValue)
        {
            // if(strcmp($sysParamKey,"timestamp") != 0)
            $requestUrl .= "$sysParamKey=" . urlencode($sysParamValue) . "&";
        }

        $fileFields = array();
        foreach ($apiParams as $key => $value) {
            if(is_array($value) && array_key_exists('type',$value) && array_key_exists('content',$value) ){
                $value['name'] = $key;
                $fileFields[$key] = $value;
                unset($apiParams[$key]);
            }
        }
        $requestUrl = substr($requestUrl, 0, -1);

        //发起HTTP请求
        try
        {
            if(count($fileFields) > 0){
                $resp = self::curl_with_memory_file($requestUrl, $apiParams, $fileFields);
            }else{
                $resp = self::curl($requestUrl, $apiParams);
            }
        }
        catch (\Exception $e)
        {
            $result['code'] = $e->getCode();
            $result['message']= $e->getMessage();
            return $result;
        }

        $jsonData = $resp ;
        $result = json_decode($jsonData,true);
        if(isset($result['alibaba_aliqin_fc_sms_num_query_response'])){
            //判断短信是否查询成功
            //Array ( [alibaba_aliqin_fc_sms_num_send_response] => Array ( [result] => Array ( [err_code] => 0 [model] => 106150460928^1108330741107 [success] => 1 ) [request_id] => zt9t6yov6eg5 ) )
            return $result;
        }else{
            //{"error_response":{"code":50,"msg":"Remote service error","sub_code":"isv.invalid-parameter","sub_msg":"非法参数"}}
            return $result;
        }
    }
    /**
     * http get请求
     */
    public static function curl($url, $postFields = null)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FAILONERROR, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt ( $ch, CURLOPT_USERAGENT, "top-sdk-php" );
        //https 请求
        if(strlen($url) > 5 && strtolower(substr($url,0,5)) == "https" ) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        }

        if (is_array($postFields) && 0 < count($postFields))
        {
            $postBodyString = "";
            $postMultipart = false;
            foreach ($postFields as $k => $v)
            {
                if(!is_string($v))
                    continue ;

                if("@" != substr($v, 0, 1))//判断是不是文件上传
                {
                    $postBodyString .= "$k=" . urlencode($v) . "&";
                }
                else//文件上传用multipart/form-data，否则用www-form-urlencoded
                {
                    $postMultipart = true;
                    if(class_exists('\CURLFile')){
                        $postFields[$k] = new \CURLFile(substr($v, 1));
                    }
                }
            }
            unset($k, $v);
            curl_setopt($ch, CURLOPT_POST, true);
            if ($postMultipart)
            {
                if (class_exists('\CURLFile')) {
                    curl_setopt($ch, CURLOPT_SAFE_UPLOAD, true);
                } else {
                    if (defined('CURLOPT_SAFE_UPLOAD')) {
                        curl_setopt($ch, CURLOPT_SAFE_UPLOAD, false);
                    }
                }
                curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
            }
            else
            {
                $header = array("content-type: application/x-www-form-urlencoded; charset=UTF-8");
                curl_setopt($ch,CURLOPT_HTTPHEADER,$header);
                curl_setopt($ch, CURLOPT_POSTFIELDS, substr($postBodyString,0,-1));
            }
        }
        $reponse = curl_exec($ch);

        if (curl_errno($ch))
        {
            throw new \Exception(curl_error($ch),0);
        }
        else
        {
            $httpStatusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            if (200 !== $httpStatusCode)
            {
                throw new \Exception($reponse,$httpStatusCode);
            }
        }
        curl_close($ch);
        return $reponse;
    }
    /**
     * http post请求
     */
    public static function curl_with_memory_file($url, $postFields = null, $fileFields = null)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FAILONERROR, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt ( $ch, CURLOPT_USERAGENT, "top-sdk-php" );
        //https 请求
        if(strlen($url) > 5 && strtolower(substr($url,0,5)) == "https" ) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        }
        //生成分隔符
        $delimiter = '-------------' . uniqid();
        //先将post的普通数据生成主体字符串
        $data = '';
        if($postFields != null){
            foreach ($postFields as $name => $content) {
                $data .= "--" . $delimiter . "\r\n";
                $data .= 'Content-Disposition: form-data; name="' . $name . '"';
                //multipart/form-data 不需要urlencode，参见 http:stackoverflow.com/questions/6603928/should-i-url-encode-post-data
                $data .= "\r\n\r\n" . $content . "\r\n";
            }
            unset($name,$content);
        }

        //将上传的文件生成主体字符串
        if($fileFields != null){
            foreach ($fileFields as $name => $file) {
                $data .= "--" . $delimiter . "\r\n";
                $data .= 'Content-Disposition: form-data; name="' . $name . '"; filename="' . $file['name'] . "\" \r\n";
                $data .= 'Content-Type: ' . $file['type'] . "\r\n\r\n";//多了个文档类型

                $data .= $file['content'] . "\r\n";
            }
            unset($name,$file);
        }
        //主体结束的分隔符
        $data .= "--" . $delimiter . "--";

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER , array(
                'Content-Type: multipart/form-data; boundary=' . $delimiter,
                'Content-Length: ' . strlen($data))
        );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

        $reponse = curl_exec($ch);
        unset($data);

        if (curl_errno($ch))
        {
            throw new \Exception(curl_error($ch),0);
        }
        else
        {
            $httpStatusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            if (200 !== $httpStatusCode)
            {
                throw new \Exception($reponse,$httpStatusCode);
            }
        }
        curl_close($ch);
        return $reponse;
    }
    /**
     * sign加密算法
     */
    public static function createSign($paramArr) {

        ksort($paramArr);
        $stringToBeSigned = self::appSecret;
        foreach ($paramArr as $k => $v)
        {
            if(is_string($v) && "@" != substr($v, 0, 1))
            {
                $stringToBeSigned .= "$k$v";
            }
        }
        unset($k, $v);
        $stringToBeSigned .= self::appSecret;

        return strtoupper(md5($stringToBeSigned));

    }

}