<?php
namespace common\helpers;

use common\helpers\Baidu;
use Yii;
use linslin\yii2\curl;
use common\models\Log;
class ExcelHelper
{

       /**
        * 生成excel表格
     * @param array $cells [ ['key'=>'group_buy_no','value'=>'团号','width'=>20],['key'=>'goods_name','value'=>'商品','width'=>50],...]
        * @param array $list
        * @param string $title
        * @param string $sheet_title
        */
    public static function CreateExcel($cells,$list,$title,$sheet_title='',$filePath = ''){
        if(!$sheet_title){
            $sheet_title=$title;
        }
            $letters=['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI'];
            $objPHPExcel = new \PHPExcel();
            $objPHPExcel
                ->getProperties()
                ->setTitle($title) ;   //设置标题

            $cell_count=count($cells);
            for($i=0;$i<$cell_count;$i++){
                $objPHPExcel->getActiveSheet()->setCellValue($letters[$i].'1', $cells[$i]['value']);
                $objPHPExcel->getActiveSheet()->getColumnDimension($letters[$i])->setWidth($cells[$i]['width']);
                $objPHPExcel->getActiveSheet()->getStyle($letters[$i].'1')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
                $objPHPExcel->getActiveSheet()->getStyle($letters[$i].'1')->getFill()->getStartColor()->setRGB('4D5F84');
                $objPHPExcel->getActiveSheet()->getStyle($letters[$i].'1')->getFont()->getColor()->setRGB('ffffff');
            }
            $o = 2;
            foreach ($list as $key=>$val){
                for($i=0;$i<$cell_count;$i++){
                    //$value = (isset($val[$cells[$i]['key']])?$val[$cells[$i]['key']]:'');
                    $value = (isset($val[$cells[$i]['key']])?$val[$cells[$i]['key']]:$key);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit($letters[$i].$o, $value, self::getDataType($value));
                }
                $o++;
            }

            //得到当前活动的表
            $objActSheet = $objPHPExcel->getActiveSheet();
            // 给当前活动的表设置名称
            $objActSheet->setTitle($sheet_title);

            if($filePath == '')
            {
                //生成2007excel格式的xlsx文件
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header('Content-Disposition: attachment;filename="'.$title.'.xlsx"');
                header('Cache-Control: max-age=0');
                $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
                $objWriter->save( 'php://output');
                exit;
            }
            else
            {
                $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
                $objWriter->save( $filePath);
                unset($objActSheet,$objPHPExcel,$objWriter);
            }
       }
       
       /**
        * 根据数据类型返回 单元格的类型
        * @param string $value
        * @return string
        * @author xi
        * @date 2017-0504
        */
       public static function getDataType($value)
       {
           if(is_int($value) && strlen($value)<=11)
           {
                return \PHPExcel_Cell_DataType::TYPE_NUMERIC;
           }
           return \PHPExcel_Cell_DataType::TYPE_STRING;
       }
}
