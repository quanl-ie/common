<?php

namespace common\helpers;
use Yii;
use Kcloze\Jobs\JobObject;
use Kcloze\Jobs\Logs;
use Kcloze\Jobs\Queue\BaseTopicQueue;
use Kcloze\Jobs\Queue\Queue;




/**
 * @desc swoole-jobs异步消息推送
 * @author chengjuanjuan <chengjuanjuan@c-ntek.com>
 * @date 2018-05-15
 * @return mixed
 *
 **/
class QueuePush
{


    public static function sendPush ($topic='',$controller='',$action='',$params=[],$ext=[])
    {
        try
        {
            $file  =  Yii::getAlias("@common/config/swoole-jobs.php");
            require($file);
            $topicArr = $config['job']['topics'];
            $topicList = array_column($topicArr,'name');
            if(!in_array($topic,$topicList)){
                return json_encode(['error'=>'1001','msg'=>'topic参数错误']);
            }
            $logger = Logs::getLogger($config['logPath'] ? $config['logPath'] : '', $config['logSaveFileApp'] ? $config['logSaveFileApp'] : '');
            $queue = Queue::getQueue($config['job']['queue'], $logger);

            //var_dump($queue);

            $queue->setTopics($config['job']['topics']);

            if (!$queue) {
                die("queue object is null\n");
            }
            //jobs的topic需要在配置文件里面定义，并且一次性注册进去

            $topics = $queue->getTopics();
            $ids = $params[0];
            foreach($ids as $v){
                $params[0] = $v;
                $job                   =new JobObject($topic, $controller, $action, $params, $ext);
                $result                =$queue->push($topic, $job);
                //var_dump($result);
            }
        }
        catch (\Exception $e)
        {

        }
    }




}

?>